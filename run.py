# -*- coding: utf-8 -*-
from core.reapy import ReaPy


class Run:

    def __init__(self):
        super(Run, self).__init__()
        print(ReaPy.colored("Starting Portal", 'red'))
        processes = []
        all_sub_dirs = [dI for dI in ReaPy.rea_os().listdir('apps') if
                        ReaPy.rea_os().path.isdir(ReaPy.rea_os().path.join('apps', dI)) and not dI.endswith('__')]
        try:
            for apps in all_sub_dirs:
                try:
                    app = ReaPy.importer().load_module('apps.' + apps + '.runner').load_class("Runner").get_object()
                    p = ReaPy.process(target=app)
                    p.daemon = True
                    p.start()
                    processes.append(p)
                    ReaPy.sleep(1)
                except Exception as err:
                    print(err)
            for process in processes:
                process.join()
        except Exception as err:
            print(err)


if __name__ == "__main__":
    try:
        Run()
    except Exception as e:
        print(e, '! Quitting threads.')
