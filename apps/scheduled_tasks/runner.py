# -*- coding: utf-8 -*-
from core.utilities.app_runner import AppRunner
from core.reapy import ReaPy


class Runner(ReaPy):

    def __init__(self):
        super(Runner).__init__()
        script_path = self.rea_os().path.dirname(self.rea_os().path.abspath(__file__))
        AppRunner(self, script_path)
