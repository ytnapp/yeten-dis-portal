# -*- coding: utf-8 -*-
from core.reapy import ReaPy

try:
    use_mediation = ReaPy.configuration().get_configuration()['system']['use_mediation']
    if use_mediation is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class FeedCompany:

    @staticmethod
    def get_company():
        data = Model(ReaPy.get_reactor_source('yeten')).select(table='view_geodi_firma_feed',
                                                               field='paydas_id,paydas_adi,adres,web_adres,twitter_adres,il_adi,'
                                                                     'ilce_ad,yetenek,nace,savunma_faaliyet',
                                                               is_first=True).data()
        if data is not None:
            return data[0]

    @staticmethod
    def insert_geodi_feed_company(paydas_id, json, status, tip):
        insert = Model(ReaPy.get_reactor_source('yeten')).insert('geodi_feed', {0: {'paydas_id': paydas_id},
                                                                                1: {'json': json},
                                                                                2: {'icerik_tipi': tip},
                                                                                3: {'gonderilme_sirasi': 1},
                                                                                4: {'guncellenme_tarihi': str(
                                                                                    ReaPy.rea_now())},
                                                                                5: {'durum_mesaji': status}}).data()
        if insert != 1:
            return False
        return True

    @staticmethod
    def is_feeded(paydas_id):
        data = Model(ReaPy.get_reactor_source('yeten')).select(table='geodi_feed', field='json,gonderilme_sirasi',
                                                               condition={
                                                                   0: {'col': 'paydas_id', 'operator': '=',
                                                                       'value': paydas_id, 'combiner': 'AND'},
                                                                   1: {'col': 'durum', 'operator': '=', 'value': True,
                                                                       'combiner': 'AND'},
                                                                   2: {'col': 'icerik_tipi', 'operator': '=',
                                                                       'value': 1}},
                                                               is_first=True).data()

        if data is not None:
            if data[0]['json'] is None:
                return False
            else:
                return ReaPy.json().loads(data[0]['json'])

    @staticmethod
    def update_company(paydas_id, json):
        Model(ReaPy.get_reactor_source('yeten')).update(table='geodi_feed',
                                                        condition={'SET': {'json': ReaPy.json().dumps(json)},
                                                                   'CONDITION': {
                                                                       0: {'col': 'paydas_id', 'operator': '=',
                                                                           'value': paydas_id, 'combiner': 'AND'},
                                                                       1: {'col': 'durum', 'operator': '=',
                                                                           'value': True,
                                                                           'combiner': 'AND'},
                                                                       2: {'col': 'icerik_tipi', 'operator': '=',
                                                                           'value': 3}}}).data()
