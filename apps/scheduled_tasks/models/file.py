# -*- coding: utf-8 -*-
from core.reapy import ReaPy

try:
    use_mediation = ReaPy.configuration().get_configuration()['system']['use_mediation']
    if use_mediation is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class File:

    @staticmethod
    def check_file():
        new_file = Model(ReaPy.get_reactor_source('yeten')).select('public.dosya_yukleme',
                                                                   'dosya_yukleme_id,dosya_tam_yolu,transfer_denemesi',
                                                                   {0: {'col': 'transfer_edildi_mi',
                                                                        'operator': '=',
                                                                        'value': False, 'combiner': 'AND'
                                                                        },
                                                                    1: {'col': 'aktif_mi',
                                                                        'operator': '=',
                                                                        'value': True
                                                                        }},
                                                                   'transfer_denemesi asc', None,
                                                                   True).data()
        return new_file

    @staticmethod
    def update_fail_file(count, file_id, file_removed=True):
        update_file = Model(ReaPy.get_reactor_source('yeten')).update('public.dosya_yukleme',
                                                                      {'SET': {
                                                                          'transfer_denemesi': count,
                                                                          'aktif_mi': file_removed},
                                                                          'CONDITION': {
                                                                              0: {
                                                                                  'col': 'dosya_yukleme_id',
                                                                                  'operator': '=',
                                                                                  'value': file_id}}}).data()
        return update_file

    @staticmethod
    def update_file(file_id):
        update_file = Model(ReaPy.get_reactor_source('yeten')).update('public.dosya_yukleme',
                                                                      {'SET': {
                                                                          'transfer_edildi_mi': True},
                                                                          'CONDITION': {
                                                                              0: {
                                                                                  'col': 'dosya_yukleme_id',
                                                                                  'operator': '=',
                                                                                  'value': file_id}}}).data()
        return update_file
