# -*- coding: utf-8 -*-
from core.reapy import ReaPy
import datetime

try:
    use_mediation = ReaPy.configuration().get_configuration()['system']['use_mediation']
    if use_mediation is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class SemanticFile:

    @staticmethod
    def check_file():
        try:
            new_file = Model(ReaPy.get_reactor_source('yeten')).select('public.semantic_index_file',
                                                                       None,
                                                                       {
                                                                           0: {'col': 'upload_status',
                                                                               'operator': '=',
                                                                               'value': 0,
                                                                               'combiner': 'AND'
                                                                               },
                                                                           1: {'col': 'reload_attempt',
                                                                               'operator': '<',
                                                                               'value': 20
                                                                               }},
                                                                       'reload_attempt asc', None,
                                                                       True).data()
        except Exception as e:
            print(e)
            return None
        return new_file

    @staticmethod
    def update_fail_file(count, file_id, file_removed, file_size):
        update_file = Model(ReaPy.get_reactor_source('yeten')).update('public.semantic_index_file',
                                                                      {'SET': {
                                                                          'reload_attempt': count,
                                                                          'upload_status': file_removed,
                                                                          'upload_file_size': file_size,
                                                                          'last_upload_attempt_time': str(
                                                                              datetime.datetime.now())
                                                                      },
                                                                          'CONDITION': {
                                                                              0: {
                                                                                  'col': 'file_id',
                                                                                  'operator': '=',
                                                                                  'value': file_id}}}).data()
        return update_file

    @staticmethod
    def update_file(file_id, file_size):
        update_file = Model(ReaPy.get_reactor_source('yeten')).update('public.semantic_index_file',
                                                                      {'SET': {
                                                                          'upload_status': 1,
                                                                          'upload_file_size': file_size,
                                                                          'last_upload_attempt_time': str(
                                                                              datetime.datetime.now())
                                                                      },
                                                                          'CONDITION': {
                                                                              0: {
                                                                                  'col': 'file_id',
                                                                                  'operator': '=',
                                                                                  'value': file_id}}}).data()
        return update_file
