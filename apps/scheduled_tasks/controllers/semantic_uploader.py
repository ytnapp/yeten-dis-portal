# -*- coding: utf-8 -*-
import base64
import binascii
import os
import requests

from core.reapy import ReaPy
from apps.scheduled_tasks.models.semantic_file import SemanticFile


class SemanticUploader(ReaPy):
    __header = {'content-type': 'application/json'}

    def __init__(self):
        super(SemanticUploader, self).__init__()
        # self.sleep(10)
        if ReaPy.platform().system() == "Linux":
            self.save_disk_location = self.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_linux']
        else:
            self.save_disk_location = self.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_windows']
        if 'semantic_uploader' in self.configuration().get_configuration()['system']['scheduled_tasks']:
            self.mediation_server = self.configuration().get_configuration()['system']['mediation_server']
            self.configurations = self.configuration().get_configuration()['system']['scheduled_tasks'][
                'semantic_uploader']
            print(self.colored("Starting Semantic Uploader", 'red'))
            thread_worker = self.thread(target=self.start)
            thread_worker.daemon = False
            thread_worker.start()

    @staticmethod
    def crc_from_file(filename):
        buf = open(filename, 'rb').read()
        buf = (binascii.crc32(buf) & 0xFFFFFFFF)
        return "%08X" % buf

    def start(self):
        while True:
            try:

                print(self.colored("Check New Semantic File", 'green'))
                check_file = SemanticFile.check_file()
                if check_file is not None:
                    print(check_file[0]['file_name'])
                    file_name = check_file[0]['file_name']
                    source_folder = check_file[0]['source_folder'] + '/' + file_name
                    target_folder = check_file[0]['target_folder'] + '/' + file_name

                    file_id = check_file[0]['file_id']
                    retry_count = check_file[0]['reload_attempt']
                    try:
                        file_size = os.path.getsize(source_folder)
                    except Exception as e:
                        print(e)
                        file_size = 0
                    try:
                        with open(source_folder, "rb") as data:
                            encoded_string = base64.b64encode(data.read())
                    except Exception as e:
                        print(e)
                        if retry_count >= 19:
                            SemanticFile.update_fail_file(retry_count + 1, file_id, 2, file_size)
                            file_id = None
                        else:
                            SemanticFile.update_fail_file(retry_count + 1, file_id, 0, file_size)
                            file_id = None
                    if file_id is not None:
                        file_crc = self.crc_from_file(source_folder)
                        post_date = encoded_string.decode('utf-8')
                        json = {
                            "file_path": target_folder,
                            "file_content": post_date}
                        request = None
                        try:
                            request = requests.put(self.mediation_server['upload_endpoint'], json=json)
                        except Exception as e:
                            print(e)
                        if request is not None:
                            if request.status_code == 200:
                                response = request.json()['data']
                                if response['data']['success'] is True:
                                    if response['data']['payload']['crc'] == file_crc:
                                        print('Upload Success ' + file_id)
                                        SemanticFile.update_file(file_id, file_size)
                                    else:
                                        if retry_count >= 19:
                                            SemanticFile.update_fail_file(retry_count + 1, file_id, 2, file_size)
                                        else:
                                            SemanticFile.update_fail_file(retry_count + 1, file_id, 0, file_size)
                                else:
                                    if retry_count >= 20:
                                        SemanticFile.update_fail_file(retry_count + 1, file_id, 2, file_size)
                                    else:
                                        SemanticFile.update_fail_file(retry_count + 1, file_id, 0, file_size)
            except Exception as e:
                print(e)
            self.sleep(self.configurations['file_read_timeout'])


SemanticUploader()
