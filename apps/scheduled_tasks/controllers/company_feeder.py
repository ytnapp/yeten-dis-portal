from core.reapy import ReaPy
import datetime
from apps.scheduled_tasks.models.feed_company import FeedCompany

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class CompanyFeeder(ReaPy):

    def __init__(self):
        super(CompanyFeeder, self).__init__()
        self.feed_api_url = self.configuration().get_configuration()['system']['rest_server']['geodi']['feed_api_url']
        self.enumarator_company_web = self.configuration().get_configuration()['system']['rest_server']['geodi'][
            'enumarator_company_web']
        self.enumarator_company_twitter = self.configuration().get_configuration()['system']['rest_server']['geodi'][
            'enumarator_company_twitter']
        print(self.colored("Starting Geodi Company Feed", 'red'))
        thread_worker = self.thread(target=self.start)
        thread_worker.daemon = False
        thread_worker.start()

    def start_feed(self, enumarators, paydas_id, json_list, tip):
        if len(json_list) > 0:
            dct = {}
            dct.update({'Contents': json_list})
            url = self.feed_api_url + enumarators + ReaPy.json().dumps(dct)
            sonuc = None

            try:
                sonuc = ReaPy.requests().get(url, timeout=5)
            except Exception as exc:
                print('geodi request', exc)

            FeedCompany.insert_geodi_feed_company(paydas_id, dct, sonuc.text, tip)
            print('geodi feed sonuc : ', sonuc.text)

    def send_crawler(self, paydas_id, json):
        json.update({"CrawlSettings": {
            "DisableChangeHost": True,
            "TotalContentCount": 100
        },
            "CopyMetaDataToSubContentTree": True})
        self.start_feed(self.enumarator_company_web, paydas_id, [json], 2)

    def send_twitter(self, paydas_id, twitter):
        new_json = {
            "ContentURL": 'advancedmode',
            "AdvSettings": {
                "IncludeSearch": True,
                "Queries": "[{From:'@" + str(twitter) + "'}]"
            },
            "CopyMetaDataToSubContentTree": True

        }
        self.start_feed(self.enumarator_company_twitter, paydas_id, [new_json], 2)

    def start(self):
        while True:
            try:
                print(self.colored("Check New Company Feed", 'cyan'))
                data = FeedCompany.get_company()
                if data is not None:
                    web_adres = data['web_adres']
                    if web_adres.startswith('www.'):
                        web_adres = 'http://' + web_adres
                    elif web_adres.startswith('http'):
                        pass
                    else:
                        web_adres = 'http://www.' + web_adres
                    json_content = [
                        {
                            "Mode": 0,
                            "ContentURL": web_adres,
                            "DisplayName": data['paydas_adi'],
                            "ContentID": data['paydas_id'],
                            "ViewURL": web_adres,
                            "ContentDate": str(ReaPy.rea_now().strftime("%a, %d %b %Y %H:%M:%S"))
                        }
                    ]
                    self.send_twitter(data['paydas_id'], data['twitter_adres'])
                    self.send_crawler(data['paydas_id'], json_content[0])

            except Exception as e:
                print(e)
            self.sleep(10)


CompanyFeeder().start()
