# -*- coding: utf-8 -*-
import base64
import binascii

import requests

from core.reapy import ReaPy
from apps.scheduled_tasks.models.file import File


class FileReplicator(ReaPy):
    __header = {'content-type': 'application/json'}

    def __init__(self):
        super(FileReplicator, self).__init__()
        self.sleep(10)
        if ReaPy.platform().system() == "Linux":
            self.save_disk_location = self.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_linux']
        else:
            self.save_disk_location = self.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_windows']
        if 'file_replicator' in self.configuration().get_configuration()['system']['scheduled_tasks']:
            self.mediation_server = self.configuration().get_configuration()['system']['mediation_server']
            self.configurations = self.configuration().get_configuration()['system']['scheduled_tasks'][
                'file_replicator']
            print(self.colored("Starting File Replicator", 'red'))
            thread_worker = self.thread(target=self.start)
            thread_worker.daemon = False
            thread_worker.start()

    @staticmethod
    def crc_from_file(filename):
        buf = open(filename, 'rb').read()
        buf = (binascii.crc32(buf) & 0xFFFFFFFF)
        return "%08X" % buf

    def start(self):
        while True:
            try:

                print(self.colored("Check New File", 'green'))
                check_file = File.check_file()
                print(check_file)
                if check_file is not None:
                    file_location = check_file[0]['dosya_tam_yolu'].replace('/nfs-share/',
                                                                            self.save_disk_location + '/')
                    file_id = check_file[0]['dosya_yukleme_id']
                    retry_count = check_file[0]['transfer_denemesi']

                    try:
                        with open(file_location, "rb") as data:
                            encoded_string = base64.b64encode(data.read())
                    except Exception as e:
                        print(e)
                        if retry_count >= 20:
                            File.update_fail_file(retry_count + 1, file_id, False)
                        else:
                            File.update_fail_file(retry_count + 1, file_id)
                        file_id = None
                    if file_id is not None:
                        file_crc = self.crc_from_file(file_location)
                        post_date = encoded_string.decode('utf-8')
                        json = {
                            "file_path": file_location.replace(self.save_disk_location + '/', ''),
                            "file_content": post_date}
                        request = None
                        try:
                            request = requests.put(self.mediation_server['upload_endpoint'], json=json)
                            print(request)

                        except Exception as e:
                            print(e)
                        if request is not None:
                            if request.status_code == 200:
                                response = request.json()['data']
                                if response['data']['success'] is True:
                                    if response['data']['payload']['crc'] == file_crc:
                                        print('Upload Success ' + file_id)
                                        File.update_file(file_id)
                                    else:
                                        File.update_fail_file(retry_count + 1, file_id)
                                else:
                                    File.update_fail_file(retry_count + 1, file_id)
            except Exception as e:
                print(e)
            self.sleep(self.configurations['file_read_timeout'])


FileReplicator()
