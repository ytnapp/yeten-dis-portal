from core.reapy import ReaPy
import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class MailSender(ReaPy):

    def __init__(self):
        super(MailSender, self).__init__()
        self.mail_settings = ReaPy.configuration().get_configuration()['system']['rest_server']['mail']
        # self.sleep(10)
        print(self.colored("Starting Mail Sender", 'red'))
        thread_worker = self.thread(target=self.start)
        thread_worker.daemon = False
        thread_worker.start()

    @staticmethod
    def send(host, port, sender_email, password, receiver_email, subject, message):
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = receiver_email
        msg['Subject'] = subject
        msg.attach(MIMEText(message, 'plain'))
        try:
            server = smtplib.SMTP(host, port)
            # server.set_debuglevel(1)
            server.ehlo()
            # server.starttls()
            # server.ehlo()
            # server.login(sender_email, password)
            message = msg.as_string()
            server.sendmail(sender_email, receiver_email, message)
            server.quit()
            return True
        except Exception as e:
            print(e)
            return False

    def start(self):
        while True:
            try:
                print(self.colored("Check New Mail", 'green'))
                send = Model(ReaPy.get_reactor_source('yeten')).select(table='mail_gonder', condition={
                    0: {'col': 'gonderildi_mi', 'operator': '=', 'value': False}}, is_first=True).data()

                if send is not None:
                    send = send[0]
                    print(send['paydas_eposta'])
                    if int(send['deneme_sayisi']) < 21:
                        if int(send['talep_durumu']) != -1:
                            kullanici = Model(ReaPy.get_reactor_source('yeten')).select('dis_kullanici_talep', None, {
                                0: {'col': 'dis_kullanici_talep_id', 'operator': '=', 'value': send['dis_kullanici_id'],
                                    'combiner': 'AND'},
                                1: {'col': 'aktif_mi', 'operator': '=', 'value': True}
                            }).data()
                            if kullanici is not None:
                                kullanici = kullanici[0]
                            e_posta_text = "Kullanıcı oluşturma talebiniz "
                            if int(kullanici['talep_durumu']) == 2:
                                e_posta_text += "onaylanmıştır. Kullanıcı bilgileri aşağıdadır.\n\n" + "Kullanıcı T.C Kimlik Numarası: " + \
                                                kullanici['tc'] + "\nKullanıcı Adı: " + kullanici[
                                                    'ad'] + "\nKullanıcı Soyadı: " + \
                                                kullanici['soyad'] + "\n Kullanıcı Cep Telefon No: " + kullanici[
                                                    'telefon']
                            elif int(kullanici['talep_durumu']) == 3:
                                e_posta_text += "reddedilmiştir. \n\nRed sebebi:" + kullanici[
                                    'red_aciklamasi'] + "Kullanıcı bilgileri aşağıdadır.\n\n" + "Kullanıcı T.C Kimlik Numarası: " + \
                                                kullanici['tc'] + "\nKullanıcı Adı: " + kullanici[
                                                    'ad'] + "\nKullanıcı Soyadı: " + \
                                                kullanici['soyad'] + "\n Kullanıcı Cep Telefon No: " + kullanici[
                                                    'telefon']
                            kullanici_mail = self.send(self.mail_settings['host'], self.mail_settings['port'],
                                                               self.mail_settings['user'],
                                                               self.mail_settings['password'],
                                                               send['kullanici_eposta'].strip(),
                                                               'YETEN | Kullanıcı Oluşturma',
                                                               e_posta_text)

                            paydas_mail = self.send(self.mail_settings['host'], self.mail_settings['port'],
                                                            self.mail_settings['user'],
                                                            self.mail_settings['password'],
                                                            send['paydas_eposta'].strip(),
                                                            'YETEN | Kullanıcı Oluşturma',
                                                            e_posta_text
                                                            )

                            paydas_kep = self.send(self.mail_settings['host'], self.mail_settings['port'],
                                                           self.mail_settings['user'],
                                                           self.mail_settings['password'],
                                                           send['paydas_kep_adres'].strip(),
                                                           'YETEN | Kullanıcı Oluşturma',
                                                           e_posta_text
                                                           )
                            if all([kullanici_mail, paydas_mail, paydas_kep]):
                                update_data = {
                                    'gonderildi_mi': True,
                                    'deneme_sayisi': int(send['deneme_sayisi']) + 1
                                }
                                Model(ReaPy.get_reactor_source('yeten')).update('public.mail_gonder',
                                                                                {'SET': update_data,
                                                                                 'CONDITION': {
                                                                                     0: {
                                                                                         'col': 'mail_send_id',
                                                                                         'operator': '=',
                                                                                         'value': send[
                                                                                             'mail_send_id']
                                                                                     }
                                                                                 }}).data()
                        else:
                            paydas = self.send(self.mail_settings['host'], self.mail_settings['port'],
                                                       self.mail_settings['user'],
                                                       self.mail_settings['password'],
                                                       send['paydas_eposta'].strip(),
                                                       'YETEN | Başvuru Durumu',
                                                       send['red_aciklamasi']
                                                       )
                            if paydas:
                                Model(ReaPy.get_reactor_source('yeten')).update('public.mail_gonder',
                                                                                {'SET': {
                                                                                    'gonderildi_mi': True,
                                                                                    'deneme_sayisi': int(
                                                                                        send['deneme_sayisi']) + 1
                                                                                },
                                                                                    'CONDITION': {
                                                                                        0: {
                                                                                            'col': 'mail_send_id',
                                                                                            'operator': '=',
                                                                                            'value': send[
                                                                                                'mail_send_id']
                                                                                        }
                                                                                    }}).data()
            except Exception as e:
                print(e)
            self.sleep(5)


MailSender()
