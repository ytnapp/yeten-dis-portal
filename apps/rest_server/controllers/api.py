# -*- coding: utf-8 -*-

from core.reapy import ReaPy, request

configurations = ReaPy.configuration().get_configuration()['system']['rest_server']
print(ReaPy.colored("rest server init", 'magenta'))

app = ReaPy.rest_server(__name__, static_url_path='/none',
                        template_folder='../../../statics/dis-portal')
app.secret_key = configurations['secret_key']
handle_exceptions = app.handle_exception
handle_user_exception = app.handle_user_exception
api = ReaPy.rest_api(app)
app.handle_user_exception = handle_exceptions
app.handle_user_exception = handle_user_exception
api.session = ReaPy.rest_session()
ReaPy.importer().load_module('apps.rest_server.applications.apprunner').load_class("AppRunner").get_object()(
    api)


@app.route('/genel-bilgiler')
def index1():
    user_identity_no = ''
    try:
        if ReaPy.rest_session().get('USER_IDENTITY') is not None:
            user_identity_no = ReaPy.rest_session().get('USER_IDENTITY')  # self.master.session['USER_IDENTITY']
    except Exception as exc:
        print(exc)

    if user_identity_no == '':
        return ReaPy.redirect('/sso?url=genel', 302)
    else:
        return ReaPy.redirect('/genel', 302)


@app.route('/hemen-kaydol')
def basvurukaydol():
    user_identity_no = ''
    try:
        if ReaPy.rest_session().get('USER_IDENTITY') is not None:
            user_identity_no = ReaPy.rest_session().get(
                'USER_NAME_SURNAME')  # self.master.session['USER_IDENTITY']
            user_identity_no = ReaPy.rest_session().get('USER_IDENTITY')  # self.master.session['USER_IDENTITY']
    except Exception as exc:
        print(exc)

    if user_identity_no == '':
        return ReaPy.redirect('/sso?url=basvuru', 302)
    else:
        return ReaPy.redirect('/basvuru', 302)


@app.errorhandler(400)
def bad_request(error):
    code = 400
    response = {'success': False, 'code': code, 'message': str(error)}
    return ReaPy.jsonify(data=response), code


@app.errorhandler(403)
def forbidden(error):
    code = 403
    response = {'success': False, 'code': code, 'message': str(error)}
    return ReaPy.jsonify(data=response), code


@app.before_request
def log_request():
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    print(ReaPy.colored(ReaPy.now(), 'green'),
          ReaPy.colored("Remote Addr : " + ip, 'cyan'),
          ReaPy.colored("Endpoint : " + request.path, 'yellow'),
          ReaPy.colored(request.method, 'red'))
    return None


@app.errorhandler(404)
def page_not_found(error):
    try:
        if ReaPy.rest_session().get('IAM-User') is not None:
            return ReaPy.redirect('/genel', 302)
        else:
            return ReaPy.redirect('/', 302)
            # code = 404
            # response = {'success': False, 'code': code, 'message': str(error)}
            # return ReaPy.jsonify(data=response), code
    except Exception as exc:
        print(exc)


@app.errorhandler(405)
def method_not_allowed(error):
    code = 405
    response = {'success': False, 'code': code, 'message': str(error)}
    return ReaPy.jsonify(data=response), code


@app.errorhandler(500)
def internal_error(error):
    code = 500
    response = {'success': False, 'code': code, 'message': str(error)}
    return ReaPy.jsonify(data=response), code


@app.after_request
def add_header(response):
    response.headers['X-Powered-By'] = 'Core-ReActor/1.1.0'
    response.headers['Server'] = 'ReActor/1.1.0'
    response.headers['X-Server'] = 'ReActor/1.1.0'
    if 'Cache-Control' not in response.headers:
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate'

    response.headers.add('Access-Control-Allow-Origin', configurations['cors_url'])
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type')
    response.headers.add('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE,HEAD,PATCH')

    return response


# app.config['SESSION_REFRESH_EACH_REQUEST'] = True
# app.config['SESSION_COOKIE_SECURE'] = True
# app.config['REMEMBER_COOKIE_SECURE'] = True
# app.config['SESSION_COOKIE_HTTPONLY'] = True
# app.config['REMEMBER_COOKIE_HTTPONLY'] = True
app.config['JSON_SORT_KEYS'] = configurations['sort_json']
app.config['SESSION_TYPE'] = configurations['session_type']
app.config['SESSION_COOKIE_NAME'] = configurations['session_cookie_name']
app.config['SESSION_KEY_PREFIX'] = configurations['session_key_prefix']
app.config['SESSION_REDIS'] = ReaPy.redis(host=configurations['redis']['host'],
                                          port=configurations['redis']['port'],
                                          db=configurations['session_db'],
                                          password=configurations['redis']['redis_auth'])
app.config['PERMANENT_SESSION_LIFETIME'] = ReaPy.timedelta(minutes=configurations['session_timeout'])
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = ReaPy.timedelta(minutes=configurations['file_cache_timeout'])
session = ReaPy.session(app)
session.init_app(app)

if __name__ == '__main__':
    if ReaPy.platform().system() == "Linux":
        app.run(host=configurations['host'], port=configurations['https_port'],
                debug=configurations['debug'], threaded=True,
                use_reloader=configurations['use_re_loader'])
    elif ReaPy.platform().system() == "Windows":
        app.run(host=configurations['host'], port=configurations['http_port'],
                debug=configurations['debug'], threaded=True,
                use_reloader=configurations['use_re_loader'])
