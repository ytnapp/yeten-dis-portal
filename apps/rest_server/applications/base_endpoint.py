# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers


class BaseEndpoint:

    @staticmethod
    def check_session(main, request):
        iam_user_ip_addr = None
        iam_user_company_id = None
        iam_user_name_surname = None
        iam_user_domain_account_name = None

        debug_data = {
            'iam_user_domain_account_name': '11111111112',
            'USER_NAME_SURNAME': 'UMUTCAN KARACA',
            'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574'
        }
        # print(main.master.session.get('USER_IDENTITY'))
        # print(main.master.configurations['debug'])
        try:
            if main.master.configurations['debug'] is True:

                main.master.session['APPLICATION_AUTHORIZATIONS'] = [

                    {
                        "code": "yeten-dis-portal-test",
                        "name": "Dış Portal - Test Modülü Yetkisi"
                    },
                    {
                        "code": "yeten-dis-portal-altyapi",
                        "name": "Dış Portal - Altyapı Modülü Yetkisi"
                    },
                    {
                        "code": "yeten-dis-portal-urun",
                        "name": "Dış Portal - Ürün Modülü Yetkisi"
                    },
                    {
                        "code": "yeten-dis-portal-finans",
                        "name": "Dış Portal - Finans Modülü Yetkisi"
                    },
                    {
                        "code": "yeten-dis-portal-admin",
                        "name": "Yeten Dış Portal Yönetici Yetkisi"
                    }
                ]
                iam_user_company_id = debug_data['iam_user_company_id']
                iam_user_name_surname = debug_data['USER_NAME_SURNAME']
                iam_user_domain_account_name = debug_data['iam_user_domain_account_name']
            else:

                iam_user = Helpers().session_control(main.master.session.get('IAM-User'))

                if main.master.session.get('USER_IDENTITY') is None and iam_user is None:
                    return main.master.messages['errors']['user_sn_err_101']

                iam_user_name_surname = main.master.session['USER_NAME_SURNAME']

                if iam_user is not None:
                    if 'iam_user_company_id' in iam_user:
                        iam_user_company_id = iam_user['iam_user_company_id']

                    if 'iam_user_domain_account_name' in iam_user:
                        iam_user_domain_account_name = iam_user['iam_user_domain_account_name']
                else:
                    iam_user_domain_account_name = main.master.session.get('USER_IDENTITY')

            iam_user_ip_addr = request.remote_addr

            return (iam_user_ip_addr, iam_user_company_id, iam_user_name_surname, iam_user_domain_account_name)
        except Exception as exc:
            print(exc)
            return main.master.messages['errors']['user_sn_err_102']

    def check_auth(self, master, auth):
        auths = master.session.get('APPLICATION_AUTHORIZATIONS')
        if auths is not None:
            for key in auths:
                if key['code'] == auth:
                    return True
            return False
