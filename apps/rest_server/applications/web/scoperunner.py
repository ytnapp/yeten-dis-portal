# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class ScopeRunner(object):

    def __init__(self, api):
        super(ScopeRunner).__init__()
        print(ReaPy.colored("start ", 'cyan'), ReaPy.colored(__name__, 'green'))

        view_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'

        schema_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/models/api/schema.json'
        self.messages = ReaPy.presenter().get_message(view_path, 'messages')
        self.schema = ReaPy.presenter().get_schema(schema_path, 'schemas')
        self.configurations = ReaPy.configuration().get_configuration()
        self.session = api.session
        script_path = ReaPy.rea_os().path.dirname(ReaPy.rea_os().path.abspath(__file__))
        working_space = ReaPy.rea_os().path.basename(script_path)

        print(ReaPy.colored(' * ' + working_space + ' Endpoints :', 'white'))

        i = 1
        try:

            modules = [f for f in ReaPy.rea_os().listdir(
                ReaPy.rea_os().path.dirname(
                    ReaPy.rea_os().path.abspath(__file__)) + '/scopes/') if
                       f.endswith('.py') and not f.startswith('__')]

            for module in modules:
                class_name = ReaPy.rea_os().path.splitext(module)[0]
                module_name = "apps.rest_server.applications." + working_space + '.scopes.' + class_name
                get_class = self.get_class_package(self, module_name, class_name)
                if class_name == 'root':
                    route = '/'
                else:
                    route = '/' + class_name

                    if class_name == 'haberler':
                        route = route + '/<int:haber_id>'
                    if class_name == 'duyurular':
                        route = route + '/<int:haber_id>'
                    if class_name == 'static':
                        route = route + '/<string:dosya_numara>'

                api.add_resource(self.factory(get_class, route), route,
                                 endpoint=class_name + "_" + str(ReaPy.hash().get_uuid()))
                print(ReaPy.colored('  ' + str(i) + ' -', 'yellow'), ReaPy.colored(route, 'yellow'))
                i += 1

        except Exception as err:
            print(err)

    @staticmethod
    def get_class_package(master, module_name, class_name):
        m = __import__(module_name, globals(), locals(), class_name)
        import_module = ReaPy.inspect().getmembers(m)
        new_key = None
        for key, value in import_module:
            if class_name.capitalize() == key:
                new_key = [x for x, y in enumerate(import_module) if y[0] == key]
        import_module_name = import_module[new_key[0]]
        c = getattr(m, import_module_name[0])
        return c

    def factory(self, base_class, spaces):
        class NewClass(base_class):
            pass

        NewClass.__name__ = spaces + "_%s" % base_class.__name__
        NewClass.master = self
        return NewClass
