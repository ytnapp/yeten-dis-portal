# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Cikis(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())

        access_token = ''
        try:
            if self.master.session.get('USER_IDENTITY') is not None:
                access_token = self.master.session.get('access_token')
        except Exception as exc:
            print(exc)

        ReaPy.rest_session().clear()

        if access_token != '':
            sso = ReaPy.configuration().get_configuration()['system']['dis_portal_conf']['sso']

            state = str(ReaPy.hash().get_uuid())
            sso['sso_authorize_link'] = sso['sso_authorize_link'].replace('__state__', state)

            ReaPy.requests().delete(url=sso['sso_hostname'] + '/sso/oauth2/logout',
                                    headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                    data={
                                        'client_id': sso['client_id'],
                                        'client_secret': sso['client_secret'],
                                        'access_token': access_token
                                    }
                                    )

        return ReaPy.redirect('/', 302)
