# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Izinyok(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())
        template = ReaPy.render_template("403.html")

        return ReaPy.response(response=template, status=403, mimetype="text/html")
