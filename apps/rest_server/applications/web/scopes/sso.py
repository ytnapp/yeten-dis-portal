# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.kullanici.kulanici import Kullanici


class Sso(ReaPy().rest_resource()):
    sso_url = ''
    client_id = ''
    redirect_uri = ''
    client_secret = ''
    basvuru_sso_link = ''

    def __init__(self):

        configurations = ReaPy.configuration().get_configuration()['system']['dis_portal_conf']['sso']
        self.sso_url = configurations['sso_hostname']
        self.client_id = configurations['client_id']
        self.client_secret = configurations['client_secret']
        self.redirect_uri = configurations['redirect_url_hostname']
        state = str(ReaPy.hash().get_uuid())
        sso_auth_link = configurations['sso_authorize_link'].replace('__state__', state)
        self.master.session['USER_STATE'] = state
        self.basvuru_sso_link = sso_auth_link

    def get(self):

        ReaPy.set_request(ReaPy.rest_request())

        get_code = ''
        user_identity_no = ''

        try:
            get_code = ReaPy.rest_request().args['code']
            self.__sso_check_token_code_resource(get_code)
            print('get_code : ' + get_code)
        except Exception as exc:
            print("ilk exc: " + get_code)
            print(exc)

        try:
            if self.master.session['USER_IDENTITY'] is not None:
                user_identity_no = self.master.session['USER_IDENTITY']
        except Exception as exc:
            print(exc)

        if user_identity_no == '':

            from_self_url = ''
            try:
                from_self_url = ReaPy.rest_request().args['url']
                print('from_self_url : ' + from_self_url)
                self.master.session['USER_RETURN_URL'] = from_self_url
            except Exception:
                pass

            basvuru_sso_link = self.sso_url + self.basvuru_sso_link + self.redirect_uri

            return ReaPy.redirect(basvuru_sso_link, 302)
        else:
            return_url = ''
            try:
                return_url = self.master.session['USER_RETURN_URL']
            except Exception:
                pass

            return ReaPy.redirect('/' + return_url, 302)

    def __sso_check_token_code_resource(self, code):

        token_data = {
            'grant_type': 'authorization_code',
            'client_id': self.client_id,
            'code': code,
            'client_secret': self.client_secret,
            'redirect_uri': self.redirect_uri
        }

        r = ReaPy.requests().post(self.sso_url + '/sso/oauth2/token', data=token_data)

        token_data = r.json()

        if r.status_code == 200:
            token = token_data['data']['payload']['access_token']
            self.master.session['access_token'] = token
            return self.__sso_get_resource(token)
        else:
            return False

    def __sso_get_resource(self, access_token):

        data = {
            'client_id': self.client_id,
            'access_token': access_token,
            'scope': 'Kimlik-Dogrula;Ad-Soyad;Temel-Bilgileri;Iletisim-Bilgileri;Uygulama-Yetkileri;IAM-User',
            'resource_id': 1
        }

        rs = ReaPy.requests().post(self.sso_url + '/sso/oauth2/resource', data=data)

        return_data = rs.json()
        print(return_data)

        if rs.status_code == 200:
            iam_user = None
            try:
                iam_user = return_data['data']['payload']['IAM-User']
            except Exception as exc:
                print(exc)
            self.master.session.sid = str(ReaPy.hash().get_uuid())
            self.master.session['USER_IDENTITY'] = return_data['data']['payload']['Kimlik-Dogrula']['username']
            self.master.session['USER_NAME_SURNAME'] = return_data['data']['payload']['Ad-Soyad']['name'] + ' ' + \
                                                       return_data['data']['payload']['Ad-Soyad']['surname']

            if iam_user is None:
                try:
                    get_iliskili_kisi = Kullanici().get_detay_from_tckn(
                        return_data['data']['payload']['Kimlik-Dogrula']['username'])
                    iam_user = {
                        'iam_user_company_id': get_iliskili_kisi['paydas_id'],
                        'iam_user_domain_account_name': return_data['data']['payload']['Kimlik-Dogrula']['username']
                    }
                except Exception as exc:
                    print(exc)

            self.master.session['IAM-User'] = iam_user
            if 'Uygulama-Yetkileri' in return_data['data']['payload']:
                self.master.session['APPLICATION_AUTHORIZATIONS'] = return_data['data']['payload']['Uygulama-Yetkileri']

            return_url = ''
            try:
                return_url = self.master.session['USER_RETURN_URL']
            except Exception as exc:
                pass
            finally:
                if return_url == '':
                    return_url = '/'

            user_return_url = self.redirect_uri.replace('sso', return_url)

            ReaPy.redirect(user_return_url, 302)
        else:
            ReaPy.redirect('/izinyok', 302)

        return return_data
