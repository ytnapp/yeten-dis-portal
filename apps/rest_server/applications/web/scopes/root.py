# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Root(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())

        api_url = ReaPy.configuration().get_configuration()['system']['dis_portal_conf']['api_url']

        template = ReaPy.render_template('index.html', api_url=api_url)

        return ReaPy.response(response=template, status=200, mimetype="text/html")
