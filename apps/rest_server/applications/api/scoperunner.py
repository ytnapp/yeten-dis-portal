# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class ScopeRunner(ReaPy):

    def __init__(self, api):
        super(ScopeRunner).__init__()
        scope_folder_name = '/scopes/'
        print(self.colored("start ", 'cyan'), self.colored(__name__, 'green'))
        view_path = str(self.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'
        schema_path = str(self.presenter().get_project_root()) + '/apps/rest_server/models/api/schema.json'
        self.messages = self.presenter().get_message(view_path, 'messages')
        self.schema = self.presenter().get_schema(schema_path, 'schemas')
        self.configurations = self.configuration().get_configuration()['system']['rest_server']
        self.session = api.session
        script_path = self.rea_os().path.dirname(self.rea_os().path.abspath(__file__))
        working_space = self.rea_os().path.basename(script_path)

        all_sub_dirs = [dI for dI in self.rea_os().listdir(script_path + scope_folder_name) if
                        self.rea_os().path.isdir(
                            self.rea_os().path.join(script_path + scope_folder_name, dI)) and not dI.endswith('__')]
        print(self.colored(' * ' + working_space + ' Static Endpoints :', 'white'))

        i = 1
        try:
            for apps in all_sub_dirs:
                modules = [f for f in self.rea_os().listdir(
                    self.rea_os().path.dirname(
                        self.rea_os().path.abspath(__file__)) + scope_folder_name + apps + '/endpoints') if
                           f.endswith('.py') and not f.startswith('__')]

                for module in modules:
                    class_name = self.rea_os().path.splitext(module)[0]

                    module_name = "apps.rest_server.applications." + working_space + '.scopes.' + apps + ".endpoints." \
                                  + class_name
                    if '_' in class_name:
                        class_name = class_name.replace('_', '')
                    get_class = self.get_class_package(self, module_name, class_name)
                    route = '/' + working_space + '/' + apps + '/' + class_name
                    api.add_resource(self.factory(get_class, route), route,
                                     endpoint=class_name + "_" + str(self.hash().get_uuid()))
                    print(self.colored('  ' + str(i) + ' -', 'yellow'), self.colored(route, 'yellow'))
                    i += 1

        except Exception as err:
            print(err)

    def get_class_package(self, master, module_name, class_name):
        m = __import__(module_name, globals(), locals(), class_name)
        import_module = self.inspect().getmembers(m)
        new_key = None
        for key, value in import_module:
            if class_name == key.lower():
                new_key = [x for x, y in enumerate(import_module) if y[0] == key]
        import_module_name = import_module[new_key[0]]
        c = getattr(m, import_module_name[0])
        return c

    def factory(self, base_class, spaces):
        class NewClass(base_class):
            pass

        NewClass.__name__ = spaces + "_%s" % base_class.__name__
        NewClass.master = self
        return NewClass
