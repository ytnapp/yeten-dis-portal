# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun_kirilim.diffraction import Diffraction


class AddDiffraction(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id != None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.remote_addr = request.remote_addr
        product_type = request.json['diffraction_type']
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def own_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction_own_product'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_product_id = self.request_data[
            'diffraction_product_id'] if 'diffraction_product_id' in self.request_data else None
        product_id = self.request_data['product_id'] if 'product_id' in self.request_data else None
        product_amount = self.request_data['product_amount'] if 'product_amount' in self.request_data else None
        unit_id = str(self.request_data['unit_id']) if 'unit_id' in self.request_data else None
        unit_other = str(self.request_data['unit_other']) if 'unit_other' in self.request_data else None
        product_name = str(self.request_data['product_name']) if 'product_name' in self.request_data else None
        product_brand = str(self.request_data['product_brand']) if 'product_brand' in self.request_data else None
        product_model = str(self.request_data['product_model']) if 'product_model' in self.request_data else None
        product_type = str(self.request_data['product_type']) if 'product_type' in self.request_data else None
        diffraction_type = 1

        if unit_id is None and unit_other is None:
            message = self.master.messages['errors']['diffraction_ue_err_103']
            return ReaPy.rest_response(message['data'], 400)

        check_diffraction_product_id = Diffraction.check_product(self.iam_user_company_id, diffraction_product_id)
        if check_diffraction_product_id is None:
            message = self.master.messages['errors']['diffraction_pn_err_100']
            return ReaPy.rest_response(message['data'], 400)

        if 'kirilim_tipi' in check_diffraction_product_id[0]:
            parent_diffraction_type = check_diffraction_product_id[0]['kirilim_tipi']
        else:
            parent_diffraction_type = 1

        if 'derinlik' in check_diffraction_product_id[0]:
            parent_depth = check_diffraction_product_id[0]['derinlik'] + 1
            child_depth = parent_depth
        else:
            parent_depth = 0
            child_depth = 1

        check_first_diffraction = Diffraction().get_diffraction_by_id(self.iam_user_company_id, diffraction_product_id)

        first_diffraction_id = str(ReaPy.hash().get_uuid())

        if check_first_diffraction is None:
            parent = None
            child = diffraction_product_id
            depth = parent_depth
            insert_root_diffraction = Diffraction().insert_diffraction(first_diffraction_id, parent_diffraction_type,
                                                                       parent,
                                                                       child,
                                                                       self.iam_user_company_id, depth,
                                                                       self.iam_user_domain_account_name,
                                                                       self.remote_addr)
            if insert_root_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_105']
                return ReaPy.rest_response(message['data'], 400)

        diffraction_id = str(ReaPy.hash().get_uuid())
        own_diffraction_id = str(ReaPy.hash().get_uuid())

        if product_id != None:

            if product_id == diffraction_product_id:
                message = self.master.messages['errors']['diffraction_pn_err_101']
                return ReaPy.rest_response(message['data'], 400)
            check_product_id = Diffraction.check_product(self.iam_user_company_id, product_id)
            if check_product_id is None:
                message = self.master.messages['errors']['diffraction_pn_err_102']
                return ReaPy.rest_response(message['data'], 400)
            else:
                check_diffraction = Diffraction().get_diffraction_by_child(self.iam_user_company_id,
                                                                           diffraction_product_id,
                                                                           product_id, 'kendi')
                if check_diffraction != None:
                    message = self.master.messages['errors']['diffraction_ad_err_104']
                    return ReaPy.rest_response(message['data'], 400)

                parent = diffraction_product_id
                child = own_diffraction_id
                depth = child_depth
                insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                                      self.iam_user_company_id, depth,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)
                if insert_diffraction != 1:
                    message = self.master.messages['errors']['diffraction_ie_err_106']
                    return ReaPy.rest_response(message['data'], 400)

                insert_own_diffraction = Diffraction().insert_own_diffraction(own_diffraction_id, diffraction_id,
                                                                              self.iam_user_company_id,
                                                                              None, check_product_id[0]['marka'],
                                                                              check_product_id[0]['model'],
                                                                              product_amount, unit_id, unit_other,
                                                                              product_id,
                                                                              check_product_id[0]['urun_turu'],
                                                                              self.iam_user_domain_account_name,
                                                                              self.remote_addr, diffraction_product_id)

                if insert_own_diffraction != 1:
                    Diffraction().insert_own_diffraction_rollback(first_diffraction_id, diffraction_id)
                    message = self.master.messages['errors']['diffraction_ie_err_107']
                    return ReaPy.rest_response(message['data'], 400)

                message = self.master.messages['success']['success_temp']
                message['payload'] = {}
                message['payload'].clear()
                product_data = {
                    'kirilim_id': diffraction_id,
                }
                message['payload'] = product_data
                return ReaPy.rest_response(message)

        else:
            check_referenced_diffraction = Diffraction().get_diffraction_by_referenced(self.iam_user_company_id,
                                                                                       self.iam_user_company_id,
                                                                                       product_name,
                                                                                       product_brand, product_model,
                                                                                       diffraction_product_id)
            if check_referenced_diffraction != None:
                message = self.master.messages['errors']['diffraction_ad_err_111']
                return ReaPy.rest_response(message['data'], 400)

            insert_own_diffraction = Diffraction().insert_own_diffraction(own_diffraction_id, diffraction_id,
                                                                          self.iam_user_company_id,
                                                                          product_name, product_brand,
                                                                          product_model,
                                                                          product_amount, unit_id, unit_other,
                                                                          product_id,
                                                                          product_type,
                                                                          self.iam_user_domain_account_name,
                                                                          self.remote_addr, diffraction_product_id)
            if insert_own_diffraction != 1:
                Diffraction().insert_own_diffraction_rollback(first_diffraction_id, diffraction_id)
                message = self.master.messages['errors']['diffraction_ie_err_108']
                return ReaPy.rest_response(message['data'], 400)

            parent = diffraction_product_id
            child = own_diffraction_id
            depth = 1
            insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                                  self.iam_user_company_id, depth,
                                                                  self.iam_user_domain_account_name,
                                                                  self.remote_addr)
            if insert_diffraction != 1:
                Diffraction().insert_own_diffraction_rollback(first_diffraction_id, diffraction_id)
                message = self.master.messages['errors']['diffraction_ie_err_109']
                return ReaPy.rest_response(message['data'], 400)

            insert_referenced_product = Diffraction().insert_referenced_product(self.iam_user_company_id,
                                                                                self.iam_user_company_id,
                                                                                None,None,
                                                                                product_name, product_brand,
                                                                                product_model, diffraction_id,
                                                                                diffraction_product_id, 0, product_type,
                                                                                self.iam_user_domain_account_name,
                                                                                self.remote_addr)

            if insert_referenced_product != 1:
                Diffraction().insert_own_diffraction_rollback(first_diffraction_id, diffraction_id)
                message = self.master.messages['errors']['diffraction_ie_err_110']
                return ReaPy.rest_response(message['data'], 400)

            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'kirilim_id': diffraction_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)

    def domestic_supply(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction_domestic_supply'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_product_id = self.request_data[
            'diffraction_product_id'] if 'diffraction_product_id' in self.request_data else None
        product_company_id = self.request_data[
            'product_company_id'] if 'product_company_id' in self.request_data else None
        product_company_tax_number = self.request_data[
            'product_company_tax_number'] if 'product_company_tax_number' in self.request_data else None
        product_company_name = self.request_data[
            'product_company_name'] if 'product_company_name' in self.request_data else None

        product_id = self.request_data['product_id'] if 'product_id' in self.request_data else None
        product_amount = self.request_data['product_amount'] if 'product_amount' in self.request_data else None
        unit_id = str(self.request_data['unit_id']) if 'unit_id' in self.request_data else None
        unit_other = str(self.request_data['unit_other']) if 'unit_other' in self.request_data else None
        product_name = str(self.request_data['product_name']) if 'product_name' in self.request_data else None
        product_brand = str(self.request_data['product_brand']) if 'product_brand' in self.request_data else None
        product_model = str(self.request_data['product_model']) if 'product_model' in self.request_data else None
        product_type = str(self.request_data['product_type']) if 'product_type' in self.request_data else None
        alternative_manufacturer = str(
            self.request_data['alternative_manufacturer']) if 'alternative_manufacturer' in self.request_data else None
        alternative_manufacturer_list = self.request_data[
            'alternative_manufacturer_list'] if 'alternative_manufacturer_list' in self.request_data else []
        diffraction_type = 2

        if product_id == '00000000-0000-0000-0000-000000000000':
            product_id = None
        if product_company_id == '00000000-0000-0000-0000-000000000000':
            product_company_id = None
        if unit_id == '00000000-0000-0000-0000-000000000000':
            unit_id = None

        check_diffraction_product_id = Diffraction.check_product(self.iam_user_company_id, diffraction_product_id)
        if check_diffraction_product_id is None:
            message = self.master.messages['errors']['diffraction_pn_err_100']
            return ReaPy.rest_response(message['data'], 400)

        if 'kirilim_tipi' in check_diffraction_product_id[0]:
            parent_diffraction_type = check_diffraction_product_id[0]['kirilim_tipi']
        else:
            parent_diffraction_type = 1

        if 'derinlik' in check_diffraction_product_id[0]:
            parent_depth = check_diffraction_product_id[0]['derinlik'] + 1
            child_depth = parent_depth
        else:
            parent_depth = 0
            child_depth = 1

        if unit_id is None and unit_other is None:
            message = self.master.messages['errors']['diffraction_ue_err_103']
            return ReaPy.rest_response(message['data'], 400)

        if product_company_id is not None:
            check_product_company_id = Diffraction.check_product_company(product_company_id)
            if check_product_company_id is None:
                message = self.master.messages['errors']['diffraction_ad_err_100']
                return ReaPy.rest_response(message['data'], 400)
            if product_id is not None:
                check_product_id = Diffraction.check_product(product_company_id, product_id)
                if check_product_id is None:
                    message = self.master.messages['errors']['diffraction_ad_err_101']
                    return ReaPy.rest_response(message['data'], 400)

        check_first_diffraction = Diffraction().get_diffraction_by_id(self.iam_user_company_id, diffraction_product_id)

        if alternative_manufacturer is True and len(alternative_manufacturer_list) < 1:
            message = self.master.messages['errors']['diffraction_ad_err_105']
            return ReaPy.rest_response(message['data'], 400)

        first_diffraction_id = str(ReaPy.hash().get_uuid())
        if check_first_diffraction is None:
            parent = None
            child = diffraction_product_id
            depth = parent_depth
            insert_root_diffraction = Diffraction().insert_diffraction(first_diffraction_id, parent_diffraction_type,
                                                                       parent,
                                                                       child,
                                                                       self.iam_user_company_id, depth,
                                                                       self.iam_user_domain_account_name,
                                                                       self.remote_addr)
            if insert_root_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_105']
                return ReaPy.rest_response(message['data'], 400)

        diffraction_id = str(ReaPy.hash().get_uuid())
        domestic_diffraction_id = str(ReaPy.hash().get_uuid())

        if product_id is not None:
            if product_id == diffraction_product_id:
                message = self.master.messages['errors']['diffraction_pn_err_101']
                return ReaPy.rest_response(message['data'], 400)

            check_product_id = Diffraction.check_product_by_id(product_id)
            if check_product_id is None:
                message = self.master.messages['errors']['diffraction_ad_err_101']
                return ReaPy.rest_response(message['data'], 400)
            else:
                check_diffraction = Diffraction().get_diffraction_by_child(product_company_id,
                                                                           diffraction_product_id,
                                                                           product_id, 'yerli')
                if check_diffraction != None:
                    message = self.master.messages['errors']['diffraction_ad_err_104']
                    return ReaPy.rest_response(message['data'], 400)

                parent = diffraction_product_id
                child = domestic_diffraction_id
                depth = child_depth
                insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                                      self.iam_user_company_id, depth,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)
                if insert_diffraction != 1:
                    message = self.master.messages['errors']['diffraction_ie_err_106']
                    return ReaPy.rest_response(message['data'], 400)

                insert_domestic_supply_diffraction = Diffraction().insert_domestic_supply_diffraction(
                    domestic_diffraction_id,
                    diffraction_id,
                    product_company_id,
                    product_company_name,
                    product_id, None,
                    check_product_id[0]['marka'],
                    check_product_id[0]['model'],
                    product_amount,
                    unit_id, unit_other,
                    diffraction_product_id,
                    alternative_manufacturer,
                    check_product_id[0][
                        'urun_turu'],
                    self.iam_user_domain_account_name,
                    self.remote_addr, None)

                if insert_domestic_supply_diffraction != 1:
                    Diffraction().insert_domestic_supply_diffraction_rollback(first_diffraction_id, diffraction_id)
                    message = self.master.messages['errors']['diffraction_ie_err_107']
                    return ReaPy.rest_response(message['data'], 400)

                if len(alternative_manufacturer_list) > 0:
                    for key in alternative_manufacturer_list:
                        alternative_manufacturer_id = str(ReaPy.hash().get_uuid())

                        company_type = key['company_type'] if 'company_type' in key else None
                        company_id = str(
                            key['company_id']) if 'company_id' in key else None
                        company_name = str(
                            key['company_name']) if 'company_name' in key else None
                        tax_number = str(
                            key['tax_number']) if 'tax_number' in key else None
                        company_country = str(
                            key['company_country']) if 'company_country' in key else None

                        insert_alternative_manufacturer = Diffraction().insert_alternative_manufacturer_id(
                            alternative_manufacturer_id, company_type, company_id, company_name, tax_number,
                            company_country, self.iam_user_domain_account_name,
                            self.remote_addr)

                        if insert_alternative_manufacturer == 1:
                            Diffraction().insert_alternative_domestic_manufacturer_id(
                                alternative_manufacturer_id, diffraction_id, domestic_diffraction_id,
                                self.iam_user_domain_account_name,
                                self.remote_addr)

                message = self.master.messages['success']['success_temp']
                message['payload'] = {}
                message['payload'].clear()
                product_data = {
                    'kirilim_id': diffraction_id,
                }
                message['payload'] = product_data
                return ReaPy.rest_response(message)
        else:
            check_referenced_diffraction = Diffraction().get_diffraction_by_referenced(self.iam_user_company_id,
                                                                                       product_company_id,
                                                                                       product_name,
                                                                                       product_brand, product_model,
                                                                                       diffraction_product_id)
            if check_referenced_diffraction != None:
                message = self.master.messages['errors']['diffraction_ad_err_111']
                return ReaPy.rest_response(message['data'], 400)

            if product_company_id is None and product_id is None:

                check_referenced_diffraction = Diffraction().get_diffraction_by_domestic(diffraction_product_id,
                                                                                         product_company_name,
                                                                                         product_name, product_brand,
                                                                                         product_model)
                if check_referenced_diffraction != None:
                    message = self.master.messages['errors']['diffraction_ad_err_111']
                    return ReaPy.rest_response(message['data'], 400)

            parent = diffraction_product_id
            child = domestic_diffraction_id
            depth = 1
            insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                                  self.iam_user_company_id, depth,
                                                                  self.iam_user_domain_account_name,
                                                                  self.remote_addr)
            if insert_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_106']
                return ReaPy.rest_response(message['data'], 400)

            if product_company_id is None and product_company_name is None:
                message = self.master.messages['errors']['diffraction_ad_err_102']
                return ReaPy.rest_response(message['data'], 400)

            if product_company_name is not None and product_company_tax_number is None:
                message = self.master.messages['errors']['diffraction_ad_err_103']
                return ReaPy.rest_response(message['data'], 400)

            insert_domestic_supply_diffraction = Diffraction().insert_domestic_supply_diffraction(
                domestic_diffraction_id,
                diffraction_id,
                product_company_id,
                product_company_name,
                None, product_name,
                product_brand,
                product_model,
                product_amount,
                unit_id, unit_other,
                diffraction_product_id,
                alternative_manufacturer,
                product_type,
                self.iam_user_domain_account_name,
                self.remote_addr, product_company_tax_number)

            if insert_domestic_supply_diffraction != 1:
                Diffraction().insert_domestic_supply_diffraction_rollback(first_diffraction_id, diffraction_id)
                message = self.master.messages['errors']['diffraction_ie_err_107']
                return ReaPy.rest_response(message['data'], 400)

            # if product_company_id is not None and product_name is not None:
            if product_name is not None:

                insert_referenced_product = Diffraction().insert_referenced_product(self.iam_user_company_id,
                                                                                    product_company_id,
                                                                                    product_company_name,
                                                                                    product_company_tax_number,
                                                                                    product_name, product_brand,
                                                                                    product_model, diffraction_id,
                                                                                    diffraction_product_id, 0,
                                                                                    product_type,
                                                                                    self.iam_user_domain_account_name,
                                                                                    self.remote_addr)

                if insert_referenced_product != 1:
                    Diffraction().insert_domestic_supply_diffraction_rollback(first_diffraction_id, diffraction_id)
                    message = self.master.messages['errors']['diffraction_ie_err_110']
                    return ReaPy.rest_response(message['data'], 400)

            if len(alternative_manufacturer_list) > 0:
                for key in alternative_manufacturer_list:
                    alternative_manufacturer_id = str(ReaPy.hash().get_uuid())

                    company_type = key['company_type'] if 'company_type' in key else None
                    company_id = str(
                        key['company_id']) if 'company_id' in key else None
                    company_name = str(
                        key['company_name']) if 'company_name' in key else None
                    tax_number = str(
                        key['tax_number']) if 'tax_number' in key else None
                    company_country = str(
                        key['company_country']) if 'company_country' in key else None

                    insert_alternative_manufacturer = Diffraction().insert_alternative_manufacturer_id(
                        alternative_manufacturer_id, company_type, company_id, company_name, tax_number,
                        company_country, self.iam_user_domain_account_name,
                        self.remote_addr)

                    if insert_alternative_manufacturer == 1:
                        Diffraction().insert_alternative_domestic_manufacturer_id(
                            alternative_manufacturer_id, diffraction_id, domestic_diffraction_id,
                            self.iam_user_domain_account_name,
                            self.remote_addr)

            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'kirilim_id': diffraction_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)

    def foreign_supply(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction_foreign_supply'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_product_id = self.request_data[
            'diffraction_product_id'] if 'diffraction_product_id' in self.request_data else None
        product_company_id = self.request_data[
            'product_company_id'] if 'product_company_id' in self.request_data else None
        product_company_name = self.request_data[
            'product_company_name'] if 'product_company_name' in self.request_data else None
        product_country_origin = self.request_data[
            'product_country_origin'] if 'product_country_origin' in self.request_data else None
        product_type = self.request_data['product_type'] if 'product_type' in self.request_data else None
        product_name = self.request_data['product_name'] if 'product_name' in self.request_data else None
        product_brand = self.request_data['product_brand'] if 'product_brand' in self.request_data else None
        product_model = self.request_data['product_model'] if 'product_model' in self.request_data else None
        production_country = self.request_data[
            'production_country'] if 'production_country' in self.request_data else None
        product_origin_country = self.request_data[
            'product_origin_country'] if 'product_origin_country' in self.request_data else None
        supplier_company_id = self.request_data[
            'supplier_company_id'] if 'supplier_company_id' in self.request_data else None
        supplier_company_name = self.request_data[
            'supplier_company_name'] if 'supplier_company_name' in self.request_data else None
        supplier_origin_country = self.request_data[
            'supplier_origin_country'] if 'supplier_origin_country' in self.request_data else None
        supply_time = self.request_data['supply_time'] if 'supply_time' in self.request_data else None
        supply_life = self.request_data['supply_life'] if 'supply_life' in self.request_data else None
        product_amount = self.request_data['product_amount'] if 'product_amount' in self.request_data else None
        unit_id = self.request_data['unit_id'] if 'unit_id' in self.request_data else None
        unit_other = self.request_data['unit_other'] if 'unit_other' in self.request_data else None
        buying_unit_price = self.request_data['buying_unit_price'] if 'buying_unit_price' in self.request_data else None
        buying_unit_price_unit = self.request_data[
            'buying_unit_price_unit'] if 'buying_unit_price_unit' in self.request_data else None
        cpc_id = self.request_data['cpc_id'] if 'cpc_id' in self.request_data else None
        dual_use = self.request_data['dual_use'] if 'dual_use' in self.request_data else None
        infrastructure = self.request_data['infrastructure'] if 'infrastructure' in self.request_data else None
        end_user_document = self.request_data['end_user_document'] if 'end_user_document' in self.request_data else None
        taksonomi = self.request_data['taksonomi'] if 'taksonomi' in self.request_data else None
        keywords = self.request_data['keywords'] if 'keywords' in self.request_data else None
        reason_for_supply = self.request_data['reason_for_supply'] if 'reason_for_supply' in self.request_data else None
        alternative_manufacturer_list = self.request_data[
            'alternative_manufacturer_list'] if 'alternative_manufacturer_list' in self.request_data else []
        alternative_manufacturer = self.request_data[
            'alternative_manufacturer'] if 'alternative_manufacturer' in self.request_data else None
        diffraction_type = 3

        check_diffraction_product_id = Diffraction.check_product(self.iam_user_company_id, diffraction_product_id)
        if check_diffraction_product_id is None:
            message = self.master.messages['errors']['diffraction_pn_err_100']
            return ReaPy.rest_response(message['data'], 400)

        if 'kirilim_tipi' in check_diffraction_product_id[0]:
            parent_diffraction_type = check_diffraction_product_id[0]['kirilim_tipi']
        else:
            parent_diffraction_type = 1

        if 'derinlik' in check_diffraction_product_id[0]:
            parent_depth = check_diffraction_product_id[0]['derinlik'] + 1
            child_depth = parent_depth
        else:
            parent_depth = 0
            child_depth = 1

        if unit_id is None and unit_other is None:
            message = self.master.messages['errors']['diffraction_ue_err_103']
            return ReaPy.rest_response(message['data'], 400)

        check_first_diffraction = Diffraction().get_diffraction_by_id(self.iam_user_company_id, diffraction_product_id)

        if alternative_manufacturer is True and len(alternative_manufacturer_list) < 1:
            message = self.master.messages['errors']['diffraction_ad_err_105']
            return ReaPy.rest_response(message['data'], 400)

        first_diffraction_id = str(ReaPy.hash().get_uuid())

        if check_first_diffraction is None:
            parent = None
            child = diffraction_product_id
            depth = parent_depth
            insert_root_diffraction = Diffraction().insert_diffraction(first_diffraction_id, parent_diffraction_type,
                                                                       parent,
                                                                       child,
                                                                       self.iam_user_company_id, depth,
                                                                       self.iam_user_domain_account_name,
                                                                       self.remote_addr)
            if insert_root_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_105']
                return ReaPy.rest_response(message['data'], 400)

        diffraction_id = str(ReaPy.hash().get_uuid())
        foreign_diffraction_id = str(ReaPy.hash().get_uuid())

        check_referenced_diffraction = Diffraction().get_diffraction_by_foreign(diffraction_product_id,
                                                                                product_company_id,
                                                                                product_company_name,
                                                                                product_name, product_brand,
                                                                                product_model)
        if check_referenced_diffraction != None:
            message = self.master.messages['errors']['diffraction_ad_err_111']
            return ReaPy.rest_response(message['data'], 400)

        parent = diffraction_product_id
        child = foreign_diffraction_id
        depth = child_depth
        insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                              self.iam_user_company_id, depth,
                                                              self.iam_user_domain_account_name,
                                                              self.remote_addr)
        if insert_diffraction != 1:
            message = self.master.messages['errors']['diffraction_ie_err_106']
            return ReaPy.rest_response(message['data'], 400)

        insert_foreign_supply_diffraction = Diffraction().insert_foreign_supply_diffraction(
            foreign_diffraction_id, diffraction_id, product_company_id,
            product_company_name, product_name, product_brand, product_model,
            production_country, supplier_company_id, supplier_company_name,
            supplier_origin_country, supply_time, buying_unit_price,
            buying_unit_price_unit, cpc_id, supply_life, dual_use, end_user_document,
            taksonomi, product_amount, unit_id, unit_other,
            alternative_manufacturer, diffraction_product_id, product_type,
            self.iam_user_domain_account_name,
            self.remote_addr, reason_for_supply, product_origin_country, product_country_origin)

        if insert_foreign_supply_diffraction != 1:
            Diffraction().insert_foreign_supply_diffraction_rollback(first_diffraction_id, diffraction_id)
            message = self.master.messages['errors']['diffraction_ie_err_107']
            return ReaPy.rest_response(message['data'], 400)

        if len(alternative_manufacturer_list) > 0:
            for key in alternative_manufacturer_list:
                alternative_manufacturer_id = str(ReaPy.hash().get_uuid())

                company_type = key['company_type'] if 'company_type' in key else None
                company_id = str(
                    key['company_id']) if 'company_id' in key else None
                company_name = str(
                    key['company_name']) if 'company_name' in key else None
                tax_number = str(
                    key['tax_number']) if 'tax_number' in key else None
                company_country = str(
                    key['company_country']) if 'company_country' in key else None

                insert_alternative_manufacturer = Diffraction().insert_alternative_manufacturer_id(
                    alternative_manufacturer_id, company_type, company_id, company_name, tax_number,
                    company_country, self.iam_user_domain_account_name,
                    self.remote_addr)

                if insert_alternative_manufacturer == 1:
                    Diffraction().insert_alternative_foreign_manufacturer_id(
                        alternative_manufacturer_id, diffraction_id, foreign_diffraction_id,
                        self.iam_user_domain_account_name,
                        self.remote_addr)

            if infrastructure is not None:
                for key in infrastructure:
                    Diffraction().insert_diffraction_foreign_infrastructure(foreign_diffraction_id, key,
                                                                            self.iam_user_domain_account_name,
                                                                            self.remote_addr)

            for key in keywords:
                Diffraction().insert_diffraction_foreign_keyword(foreign_diffraction_id, key,
                                                                 self.iam_user_domain_account_name,
                                                                 self.remote_addr)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'kirilim_id': diffraction_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)

    def other_software(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction_other_software'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_product_id = self.request_data[
            'diffraction_product_id'] if 'diffraction_product_id' in self.request_data else None
        product_name = self.request_data['product_name'] if 'product_name' in self.request_data else None
        product_description = self.request_data[
            'product_description'] if 'product_description' in self.request_data else None
        keywords_tr = self.request_data['keywords_tr'] if 'keywords_tr' in self.request_data else []
        keywords_en = self.request_data['keywords_en'] if 'keywords_en' in self.request_data else []
        active_users = self.request_data['active_users '] if 'active_users ' in self.request_data else None
        os_id = self.request_data['os_id'] if 'os_id' in self.request_data else []
        language_id = self.request_data['language_id'] if 'language_id' in self.request_data else []
        is_it_embedded = self.request_data['is_it_embedded'] if 'is_it_embedded' in self.request_data else None
        open_source_list = self.request_data['open_source_list'] if 'open_source_list' in self.request_data else []
        development_period = self.request_data[
            'development_period'] if 'development_period' in self.request_data else None
        sold_under_license = self.request_data[
            'sold_under_license'] if 'sold_under_license' in self.request_data else None
        used_under_license = self.request_data[
            'used_under_license'] if 'used_under_license' in self.request_data else None
        license_price = self.request_data['license_price'] if 'license_price' in self.request_data else None
        license_type = self.request_data['license_type'] if 'license_type' in self.request_data else None
        license_period = self.request_data['license_period'] if 'license_period' in self.request_data else None
        license_country = self.request_data['license_country'] if 'license_country' in self.request_data else None
        is_it_exported = self.request_data['is_it_exported'] if 'is_it_exported' in self.request_data else None
        exported_country_list = self.request_data[
            'exported_country_list'] if 'exported_country_list' in self.request_data else []
        api_support = self.request_data['api_support'] if 'api_support' in self.request_data else None
        road_map = self.request_data['road_map'] if 'road_map' in self.request_data else None
        mobile_compatible = self.request_data['mobile_compatible'] if 'mobile_compatible' in self.request_data else None
        dual_use = self.request_data['dual_use'] if 'dual_use' in self.request_data else None
        use_ssb_products = self.request_data['use_ssb_products'] if 'use_ssb_products' in self.request_data else None
        compatible_standarts = self.request_data[
            'compatible_standarts'] if 'compatible_standarts' in self.request_data else []
        physical_hardware = self.request_data['physical_hardware'] if 'physical_hardware' in self.request_data else None
        equivalent_domestic_foreign = self.request_data[
            'equivalent_domestic_foreign'] if 'equivalent_domestic_foreign' in self.request_data else None
        reason_for_foreign_supply = self.request_data[
            'reason_for_foreign_supply'] if 'reason_for_foreign_supply' in self.request_data else None
        cpc_id = self.request_data['cpc_id'] if 'cpc_id' in self.request_data else None
        prodtr = self.request_data['prodtr'] if 'prodtr' in self.request_data else None

        diffraction_type = 5

        check_diffraction_product_id = Diffraction.check_product(self.iam_user_company_id, diffraction_product_id)
        if check_diffraction_product_id is None:
            message = self.master.messages['errors']['diffraction_pn_err_100']
            return ReaPy.rest_response(message['data'], 400)

        if 'kirilim_tipi' in check_diffraction_product_id[0]:
            parent_diffraction_type = check_diffraction_product_id[0]['kirilim_tipi']
        else:
            parent_diffraction_type = 1

        if 'derinlik' in check_diffraction_product_id[0]:
            parent_depth = check_diffraction_product_id[0]['derinlik'] + 1
            child_depth = parent_depth
        else:
            parent_depth = 0
            child_depth = 1

        check_first_diffraction = Diffraction().get_diffraction_by_id(self.iam_user_company_id, diffraction_product_id)
        first_diffraction_id = str(ReaPy.hash().get_uuid())

        if check_first_diffraction is None:
            parent = None
            child = diffraction_product_id
            depth = parent_depth
            insert_root_diffraction = Diffraction().insert_diffraction(first_diffraction_id, parent_diffraction_type,
                                                                       parent,
                                                                       child,
                                                                       self.iam_user_company_id, depth,
                                                                       self.iam_user_domain_account_name,
                                                                       self.remote_addr)
            if insert_root_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_105']
                return ReaPy.rest_response(message['data'], 400)

        diffraction_id = str(ReaPy.hash().get_uuid())
        other_software_diffraction_id = str(ReaPy.hash().get_uuid())

        check_referenced_diffraction = Diffraction().get_diffraction_by_other_software(diffraction_product_id,
                                                                                       product_name)

        if check_referenced_diffraction != None:
            message = self.master.messages['errors']['diffraction_ad_err_111']
            return ReaPy.rest_response(message['data'], 400)

        parent = diffraction_product_id
        child = other_software_diffraction_id
        depth = child_depth
        insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                              self.iam_user_company_id, depth,
                                                              self.iam_user_domain_account_name,
                                                              self.remote_addr)
        if insert_diffraction != 1:
            message = self.master.messages['errors']['diffraction_ie_err_106']
            return ReaPy.rest_response(message['data'], 400)

        open_source = False

        if len(open_source_list) > 0:
            open_source = True

        insert_other_software_diffraction = Diffraction().insert_other_software_diffraction(
            other_software_diffraction_id, diffraction_id, diffraction_product_id,
            product_name, product_description,
            active_users,
            development_period,
            sold_under_license, used_under_license, license_price, license_type,
            license_period, license_country,
            is_it_exported, api_support, is_it_embedded, open_source, road_map,
            mobile_compatible, dual_use,
            use_ssb_products,
            physical_hardware, equivalent_domestic_foreign,
            reason_for_foreign_supply, cpc_id,
            prodtr,
            self.iam_user_domain_account_name, self.remote_addr)

        if insert_other_software_diffraction != 1:
            Diffraction().insert_other_software_diffraction_rollback(first_diffraction_id, diffraction_id)
            message = self.master.messages['errors']['diffraction_ie_err_107']
            return ReaPy.rest_response(message['data'], 400)

        for key in keywords_tr:
            Diffraction().insert_diffraction_other_software_keyword(other_software_diffraction_id, key, '1',
                                                                    self.iam_user_domain_account_name,
                                                                    self.remote_addr)

        for key in keywords_en:
            Diffraction().insert_diffraction_other_software_keyword(other_software_diffraction_id, key, '2',
                                                                    self.iam_user_domain_account_name,
                                                                    self.remote_addr)

        for key in os_id:
            Diffraction().insert_diffraction_other_software_os(other_software_diffraction_id, key,
                                                               self.iam_user_domain_account_name,
                                                               self.remote_addr)

        for key in language_id:
            Diffraction().insert_diffraction_other_software_lang(other_software_diffraction_id, key,
                                                                 self.iam_user_domain_account_name,
                                                                 self.remote_addr)

        for key in compatible_standarts:
            Diffraction().insert_diffraction_other_software_standarts(other_software_diffraction_id, key,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)

        for key in exported_country_list:
            Diffraction().insert_diffraction_other_software_export_country(other_software_diffraction_id, key,
                                                                           self.iam_user_domain_account_name,
                                                                           self.remote_addr)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'kirilim_id': diffraction_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)

    def other_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_add_diffraction_other_product'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_product_id = self.request_data[
            'diffraction_product_id'] if 'diffraction_product_id' in self.request_data else None
        product_name = self.request_data[
            'product_name'] if 'product_name' in self.request_data else None
        design = self.request_data['design'] if 'design' in self.request_data else None
        produce_at_license = self.request_data[
            'produce_at_license'] if 'produce_at_license' in self.request_data else None
        product_origin_country = self.request_data[
            'product_origin_country'] if 'product_origin_country' in self.request_data else None
        supplier_company_id = self.request_data[
            'supplier_company_id'] if 'supplier_company_id' in self.request_data else None
        supplier_company_name = self.request_data[
            'supplier_company_name'] if 'supplier_company_name' in self.request_data else None
        supplier_origin_country = self.request_data[
            'supplier_origin_country'] if 'supplier_origin_country' in self.request_data else None
        product_company_id = self.request_data[
            'product_company_id'] if 'product_company_id' in self.request_data else None
        product_company_name = self.request_data[
            'product_company_name'] if 'product_company_name' in self.request_data else None
        product_company_country = self.request_data[
            'product_company_country'] if 'product_company_country' in self.request_data else None
        production_country = self.request_data[
            'production_country'] if 'production_country' in self.request_data else None
        alternative_manufacturer_list = self.request_data[
            'alternative_manufacturer_list'] if 'alternative_manufacturer_list' in self.request_data else []
        test_infrastracture_country = self.request_data[
            'test_infrastracture_country'] if 'test_infrastracture_country' in self.request_data else []
        production_time = self.request_data['production_time'] if 'production_time' in self.request_data else None
        supply_time = self.request_data['supply_time'] if 'supply_time' in self.request_data else None
        product_capacity = self.request_data['product_capacity'] if 'product_capacity' in self.request_data else None
        product_capacity_unit_id = self.request_data[
            'product_capacity_unit_id'] if 'product_capacity_unit_id' in self.request_data else None
        product_capacity_unit_other = self.request_data[
            'product_capacity_unit_other'] if 'product_capacity_unit_other' in self.request_data else None
        buying_unit_price = self.request_data['buying_unit_price'] if 'buying_unit_price' in self.request_data else None
        buying_unit_price_unit = self.request_data[
            'buying_unit_price_unit'] if 'buying_unit_price_unit' in self.request_data else None
        request_amount = self.request_data['request_amount'] if 'request_amount' in self.request_data else None
        request_amount_unit_id = self.request_data[
            'request_amount_unit_id'] if 'request_amount_unit_id' in self.request_data else None
        request_amount_unit_other = self.request_data[
            'request_amount_unit_other'] if 'request_amount_unit_other' in self.request_data else None
        cpc_id = self.request_data['cpc_id'] if 'cpc_id' in self.request_data else None
        ths = self.request_data['ths'] if 'ths' in self.request_data else None
        tlc = self.request_data['tlc'] if 'tlc' in self.request_data else None
        dual_use = self.request_data['dual_use'] if 'dual_use' in self.request_data else None
        infrastructure = self.request_data['infrastructure'] if 'infrastructure' in self.request_data else []
        end_user_document = self.request_data['end_user_document'] if 'end_user_document' in self.request_data else None
        keywords_tr = self.request_data['keywords_tr'] if 'keywords_tr' in self.request_data else []
        keywords_en = self.request_data['keywords_en'] if 'keywords_en' in self.request_data else []
        taksonomi = self.request_data['taksonomi'] if 'taksonomi' in self.request_data else None
        reason_for_foreign_supply = self.request_data[
            'reason_for_foreign_supply'] if 'reason_for_foreign_supply' in self.request_data else None
        export_blocker = self.request_data['export_blocker'] if 'export_blocker' in self.request_data else []
        import_blocker = self.request_data['import_blocker'] if 'import_blocker' in self.request_data else []

        diffraction_type = 4

        check_diffraction_product_id = Diffraction.check_product(self.iam_user_company_id, diffraction_product_id)
        if check_diffraction_product_id is None:
            message = self.master.messages['errors']['diffraction_pn_err_100']
            return ReaPy.rest_response(message['data'], 400)

        if 'kirilim_tipi' in check_diffraction_product_id[0]:
            parent_diffraction_type = check_diffraction_product_id[0]['kirilim_tipi']
        else:
            parent_diffraction_type = 1

        if 'derinlik' in check_diffraction_product_id[0]:
            parent_depth = check_diffraction_product_id[0]['derinlik'] + 1
            child_depth = parent_depth
        else:
            parent_depth = 0
            child_depth = 1

        check_first_diffraction = Diffraction().get_diffraction_by_id(self.iam_user_company_id, diffraction_product_id)
        first_diffraction_id = str(ReaPy.hash().get_uuid())

        if check_first_diffraction is None:
            parent = None
            child = diffraction_product_id
            depth = parent_depth
            insert_root_diffraction = Diffraction().insert_diffraction(first_diffraction_id, parent_diffraction_type,
                                                                       parent,
                                                                       child,
                                                                       self.iam_user_company_id, depth,
                                                                       self.iam_user_domain_account_name,
                                                                       self.remote_addr)
            if insert_root_diffraction != 1:
                message = self.master.messages['errors']['diffraction_ie_err_105']
                return ReaPy.rest_response(message['data'], 400)

        diffraction_id = str(ReaPy.hash().get_uuid())
        other_product_diffraction_id = str(ReaPy.hash().get_uuid())

        check_referenced_diffraction = Diffraction().get_diffraction_by_other_product(diffraction_product_id,
                                                                                      product_name)
        if check_referenced_diffraction != None:
            Diffraction().insert_other_software_diffraction_rollback(first_diffraction_id, diffraction_id)
            message = self.master.messages['errors']['diffraction_ad_err_111']
            return ReaPy.rest_response(message['data'], 400)

        parent = diffraction_product_id
        child = other_product_diffraction_id
        depth = child_depth
        insert_diffraction = Diffraction().insert_diffraction(diffraction_id, diffraction_type, parent, child,
                                                              self.iam_user_company_id, depth,
                                                              self.iam_user_domain_account_name,
                                                              self.remote_addr)
        if insert_diffraction != 1:
            message = self.master.messages['errors']['diffraction_ie_err_106']
            return ReaPy.rest_response(message['data'], 400)

        alternative_manufacturer = False
        if len(alternative_manufacturer_list) > 0:
            alternative_manufacturer = True

        insert_other_product_diffraction = Diffraction().insert_other_product_diffraction(
            other_product_diffraction_id, diffraction_id, diffraction_product_id, product_name, design,
            product_origin_country, supplier_company_id, supplier_company_name, supplier_origin_country,
            product_company_id, product_company_name, product_company_country, production_country,
            alternative_manufacturer, production_time, supply_time, product_capacity, product_capacity_unit_id,
            product_capacity_unit_other, buying_unit_price, buying_unit_price_unit, request_amount,
            request_amount_unit_id, request_amount_unit_other, cpc_id, ths, tlc, dual_use, end_user_document, taksonomi,
            reason_for_foreign_supply, self.iam_user_domain_account_name, self.remote_addr, produce_at_license)

        if insert_other_product_diffraction != 1:
            Diffraction().insert_other_software_diffraction_rollback(first_diffraction_id, diffraction_id)
            message = self.master.messages['errors']['diffraction_ie_err_107']
            return ReaPy.rest_response(message['data'], 400)

        for key in keywords_tr:
            Diffraction().insert_diffraction_other_product_keyword(other_product_diffraction_id, key, '1',
                                                                   self.iam_user_domain_account_name,
                                                                   self.remote_addr)

        for key in keywords_en:
            Diffraction().insert_diffraction_other_product_keyword(other_product_diffraction_id, key, '2',
                                                                   self.iam_user_domain_account_name,
                                                                   self.remote_addr)
        if infrastructure is not None:
            for key in infrastructure:
                Diffraction().insert_diffraction_other_infrastructure(other_product_diffraction_id, key,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)

        if test_infrastracture_country is not None:
            for key in test_infrastracture_country:
                Diffraction().insert_diffraction_other_test_country(other_product_diffraction_id, key,
                                                                    self.iam_user_domain_account_name,
                                                                    self.remote_addr)

        if export_blocker is not None:
            for key in export_blocker:
                Diffraction().insert_diffraction_other_export_blocker(other_product_diffraction_id, key,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)

        if import_blocker is not None:
            for key in import_blocker:
                Diffraction().insert_diffraction_other_import_blocker(other_product_diffraction_id, key,
                                                                      self.iam_user_domain_account_name,
                                                                      self.remote_addr)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'kirilim_id': diffraction_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)
