# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun_kirilim.diffraction import Diffraction


class UpdatePendingProduct(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None
    pending_product = None
    accepted_product = None
    pending_product_id = None
    own_product_id = None
    pending_type = None

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id != None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_update_pending_product'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.remote_addr = request.remote_addr

        pending_product_id = self.request_data[
            'pending_product_id'] if 'pending_product_id' in self.request_data else None
        check_pending_product = Diffraction().get_diffraction_pending_product(self.iam_user_company_id,
                                                                              pending_product_id, 0)
        if check_pending_product is None:
            message = self.master.messages['errors']['diffraction_up_err_100']
            return ReaPy.rest_response(message['data'], 400)
        self.pending_product_id = pending_product_id

        self.pending_product = check_pending_product[0]
        if self.pending_product['referans_veren_paydas_id'] == self.iam_user_company_id:
            self.pending_type = 'kendi'
        else:
            self.pending_type = 'yerli'
        product_type = str(request.json['acceptance']).lower()
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def false(self):
        refusal_description = self.request_data[
            'refusal_description'] if 'refusal_description' in self.request_data else None
        refusal_referencing = Diffraction().update_pending_product(self.pending_product_id, None, 2,
                                                                   refusal_description,
                                                                   self.iam_user_domain_account_name,
                                                                   self.remote_addr)
        if refusal_referencing != 1:
            message = self.master.messages['errors']['diffraction_up_err_102']
            return ReaPy.rest_response(message['data'], 400)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'pending_product_id': self.pending_product_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)

    def true(self):
        own_product_id = self.request_data[
            'own_product_id'] if 'own_product_id' in self.request_data else None
        check_own_product = Diffraction().check_product(self.iam_user_company_id, own_product_id)
        if check_own_product is None:
            message = self.master.messages['errors']['diffraction_up_err_101']
            return ReaPy.rest_response(message['data'], 400)
        self.own_product_id = own_product_id
        self.accepted_product = check_own_product[0]
        reference_type = str(self.pending_product['referans_urun_tipi'])
        call_product_type = getattr(self, 'accept_' + reference_type)
        return call_product_type()

    def accept_1(self):

        update_diffraction = Diffraction.update_pending_diffraction_product(self.pending_product['kirilim_id'],
                                                                            self.own_product_id,
                                                                            self.accepted_product['urun_turu'],
                                                                            self.accepted_product['marka'],
                                                                            self.accepted_product['model'],
                                                                            self.pending_type,
                                                                            self.iam_user_domain_account_name,
                                                                            self.remote_addr)

        if update_diffraction != 1:
            message = self.master.messages['errors']['diffraction_up_err_102']
            return ReaPy.rest_response(message['data'], 400)

        accept_referencing = Diffraction().update_pending_product(self.pending_product_id, self.own_product_id, 1, None,
                                                                  self.iam_user_domain_account_name,
                                                                  self.remote_addr)
        if accept_referencing != 1:
            message = self.master.messages['errors']['diffraction_up_err_102']
            return ReaPy.rest_response(message['data'], 400)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'pending_product_id': self.pending_product_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)

    def accept_2(self):

        update_diffraction = Diffraction.update_pending_diffraction_product(self.pending_product['kirilim_id'],
                                                                            self.own_product_id,
                                                                            self.accepted_product['urun_turu'],
                                                                            self.accepted_product['marka'],
                                                                            self.accepted_product['model'],
                                                                            self.pending_type,
                                                                            self.iam_user_domain_account_name,
                                                                            self.remote_addr)

        if update_diffraction != 1:
            message = self.master.messages['errors']['diffraction_up_err_102']
            return ReaPy.rest_response(message['data'], 400)

        accept_referencing = Diffraction().update_pending_product(self.pending_product_id, self.own_product_id, 1, None,
                                                                  self.iam_user_domain_account_name,
                                                                  self.remote_addr)
        if accept_referencing != 1:
            message = self.master.messages['errors']['diffraction_up_err_102']
            return ReaPy.rest_response(message['data'], 400)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'pending_product_id': self.pending_product_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)
