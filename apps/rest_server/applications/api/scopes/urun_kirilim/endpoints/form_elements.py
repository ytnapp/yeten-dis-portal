# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun_kirilim.element_values import ElementValues


class FormElements(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None

    def __init__(self):
        self.request = ReaPy.set_request(ReaPy.rest_request())
        self.request_data = self.request.json
        self.message = self.master.messages['success']['success_temp']
        self.message['payload'] = []

    def post(self):
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_diffraction_form_elements'],
                                                   self.request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        diffraction_type = self.request_data['diffraction_type']
        call_product_type = getattr(self, diffraction_type)
        return call_product_type()

    def own_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_diffraction_own_product_form_elements'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholder_products = ElementValues.get_stakeholder_product(str(self.iam_user_company_id), keyword)
        units = ElementValues.get_units(1)
        payload = {'products': stakeholder_products, 'units': units}
        self.message['payload'] = payload
        return ReaPy.rest_response(self.message)

    def domestic_supply(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_domestic_supply_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""
        units = ElementValues.get_units(1)
        stakeholder_list_foreign = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword, 'yerli')
        stakeholder_list_domestic = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword,
                                                                       'yabanci')
        countries = ElementValues.get_countries()

        payload = {'foreign_supplier': stakeholder_list_foreign,
                   'domestic_supplier': stakeholder_list_domestic,
                   'units': units,
                   'countries': countries
                   }
        self.message['payload'] = payload
        return ReaPy.rest_response(self.message)

    def foreign_supply(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_foreign_supply_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholder_list_foreign = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword, 'yerli')
        stakeholder_list_domestic = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword,
                                                                       'yabanci')
        countries = ElementValues.get_countries()
        supply_time = ElementValues.get_sys_definition('um_kirilim_yabanci', 'tedarik_temin_suresi')
        supply_life = ElementValues.get_sys_definition('um_kirilim_yabanci', 'teknoloji_omur_devri_safhasi')
        dual_use = ElementValues.get_sys_definition('um_kirilim_yabanci', 'askeri_alanda_kullanim_dual_use')
        infrastructure = ElementValues.get_sys_definition('um_kirilim_yabanci', 'alt_yapi')
        end_user_document = ElementValues.get_sys_definition('um_kirilim_yabanci', 'son_kullanici_belgesi_var_mi')
        unit_price_unit = ElementValues.get_sys_definition('um_kirilim_yabanci', 'alis_birim_fiyati_cinsi')
        units = ElementValues.get_units(1)
        cpc = ElementValues.get_cpc('root', 2)
        taksonomi = ElementValues.get_taksonomi()
        payload = {'foreign_supplier': stakeholder_list_foreign,
                   'domestic_supplier': stakeholder_list_domestic,
                   'countries': countries,
                   'units': units,
                   'cpc': cpc,
                   'taksonomi': taksonomi,
                   'supply_time': supply_time,
                   'supply_life': supply_life,
                   'dual_use': dual_use,
                   'infrastructure': infrastructure,
                   'end_user_document': end_user_document,
                   'unit_price_unit': unit_price_unit

                   }
        self.message['payload'] = payload
        return ReaPy.rest_response(self.message)

    def supplier_product(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_supplier_product_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholder_products = ElementValues.get_stakeholder_product(str(self.request_data['supplier_id']), keyword)
        self.message['payload'] = stakeholder_products
        return ReaPy.rest_response(self.message)

    def cpc(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements_cpc'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        data = ElementValues.get_cpc(self.request_data['parent'], int(self.request_data['level']) + 1,
                                     self.request_data['finder'])

        message['payload'] = data
        return ReaPy.rest_response(message)

    def taksonomi(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements_taksonomi'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        data = ElementValues.find_taksonomi(self.request_data['finder'])

        message['payload'] = data
        return ReaPy.rest_response(message)

    def pending_product(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_pending_product_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        pending_products = ElementValues.get_pending_product(str(self.iam_user_company_id), keyword)
        stakeholder_products = ElementValues.get_stakeholder_product(str(self.iam_user_company_id), '', 1)
        payload = {'pending_products': pending_products, 'products': stakeholder_products}
        self.message['payload'] = payload
        return ReaPy.rest_response(message)

    def assigned_product(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_assigned_product_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        assigned_products = ElementValues.get_assigned_product(str(self.iam_user_company_id), keyword)
        payload = {'assigned_products': assigned_products}
        self.message['payload'] = payload
        return ReaPy.rest_response(message)

    def other_product(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_other_product_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholder_list_foreign = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword, 'yerli')
        stakeholder_list_domestic = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword,
                                                                       'yabanci')
        countries = ElementValues.get_countries()
        production_time = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'uretim_suresi')
        supply_time = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'tedarik_temin_suresi')
        preparation_level = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'teknoloji_hazirlik_seviyesi')
        supply_life = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'teknoloji_omur_devri_safhasi')
        dual_use = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'askeri_alanda_kullanim_dual_use')
        infrastructure = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'alt_yapi')
        end_user_document = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'son_kullanici_belgesi_var_mi')
        design = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'tasarim')
        under_license = ElementValues.get_sys_definition('um_kirilim_diger_urun', 'lisans_altinda_uretim_mi')
        import_barriers = ElementValues.get_sys_definition('um_kirilim_ithalat_engel', 'ithalat_engeli')
        units = ElementValues.get_units(1)
        cpc = ElementValues.get_cpc('root', 2)
        taksonomi = ElementValues.get_taksonomi()
        payload = {'foreign_supplier': stakeholder_list_foreign,
                   'domestic_supplier': stakeholder_list_domestic,
                   'countries': countries,
                   'units': units,
                   'cpc': cpc,
                   'taksonomi': taksonomi,
                   'supply_time': supply_time,
                   'supply_life': supply_life,
                   'preparation_level': preparation_level,
                   'production_time': production_time,
                   'dual_use': dual_use,
                   'design': design,
                   'under_license': under_license,
                   'import_barriers': import_barriers,
                   'infrastructure': infrastructure,
                   'end_user_document': end_user_document

                   }
        self.message['payload'] = payload
        return ReaPy.rest_response(self.message)

    def other_software(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_other_software_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholder_list_foreign = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword, 'yerli')
        stakeholder_list_domestic = ElementValues.get_stakeholder_list(str(self.iam_user_company_id), keyword,
                                                                       'yabanci')

        dual_use = ElementValues.get_sys_definition('um_kirilim_diger_yazilim', 'askeri_alanda_kullanim_dual_use')
        software_languages = ElementValues.get_sys_definition('um_kirilim_diger_yazilim_dil', 'dil_id')
        operating_systems = ElementValues.get_sys_definition('um_yazilim_isletim_sistemi', 'isletim_sistemi_id')
        license_type = ElementValues.get_sys_definition('um_kirilim_diger_yazilim', 'lisans_turu')
        software_standards = ElementValues.get_software_standards()
        prod_tr = ElementValues.get_prod_tr()
        countries = ElementValues.get_countries()
        cpc = ElementValues.get_cpc('root', 2)
        payload = {
            'foreign_supplier': stakeholder_list_foreign,
            'domestic_supplier': stakeholder_list_domestic,
            'countries': countries,
            'cpc': cpc,
            'prod_tr': prod_tr,
            'dual_use': dual_use,
            'software_languages': software_languages,
            'operating_systems': operating_systems,
            'software_standards': software_standards,
            'license_type': license_type

        }
        self.message['payload'] = payload
        return ReaPy.rest_response(self.message)

    def supplier(self):
        validate = ReaPy.validator().json_validate(
            self.master.schema['product_diffraction_supplier_form_elements'],
            self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)

        keyword = str(self.request_data['keyword']) if 'keyword' in self.request_data else ""

        stakeholders = ElementValues.get_stakeholder_list(self.iam_user_company_id, keyword, '0')
        self.message['payload'] = stakeholders
        return ReaPy.rest_response(self.message)
