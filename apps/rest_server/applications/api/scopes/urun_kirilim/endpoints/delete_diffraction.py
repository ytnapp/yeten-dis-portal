# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun_kirilim.diffraction import Diffraction


class DeleteDiffraction(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None

    def delete(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id != None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_delete_diffraction'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.remote_addr = request.remote_addr
        message = self.master.messages['success']['success_temp']
        message['payload'] = []
        diffraction_id = request.json['diffraction_id']
        check_diffraction = Diffraction().check_product_diffraction_by_id(self.iam_user_company_id, diffraction_id)
        if check_diffraction is None:
            message = self.master.messages['errors']['diffraction_dd_err_100']
            return ReaPy.rest_response(message['data'], 400)
        get_diffraction = Diffraction().get_product_diffraction_by_id(self.iam_user_company_id, diffraction_id)
        if get_diffraction is not None:
            message = self.master.messages['errors']['diffraction_dd_err_102']
            return ReaPy.rest_response(message['data'], 400)
        delete_diffraction = Diffraction().delete_product_diffraction_by_id(self.iam_user_company_id, diffraction_id,
                                                                            self.iam_user_domain_account_name,
                                                                            self.remote_addr)
        if delete_diffraction != 1:
            message = self.master.messages['errors']['diffraction_dd_err_101']
            return ReaPy.rest_response(message['data'], 400)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        product_data = {
            'kirilim_id': diffraction_id,
        }
        message['payload'] = product_data
        return ReaPy.rest_response(message)
