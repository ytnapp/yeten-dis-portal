# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun_kirilim.diffraction import Diffraction


class GetDiffraction(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None
    sub_dif_level = 0
    sub_dif_level_diffraction = 0
    sub_diffraction = []

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}
        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id != None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_get_diffraction'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.remote_addr = request.remote_addr
        message = self.master.messages['success']['success_temp']
        message['payload'] = []
        product_id = self.request_data['product_id']
        prefix = self.request_data['prefix'] if 'prefix' in self.request_data else None

        diffraction_list = []
        get_diffraction = Diffraction().get_product_diffraction_by_id(self.iam_user_company_id, product_id)

        if get_diffraction is not None:
            i = 1
            for key in get_diffraction:
                self.sub_dif_level = 0
                self.sub_dif_level_diffraction = 0
                self.sub_diffraction = []
                if key['kirilim_tipi'] > 3:
                    key['urun_id'] = key['kirilim_id']
                sub_dif = Diffraction().get_product_sub_diffraction_by_id(self.iam_user_company_id, key['urun_id'],
                                                                          self)
                # total_dif = Diffraction().get_product_total_diffraction_by_id(self.iam_user_company_id, key['urun_id'])
                if key['urun_id'] is None:
                    keywords = Diffraction().get_product_keywords_diffraction_by_id(key['kirilim_id'])
                else:
                    keywords = Diffraction().get_product_keywords_diffraction_by_id(key['urun_id'])
                cpc_data = Diffraction().get_product_cpc_diffraction_by_id(key['cpc'])
                key['cpc'] = cpc_data[0]
                key['cpc_sembol'] = cpc_data[1]
                key['anahtar_kelime'] = keywords
                if sub_dif == 0:
                    key['alt_kirilim_sayisi'] = 0
                else:
                    key['alt_kirilim_sayisi'] = self.sub_dif_level

                key['toplam_kirilim_sayisi'] = self.sub_dif_level_diffraction
                if prefix is None:
                    key['no'] = str(key['derinlik']) + '.' + str(i)
                else:
                    key['no'] = prefix + '.' + str(i)
                del key['derinlik']
                diffraction_list.append(key)
                i += 1

        message['payload'] = diffraction_list
        return ReaPy.rest_response(message)
