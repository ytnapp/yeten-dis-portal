# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.kullanici.kulanici import Kullanici
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Talep(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __init__(self):
        configurations = ReaPy.configuration().get_configuration()['system']['dis_portal_conf']['sso']
        self.sso_lock_url = configurations['sso_hostname'] + '/iam/user/lock'
        self.sso_unlock_url = configurations['sso_hostname'] + '/iam/user/unlock'
        self.sso_client_id = configurations['iam_client_id']
        self.sso_client_secret = configurations['client_secret']
        self.data = {'client_id': self.sso_client_id, "client_secret": self.sso_client_secret}

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)

        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False, 'payload': {}}

        dis_kullanici_talep_id = None
        try:
            dis_kullanici_talep_id = ReaPy.rest_request().args['dis_kullanici_talep_id']
        except Exception:
            pass

        try:
            dashboard_data = Kullanici().get_detay(self.iam_user_company_id, dis_kullanici_talep_id)
            frontend_data['payload'] = dashboard_data
            frontend_data['success'] = True
        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

    # noinspection PyMethodMayBeStatic
    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['kullanici_taleb_insert_schema'], post_data)

        frontend_data['payload'] = {}
        if validate['success'] is False:
            # TODO düzgün bi validate message
            frontend_data['payload'] = validate['message']
            frontend_data['message'] = 'Bilgi alanlarında hata bulunmaktadır.'
            return ReaPy.rest_response(frontend_data, 400)

        tc_varmi = Kullanici().user_is_exist(post_data['tc'])
        if tc_varmi:
            durum = Kullanici().insert(post_data, self.iam_user_company_id, self.iam_user_domain_account_name,
                                       self.iam_user_ip_addr)
            if durum is not None:
                frontend_data['success'] = True
                return ReaPy.rest_response(frontend_data, 201)
            else:
                err_data = self.master.messages['errors']['general_server_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 500)
        else:
            err_data = self.master.messages['errors']['user_kt_err_102']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

    # noinspection PyMethodMayBeStatic
    def put(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['kullanici_taleb_update_schema'], post_data)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False, 'payload': {}}

        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        durum = Kullanici().update(post_data, self.iam_user_company_id, self.iam_user_domain_account_name,
                                   self.iam_user_ip_addr)

        if durum is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def patch(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False, 'payload': {}}

        post_data = ReaPy.rest_request().get_json()
        validate = ReaPy.validator().json_validate(self.master.schema['kullanici_durum_update_schema'], post_data)

        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        try:
            tckn = Kullanici().get_tckn(post_data['dis_kullanici_talep_id'])
            if tckn is not None and post_data['pasif_durum'] == 1:
                self.data.update({'user_domain_account_name': tckn})
                # TODO lock a bakilacak
                do_lock = ReaPy.requests().put(url=self.sso_lock_url,
                                               headers={
                                                   'Content-Type': 'application/json'
                                               }, data=ReaPy.json().dumps(self.data))
                if do_lock.ok:
                    durum = Kullanici().update(post_data, self.iam_user_company_id, self.iam_user_domain_account_name,
                                               self.iam_user_ip_addr)
                    if durum is not None:
                        frontend_data['success'] = True
                        frontend_data['message'] = 'Pasifleştirildi'
                        return ReaPy.rest_response(frontend_data, 200)
                else:
                    return ReaPy.rest_response(do_lock.json()['data'], 404)
            elif tckn is not None and post_data['pasif_durum'] == 0:
                self.data.update({'user_domain_account_name': tckn})
                # TODO lock a bakilacak
                do_unlock = ReaPy.requests().put(url=self.sso_unlock_url,
                                                 headers={
                                                     'Content-Type': 'application/json'
                                                 }, data=ReaPy.json().dumps(self.data))
                durum_un = None
                if do_unlock.ok:
                    durum_un = Kullanici().update(post_data, self.iam_user_company_id,
                                                  self.iam_user_domain_account_name,
                                                  self.iam_user_ip_addr)
                if durum_un is not None:
                    frontend_data['success'] = True
                    frontend_data['message'] = 'Aktifleştirildi'
                    return ReaPy.rest_response(frontend_data, 200)
                else:
                    return ReaPy.rest_response(do_unlock.json()['data'], 404)
            else:
                err_data = self.master.messages['errors']['general_server_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 500)
        except Exception as e:
            print(e)
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def delete(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        frontend_data['payload'] = {}

        if Kullanici().delete(post_data['dis_kullanici_talep_id']) is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
