# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.kullanici.kulanici import Kullanici
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Yetki(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no
    sso_url_resource_sso_domain_id = None
    sso_url_resource_modul_yetki_url = None
    sso_url_resource_sso_domain_token = None

    def __init__(self):
        configurations = ReaPy.configuration().get_configuration()['system']['dis_portal_conf']['sso']
        self.sso_url_resource_modul_yetki_url = configurations['sso_hostname'] + '/iam/role/list'
        self.sso_url_unassign = configurations['sso_hostname'] + '/iam/role/unassign'
        self.sso_url_assign = configurations['sso_hostname'] + '/iam/role/assign'
        self.sso_user_get = configurations['sso_hostname'] + '/iam/user/get'
        self.sso_client_id = configurations['iam_client_id']
        self.sso_client_secret = configurations['client_secret']
        self.data = {'client_id': self.sso_client_id, "client_secret": self.sso_client_secret}
        self.roles_list = self.__get_role_list()

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False, 'payload': {'moduller': [], 'kullanicilar': []}}
        try:
            users_list = []
            kullanici_list = Kullanici().get_detay_yetki(self.iam_user_company_id)
            modul_liste = ReaPy.requests().get(url=self.sso_url_resource_modul_yetki_url,
                                               headers={'content-type': 'application/json'},
                                               json=self.data)
            frontend_data['payload']['moduller'] = modul_liste.json()['data']['payload']
            for item in kullanici_list:
                user = self.__get_iam_user(item['tc'])
                if isinstance(user, list) and len(user) == 0:
                    pass
                else:
                    roles = []
                    for key in user['assigned_iam_roles']:
                        roles.append(key['iam_role_id'])
                    user['roles'] = roles
                    del user['assigned_iam_roles']

                    users_list.append(user)
            frontend_data['payload']['kullanicilar'] = users_list
            frontend_data['success'] = True

        except Exception as exc:
            print(exc, frontend_data)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

    # noinspection PyMethodMayBeStatic
    def put(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': 'd843bc87-08c5-48db-a1ef-47964286982a',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        post_data = ReaPy.rest_request().get_json()
        validate = ReaPy.validator().json_validate(self.master.schema['kullanici_yetki_atama_silme_schema'], post_data)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-admin')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)
        frontend_data = {'success': False, 'payload': {}}

        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        try:
            for item in post_data['roles']:
                if item in self.roles_list:
                    self.roles_list.remove(item)
            assign_dict = {}
            unassign_dict = {}
            tckn = post_data['dis_kullanici_talep_id']
            if tckn is not None and len(post_data['roles']) != 0:
                assign_dict.update(self.data)
                unassign_dict.update(self.data)
                assign_dict.update({'user_domain_account_name': tckn, 'roles_to_assign': post_data['roles']})
                get_assign = ReaPy.requests().put(url=self.sso_url_assign,
                                                  headers={'Content-Type': 'application/json'},
                                                  data=ReaPy.json().dumps(assign_dict))
                if get_assign.ok:
                    unassign_dict.update(
                        {'user_domain_account_name': tckn, 'roles_to_unassign': self.__get_iam_user(tckn, True)})
                    del_assign = ReaPy.requests().delete(url=self.sso_url_unassign,
                                                         headers={
                                                             'Content-Type': 'application/json'
                                                         }, data=ReaPy.json().dumps(unassign_dict))
                    frontend_data['unassign'] = del_assign
                    return ReaPy.rest_response(get_assign.json()['data'], 200)
                else:
                    try:
                        return ReaPy.rest_response(get_assign.json()['data'], 404)
                    except Exception as e:
                        frontend_data['data'] = get_assign.text
                        return ReaPy.rest_response(frontend_data, 404)
            elif tckn is not None and len(post_data['roles']) == 0:
                unassign_dict.update(self.data)
                unassign_dict.update(
                    {'user_domain_account_name': tckn, 'roles_to_unassign': self.__get_iam_user(tckn, True)})
                all_unassign = ReaPy.requests().delete(url=self.sso_url_unassign,
                                                       headers={
                                                           'Content-Type': 'application/json'
                                                       }, data=ReaPy.json().dumps(unassign_dict))
                return ReaPy.rest_response(ReaPy.json().loads(all_unassign.text)['data'], 200)
            else:
                err_data = self.master.messages['errors']['general_server_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 500)
        except Exception as e:
            print(e)
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    def __get_iam_user(self, tc, unassign=False):
        self.data.update({'user_domain_account_name': tc})
        try:
            user = ReaPy.requests().get(url=self.sso_user_get,
                                        headers={'content-type': 'application/json'},
                                        json=self.data)
            users = ReaPy.json().loads(user.text)['data']
            if not users['success'] and not unassign:
                return []
            if unassign and users['success']:
                unassign_list = []
                for item in users['payload']['assigned_iam_roles']:
                    if item['iam_role_id'] in self.roles_list:
                        unassign_list.append(item['iam_role_id'])
                return unassign_list
            elif unassign and not users['success']:
                print(users)
                return []
            return users['payload']
        except Exception as e:
            print(e)

    def __get_role_list(self):
        role_list = []
        try:
            role_list_q = ReaPy.requests().get(url=self.sso_url_resource_modul_yetki_url,
                                               headers={'content-type': 'application/json'},
                                               json=self.data)

            if role_list_q.ok:
                data_json = ReaPy.json().loads(role_list_q.text)
                for item in data_json['data']['payload']:
                    if item['iam_role_id'] not in role_list:
                        role_list.append(item['iam_role_id'])
                return role_list
        except Exception as e:
            print(e)
            return role_list
