# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.kullanici.kulanici import Kullanici
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Destekveren(ReaPy().rest_resource()):

    def get(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        frontend_data = {'success': False, 'payload': {}}
        try:
            dashboard_data = Kullanici().get_destek_verenler()
            frontend_data['payload'] = dashboard_data
            frontend_data['success'] = True
        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

