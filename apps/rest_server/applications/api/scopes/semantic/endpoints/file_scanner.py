# -*- coding: utf-8 -*-
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model

import os
from apps.rest_server.models.api.semantic.main import Main


class FileScanner(ReaPy().rest_resource()):

    def __init__(self):
        configuration = ReaPy.configuration()
        if ReaPy.platform().system() == "Linux":
            self.save_disk_location = configuration.get_configuration()['system']['rest_server'][
                'save_disk_location_linux']
        else:
            self.save_disk_location = configuration.get_configuration()['system']['rest_server'][
                'save_disk_location_windows']
        self.request = ReaPy.set_request(ReaPy.rest_request())
        self.request_data = self.request.json
        self.message = self.master.messages['success']['success_temp']
        self.message['payload'] = {}

    def post(self):
        token = Main(self).check_token()
        if token:
            return token
        validator = Main(self).json_validate('semantic_file_scanner')
        if validator:
            return validator
        file_list = []
        source_folder = self.save_disk_location + '/geodi/' + self.request_data['source_folder']
        target_folder = self.save_disk_location + '/geodi/' + self.request_data['target_folder']
        if os.path.isdir(source_folder) is False:
            message = self.master.messages['errors']['resource_sf_err_101']
            return ReaPy.rest_response(message['data'], 400)

        for file in os.listdir(source_folder):
            if os.path.isfile(source_folder + '/' + file) is True:
                file_list.append(file)

        if len(file_list) == 0:
            message = self.master.messages['errors']['resource_sf_err_102']
            return ReaPy.rest_response(message['data'], 400)

        upload_successful_file_count = 0
        upload_unsuccessful_file_count = 0
        upload_pending_file_count = 0

        pre_scheduled_file = []
        insert_successful_file = []
        insert_unsuccessful_file = []
        for file in file_list:
            check_file = Model(ReaPy.get_reactor_source('yeten')).select('public.semantic_index_file',
                                                                         None,
                                                                         {
                                                                             0: {'col': 'source_folder',
                                                                                 'operator': '=',
                                                                                 'value': source_folder,
                                                                                 'combiner': 'AND'
                                                                                 },
                                                                             1: {'col': 'target_folder',
                                                                                 'operator': '=',
                                                                                 'value': target_folder,
                                                                                 'combiner': 'AND'
                                                                                 },
                                                                             2: {'col': 'file_name',
                                                                                 'operator': '=',
                                                                                 'value': file
                                                                                 }
                                                                         },
                                                                         'last_scan_time asc', None,
                                                                         True).data()
            if check_file is None:
                add_file = Model(ReaPy.get_reactor_source('yeten')). \
                    insert('public.semantic_index_file',
                           {
                               0: {
                                   'source_folder': source_folder,
                                   'target_folder': target_folder,
                                   'file_name': file,
                               }
                           }).data()
                if add_file > 0:
                    insert_successful_file.append(file)
                else:
                    insert_unsuccessful_file.append(file)
            else:
                if check_file[0]['upload_status'] == 0:
                    upload_pending_file_count += 1
                if check_file[0]['upload_status'] == 1:
                    upload_successful_file_count += 1
                if check_file[0]['upload_status'] == 2:
                    upload_unsuccessful_file_count += 1

                pre_scheduled_file.append(check_file)

        scheduled_file_count = len(insert_successful_file)
        unscheduled_file_count = len(insert_unsuccessful_file)
        pre_scheduled_file_count = len(pre_scheduled_file)
        self.message['payload'].clear()
        self.message['payload']['scan time'] = str(ReaPy.now())
        self.message['payload']['scanned_files_count'] = len(file_list)
        self.message['payload']['scheduled_file_count'] = scheduled_file_count
        self.message['payload']['unscheduled_file_count'] = unscheduled_file_count
        self.message['payload']['pre_scheduled_file_count'] = pre_scheduled_file_count
        self.message['payload']['upload_successful_file_count'] = upload_successful_file_count
        self.message['payload']['upload_unsuccessful_file_count'] = upload_unsuccessful_file_count
        self.message['payload']['upload_pending_file_count'] = upload_pending_file_count
        self.message['payload']['scheduled_files'] = insert_successful_file
        self.message['payload']['unscheduled_files'] = insert_unsuccessful_file
        self.message['payload']['pre_scheduled_files'] = pre_scheduled_file
        return ReaPy.rest_response(self.message, 200)
