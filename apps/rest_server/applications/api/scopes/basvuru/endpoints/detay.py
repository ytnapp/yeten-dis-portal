# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.dispaydas import Dispaydas
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Detay(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'payload': {}, 'success': False}

        try:

            detay = Dispaydas().get_detay_basvuru(self.iam_user_company_id)
            frontend_data['payload'] = detay
            frontend_data['success'] = True

        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data, 404)

    # Eski vergi noları sorgulama
    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)
        try:
            company_query_limit = self.master.session.get('company_query_limit')
            if company_query_limit is None:
                self.master.session['company_query_limit'] = 1

            if company_query_limit >= 5:
                return ReaPy.rest_response({'success': False, 'error': 'company_query_limit'}, 200)
            else:
                self.master.session['company_query_limit'] = company_query_limit + 1

        except Exception as e:
            print(e)
        frontend_data = {'success': False}

        vergi_numarasi = ''
        post_data = ReaPy.rest_request().get_json()
        try:
            vergi_numarasi = post_data['vergi_numarasi']

        except Exception as exc:
            print(exc)

        if vergi_numarasi == '':
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        frontend_data['payload'] = {}
        try:
            detay = Dispaydas().eski_vergi_no(vergi_numarasi)
            frontend_data['payload'] = detay
            frontend_data['success'] = True
        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)
