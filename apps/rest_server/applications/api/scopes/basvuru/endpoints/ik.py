# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.ik import Ik as IkModel
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Ik(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    # noinspection PyMethodMayBeStatic
    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-personel')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 403)

        frontend_data = {'success': False, 'payload': {}}

        try:
            frontend_data['payload'] = IkModel().get_one_ik(self.iam_user_company_id, self.iam_user_domain_account_name)
            frontend_data['success'] = True
        except Exception as exc:
            print("ik get hata")
            print(exc)

        if frontend_data['success'] == True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        basvuru_id = self.iam_user_company_id
        try:
            if basvuru_id is not None:
                if IkModel().basvuru_id_kontrol(basvuru_id, self.iam_user_domain_account_name,
                                                post_data['yetkili_kisi_tckn']) is not None:
                    # frontend_data['message'] = 'Daha önce IK kaydı oluşturulmuştur.'
                    err_data = self.master.messages['errors']['general_record_exist_err']
                    frontend_data['data'] = err_data['data']
                    return ReaPy.rest_response(frontend_data['data'], 406)
        except Exception as e:
            print(e)

        validate = ReaPy.validator().json_validate(self.master.schema['ik_insert_schema'], post_data)
        durum = None
        frontend_data['payload'] = {}
        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)
        try:
            durum = IkModel().ik_insert(post_data, self.iam_user_company_id, self.iam_user_domain_account_name,
                                        self.iam_user_ip_addr)
        except Exception as e:
            print(e)

        if durum is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 201)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def put(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        basvuru_id = self.iam_user_company_id

        try:

            if basvuru_id is not None:
                if IkModel().basvuru_id_kontrol(basvuru_id, self.iam_user_domain_account_name,
                                                post_data['yetkili_kisi_tckn']) is None:
                    err_data = self.master.messages['errors']['general_record_err_404']
                    frontend_data['data'] = err_data['data']
                    return ReaPy.rest_response(frontend_data['data'], 404)

            validate = ReaPy.validator().json_validate(self.master.schema['ik_insert_schema'], post_data)
            frontend_data['payload'] = {}
            if validate['success'] is False:
                print(validate['message'])
                err_data = self.master.messages['errors']['general_value_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 409)
            else:
                frontend_data['payload'] = IkModel().basvuru_update(post_data, basvuru_id,
                                                                    self.iam_user_domain_account_name,
                                                                    self.iam_user_ip_addr)
                frontend_data['success'] = True
                return ReaPy.rest_response(frontend_data, 200)
        except Exception as e:
            print(e)
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
