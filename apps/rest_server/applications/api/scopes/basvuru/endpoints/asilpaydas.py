# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.paydas import Paydas


class Asilpaydas(ReaPy().rest_resource()):

    # noinspection PyMethodMayBeStatic
    def get(self):
        ReaPy.set_request(ReaPy.rest_request())
        frontend_data = {'success': False}

        user_identity_no = ''
        try:
            if self.master.session['USER_IDENTITY'] is not None:
                user_identity_no = self.master.session['USER_IDENTITY'] # self.master.session.get('USER_IDENTITY')
        except Exception as exc:
            print(exc)

        if self.master.configurations['debug'] is not True:
            if user_identity_no == '':
                frontend_data['message'] = 'Giriş yapmalısınız'
                return ReaPy.rest_response(frontend_data, 401)
        else:
            user_identity_no = '11111111112'

        frontend_data['payload'] = {}
        try:
            frontend_data['payload'] = Paydas().get_all_record()
            frontend_data['success'] = True

        except Exception as exc:
            print("Paydas get hata")
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            return ReaPy.rest_response(frontend_data, 500)
