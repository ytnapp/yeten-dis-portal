# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.dispaydas import Dispaydas


class Ekle(ReaPy().rest_resource()):

    def post(self):
        ReaPy.set_request(ReaPy.rest_request())
        frontend_data = {}
        frontend_data['success'] = False

        post_data = ReaPy.rest_request().get_json()

        print(post_data)
        basvuru_id = Dispaydas().set_dis_basvuru(post_data, 'basvuru_id')
        vergi_numarasi = Dispaydas().set_dis_basvuru(post_data, 'vergi_numarasi')

        if len(basvuru_id) < 1:
            if len(vergi_numarasi) != 10:
                frontend_data['message'] = 'Vergi no bilgisini kontrol ediniz'
                return ReaPy.rest_response(frontend_data, 400)
            else:
                if Dispaydas().basvuru_vergi_no_kontrol(vergi_numarasi) != None:
                    frontend_data['message'] = 'Daha önce bu vergi no kayıt edilmiştir.'
                    return ReaPy.rest_response(frontend_data, 409)

        durum = Dispaydas().basvuru_insert(post_data)

        if durum is not None:
            frontend_data['success'] = True
            frontend_data['message'] = 'Başarılı'
            return ReaPy.rest_response(frontend_data, 201)
        else:
            return ReaPy.rest_response(frontend_data, 400)
