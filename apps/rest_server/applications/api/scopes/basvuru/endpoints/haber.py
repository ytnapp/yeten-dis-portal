# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Haber(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())

        frontend_data = {'success': True, 'payload': [
            {
                'id': 1,
                'tarih': '21.11.2019',
                'baslik': 'NATO İstihkâm Mükemmeliyet Merkezi "Endüstri Günü-2019" Faaliyeti',
                'gorsel': '/statics/images/tcgAnadolu.png'
            },
            {
                'id': 2,
                'tarih': '03.12.2019',
                'baslik': ' SSB ÜRÜN KÜTÜPHANESİ ÇALIŞMASI ',
                'gorsel': '/statics/images/akinci.png'
            },
            {
                'id': 3,
                'tarih': '02.12.2019',
                'baslik': ' International Future Soldier Conference ',
                'gorsel': '/statics/images/akinci.png'
            },
            {
                'id': 4,
                'tarih': '14.02.2018',
                'baslik': ' NATO Destek ve Tedarik Ajansı NSPA-Geleceğin Gözetleme ve Kontrol Programı',
                'gorsel': '/statics/images/akinci.png'
            }
        ]}

        return ReaPy.rest_response(frontend_data, 200)

    def post(self):
        ReaPy.set_request(ReaPy.rest_request())

        frontend_data = {'success': True}

        post_data = ReaPy.rest_request().get_json()
        print(post_data)

        duyuru_detay = {}
        if post_data['haber_id'] == 1:
            duyuru_detay = {
                'id': 1,
                'tarih': '21.11.2019',
                'baslik': 'NATO İstihkâm Mükemmeliyet Merkezi "Endüstri Günü-2019" Faaliyeti',
                'gorsel': '/statics/images/tcgAnadolu.png',
                'icerik': '''Sanayi Günü (Industry Day), savunma sanayii firmalarının uluslararası ortamda tanıtım yapabilmeleri maksadıyla; Almanya'da konuşlu bulunan NATO İstihkâm Mükemmeliyet Merkezi'nde (İMM) her yıl düzenli olarak icra edilmektedir.
                    "Industry Day-2019" faaliyeti 04 Aralık 2019 tarihinde Almanya İstihkâm Okulu ve Eğitim Merkezi Komutanlığında (Ingolstadt/Almanya) icra edilecektir.
                    Savunma sanayiimizin uluslararası ortamda tanıtımını yapmak, diğer ülkelerdeki üretim ve gelişmeler hakkında bilgi sahibi olmak maksadıyla; uluslararası ortamda icra edilecek olan Industry Day-2019'a katılım sağlamak isteyen savunma sanayii firmalarının 31 Ekim 2019 tarihine kadar mükemmeliyet merkezinin www.milengcoe.org resmi sitesindeki başvuru formunu doldurması gerekmektedir.'''
            }
        elif post_data['haber_id'] == 2:
            duyuru_detay = {
                'id': 2,
                'tarih': '03.12.2019',
                'baslik': ' SSB ÜRÜN KÜTÜPHANESİ ÇALIŞMASI ',
                'gorsel': '/statics/images/tcgAnadolu.png',
                'icerik': '''Ülkemizin savunma ve güvenliğine ilişkin ihtiyaçların azami oranda yerli ve milli imkanlarla karşılanması hedefi istikametinde, savunma sanayii firmalarımızın yerli tasarım/üretim kapsamında sahip olduğu yeteneklerin SSB tarafından yürütülmekte olan projelerde azami oranda kullanılmasını teminen Ürün Kütüphanesi oluşturma çalışması yürütülmektedir.

        Ürün Kütüphanesi’ne SSB tarafından yürütülecek olan tüm projelerin Sözleşmelerinde yer verilerek; projeleri yürüten tüm yüklenicilerin Sanayileşme Portaline üye olan firmaların ürün bilgileriyle buluşturulması ve yerli ürünlerin projelerde kullanımının azami oranda artırılmasına yönelik belirli yaptırım/teşviklerin uygulanması hedeflenmektedir.

        Bu itibarla, Ürün Kütüphanesi oluşturma çalışmaları kapsamında halihazırda kayıtlı olan ürün bilgileri kayıt formatı Ürün Kütüphanesinde kullanılmaya uygun olmadığından, tasarımını ve/veya üretimini gerçekleştirmiş olduğunuz kullanılabilir seviyedeki ürünlerinizin  bilgilerinin tarafınızca yeni formatta sağlanması talep edilmektedir.

        Ürünlerinizin projelerde yer alması için belirlenen yeni temel yöntemin Ürün Kütüphanesi olması sebebiyle ürün bilgilerinizi doğru ve anlaşılır şekilde girmeniz ürünlerinizin projelerde kullanım olasılığını en çok artıracak faktör olacaktır.

        Ancak bilgi kirliliğinin önüne geçebilmek, zamanı verimli kullanabilmek ve Ürün Kütüphanesinin etkinliğini artırmak amacıyla tarafınızca ürün kaydı gerçekleştirilirken;

                      Tasarım ve/veya üretim faaliyetleri firmanız tarafından yürütülen ürünlerin bilgilerine yer verilmesi,
                      Tasarım ve/veya üretim kabiliyetleri doğrultusunda yapılabileceklerin değil, yalnızca ortaya çıkmış ürünlerin (en az prototip seviyesinde olmak şartı ile) bilgilerine yer verilmesi,
                      Temsilcilik/Distribütörlük kapsamında tedarik edilmesi planlanan ürünlerin bilgilerine kesinlikle yer verilmemesi

        hususlarının göz önünde bulundurulması önem arz etmektedir.

        Ürün Kütüphanesi birkaç ayda bir güncellenecek olup, henüz ürüne dönmemiş çalışmaların ürün olarak tanımlanmaması,  ürün haline ulaşmasını müteakip kaydının gerçekleştirilmesi önem arz etmektedir. Yeni ürününüz, tarafınızca kaydedildikten sonra oluşturulacak ilk güncel Ürün Kütüphanesinde yer alacaktır.

        Kurum/kuruluşların Sanayileşme Portaline kayıtlı ürünleri için beyan edilen bilgilerin doğru olmadığının ve/veya distribütörlüğünün yapıldığının tespit edilmesi durumunda, ilgili ürünlere ÜK’de yer verilmeyecektir.



        Yukarıdaki hususlar gözetilmeden ve/veya gerçeğe aykırı beyanda bulunan firmaların Sanayileşme Portali üyelikleri ve/veya ürün bilgileri SSB’nin kararına bağlı olarak geçici veya kalıcı şekilde silinebilecektir.



        SSB tarafından bir ürünün silinmesi halinde, SSB tarafından uygun görülmeden, aynı kurum/kuruluş tarafından Sanayileşme Portaline tekrar kaydedildiği tespit edilirse ilgili kurum/kuruluşun Sanayileşme Portalindeki ürünleri bir yıl boyunca yayımlanacak ÜK’lerde yer almaz.



        Oluşturulacak olan ilk versiyonda yer alacak ürünlerinizin kaydının 15 Ocak 2020’a kadar tarafınızca yapılması gerekmektedir.
        '''
            }
        elif post_data['haber_id'] == 3:
            duyuru_detay = {
                'id': 3,
                'tarih': '02.12.2019',
                'baslik': ' International Future Soldier Conference ',
                'gorsel': '/statics/images/tcgAnadolu.png',
                'icerik': '''“International  Future Soldier Conference” etkinliği, MSB ve SSB desteği  ile 23- 24 Mart 2020  tarihlerinde Ankara Sheraton Otel’de SASAD, Defence Turkey ve ODTÜ Teknokent  tarafından organize edilecektir.  Bu kapsamda konferans “Future Soldier” konusunda çalışmaları bulunan ülkelerin yetkilileri, yabancı askeri delegasyon, bu alanda çalışma yapan global savunma ve teknoloji firmaları, Türk savunma sanayi ve askeri kurum ve kuruluşlarının yetkilileri ile buluşturacak ve taraflar arasında bilgi paylaşımı açısından optimum fayda sağlayan ve farkındalık yaratan bir platform olacaktır.

        Bu yıl konferansın ilk defa yapılacak olması nedeniyle genel amacın daha net ifade edilmesi açısından “Future soldier Konsepti ve Teknolojileri” alanında ortak sinerji ve işbirliğine yönelik olanakların” ortaya konmasını içeren genel başlıklar çevresinde belirlenmesi daha uygun görülmüş ve ana tema “The Warrior, Today and Tomorrow” olarak belirlenmiştir. 2 gün olarak organize edilen konferans, Tek- Er Modernizasyonu yürüten ülkelerin programları, politikaları, bu alandaki yeni teknolojileri içeren paneller ile global ve Türk firmaların ikili işbirliği görüşmelerini içerecektir. 

        Konferans aşağıdaki konuları kapsayacaktır:

        • Combat Clothing, Individual Equipment & Balistic Protection

        • Weapons, Sensors, Non Lethal Weapons, Ammunition

        • Power Solutions

        • Soft Target Protection

        • Soldier Physical, Mental and Cognitive Performance

        • Robotics and Autonomous Systems

        • Medical

        • C4ISTAR Systems

        • Exoskeleton Technology

        • CBRN

        • Logistics Capability



        Konferans hakkında detaylı bilgi için aşağıdaki web sitesine ulaşabilirsiniz.

        www.ifscturkey.com
        '''
            }
        elif post_data['haber_id'] == 4:
            duyuru_detay = {
                'id': 4,
                'tarih': '14.02.2018',
                'baslik': ' NATO Destek ve Tedarik Ajansı NSPA-Geleceğin Gözetleme ve Kontrol Programı',
                'gorsel': '/statics/images/tcgAnadolu.png',
                'icerik': '''  NATO Destek ve Tedarik Ajansı NSPA-Geleceğin Gözetleme ve Kontrol Programı AFSC Alliance Future Surveillance and Control  - NATO Destek ve Tedarik Ajansı NSPA-Geleceğin Gözetleme ve Kontrol Programı AFSC (Alliance Future Surveillance and Control) hakkında 10 Nisan 2018’de Lüksemburg’da bir Sanayi günü düzenlemeyi planlamaktadır.

        NSPA AFSC Endüstri Günü ile amaçlanan AFSC programı hakkında sanayinin farkındalığını artırmak, AFSC Concept Aşamasında firmalar için ne gibi iş fırsatları olabileceğine yönelik bilgi vermek ve özellikle programa katkı sağlama potansiyeli olan sanayi temsilcilerini belirlemektir.

        2016 Warşova Zirvesinde NATO’nun geleceğin gözetleme ve kontrol yeteneklerine yönelik seçenekler geliştirmek hedefiyle NATO AFSC programını başlatmıştır.

        Şubat 2017’de Kuzey Atlantik Konseyi AFSC Konsept Aşaması için NSPA’yı çalışmaları yürütmek ve teknik konseptlerin geliştirilmesinden sorumlu NATO ajansı olarak görevlendirmiştir.

        Konferansa katılım için davetiye gerekmektedir. Davetiye almak isteyen firmaların 22 Şubat tarihine kadar afsc_industry_day@nspa.nato.int  mail adresine davetiye alma taleplerini iletmeleri gerekmektedir.

        NSPA duyurusu: http://www.nspa.nato.int/en/organization/logistics/LogServ/afsc.htm'''
            }

        frontend_data['payload'] = {}
        frontend_data['payload'] = duyuru_detay

        return ReaPy.rest_response(frontend_data, 200)
