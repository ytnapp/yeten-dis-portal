# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.tesis import Tesis as Model
from apps.rest_server.models.helpers import Helpers
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Tesis(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False, 'payload': {}}

        try:
            dashboard_data = Helpers().payload_parameters_control(
                Model().get_tesis(self.iam_user_company_id, self.iam_user_domain_account_name))
            frontend_data['payload'] = dashboard_data
            frontend_data['success'] = True
        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

    # noinspection PyMethodMayBeStatic
    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        if post_data['tesisler'][0]['ana_tesis'] == 0:
            var_mi = Model().get_check_for_ana_tesis(self.iam_user_company_id)
            if var_mi > 0:
                err_data = self.master.messages['errors']['tesis_main_record_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 409)
        drm = ''
        try:
            drm = Model().ekle_tesis_bilgileri(post_data['tesisler'],
                                               self.iam_user_company_id,
                                               self.iam_user_domain_account_name, self.iam_user_ip_addr)
        except Exception as exc:
            print(exc)

        if drm is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 201)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def put(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        post_data = ReaPy.rest_request().get_json()

        paydas_id = self.iam_user_company_id
        tesis_id = ''
        frontend_data = {'success': False}
        try:
            tesis_id = post_data['tesis_id']
        except KeyError as keyExc:
            print(keyExc)
            frontend_data = {'success': False}

        if post_data['ana_tesis'] == 0:
            var_mi = Model().get_check_for_ana_tesis_update(self.iam_user_company_id, tesis_id)
            if var_mi > 0:
                var_mi_ki = Model().get_check_for_ana_tesis(self.iam_user_company_id)
                if var_mi_ki > 0:
                    err_data = self.master.messages['errors']['tesis_main_record_err']
                    frontend_data['data'] = err_data['data']
                    return ReaPy.rest_response(frontend_data['data'], 409)

        durum = Model().tesis_update(post_data, paydas_id, tesis_id, self.iam_user_domain_account_name,
                                     self.iam_user_ip_addr)
        if durum is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def delete(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)
        tesis_id = ''

        frontend_data = {'success': False}

        try:
            tesis_id = ReaPy.rest_request().args['tesis_id']
        except Exception as exc:
            print(exc)

        if tesis_id == '':
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        var_mi = Model().get_check_tesis_for_delete(self.iam_user_company_id, tesis_id)
        if var_mi > 0:
            err_data = self.master.messages['errors']['tesis_delete_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        durum = Model().delete_tesis(self.iam_user_company_id, tesis_id)

        if durum is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
