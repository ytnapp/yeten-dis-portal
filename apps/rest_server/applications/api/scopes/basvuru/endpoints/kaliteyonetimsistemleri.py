# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.kaliteyonetimsistemi import Kaliteyonetimsistemi as Kaliteyonetimsistemi
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Kaliteyonetimsistemleri(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    # noinspection PyMethodMayBeStatic
    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)
        
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-personel')
        if auth is False:
            return ReaPy.rest_response(self.master.messages['errors']['user_sn_err_100'], 404)

        frontend_data = {'success': False, 'payload': {}}

        firmaadi = ReaPy.rest_request().args['firma']
        firmatype = ReaPy.rest_request().args['type']
        try:
            frontend_data['payload'] = Kaliteyonetimsistemi().get()
            frontend_data['success'] = True
        except Exception as exc:
            print("hata")
            print(exc)

        if frontend_data['success'] == True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    
