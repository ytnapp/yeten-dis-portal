# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.paydas import Paydas as PaydasModel
from apps.rest_server.models.api.dispaydas import Dispaydas
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Paydas(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    # noinspection PyMethodMayBeStatic
    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'payload': {}, 'success': False, 'code': True}

        try:
            if self.iam_user_company_id is None:
                detay = Dispaydas().get_detay_basvuru_tc(self.iam_user_domain_account_name)
            else:
                detay = Dispaydas().get_detay_basvuru(self.iam_user_company_id)
            frontend_data['payload'] = detay
            frontend_data['success'] = True
            frontend_data['code'] = True
        except Exception as exc:
            print(exc)

        if 'basvuru_durum' in frontend_data['payload'] and frontend_data['payload'][
            'basvuru_durum'] == 2 and self.iam_user_company_id is None:
            frontend_data['payload'] = None
            frontend_data['code'] = False
        elif 'basvuru_durum' in frontend_data['payload'] and frontend_data['payload'][
            'basvuru_durum'] == 2:
            sozlesme = PaydasModel().get_detay(self.iam_user_company_id)
            if sozlesme is not None:
                frontend_data['payload'].update(
                    {'paydas_urun_sozlesme_okudu_mu': sozlesme['paydas_urun_sozlesme_okudu_mu'],
                     'paydas_sozlesme_okudu_mu': sozlesme['paydas_sozlesme_okudu_mu']})

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 404)

    # noinspection PyMethodMayBeStatic
    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['basvuru_insert_schema'], post_data)

        frontend_data['payload'] = {}
        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        durum = None

        if len(str(post_data['vergi_tc_no'])) == 10:
            if Dispaydas().basvuru_vergi_no_kontrol(str(post_data['vergi_tc_no'])) != None:
                err_data = self.master.messages['paydas']['vergi_no_kayitli']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 409)
            else:
                post_data['olusturan_ip'] = self.iam_user_ip_addr
                if Dispaydas().user_domain_account_is_exist(self.iam_user_domain_account_name):
                    durum = Dispaydas().basvuru_insert(post_data, self.iam_user_domain_account_name)
                else:
                    err_data = self.master.messages['paydas']['tc_no_kayitli']
                    frontend_data['data'] = err_data['data']
                    return ReaPy.rest_response(frontend_data['data'], 409)

        if durum is not None:
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 201)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    # noinspection PyMethodMayBeStatic
    def put(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}

        basvuru_id = self.iam_user_company_id

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['basvuru_update_schema'], post_data)

        frontend_data['payload'] = {}
        if validate['success'] is False:
            print(validate['message'])
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        if basvuru_id is None:
            try:
                basvuru_id = post_data['basvuru_id']
            except Exception as exc:
                print(exc)

        try:
            if len(str(post_data['vergi_tc_no'])) != 10:
                print('Vergi no bilgisini kontrol ediniz')
                err_data = self.master.messages['errors']['general_value_err']
                frontend_data['data'] = err_data['data']
                return ReaPy.rest_response(frontend_data['data'], 400)

            basvuru_durum = Dispaydas().basvuru_id_drum_kontrol(basvuru_id, self.iam_user_domain_account_name)

            post_data['guncelleyen_ip'] = ReaPy.rest_request().remote_addr

            if basvuru_durum != None:

                if basvuru_durum == 2:
                    PaydasModel().update_paydas(post_data, basvuru_id, self.iam_user_domain_account_name)
                    frontend_data['success'] = True
                    return ReaPy.rest_response(frontend_data, 200)

                elif basvuru_durum == 1:
                    print('Başvurunuz işleme alındığından düzenleme yapamazsınız.')
                    err_data = self.master.messages['errors']['paydas']['basvuru_isleme_alinmistir']
                    frontend_data['data'] = err_data['data']
                    return ReaPy.rest_response(frontend_data['data'], 409)

            basvuru_ids = Dispaydas.get_basvuru_id(self.iam_user_domain_account_name)
            basvuru_id = str(basvuru_ids[0]['basvuru_id'])

            Dispaydas().basvuru_update(post_data, basvuru_id, self.iam_user_domain_account_name)
            frontend_data['success'] = True
            frontend_data['basvuru_id'] = basvuru_id

            return ReaPy.rest_response(frontend_data, 200)
        except Exception as e:
            print(e)
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
