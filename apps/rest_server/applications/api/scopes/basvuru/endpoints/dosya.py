# -*- coding: utf-8 -*-
import os
import base64

from core.reapy import ReaPy
from datetime import datetime
from apps.rest_server.models.api.dispaydas import Dispaydas
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Dosya(ReaPy().rest_resource()):
    save_disk_location = '/nfs-share/'
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no
    
    def __init__(self):
        super(ReaPy().rest_resource(), self).__init__()
        if ReaPy.platform().system() == "Linux":
            self.save_disk_location = ReaPy.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_linux'] + '/'
        else:
            self.save_disk_location = ReaPy.configuration().get_configuration()['system']['rest_server'][
                'save_disk_location_windows']

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}
        file_types = {}
        try:
            file_type = ReaPy.rest_request().args['type']
            if file_type == 'urun-katalog':
                file_types = {'pdf'}
            elif file_type == 'urun-fotograf':
                file_types = {'png', 'jpg', 'jpeg'}
            else:
                file_types = {'docx', 'pdf', 'png', 'jpg', 'jpeg', 'doc', 'xls', 'xlsx'}
        except Exception as e:
            print(e)
        now = datetime.now()

        if 'file' not in ReaPy.rest_request().files:
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 400)
        file = ReaPy.rest_request().files['file']
        if file.filename == '':
            print('No selected file')
            err_data = self.master.messages['errors']['general_record_err_404']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data, 400)
        try:
            if file and self.allowed_file(file.filename, file_types):

                file_hash = str(ReaPy.hash().get_uuid())
                file_name = ReaPy.secure_filename(file.filename)

                full_path = self.save_disk_location + now.strftime("%d-%m-%Y")

                dosya_tam_yolu = full_path + '/' + file_hash + '_' + file_name

                if os.path.isdir(full_path) is False:
                    try:
                        os.mkdir(full_path)
                    except OSError as exc:
                        print("Creation of the directory %s failed" % full_path)
                        print(exc)
                    else:
                        print("Successfully created the directory %s " % full_path)

                file.save(dosya_tam_yolu)

                self.__send_file(full_path + '/', file_hash + '_' + file_name)

                print(dosya_tam_yolu)
                data = Dispaydas().file_name_save(file_name, dosya_tam_yolu, self.iam_user_company_id)
                frontend_data['success'] = True
                frontend_data['payload'] = {}
                frontend_data['payload'] = data
                return ReaPy.rest_response(frontend_data, 200)
            err_data = self.master.messages['errors']['general_file_err_400']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data, 400)
        except Exception as exc:
            print(exc)

    def allowed_file(self, filename, file_types):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in file_types

    def __send_file(self, file_path, file_name):

        file_full_path_and_name = file_path + file_name

        print(file_full_path_and_name)
        encoded_string = None
        try:
            with open(file_full_path_and_name, "rb") as file_content:
                encoded_string = base64.b64encode(file_content.read())

            if encoded_string is not None:
                mediation = ReaPy.configuration().get_configuration()['system']['mediation_server']
                url = 'http://' + mediation['host'] + ':' + str(mediation['port']) + '/source/db/file'
                print(url)

                # send_data = '''{"file_name":"'''+file_name+'''","file_path":"'''+file_path+'''""file_content":"'''+encoded_string.decode("utf-8")+'''"}'''
                send_data = {
                    "file_name": file_name,
                    "file_path": file_path,
                    "file_content": encoded_string.decode("utf-8")
                }

                # request = ReaPy.requests().put(url, data=ReaPy.json().dumps(send_data))
                request = ReaPy.requests().put(url, json=ReaPy.json().dumps(send_data))

                print(request)

        except Exception as exc:
            print('__send_file hata : ', __file__)
            print(exc)

    def delete(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}
        try:
            post_data = ReaPy.rest_request().get_json()
            dosya_id = post_data['dosya_id']
            delete_dosya = Dispaydas.delete_dosya(dosya_id)
            if delete_dosya:
                frontend_data['success'] = True
                return ReaPy.rest_response(frontend_data, 200)
        except Exception as e:
            print(e)
            return ReaPy.rest_response(frontend_data, 400)
