# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.applications.base_endpoint import BaseEndpoint
from apps.rest_server.models.api.ualtyapi.uamodel import Uamodel


class Modul(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        # check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        # if type(check) is dict:
        #     return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        frontend_data = {'success': True}

        frontend_data['payload'] = {}

        try:
            data = Uamodel().get_uretim_alt_yapi(self.iam_user_domain_account_name)
            frontend_data['payload'] = data
        except Exception as exc:
            print(exc)
            frontend_data['success'] = False
            frontend_data['message'] = 'Kayıt bulunamadı'

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            return ReaPy.rest_response(frontend_data, 404)

    # noinspection PyMethodMayBeStatic
    def post(self):
        # check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        # if type(check) is dict:
        #     return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['uretim_alt_yapi_insert_schema'], post_data)

        frontend_data['payload'] = {}
        if validate['success'] is False:
            # TODO düzgün bi validate message
            frontend_data['payload'] = validate['message']
            frontend_data['message'] = 'Bilgi alanlarında hata bulunmaktadır.'
            return ReaPy.rest_response(frontend_data, 400)

        durum = Uamodel().uretim_alt_yapi_insert(post_data, self.iam_user_domain_account_name)

        if durum is not None:
            frontend_data['message'] = 'Başarılı'
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 201)
        else:
            frontend_data['success'] = False
            return ReaPy.rest_response(frontend_data, 400)

    # noinspection PyMethodMayBeStatic
    def put(self):
        # check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        # if type(check) is dict:
        #     return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        validate = ReaPy.validator().json_validate(self.master.schema['uretim_alt_yapi_insert_schema'], post_data)

        frontend_data['payload'] = {}
        if validate['success'] is False:
            # TODO düzgün bi validate message
            frontend_data['payload'] = validate['message']
            frontend_data['message'] = 'Bilgi alanlarında hata bulunmaktadır.'
            return ReaPy.rest_response(frontend_data, 400)

        durum = Uamodel().update_uretim_altyapi(post_data, self.iam_user_domain_account_name)

        if durum is not None:
            frontend_data['message'] = 'Başarılı'
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            frontend_data['success'] = False
            return ReaPy.rest_response(frontend_data, 400)

    def delete(self):
        # check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        # if type(check) is dict:
        #     return ReaPy.rest_response(check['data'], 403)

        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        frontend_data = {'success': False}

        post_data = ReaPy.rest_request().get_json()

        bilesen_id = ''
        try:
            bilesen_id = post_data['uretim_altyapi_id']
        except Exception as exc:
            print(exc)

        frontend_data['payload'] = {}
        if bilesen_id == '':
            # TODO düzgün bi validate message
            frontend_data['message'] = 'Bilgi alanlarında hata bulunmaktadır.'
            return ReaPy.rest_response(frontend_data, 400)

        durum = Uamodel().delete_ua(bilesen_id)

        if durum is not None:
            frontend_data['message'] = 'Başarılı'
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            frontend_data['success'] = False
            return ReaPy.rest_response(frontend_data, 400)
