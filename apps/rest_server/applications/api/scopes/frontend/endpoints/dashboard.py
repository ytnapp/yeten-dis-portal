# -*- coding: utf-8 -*-
from apps.rest_server.applications.base_endpoint import BaseEndpoint
from core.reapy import ReaPy


class Dashboard(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        is_admin = False
        main_auths = {
            "yeten-dis-portal-personel": {
                "icon": "icon-personel",
                "text": "personel",
                "num": 0
            },
            "yeten-dis-portal-test": {
                "icon": "icon-test",
                "text": "test",
                "num": 0
            },
            "yeten-dis-portal-altyapi": {
                "icon": "icon-altyapi",
                "text": "altyapı",
                "num": 0
            },
            "yeten-dis-portal-urun": {
                "icon": "icon-urun",
                "text": "ürün",
                "num": 0
            },
            "yeten-dis-portal-finans": {
                "icon": "icon-finans",
                "text": "finans",
                "num": 0
            }
        }
        dashboard_auths = {
            "yeten-dis-portal-personel": {
                "icon": "icon-personel",
                "text": "İnsan Kaynakları",
                "num": 0
            },
            "yeten-dis-portal-test": {
                "icon": "icon-test",
                "text": "Test bilgisi",
                "num": 0
            },
            "yeten-dis-portal-altyapi": {
                "icon": "icon-altyapi",
                "text": "Altyapı bilgisi",
                "num": 0
            },
            "yeten-dis-portal-urun": {
                "icon": "icon-urun",
                "text": "Ürünler / Kırılımlar",
                "num": 0
            },
            "yeten-dis-portal-finans": {
                "icon": "icon-finans",
                "text": "Finansal Bilgiler",
                "num": 0
            }
        }

        side_bar_icons = [{
            "icon": "icon-genel",
            "text": "genel",
            "num": 0
        }]

        dashboard_block_box = [{
            "icon": "icon-genel",
            "text": "Genel bilgiler",
            "num": 0
        }]
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)
        if self.master.session.get('APPLICATION_AUTHORIZATIONS') is not None:
            auth = self.master.session.get('APPLICATION_AUTHORIZATIONS')
            new_auth = []
            try:
                for a_key in auth:
                    if a_key['code'] == 'yeten-dis-portal-admin':
                        is_admin = True
                    else:
                        is_admin = False
                    if a_key['code'] not in new_auth:
                        new_auth.append(a_key['code'])
                for key in new_auth:
                    side_bar_icons.append(main_auths[key])
                    dashboard_block_box.append(dashboard_auths[key])
            except Exception as e:
                print(e)

        frontend_data = {'success': True, 'payload': {
            "dashboard_bar": [
                {
                    "title": "YETEN Sırası",
                    "num": 0,
                    "description": "Potansiyel Sıra: 17"
                },
                {
                    "title": "Bilgi Giriş Oranı",
                    "num": 0,
                    "description": "Sektör Ortalaması: %90"
                },
                {
                    "title": "Bilgi Doğrulama Oranı",
                    "num": 0,
                    "description": "EYDEP Denetimi Talep Et"
                },
                {
                    "title": "Yerlilik oranı",
                    "num": 0,
                    "description": "Potansiyel Oran: 93 "
                }
            ],
            "dashboard_block_eydep": [
                {
                    "icon": "fa-hourglass-half",
                    "text": "Bekleyen",
                    "num": 0
                },
                {
                    "icon": "fa-user-check",
                    "text": "Ziyaret Edilen",
                    "num": 0
                },
                {
                    "icon": "fa-check",
                    "text": "Onaylanan",
                    "num": 0
                },
            ], "is_admin": is_admin

        }
                         }

        frontend_data['payload']['sidebar'] = side_bar_icons
        frontend_data['payload']['dashboard_block_box'] = dashboard_block_box

        return ReaPy.rest_response(frontend_data, 200)
