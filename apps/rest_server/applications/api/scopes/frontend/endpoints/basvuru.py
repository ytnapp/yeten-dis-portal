# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy
from apps.rest_server.models.api.frontend import Frontend
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Basvuru(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False, 'payload': {}}

        try:
            sehirler = Helpers().payload_parameters_control(Frontend().get_tanim_sehir())
            meslek = Helpers().payload_parameters_control(Frontend().get_tanim_meslek_odasi())
            vergi_daire = Helpers().payload_parameters_control(Frontend().get_vergi_daire())
            # naceler = Frontend().get_tanim_nace()
            frontend_data = {
                'success': True,
                'payload': {
                    'sehir': sehirler,
                    'meslek_odasi': meslek,
                    'vergi_daire': vergi_daire,
                    'nace': None
                }}
        except Exception as exc:
            print(exc)

        if frontend_data['success']:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
