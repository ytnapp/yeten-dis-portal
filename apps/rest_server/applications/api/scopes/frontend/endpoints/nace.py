# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy
from apps.rest_server.models.api.frontend import Frontend
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Nace(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False, 'payload': {}}

        bilgi = ''
        try:
            bilgi = ReaPy.rest_request().args['bilgi']
        except Exception as exc:
            print(exc)

        if bilgi == '':
            err_data = self.master.messages['errors']['general_value_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 409)

        try:
            frontend_data = {'success': True,
                             'payload': Helpers().payload_parameters_control(Frontend().get_like_nace(bilgi))}
        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)

    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        post_data = ReaPy.rest_request().get_json()
        nace_ids = None

        try:
            nace_ids = post_data['nace_kod']
        except Exception as exc:
            print(exc)

        frontend_data = {'success': False, 'payload': {}}
        try:
            if nace_ids is not None:
                print("/***")
                frontend_data = {'payload': Frontend().get_nace_uuid(nace_ids), 'success': True}
            else:
                frontend_data = {'payload': [], 'success': True}

        except Exception as exc:
            print(exc)

        if frontend_data['success'] is True:
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
