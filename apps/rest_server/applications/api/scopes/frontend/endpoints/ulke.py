# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.frontend import Frontend


class Ulke(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())

        frontend_data = {'success': True}
        frontend_data['payload'] = {}

        frontend_data['payload'] = Frontend().get_ulke()

        return ReaPy.rest_response(frontend_data)
