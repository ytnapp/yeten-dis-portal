# -*- coding: utf-8 -*-
from apps.rest_server.applications.base_endpoint import BaseEndpoint
from core.reapy import ReaPy
from apps.rest_server.models.api.frontend import Frontend


class Ilce(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        frontend_data = {'success': False}
        post_data = ReaPy.rest_request().get_json()
        sehir_id = ''
        try:
            sehir_id = post_data['il_id']

        except Exception as exc:
            print(exc)

        if sehir_id == '' or sehir_id is None:
            print('ilce bos')
            # TODO front end null istek yapıyor düzeltilecek
            # err_data = self.master.messages['errors']['general_value_err']
            # frontend_data['data'] = err_data['data']
            # return ReaPy.rest_response(frontend_data['data'], 409)

        frontend_data['success'] = True
        frontend_data['payload'] = {}
        frontend_data['payload'] = Frontend().get_ilce(sehir_id)

        return ReaPy.rest_response(frontend_data)
