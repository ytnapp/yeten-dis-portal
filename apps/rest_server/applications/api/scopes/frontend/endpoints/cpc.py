# -*- coding: utf-8 -*-
from apps.rest_server.applications.base_endpoint import BaseEndpoint
from core.reapy import ReaPy
from apps.rest_server.models.api.cpc import Cpc as Model



class Cpc(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        post_data = ReaPy.rest_request().get_json()
        search_key = post_data.get('search_key')
        if search_key == 0:
            frontend_data = {'success': False,
                             'payload': self.__set_function__('get_cpc_data')}
            return self.__set_validate(frontend_data)
        else:
            frontend_data = {'success': False,
                             'payload': self.__set_function__('get_cpc_search')}

            return self.__set_validate(frontend_data)

    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)
        try:
            post_data = ReaPy.rest_request().get_json()
            search_key = post_data.get('search_key')
            if search_key == 0:
                frontend_data = {'success': False,
                                 'payload': self.__set_function__('get_cpc_data')}
                return self.__set_validate(frontend_data)
            else:
                frontend_data = {'success': False,
                                 'payload': self.__set_function__('get_cpc_search')}
                return self.__set_validate(frontend_data)
        except Exception as e:
            print(e)
            err_data = self.master.messages['errors']['general_server_err']
            return ReaPy.rest_response(err_data['data'], 500)

    @staticmethod
    def __set_function__(f_name):
        try:
            post_data = ReaPy.rest_request().get_json()
            root = post_data['key']
            level = post_data['seviye']
            if root is None or root == "":
                root = 'root'
                level = 1
            search_key = post_data.get('search_key')
            if f_name == 'get_cpc_data':
                return Model('gS82DaIE29AjubgpeLJujS0GRCnOHWqE', '5XeurSEjjbDLnHze').get_cpc_data(root, level + 1)
            elif f_name == 'get_cpc_search':
                return Model('gS82DaIE29AjubgpeLJujS0GRCnOHWqE', '5XeurSEjjbDLnHze').get_cpc_search(
                    search_key)
        except Exception as e:
            print(e)
            return 'err'

    @staticmethod
    def __set_validate(frontend_data):
        if frontend_data['payload'] != {}:
            frontend_data.update({'success': True})
            return ReaPy.rest_response(frontend_data, 200)
        else:
            frontend_data['error'] = "somethings wrong"
            return ReaPy.rest_response(frontend_data, 404)
