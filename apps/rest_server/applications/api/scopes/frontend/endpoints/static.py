# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.rest_server.models.api.frontend import Frontend
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Static(ReaPy().rest_resource()):

    def get(self):

        frontend_data = {'success': False}

        kolon = ''
        try:
            kolon = ReaPy.rest_request().args['kolon']

        except Exception as exc:
            print(exc)

        if kolon == '':
            frontend_data['message'] = 'Sorgulama deger hatası'
            return ReaPy.rest_response(frontend_data, 409)
        else:
            if kolon == 'iletisim':
                kolon = 'iletisim_sayfasi'
            elif kolon == 'amacimiz':
                kolon = 'amacimiz_sayfasi'
            elif kolon == 'sanayilesme':
                kolon = 'sanayilesme_sayfasi'
            elif kolon == 'tesvik_veren_kurumlar':
                kolon = 'tesvik_veren_kurumlar_sayfasi'
            elif kolon == 'patent_portfoy':
                kolon = 'patent_portfoy_sayfasi'
            elif kolon == 'faydali_linkler':
                kolon = 'faydali_linkler_sayfasi'
            else:
                frontend_data['message'] = 'Sorgulama deger hatası.'
                return ReaPy.rest_response(frontend_data, 409)
        detay = None
        try:
            detay = Frontend().static_get(kolon)
        except Exception as exc:
            print(exc)
            frontend_data['message'] = 'Veri getirme sırasında bir hata oluştu'

        if detay is not None:
            frontend_data['payload'] = detay
            frontend_data['success'] = True
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            return ReaPy.rest_response(err_data['data'], 500)
