# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.dispaydas import Dispaydas


class Sehir(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):
        debug_data = {
            'iam_user_domain_account_name': '11111111112',
            'USER_NAME_SURNAME': 'UMUTCN KARACA',
            'iam_user_company_id': '7beff594-bc44-4c8f-8bac-71dca7677355'
        }
        print(self.master.session.get('USER_IDENTITY'))
        print(self.master.configurations['debug'])
        try:
            if self.master.configurations['debug'] is True:
                self.iam_user_company_id = debug_data['iam_user_company_id']
                self.iam_user_name_surname = debug_data['USER_NAME_SURNAME']
                self.iam_user_domain_account_name = debug_data['iam_user_domain_account_name']
            else:

                iam_user = ReaPy.control_session(self.master.session.get('IAM-User'))

                if self.master.session.get('USER_IDENTITY') is None and iam_user is None:
                    return self.master.messages['errors']['user_sn_err_101']

                self.iam_user_name_surname = self.master.session['USER_NAME_SURNAME']

                if iam_user is not None:
                    if 'iam_user_company_id' in iam_user:
                        self.iam_user_company_id = iam_user['iam_user_company_id']

                    if 'iam_user_domain_account_name' in iam_user:
                        self.iam_user_domain_account_name = iam_user['iam_user_domain_account_name']
                else:
                    self.iam_user_domain_account_name = self.master.session.get('USER_IDENTITY')

            self.iam_user_ip_addr = request.remote_addr
            return True
        except Exception as exc:
            print(exc)
            return self.master.messages['errors']['user_sn_err_102']

    def post(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        post_data = ReaPy.rest_request().get_json()

        ulke_id = ''
        try:
            ulke_id = post_data['ulke']

        except Exception as exc:
            print(exc)
        frontend_data = {'success': True, 'payload': Dispaydas().get_sehir(ulke_id)}

        return ReaPy.rest_response(frontend_data)
