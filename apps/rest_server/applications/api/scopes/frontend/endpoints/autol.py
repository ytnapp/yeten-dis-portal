# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Autol(ReaPy().rest_resource()):

    def get(self):
        self.master.session['USER_IDENTITY'] = ReaPy.rest_request().args['paydas_tckn']
        self.master.session['USER_NAME_SURNAME'] = ReaPy.rest_request().args['isim_soyisim']

        return True