# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.dispaydas import Dispaydas


class Anasayfa(ReaPy().rest_resource()):

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())

        frontend_data = {'success': True, 'payload': {
            "anasayfa-sayilar": [
                {
                    "icon": "icon-network",
                    "text": "Kayıtlı Paydaş",
                    "num": 0
                },
                {
                    "icon": "icon-checklist",
                    "text": "Ürün",
                    "num": 0
                },
                {
                    "icon": "icon-patent",
                    "text": "Patent",
                    "num": 0
                },
                {
                    "icon": "icon-human-resources",
                    "text": "İnsan Kaynakları",
                    "num": 0
                }
            ],
            "anasayfa-haberler": [
                {
                    "id": 1,
                    "slug": "tcg-anadolu-cok-maksatl-amfibi-hucum-gemisi",
                    "title": "TCG Anadolu Çok Maksatlı Amfibi Hücum Gemisi",
                    "image": "/images/akinci.png",
                    "summary_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                },
                {
                    "id": 2,
                    "slug": "deneme-haber",
                    "title": "Deneme Haber",
                    "image": "/images/tcgAnadolu.png",
                    "summary_text": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
                }
            ]
        }}

        return ReaPy.rest_response(frontend_data)
