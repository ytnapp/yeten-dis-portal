# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy
from apps.rest_server.models.api.test_alt_yapi.tabilesen import Tabilesen
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class Testaltyapi(ReaPy().rest_resource()):
    iam_user_ip_addr = None
    iam_user_company_id = None
    iam_user_name_surname = None
    iam_user_domain_account_name = None  # user_identity_no

    def __check(self, request):

        check_data = BaseEndpoint().check_session(self, request)

        if type(check_data) is tuple:
            self.iam_user_ip_addr = check_data[0]
            self.iam_user_company_id = check_data[1]
            self.iam_user_name_surname = check_data[2]
            self.iam_user_domain_account_name = check_data[3]
            return True
        else:
            return check_data

    def get(self):
        check = self.__check(ReaPy.set_request(ReaPy.rest_request()))
        if type(check) is dict:
            return ReaPy.rest_response(check['data'], 403)

        marka_model = None
        kriter_secim = None
        test_alt_yapi = None
        ana_basligi = None
        standart_secim = None
        metod_secim = None
        bilesen_turu = None

        frontend_data = {'success': False}
        try:
            marka_model = Helpers().payload_parameters_control(Tabilesen().get_marka_model())
            kriter_secim = Tabilesen().get_test_kriter_secim()
            test_alt_yapi = Tabilesen().get_test_altyapi_secim()
            ana_basligi = Tabilesen().get_test_ana_basligi_secim()
            standart_secim = Tabilesen().get_test_standart_secim()
            # metod_secim = Tabilesen().get_test_metod_secim()
            bilesen_turu = Tabilesen().get_bilesen_turu()
            frontend_data = {'success': True}
        except Exception as exc:
            print(exc)

        if marka_model is None:
            marka_model = []
        if kriter_secim is None:
            kriter_secim = []
        if test_alt_yapi is None:
            test_alt_yapi = []
        if ana_basligi is None:
            ana_basligi = []
        if metod_secim is None:
            metod_secim = []
        if standart_secim is None:
            standart_secim = []
        if bilesen_turu is None:
            bilesen_turu = []

        frontend_data['payload'] = {}

        if frontend_data['success'] is True:
            frontend_data['payload']['marka_model'] = marka_model
            frontend_data['payload']['ana_basligi'] = ana_basligi
            frontend_data['payload']['test_alt_yapi'] = test_alt_yapi
            frontend_data['payload']['kriter_secim'] = kriter_secim
            frontend_data['payload']['standart_secim'] = standart_secim
            frontend_data['payload']['metod_secim'] = metod_secim
            frontend_data['payload']['bilesen_turu'] = bilesen_turu
            return ReaPy.rest_response(frontend_data, 200)
        else:
            err_data = self.master.messages['errors']['general_server_err']
            frontend_data['data'] = err_data['data']
            return ReaPy.rest_response(frontend_data['data'], 500)
