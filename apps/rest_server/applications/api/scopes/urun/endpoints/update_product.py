# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun.product import Product


class UpdateProduct(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None
    product_id = None

    def put(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)


        validate = ReaPy.validator().json_validate(self.master.schema['update_product'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.product_id = request.json['product_id']
        self.remote_addr = request.remote_addr
        product_type = request.json['product_type']
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def software_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['update_software_product'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        product = Product().check_software_product_by_id(self.iam_user_company_id, self.product_id)
        if product is None:
            message = self.master.messages['errors']['product_nf_err_102']
            return ReaPy.rest_response(message['data'], 400)

        update_software_products = Product().update_software_product(self.product_id, self.iam_user_company_id,
                                                                   self.iam_user_domain_account_name, self.remote_addr,
                                                                   self.request_data)

        if update_software_products is True:
            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'urun_id': self.product_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)
        else:
            message = self.master.messages['errors']['product_us_err_103']
            return ReaPy.rest_response(message['data'], 400)

    def normal_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['update_normal_product'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        product = Product().check_normal_product_by_id(self.iam_user_company_id, self.product_id)
        if product is None:
            message = self.master.messages['errors']['product_nf_err_102']
            return ReaPy.rest_response(message['data'], 400)

        update_normal_products = Product().update_normal_product(self.product_id, self.iam_user_company_id,
                                                               self.iam_user_domain_account_name, self.remote_addr,
                                                               self.request_data)

        if update_normal_products is True:
            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'urun_id': self.product_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)
        else:
            message = self.master.messages['errors']['product_as_err_100']
            return ReaPy.rest_response(message['data'], 400)
