# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun.product import Product
from apps.rest_server.models.api.urun.element_values import ElementValues


class GetProduct(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    product_id = None
    stakeholder = None
    stakeholder_code = None
    stakeholder_product_code = None

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_delete_elements'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.product_id = request.json['product_id']
        self.stakeholder = ElementValues.get_stakeholder(self.iam_user_company_id)
        self.stakeholder_code = str(self.stakeholder[0]['paydas_kodu']).zfill(4)
        product_type = request.json['product_type']
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def software_product(self):
        product = Product().check_software_product_by_id(self.iam_user_company_id, self.product_id)
        if product is None:
            message = self.master.messages['errors']['product_nf_err_102']
            return ReaPy.rest_response(message['data'], 400)

        software_products = Product().get_software_product_by_id(self.product_id)
        message = self.master.messages['success']['success_temp']
        software_products[0].update({'urun_numarasi': self.stakeholder_code + software_products[0]['urun_numarasi']})
        message['payload'] = {}
        message['payload'].clear()
        message['payload'] = software_products
        return ReaPy.rest_response(message)

    def normal_product(self):
        product = Product().check_normal_product_by_id(self.iam_user_company_id, self.product_id)
        if product is None:
            message = self.master.messages['errors']['product_nf_err_102']
            return ReaPy.rest_response(message['data'], 400)

        normal_products = Product().get_normal_product_by_id(self.product_id)
        normal_products[0].update({'urun_numarasi': self.stakeholder_code + normal_products[0]['urun_numarasi']})
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        message['payload'] = normal_products
        return ReaPy.rest_response(message)
