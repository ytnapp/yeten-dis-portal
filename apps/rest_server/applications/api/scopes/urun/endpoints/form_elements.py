# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun.element_values import ElementValues


class FormElements(ReaPy().rest_resource()):
    stakeholder = None
    stakeholder_code = None
    stakeholder_product_code = None
    countries = None
    prod_tr = None
    taksonomi_list = None
    cpc_data = None
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        product_type = request.json['product_type']
        if product_type != 'cpc' and product_type != 'taksonomi' and product_type != 'technical_spec':
            self.stakeholder = ElementValues.get_stakeholder(self.iam_user_company_id)
            self.stakeholder_code = str(self.stakeholder[0]['paydas_kodu']).zfill(4)
            stakeholder_products = ElementValues.get_stakeholder_product_code(str(self.iam_user_company_id))
            self.stakeholder_product_code = self.stakeholder_code + str(
                stakeholder_products[0]['function_paydas_urun_kodu']).zfill(4)
            self.countries = ElementValues.get_countries()
            self.prod_tr = ElementValues.get_prod_tr()
            self.taksonomi_list = ElementValues.get_taksonomi()
            self.cpc_data = ElementValues.get_cpc('root', 2)
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def software_product(self):
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        software_languages = ElementValues.get_sys_definition('um_yazilim_dil', 'dil_id')
        operating_systems = ElementValues.get_sys_definition('um_yazilim_isletim_sistemi', 'isletim_sistemi_id')
        software_standards = ElementValues.get_software_standards()
        software_product_data = {
            'ulkeler': self.countries,
            'urun_numarasi': self.stakeholder_product_code,
            'isletim_sistemi_id': operating_systems,
            'dil_id': software_languages,
            'standart_id': software_standards,
            'taksonomi': self.taksonomi_list,
            'prod_tr': None,
            'cpc': self.cpc_data
        }
        message['payload'] = software_product_data
        return ReaPy.rest_response(message)

    def cpc(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements_cpc'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        data = ElementValues.get_cpc(self.request_data['parent'], int(self.request_data['level']) + 1,
                                     self.request_data['finder'])

        message['payload'] = data
        return ReaPy.rest_response(message)

    def taksonomi(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements_taksonomi'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        data = ElementValues.find_taksonomi(self.request_data['finder'])

        message['payload'] = data
        return ReaPy.rest_response(message)

    def normal_product(self):
        duty_function = ElementValues.get_duty_function()
        technology_preparation_level = ElementValues.get_sys_definition('um_kutuphane_urun',
                                                                        'teknoloji_hazirlik_seviyesi')
        dual_use = ElementValues.get_sys_definition('um_kutuphane_urun', 'askeri_alanda_kullanim_dual_use')
        unit = ElementValues.get_unit(1)
        company = ElementValues.get_company(self.iam_user_company_id)
        product = ElementValues.get_product(self.iam_user_company_id)
        environmental_conditions = ElementValues.get_standards(1)
        electromagnetic_compatibility = ElementValues.get_standards(2)
        production_process = ElementValues.get_standards(3)

        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        normal_product_data = {
            'ulkeler': self.countries,
            'urun_numarasi': self.stakeholder_product_code,
            'gorev_fonksiyonu_id': duty_function,
            'cevre_sartlari': environmental_conditions,
            'elektro_uyum': electromagnetic_compatibility,
            'uretim_proses': production_process,
            'teknoloji_hazirlik_seviyesi': technology_preparation_level,
            'askeri_alanda_kullanim_dual_use': dual_use,
            'birim_id': unit,
            'paydas_id': company,
            'urun_id': product,
            'taksonomi': self.taksonomi_list,
            'prod_tr': self.prod_tr,
            'cpc': self.cpc_data
        }
        message['payload'] = normal_product_data
        return ReaPy.rest_response(message)

    def technical_spec(self):
        validate = ReaPy.validator().json_validate(self.master.schema['product_form_elements_technical_spec'],
                                                   self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        data = ElementValues.find_technical_spec(self.request_data['cpc'])
        message['payload'] = data
        return ReaPy.rest_response(message)
