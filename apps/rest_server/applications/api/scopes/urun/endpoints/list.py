# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun.product import Product
from apps.rest_server.applications.base_endpoint import BaseEndpoint


class List(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None

    def get(self):
        ReaPy.set_request(ReaPy.rest_request())
        BaseEndpoint().check_session(self, ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)
        auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-urun')
        if auth is True:
            products = Product().get_products(self.iam_user_company_id)
        else:
            auth = BaseEndpoint().check_auth(self.master, 'yeten-dis-portal-altyapi')
            if auth is True:
                products = Product().get_products(self.iam_user_company_id)
            else:
                products = None
        message = self.master.messages['success']['success_temp']
        message['payload'] = {}
        message['payload'].clear()
        message['payload'] = products
        return ReaPy.rest_response(message)
