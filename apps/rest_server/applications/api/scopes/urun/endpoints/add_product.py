# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.rest_server.models.api.urun.element_values import ElementValues
from apps.rest_server.models.api.urun.product import Product


class AddProduct(ReaPy().rest_resource()):
    iam_user_company_id = None
    iam_user_domain_account_name = None
    request_data = None
    remote_addr = None

    def post(self):
        request = ReaPy.set_request(ReaPy.rest_request())
        if self.master.configurations['debug'] is True:
            self.master.session['IAM-User'] = {'iam_user_company_id': '0dec0bb8-6efd-4ae0-a557-cdb7ba3c9574',
                                               'iam_user_domain_account_name': '11111111112'}

        try:
            if self.master.session.get('IAM-User') is None:
                message = self.master.messages['errors']['user_sn_err_100']
                return ReaPy.rest_response(message['data'], 403)
            else:
                iam_user_company_id = self.master.session.get('IAM-User')['iam_user_company_id']
                iam_user_domain_account_name = self.master.session.get('IAM-User')['iam_user_domain_account_name']
                if iam_user_company_id is not None:
                    self.iam_user_company_id = iam_user_company_id
                    self.iam_user_domain_account_name = iam_user_domain_account_name
                else:
                    message = self.master.messages['errors']['user_sn_err_101']
                    return ReaPy.rest_response(message['data'], 403)
        except Exception:
            message = self.master.messages['errors']['user_sn_err_102']
            return ReaPy.rest_response(message['data'], 403)

        validate = ReaPy.validator().json_validate(self.master.schema['add_product'], request.json)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        self.request_data = request.json
        self.remote_addr = request.remote_addr
        product_type = request.json['product_type']
        call_product_type = getattr(self, product_type)
        return call_product_type()

    def software_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['add_software_product'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        product_code = str(ElementValues.get_stakeholder_product_code(str(self.iam_user_company_id))[0][
                               'function_paydas_urun_kodu']).zfill(4)
        product_id = ReaPy.hash().get_uuid()

        check_product = Product().check_software_product(self.iam_user_company_id, self.request_data['adi'])
        if check_product is not None:
            message = self.master.messages['errors']['product_sn_err_101']
            return ReaPy.rest_response(message['data'], 400)
        add_software_products = Product().add_software_product(product_code, product_id, self.iam_user_company_id,
                                                             self.iam_user_domain_account_name, self.remote_addr,
                                                             self.request_data)

        if add_software_products is True:
            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'urun_id': product_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)
        else:
            message = self.master.messages['errors']['product_as_err_100']
            return ReaPy.rest_response(message['data'], 400)

    def normal_product(self):
        validate = ReaPy.validator().json_validate(self.master.schema['add_normal_product'], self.request_data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return ReaPy.rest_response(message, 400)
        product_code = str(ElementValues.get_stakeholder_product_code(str(self.iam_user_company_id))[0][
                               'function_paydas_urun_kodu']).zfill(4)
        product_id = ReaPy.hash().get_uuid()

        check_product = Product().check_normal_product(self.iam_user_company_id, self.request_data['adi'])
        if check_product is not None:
            message = self.master.messages['errors']['product_sn_err_101']
            return ReaPy.rest_response(message['data'], 400)
        add_normal_products = Product().add_normal_product(product_code, product_id, self.iam_user_company_id,
                                                             self.iam_user_domain_account_name, self.remote_addr,
                                                             self.request_data)

        if add_normal_products is True:
            message = self.master.messages['success']['success_temp']
            message['payload'] = {}
            message['payload'].clear()
            product_data = {
                'urun_id': product_id,
            }
            message['payload'] = product_data
            return ReaPy.rest_response(message)
        else:
            message = self.master.messages['errors']['product_as_err_100']
            return ReaPy.rest_response(message['data'], 400)
