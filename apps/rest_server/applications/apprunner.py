# -*- coding: utf-8 -*-
from core.reapy import ReaPy


class AppRunner:
    def __init__(self, api):
        super(AppRunner).__init__()
        print(ReaPy.colored("start ", 'cyan'), ReaPy.colored(__name__, 'green'))
        script_path = ReaPy.rea_os().path.dirname(ReaPy.rea_os().path.abspath(__file__))
        all_sub_dirs = [dI for dI in ReaPy.rea_os().listdir(script_path) if
                        ReaPy.rea_os().path.isdir(ReaPy.rea_os().path.join(script_path, dI)) and not dI.endswith('__')]
        for apps in all_sub_dirs:
            try:
                ReaPy.importer().load_module('apps.rest_server.applications.' + apps + '.scoperunner').load_class(
                    "ScopeRunner").get_object()(api)
            except Exception as err:
                print(err)