# -*- coding: utf-8 -*-
from core.reapy import ReaPy


class Runner:

    def __init__(self):
        super(Runner).__init__()
        print(ReaPy.colored("start ", 'red'), ReaPy.colored(__name__, 'magenta'))
    #     try:
    #         script_path = ReaPy.rea_os().path.dirname(ReaPy.rea_os().path.abspath(__file__))
    #         working_space = ReaPy.rea_os().path.basename(script_path)
    #         modules = [f for f in ReaPy.rea_os().listdir(
    #             ReaPy.rea_os().path.dirname(ReaPy.rea_os().path.abspath(__file__)) + '/controllers') if
    #                    f.endswith('.py')]
    #
    #         for module in modules:
    #             class_name = ReaPy.rea_os().path.splitext(module)[0]
    #             Runner.get_class_package("apps." + working_space + ".controllers." + class_name,
    #                                      class_name.capitalize())
    #     except Exception as e:
    #         print(e)
    #
    # @staticmethod
    # def get_class_package(module_name, class_name):
    #     try:
    #         m = __import__(module_name, globals(), locals(), class_name)
    #         import_module = ReaPy.inspect().getmembers(m)
    #         module_name = import_module[0]
    #         c = getattr(m, module_name[0])
    #         return c
    #     except Exception as e:
    #         print(e)
    #
