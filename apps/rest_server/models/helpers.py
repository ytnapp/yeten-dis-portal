# -*- coding: utf-8 -*-


class Helpers:

    @staticmethod
    def payload_parameters_control(payload_data):
        p_data = []
        out_key = [
            "aktif_mi",
            "olusturan_id",
            "guncelleyen",
            "guncelleyen_ip",
            "olusturan_kullanici",
            "olusturan",
            "guncelleme_tarihi",
            "olusturma_tarihi",
            "olusturulma_tarihi",
            "kullanici_olusturuldu_mu",
            "olusturan_ip",
            "onaylayan_id",
            "onaylayan",
            "onaylama_tarihi",
            "onaylayan_ip"]
        if not isinstance(payload_data, list):
            p_data.append(payload_data)
        else:
            p_data = payload_data
        for item in p_data:
            try:
                for o_key in out_key:
                    if o_key in item:
                        del item[o_key]
            except Exception as exc:
                print('payload_parameters_control except', exc)
                return []
        return p_data

    @staticmethod
    def session_control(iam_user, is_debug=None):
        if iam_user is None:
            return iam_user

        if is_debug is not None:
            return {
                'iam_user_company_id': iam_user['iam_user_company_id'],
                'iam_user_domain_account_name': iam_user['iam_user_domain_account_name']
            }

        assigned_iam_roles = ''  # TCKN
        iam_user_id = ''
        iam_user_company_id = ''
        iam_user_email = ''  # isim soyisim
        iam_user_name = ''  # isim soyisim
        iam_user_surname = ''  # isim soyisim
        iam_user_domain_account_name = ''  # TCKN

        if 'assigned_iam_roles' in iam_user.keys():
            assigned_iam_roles = iam_user['assigned_iam_roles']

        if 'iam_user_id' in iam_user.keys():
            iam_user_id = iam_user['iam_user_id']

        if 'iam_user_company_id' in iam_user.keys():
            iam_user_company_id = iam_user['iam_user_company_id']

        if 'iam_user_email' in iam_user.keys():
            iam_user_email = iam_user['iam_user_email']

        if 'iam_user_name' in iam_user.keys():
            iam_user_name = iam_user['iam_user_name']

        if 'iam_user_surname' in iam_user.keys():
            iam_user_surname = iam_user['iam_user_surname']

        if 'iam_user_domain_account_name' in iam_user.keys():
            iam_user_domain_account_name = iam_user['iam_user_domain_account_name']

        return {
            'iam_user_id': iam_user_id,
            'iam_user_name': iam_user_name,
            'iam_user_email': iam_user_email,
            'iam_user_surname': iam_user_surname,
            'assigned_iam_roles': assigned_iam_roles,
            'iam_user_company_id': iam_user_company_id,
            'iam_user_domain_account_name': iam_user_domain_account_name,
        }

    @staticmethod
    def set_val_safe(data, field_name, is_set_null=None):
        field_value = ''
        try:
            field_value = data[field_name]
        except KeyError:
            if is_set_null is not None:
                return None

        if field_value == '' and is_set_null is not None:
            return None

        return field_value
