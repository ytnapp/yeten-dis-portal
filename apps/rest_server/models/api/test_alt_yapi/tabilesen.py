# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Tabilesen(object):

    def get_check_for_delete(self, bilesen_id):
        try:
            check = Model(ReaPy.get_reactor_source('yeten')).select(table='public.ta_test_mevcut_alt_yapi_bilesenler',
                                                            condition=
                                                            {
                                                                0: {'col': 'bilesen_id',
                                                                    'operator': '=',
                                                                    'value': bilesen_id,
                                                                    'combiner': 'AND'},
                                                                1: {'col': 'aktif_mi',
                                                                    'operator': '=',
                                                                    'value': 'true'},
                                                            }).data()
            if check is None:
                return 0
            else:
                return 1
        except Exception as exc:
            print('Bilesen get_check_for_delete : ', exc)
            return 1

    def get_bilesenler(self, paydas_id):

        if paydas_id == '':
            return None

        try:

            query = Model(ReaPy.get_reactor_source('yeten')).select(table='view_paydas_bilesen',
                                                                    condition={
                                                                        0: {'col': 'paydas_id',
                                                                            'operator': '=',
                                                                            'value': paydas_id}
                                                                    })

            return Helpers().payload_parameters_control(query.data())
        except Exception as exc:
            print(__file__ + ' get_bilesenler')
            print(exc)

        return None

    def insert(self, data, user_identity_no, paydas_id, olusturan_ip):

        if paydas_id == '':
            return None

        insert_data = {
            "paydas_id": paydas_id,
            "olusturan_ip": olusturan_ip,
            "olusturan_id": user_identity_no,
            "bilesen_turu": data['bilesen_turu'],
            "bilesen_adi": data['bilesen_adi'],
            "marka_model": data['marka_model'],
            "ssb_payi": Helpers().set_val_safe(data, 'ssb_payi', True),
            "ssb_proje_adi": Helpers().set_val_safe(data, 'ssb_proje_adi', True),
            "bilesen_teknik_ozellik": data['bilesen_teknik_ozellik'],
            "bilesen_turu_diger": Helpers().set_val_safe(data, 'bilesen_turu_diger', True),
            "bilesen_marka_model_diger": Helpers().set_val_safe(data, 'bilesen_marka_model_diger', True),
            "ssb_payi_varmi": data['ssb_payi_varmi'],
            "aktif_mi": 'true'
        }
        query = Model(ReaPy.get_reactor_source('yeten')).insert('public.ta_bilesen', {0: insert_data})

        print("insert. bilesen insert ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def update(self, data, tckn, ip):

        update_data = {'guncelleyen': tckn, 'guncelleyen_ip': ip, 'guncelleme_tarihi': str(datetime.datetime.now())}

        if Helpers().set_val_safe(data, 'ssb_payi_varmi') != '':
            update_data['ssb_payi_varmi'] = data['ssb_payi_varmi']
            if data['ssb_payi_varmi'] == False:
                update_data['ssb_proje_adi'] = None

        if Helpers().set_val_safe(data, 'bilesen_turu') != '':
            update_data['bilesen_turu'] = data['bilesen_turu']

        if Helpers().set_val_safe(data, 'bilesen_adi') != '':
            update_data['bilesen_adi'] = data['bilesen_adi']

        if Helpers().set_val_safe(data, 'bilesen_turu_diger') != '':
            update_data['bilesen_turu_diger'] = data['bilesen_turu_diger']

        if Helpers().set_val_safe(data, 'marka_model') != '':
            update_data['marka_model'] = data['marka_model']

        if Helpers().set_val_safe(data, 'bilesen_marka_model_diger') != '':
            update_data['bilesen_marka_model_diger'] = data['bilesen_marka_model_diger']

        if Helpers().set_val_safe(data, 'ssb_payi') != '':
            update_data['ssb_payi'] = data['ssb_payi']

        if Helpers().set_val_safe(data, 'ssb_proje_adi') != '':
            update_data['ssb_proje_adi'] = data['ssb_proje_adi']

        if Helpers().set_val_safe(data, 'bilesen_teknik_ozellik') != '':
            update_data['bilesen_teknik_ozellik'] = data['bilesen_teknik_ozellik']

        query = Model(ReaPy.get_reactor_source('yeten')).update('public.ta_bilesen', {'SET': update_data,
                                                                                      'CONDITION': {
                                                                                          0: {'col': 'bilesen_id',
                                                                                              'operator': '=',
                                                                                              'value': data[
                                                                                                  'bilesen_id']}
                                                                                      }})

        print("insert. bilesen update ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def delete(self, bilesen_id, tckn, ip):

        delete_data = {'aktif_mi': 'false', 'guncelleyen': tckn, 'guncelleyen_ip': ip,
                       'guncelleme_tarihi': str(datetime.datetime.now())}

        query = Model(ReaPy.get_reactor_source('yeten')).update('public.ta_bilesen', {'SET': delete_data,
                                                                                      'CONDITION': {
                                                                                          0: {'col': 'bilesen_id',
                                                                                              'operator': '=',
                                                                                              'value': bilesen_id}
                                                                                      }
                                                                                      })

        print("insert. bilesen delete ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def get_marka_model(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_bilesen_marka_model',
                                                               sort='marka_model').data()

    def get_test_ana_basligi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_ana_basligi_secim',
                                                               field='test_ana_basligi_id,test_alt_yapi_id,test_alt_yapi_adi, adi ',
                                                               sort='adi').data()

    def get_test_altyapi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_test_alt_yapi',
                                                               field='test_alt_yapi_id, test_alt_yapi_adi',
                                                               sort='test_alt_yapi_adi').data()

    def get_test_kriter_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_test_kriter_secim',
                                                               sort='ta_test_kriteri_adi').data()

    def get_test_standart_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_standart_secim',
                                                               sort='standart_adi').data()

    def get_bilesen_turu(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(
            table='public.sys_tanim',
            field='deger, aciklama',
            sort='sira',
            condition={0:
                           {'col': 'kolon',
                            'operator': '=',
                            'value': 'bilesen_turu',
                            'combiner': 'AND'},
                       1:
                           {'col': 'aktif_mi',
                            'operator': '=',
                            'value': 'true',
                            'combiner': 'AND'},
                       2:
                           {'col': 'tablo',
                            'operator': '=',
                            'value': 'ta_bilesen'}
                       }
        ).data()

    def get_test_metod_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_method_secim',
                                                               field='standart_adi, metod_id,standart_id,metod_adi',
                                                               sort='metod_adi').data()
