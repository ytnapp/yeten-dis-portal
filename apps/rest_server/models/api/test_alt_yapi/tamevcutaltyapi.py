# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Tamevcutaltyapi(object):

    def get_bilesenler(self, paydas_test_alt_yapi_id):

        try:

            query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.view_ta_bilesen_turler',
                                                                         condition={
                                                                             0: {'col': 'paydas_test_alt_yapi_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_test_alt_yapi_id}
                                                                         })

            print(query.error())

            return query.data()
        except Exception as exc:
            print(__file__ + ' get_bilesenler')
            print(exc)

        return None

    def get_mevcutlar(self, paydas_id):

        result_data = None
        try:
            query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.view_ta_mevcut',
                                                                         condition={
                                                                             0: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id,
                                                                                 'combiner': 'AND'},
                                                                             1: {
                                                                                 'col': 'test_alt_yapi_mevcut',
                                                                                 'operator': '=',
                                                                                 'value': 'true'
                                                                             }
                                                                         })
            print(query.error())
            ihtiyac_ta = Helpers().payload_parameters_control(query.data())
            bilesenler = Helpers().payload_parameters_control(self.__get_bilesenler(ihtiyac_ta))
            result_data = ihtiyac_ta

            result_data = []

            for ta_ihtiyac in ihtiyac_ta:
                ta_ihtiyac['bilesenler'] = self.__get_list_bilesen(ta_ihtiyac, bilesenler)
                result_data.append(ta_ihtiyac)

        except Exception as exc:
            print(__file__ + ' get_mevcutlar')
            print(exc)

        return result_data

    def __get_list_bilesen(self, kayit, turler):
        biriktir = []

        if turler is None:
            return biriktir

        for tur in turler:
            if tur['paydas_test_alt_yapi_id'] == kayit['paydas_test_alt_yapi_id']:
                biriktir.append(tur['bilesen_id'])

        return biriktir

    def __get_bilesenler(self, alt_yapi_data):

        select_data = {}
        for data in alt_yapi_data:
            select_data[len(select_data)] = {'col': 'paydas_test_alt_yapi_id',
                                             'operator': '=',
                                             'value': data['paydas_test_alt_yapi_id'],
                                             'combiner': 'OR'}
        select_data[len(select_data)] = {
            'col': 'aktif_mi',
            'operator': '=',
            'value': 'true'
        }

        return Model(ReaPy.get_reactor_source('yeten')).select(
            table='public.ta_test_mevcut_alt_yapi_bilesenler',
            condition=select_data).data()

    def insert(self, data, user_identity_no, paydas_id, ip):

        if paydas_id == '':
            return None

        insert_data = {
            "paydas_id": paydas_id,
            "test_alt_yapi_id": data['test_alt_yapi_id'],
            "paydas_tesis_id": data['paydas_tesis_id'],
            "akreditasyon_var_mi": data['akreditasyon_var_mi'],
            "kalibre_eden_firma_akreditasyon_var_mi": data['kalibre_eden_firma_akreditasyon_var_mi'],
            "test_alt_yapi_mevcut": 'true',
            "olusturan_id": user_identity_no,
            "olusturan_ip": ip,
            "aktif_mi": 'true'
        }
        query = Model(ReaPy.get_reactor_source('yeten')).insert('ta_paydas_test_altyapi', {0: insert_data})

        if query.error() is None:
            degistir = Model(ReaPy.get_reactor_source('yeten')).select('ta_paydas_test_altyapi',
                                                                            'paydas_test_alt_yapi_id',
                                                                            condition=
                                                                            {
                                                                                0: {'col': 'test_alt_yapi_id',
                                                                                    'operator': '=',
                                                                                    'value': data['test_alt_yapi_id'],
                                                                                    'combiner': 'AND'},
                                                                                1: {'col': 'paydas_tesis_id',
                                                                                    'operator': '=',
                                                                                    'value': data['paydas_tesis_id'],
                                                                                    'combiner': 'AND'},
                                                                                2: {'col': 'paydas_id',
                                                                                    'operator': '=',
                                                                                    'value': paydas_id}
                                                                            }, sort='olusturulma_tarihi desc').data()

            ta_detay_test_alt_yapi_id = degistir[0]

            test_alt_yapi_id = ta_detay_test_alt_yapi_id['paydas_test_alt_yapi_id']

            self.__ana_baslik_issue(data, test_alt_yapi_id)

            bilesenler = Helpers().set_val_safe(data, 'bilesenler', True)
            self.__bilesen_iliski(bilesenler, test_alt_yapi_id)

            return query.data()
        else:
            print("insert. mevcut insert ->  ")
            print(query.error())

        return None

    def __bilesen_iliski(self, bilesenler, test_alt_yapi_id, is_delete_first=None):

        if is_delete_first is not None:
            tabd = Model(ReaPy.get_reactor_source('yeten')).delete('public.ta_test_mevcut_alt_yapi_bilesenler',
                                                                         {
                                                                              0: {'col': 'paydas_test_alt_yapi_id',
                                                                                  'operator': '=',
                                                                                  'value': test_alt_yapi_id
                                                                                  },
                                                                          })
            print(tabd.error())
            print(tabd.data())

        for key in bilesenler:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ta_test_mevcut_alt_yapi_bilesenler', {0:
                {
                    'bilesen_id': key,
                    'paydas_test_alt_yapi_id': test_alt_yapi_id,
                    'aktif_mi': 'true'
                }
            })

    def __ana_baslik_issue(self, data, test_alt_yapi_id, is_delete_first=None):

        if is_delete_first is not None:
            tabd = Model(ReaPy.get_reactor_source('yeten')).delete('ta_paydas_test_ana_basligi', {
                0: {'col': 'paydas_test_alt_yapi_id',
                    'operator': '=',
                    'value': data['paydas_test_alt_yapi_id'],
                    }
            })
            print(tabd.error())
            print(tabd.data())

        tab_data = {
            'aktif_mi': 'true',
            'paydas_test_alt_yapi_id': test_alt_yapi_id,

            'test_metot_id': data['test_metot_id'],
            'test_kriteri_id': data['test_kriteri_id'],
            'test_standart_id': data['test_standart_id'],
            'test_ana_basligi_id': data['test_ana_basligi_id'],

            'test_metot_diger': Helpers().set_val_safe(data, 'test_metot_diger', True),
            'test_kriteri_diger': Helpers().set_val_safe(data, 'test_kriteri_diger', True),
            'test_standart_diger': Helpers().set_val_safe(data, 'test_standart_diger', True),
            'test_ana_basligi_diger': Helpers().set_val_safe(data, 'test_ana_basligi_diger', True),
        }
        tab = Model(ReaPy.get_reactor_source('yeten')).insert('ta_paydas_test_ana_basligi', {0: tab_data})
        print(tab.error())
        print(tab.data())

    def update(self, data, user_identity_no, paydas_id, ip):

        update_data = {
            'guncelleyen_ip': ip,
            'guncelleyen': user_identity_no,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        if Helpers().set_val_safe(data, 'test_alt_yapi_id') != '':
            update_data['test_alt_yapi_id'] = data['test_alt_yapi_id']

        if Helpers().set_val_safe(data, 'paydas_tesis_id') != '':
            update_data['paydas_tesis_id'] = data['paydas_tesis_id']

        if Helpers().set_val_safe(data, 'akreditasyon_var_mi') != '':
            update_data['akreditasyon_var_mi'] = data['akreditasyon_var_mi']

        if Helpers().set_val_safe(data, 'kalibre_eden_firma_akreditasyon_var_mi') != '':
            update_data['kalibre_eden_firma_akreditasyon_var_mi'] = data['kalibre_eden_firma_akreditasyon_var_mi']

        if Helpers().set_val_safe(data, 'paydas_tesis_id') != '':
            update_data['paydas_tesis_id'] = data['paydas_tesis_id']

        query = None
        try:
            query = Model(ReaPy.get_reactor_source('yeten')).update('ta_paydas_test_altyapi', {'SET': update_data,
                                                                                                    'CONDITION': {
                                                                                                        0: {
                                                                                                            'col': 'paydas_test_alt_yapi_id',
                                                                                                            'operator': '=',
                                                                                                            'value': data['paydas_test_alt_yapi_id'],
                                                                                                            'combiner': 'AND'
                                                                                                            },
                                                                                                        1: {
                                                                                                            'col': 'paydas_id',
                                                                                                            'operator': '=',
                                                                                                            'value': paydas_id
                                                                                                            }
                                                                                                    }})
        except Exception as exc:
            print(exc)

        if query is not None:

            self.__ana_baslik_issue(data, data['paydas_test_alt_yapi_id'], True)

            bilesenler = Helpers().set_val_safe(data, 'bilesenler', True)
            self.__bilesen_iliski(bilesenler, data['paydas_test_alt_yapi_id'], True)

            return query.data()

        return None

    def delete(self, paydas_test_alt_yapi_id, tckn, paydas_id, ip):

        delete_data = {
            'aktif_mi': 'false',
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        query = Model(ReaPy.get_reactor_source('yeten')).update('ta_paydas_test_altyapi',
                                                                     {'SET': delete_data,
                                                                      'CONDITION': {
                                                                          0: {'col': 'paydas_test_alt_yapi_id',
                                                                              'operator': '=',
                                                                              'value': paydas_test_alt_yapi_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'paydas_id',
                                                                              'operator': '=',
                                                                              'value': paydas_id
                                                                              }
                                                                      }})

        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def get_marka_model(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_bilesen_marka_model',
                                                                    sort='marka_model').data()

    def get_test_ana_basligi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_ana_basligi_secim',
                                                                    field=' test_alt_yapi_adi, adi ',
                                                                    sort='adi').data()

    def get_test_altyapi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_test_alt_yapi',
                                                                    field='test_alt_yapi_id, test_alt_yapi_adi',
                                                                    sort='test_alt_yapi_adi').data()

    def get_test_kriter_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_test_kriter_secim',
                                                                    sort='ta_test_kriteri_adi').data()

    def get_test_standart_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_standart_secim',
                                                                    sort='standart_adi').data()

    def get_test_metod_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_testaltyapi_method_secim',
                                                                    sort='metod_adi').data()
