# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Uamodel(object):

    def update_uretim_altyapi(self, data, tckn, paydas_id, ip):

        update_data = {'guncelleyen': tckn, 'guncelleyen_ip': ip, 'guncelleme_tarihi': str(datetime.datetime.now())}

        if Helpers().set_val_safe(data, 'makine_adi') != '':
            update_data['makine_adi'] = data['makine_adi']

        if Helpers().set_val_safe(data, 'makine_prod_tr') != '':
            update_data['makine_prod_tr'] = data['makine_prod_tr']

        if Helpers().set_val_safe(data, 'makine_prod_tr_diger') != '':
            update_data['makine_prod_tr_diger'] = data['makine_prod_tr_diger']

        if Helpers().set_val_safe(data, 'marka') != '':
            update_data['marka'] = data['marka']

        if Helpers().set_val_safe(data, 'model') != '':
            update_data['model'] = data['model']

        if Helpers().set_val_safe(data, 'makine_sinifi') != '':
            update_data['makine_sinifi'] = data['makine_sinifi']

        if Helpers().set_val_safe(data, 'seri_numarasi') != '':
            update_data['seri_numarasi'] = data['seri_numarasi']

        if Helpers().set_val_safe(data, 'model_yili') != '':
            update_data['model_yili'] = data['model_yili']

        if Helpers().set_val_safe(data, 'yerli_ithal') != '':
            update_data['yerli_ithal'] = data['yerli_ithal']

        if Helpers().set_val_safe(data, 'tesis_id') != '':
            update_data['tesis_id'] = data['tesis_id']

        query_update = Model(ReaPy.get_reactor_source('yeten')).update(
            'ua_uretim_altyapi',
            {'SET': update_data,
             'CONDITION': {
                 0: {
                     'col': 'uretim_altyapi_id',
                     'operator': '=',
                     'value': data['uretim_altyapi_id'],
                     'combiner': 'AND'
                 },
                 1: {'col': 'paydas_id',
                     'operator': '=',
                     'value': paydas_id,
                     }
             }}).data()
        if query_update is not None:

            if Helpers().set_val_safe(data, 'teknik_ozellikler', True) != None:
                teknik_ozellik_data = data['teknik_ozellikler']

                self.update_urun_makine_iliski(data['uretim_altyapi_id'], data, paydas_id, tckn, ip)
                self.teknik_ozellik_iliski(teknik_ozellik_data, data['uretim_altyapi_id'], True)
                return True
            else:
                return None

    @staticmethod
    def update_urun_makine_iliski(uretim_altyapi_id, data, paydas_id, tckn, ip):
        Model(ReaPy.get_reactor_source('yeten')).delete('ua_uretim_altyapi_makina_urun',
                                                        {
                                                            0: {'col': 'uretim_altyapi_makina_urun_id',
                                                                'operator': '=',
                                                                'value': uretim_altyapi_id
                                                                },
                                                        })
        Uamodel.insert_urun_makine_iliski(uretim_altyapi_id, data, paydas_id, tckn, ip)

    def delete_ua(self, uretim_altyapi_id):
        update_data = {'aktif_mi': 'false'}

        return Model(ReaPy.get_reactor_source('yeten')).update('ua_uretim_altyapi', {'SET': update_data,
                                                                                     'CONDITION': {
                                                                                         0: {
                                                                                             'col': 'uretim_altyapi_id',
                                                                                             'operator': '=',
                                                                                             'value': uretim_altyapi_id}
                                                                                     }}).data()

    def get_uretim_alt_dashboard_data(self, paydas_id):
        yerli = 0
        ithal = 0

        try:
            yerli_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.ua_uretim_altyapi',
                                                                          field='count(*) as toplam',
                                                                          condition={
                                                                              0: {'col': 'paydas_id',
                                                                                  'operator': '=',
                                                                                  'value': paydas_id,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': 'true',
                                                                                  'combiner': 'AND'},
                                                                              2: {'col': 'yerli_ithal',
                                                                                  'operator': '=',
                                                                                  'value': 1}
                                                                          }).data()
            if yerli_query is not None:
                yerli = yerli_query[0]['toplam']

        except Exception as exc:
            print(exc)

        try:
            ihtal_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.ua_uretim_altyapi',
                                                                          field='count(*) as toplam',
                                                                          condition={
                                                                              0: {'col': 'paydas_id',
                                                                                  'operator': '=',
                                                                                  'value': paydas_id,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': 'true',
                                                                                  'combiner': 'AND'},
                                                                              2: {'col': 'yerli_ithal',
                                                                                  'operator': '=',
                                                                                  'value': 0}
                                                                          }).data()
            if ihtal_query is not None:
                ithal = ihtal_query[0]['toplam']

        except Exception as exc:
            print(exc)

        return {'yerli': yerli, 'ithal': ithal}

    def get_uretim_alt_yapi(self, paydas_id):
        yeni_liste = []
        try:
            ua_liste = Helpers().payload_parameters_control(
                Model(ReaPy.get_reactor_source('yeten')).select(table='public.view_paydas_uretim_altyapi_yeni',
                                                                condition={
                                                                    0: {'col': 'paydas_id', 'operator': '=',
                                                                        'value': paydas_id,
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'aktif_mi', 'operator': '=',
                                                                        'value': 'true'}
                                                                },
                                                                sort='olusturulma_tarihi desc').data())
            if ua_liste is not None:
                for ua in ua_liste:
                    urun_id = []
                    urun_adi_tanim = []
                    urunler = Model(ReaPy.get_reactor_source('yeten')).select(
                        table='public.view_ua_uretim_altyapi_makina_urun',
                        field='urun_id,adi,yazilim_adi',
                        condition={
                            0: {'col': 'paydas_id', 'operator': '=',
                                'value': paydas_id,
                                'combiner': 'AND'},
                            1: {'col': 'uretim_altyapi_makina_urun_id', 'operator': '=',
                                'value': ua['uretim_altyapi_id'],
                                'combiner': 'AND'},
                            2: {'col': 'aktif_mi', 'operator': '=',
                                'value': True}
                        }).data()
                    if urunler is not None:
                        for item in urunler:
                            urun_id.append(item['urun_id'])
                            if item['adi'] is not None:
                                urun_adi_tanim.append(item['adi'])
                            else:
                                urun_adi_tanim.append(item['yazilim_adi'])
                    yeni_kayit = ua
                    yeni_kayit.update({'urun_id': urun_id})
                    yeni_kayit.update({'urun_adi_tanim': urun_adi_tanim})
                    yeni_kayit['teknik_ozellikler'] = []
                    teknik_ozellikler = Helpers().payload_parameters_control(
                        Model(ReaPy.get_reactor_source('yeten')).select(
                            table='public.ua_teknik_ozellik',
                            condition={
                                0: {'col': 'uretim_altyapi_id',
                                    'operator': '=',
                                    'value': yeni_kayit['uretim_altyapi_id'],
                                    'combiner': 'AND'},
                                1: {'col': 'aktif_mi',
                                    'operator': '=',
                                    'value': 'true'}
                            },
                            sort='olusturulma_tarihi desc').data())

                    if teknik_ozellikler is not None:
                        yeni_kayit['teknik_ozellikler'] = teknik_ozellikler

                    yeni_liste.append(yeni_kayit)

            return yeni_liste

        except Exception as exc:
            print(exc)
        return None

    def uretim_alt_yapi_insert(self, data, tckn, paydas_id, ip):

        data_insert = {
            'paydas_id': paydas_id,
            'aktif_mi': 'true',
            'makine_adi': data['makine_adi'],
            'makine_prod_tr': data['makine_prod_tr'],
            'makine_prod_tr_diger': data['makine_prod_tr_diger'] if 'makine_prod_tr_diger' in data else None,
            'marka': data['marka'],
            'model': data['model'],
            'makine_sinifi': data['makine_sinifi'],
            'seri_numarasi': data['seri_numarasi'],
            'model_yili': data['model_yili'],
            'yerli_ithal': data['yerli_ithal'],
            'tesis_id': data['tesis_id'],
            'olusturan_ip': ip,
            'olusturan_id': tckn
        }

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('ua_uretim_altyapi', {
            0: data_insert
        }).data()

        if data_query is not None:
            teknik_ozellik_data = []
            if Helpers().set_val_safe(data, 'teknik_ozellikler', True) != None:
                teknik_ozellik_data = data['teknik_ozellikler']

            if len(teknik_ozellik_data) > 0:
                inserted_data = Model(ReaPy.get_reactor_source('yeten')) \
                    .select(table='ua_uretim_altyapi',
                            field='uretim_altyapi_id',
                            condition={
                                0: {'col': 'paydas_id', 'operator': '=', 'value': paydas_id, 'combiner': 'AND'},
                                1: {'col': 'makine_prod_tr', 'operator': '=', 'value': data['makine_prod_tr']},
                            },
                            sort='olusturulma_tarihi desc').data()

                uretim_altyapi_id = inserted_data[0]['uretim_altyapi_id']
                self.insert_urun_makine_iliski(uretim_altyapi_id, data, paydas_id, tckn, ip)
                self.teknik_ozellik_iliski(teknik_ozellik_data, uretim_altyapi_id)

        return data_query

    @staticmethod
    def insert_urun_makine_iliski(uretim_altyapi_id, data, paydas_id, tckn, ip):
        um_data = {
            'uretim_altyapi_makina_urun_id': uretim_altyapi_id,
            'paydas_id': paydas_id,
            'makine_id': data['makine_sinifi'],
            'olusturan_id': tckn,
            'olusturan_ip': ip
        }
        for item in data['urun_id']:
            um_data.update({'urun_id': item})
            um = Model(ReaPy.get_reactor_source('yeten')).insert('ua_uretim_altyapi_makina_urun', {0: um_data}).data()
            if um != 1:
                Model(ReaPy.get_reactor_source('yeten')).delete('ua_uretim_altyapi_makina_urun',
                                                                {
                                                                    0: {'col': 'paydas_id',
                                                                        'operator': '=',
                                                                        'value': paydas_id
                                                                        },
                                                                })

    def teknik_ozellik_iliski(self, teknik_ozellik_data, uretim_altyapi_id, is_delete_first=None):
        if is_delete_first is not None:
            sil = Model(ReaPy.get_reactor_source('yeten')).delete('ua_teknik_ozellik',
                                                                  {
                                                                      0: {'col': 'uretim_altyapi_id',
                                                                          'operator': '=',
                                                                          'value': uretim_altyapi_id
                                                                          },
                                                                  })
            print(sil.error())

        insert_teknik_ozellik = {}
        for to in teknik_ozellik_data:
            insert_teknik_ozellik[len(insert_teknik_ozellik)] = {
                'uretim_altyapi_id': uretim_altyapi_id,
                'deger1': to['deger1'],
                'deger2': to['deger2'],
                'birim': to['birim'],
                'deger_tipi': to['deger_tipi'],
                'teknik_ozellik': to['teknik_ozellik'],
                'aktif_mi': 'true',
            }

        Model(ReaPy.get_reactor_source('yeten')).insert('ua_teknik_ozellik', insert_teknik_ozellik)

    def get_prodtr_like(self, bilgi):
        try:
            aa = Model(ReaPy.get_reactor_source('yeten')).select(table='public.sys_prodtr',
                                                                 field='prodtr_id,kodu,adi,birim',
                                                                 condition={
                                                                     0: {'col': 'kodu', 'operator': 'LIKE',
                                                                         'value': '%' + bilgi + '%',
                                                                         'combiner': 'OR'},
                                                                     1: {'col': 'adi', 'operator': 'LIKE',
                                                                         'value': '%' + bilgi + '%',
                                                                         'combiner': 'OR'},
                                                                     2: {'col': 'birim', 'operator': 'LIKE',
                                                                         'value': '%' + bilgi + '%'}
                                                                 },
                                                                 sort='adi')
            print(aa.error())
            return aa.data()

        except Exception as exc:
            print(exc)
        return None

    def get_uretim_genel_bilgiler(self):

        tumu = {}
        makine_sinifi = Model(ReaPy.get_reactor_source('yeten')).select(table='sys_makine_sinifi', sort='adi',
                                                                        cache=True).data()
        yerli_ithal = Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='sys_tanim',
                    field='deger, aciklama',
                    condition={0: {'col': 'kolon', 'operator': '=', 'value': 'yerli_ithal'}
                               },
                    sort='aciklama').data()

        tumu['makine_sinifi'] = makine_sinifi
        tumu['yerli_ithal'] = yerli_ithal

        return tumu

    def get_uretim_surec_paydas(self, paydas_id):

        return Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='view_paydas_uretim_alt_yapi_surec',
                    field='uretim_surec_id, surec_id, surec_diger, aciklama',
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': paydas_id},
                    },
                    sort='aciklama').data()

    def get_uretim_kalite_paydas(self, paydas_id):

        return Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='view_paydas_uretim_alt_yapi_kalite',
                    field='uretim_kalite_id, kalite_id, kalite_diger, aciklama',
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': paydas_id}
                    },
                    sort='aciklama').data()

    def insert_uretim_surec(self, deger, paydas_id, tckn, olusturan_ip):

        insert_data = {
            'surec_id': deger, 'paydas_id': paydas_id, 'aktif_mi': 'true',
            "olusturan_ip": olusturan_ip,
            "olusturan_id": tckn,
        }

        return Model(ReaPy.get_reactor_source('yeten')).insert('ua_uretim_surec', {
            0: insert_data
        }).data()

    def insert_uretim_kalite(self, deger, tckn, paydas_id, olusturan_ip):

        insert_data = {
            'kalite_id': deger, 'paydas_id': paydas_id, 'aktif_mi': 'true',
            "olusturan_ip": olusturan_ip,
            "olusturan_id": tckn,
        }

        return Model(ReaPy.get_reactor_source('yeten')).insert('ua_uretim_kalite', {
            0: insert_data
        }).data()

    def get_uretim_kalite(self):
        return Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='sys_tanim',
                    field='deger, aciklama',
                    condition={0: {'col': 'kolon', 'operator': '=', 'value': 'kalite_id', 'combiner': 'AND'},
                               1: {'col': 'tablo', 'operator': '=', 'value': 'ua_uretim_kalite'},
                               },
                    sort='aciklama').data()

    def get_uretim_surec(self):
        return Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='sys_tanim',
                    field='deger, aciklama',
                    condition={0: {'col': 'kolon', 'operator': '=', 'value': 'surec_id', 'combiner': 'AND'},
                               1: {'col': 'tablo', 'operator': '=', 'value': 'ua_uretim_surec'},
                               },
                    sort='aciklama').data()

    def delete_uretim_surec(self, uretim_surec_id, paydas_id):

        return Model(ReaPy.get_reactor_source('yeten')).delete('ua_uretim_surec',
                                                               {
                                                                   0: {'col': 'paydas_id', 'operator': '=',
                                                                       'value': paydas_id, 'combiner': 'AND'},
                                                                   1: {'col': 'uretim_surec_id', 'operator': '=',
                                                                       'value': uretim_surec_id},
                                                               })

    def delete_uretim_kalite(self, uretim_kalite_id, paydas_id):

        return Model(ReaPy.get_reactor_source('yeten')).delete('ua_uretim_kalite',
                                                               {
                                                                   0: {'col': 'paydas_id', 'operator': '=',
                                                                       'value': paydas_id, 'combiner': 'AND'},
                                                                   1: {'col': 'uretim_kalite_id', 'operator': '=',
                                                                       'value': uretim_kalite_id},
                                                               })
