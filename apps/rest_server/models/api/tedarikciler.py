# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Tedarikciler(object):

    def __init__(self):
        view_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'
        self.messages = ReaPy.presenter().get_message(view_path, 'messages')['paydas']

  
    # noinspection PyMethodMayBeStatic
    def get_tedarikci(self, firmaadi,firmatype):

        print(firmatype.lower())
        print(firmaadi.lower())
        try:
            tedarikci = Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_yerli_yabanci_tum_firmalar',
                condition={
                    0: {'col': 'LOWER(paydas_adi)', 'operator': 'LIKE', 'value': '%'+firmaadi.lower()+'%', 'combiner': 'AND'}, 
                    1: {'col': 'yerli_yabanci', 'operator': '=', 'value': firmatype.lower()}  
                    }).data()
            tedarikci.append({'paydas_id':'','paydas_adi':'Diğer'})
            return tedarikci

        except Exception as exc:
            print(__file__ + ' get_detay_basvuru')
            print(exc)

        return None

  


