# -*- coding: utf-8 -*-

from core.reapy import ReaPy

try:
    use_mediation = ReaPy.configuration().get_configuration()['system']['use_mediation']
    if use_mediation is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Tamevcutaltyapi(object):

    # def get_paydas_id_from_tckn(self, tckn):
    #     paydas_id = ''
    #
    #     try:
    #         list = Model(ReaPy.get_reactor_source('yeten')) \
    #             .select(table='view_paydas_basvuru',
    #                     field='basvuru_id',
    #                     condition={0: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': tckn}},
    #                     is_first=True,
    #                     sort='basvuru_tarihi DESC').data()
    #
    #         paydas_id = list[0]['basvuru_id']
    #
    #     except Exception as exc:
    #         print(__file__ + ' get_paydas_id_from_tckn')
    #         print(exc)
    #
    #     return paydas_id

    def get_bilesenler(self, user_identity_no):

        paydas_id = self.get_paydas_id_from_tckn(user_identity_no)

        if paydas_id == '':
            return None

        try:

            query = Model(ReaPy.get_reactor_source('yeten')).select(table='view_paydas_bilesen',
                                                                         condition={
                                                                             0: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         })

            print(query.error())

            return query.data()
        except Exception as exc:
            print(__file__ + ' get_bilesenler')
            print(exc)

        return None

    def insert(self, data, user_identity_no):

        paydas_id = self.get_paydas_id_from_tckn(user_identity_no)

        if paydas_id == '':
            return None

        insert_data = {
            "paydas_id": paydas_id,
            "bilesen_turu": data['bilesen_turu'],
            "bilesen_adi": data['bilesen_adi'],
            "marka_model": data['marka_model'],
            "ssb_payi": data['ssb_payi'],
            "ssb_proje_adi": data['ssb_proje_adi'],
            "bilesen_teknik_ozellik": data['bilesen_teknik_ozellik'],
            "bilesen_turu_diger": self.set_val_safe(data, 'bilesen_turu_diger', True),
            "bilesen_marka_model_diger": self.set_val_safe(data, 'bilesen_marka_model_diger', True),
            "aktif_mi": 'true'
        }
        query = Model(ReaPy.get_reactor_source('yeten')).insert('ta_bilesen', {0: insert_data})

        print("insert. bilesen insert ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def update(self, data):

        update_data = {}

        if self.set_val_safe(data, 'bilesen_turu') != '':
            update_data['bilesen_turu'] = data['bilesen_turu']

        if self.set_val_safe(data, 'bilesen_adi') != '':
            update_data['bilesen_adi'] = data['bilesen_adi']

        if self.set_val_safe(data, 'bilesen_turu_diger') != '':
            update_data['bilesen_turu_diger'] = data['bilesen_turu_diger']

        if self.set_val_safe(data, 'marka_model') != '':
            update_data['marka_model'] = data['marka_model']

        if self.set_val_safe(data, 'bilesen_marka_model_diger') != '':
            update_data['bilesen_marka_model_diger'] = data['bilesen_marka_model_diger']

        if self.set_val_safe(data, 'ssb_payi') != '':
            update_data['ssb_payi'] = data['ssb_payi']

        if self.set_val_safe(data, 'ssb_proje_adi') != '':
            update_data['ssb_proje_adi'] = data['ssb_proje_adi']

        if self.set_val_safe(data, 'bilesen_teknik_ozellik') != '':
            update_data['bilesen_teknik_ozellik'] = data['bilesen_teknik_ozellik']

        query = Model(ReaPy.get_reactor_source('yeten')).update('ta_bilesen', {'SET': update_data,
                                                                                    'CONDITION': {
                                                                                        0: {'col': 'bilesen_id',
                                                                                            'operator': '=',
                                                                                            'value': data['bilesen_id']}
                                                                                    }})

        print("insert. bilesen update ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def delete(self, bilesen_id):

        query = Model(ReaPy.get_reactor_source('yeten')).update('ta_bilesen', {'SET': {'aktif_mi': 'false'},
                                                                                    'CONDITION': {
                                                                                        0: {'col': 'bilesen_id',
                                                                                            'operator': '=',
                                                                                            'value': bilesen_id}
                                                                                    }
                                                                                    })

        print("insert. bilesen delete ->  ")
        print(query.error())

        if query.error() is None:
            return query.data()

        return None

    def get_marka_model(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_bilesen_marka_model',
                                                                    sort='marka_model').data()

    def get_test_ana_basligi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_test_altyapi_ana_basligi_secim',
                                                                    field=' test_alt_yapi_adi, adi ',
                                                                    sort='adi').data()

    def get_test_altyapi_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='ta_test_alt_yapi',
                                                                    field='test_alt_yapi_id, test_alt_yapi_adi',
                                                                    sort='test_alt_yapi_adi').data()

    def get_test_kriter_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_test_altyapi_test_kriter_secim',
                                                                    sort='ta_test_kriteri_adi').data()

    def get_test_standart_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_test_altyapi_standart_secim',
                                                                    sort='standart_adi').data()

    def get_test_metod_secim(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_test_altyapi_method_secim',
                                                                    sort='metod_adi').data()

    # noinspection PyMethodMayBeStatic
    def set_val_safe(self, data, field_name, is_set_null=False):
        field_value = ''
        try:
            field_value = data[field_name]
        except KeyError as exc:
            print('set_val_safe Exception -> ')
            print(exc)
            if is_set_null:
                field_value = None

        return field_value
