# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Ik(object):

    def get_ik_paydaslar(self, user_identity_no):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_paydas_ik', condition=
        {0: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': user_identity_no}}).data()

    def get_one_ik(self, ik_id, user_identity_no):
        ik_detay = None
        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_paydas_ik',
                condition=
                {
                    0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id}
                    #0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id, 'combiner': 'AND'},
                    #1: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': user_identity_no}
                }
            ).data())
            if list is not None:
                ik_detay = list[0]

            ik_detay['aktif_ogrenci'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='ik_aktif_ogrenci',
                field='derece,personel_sayisi,fakulte_id,donem',
                condition=
                {
                    0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id, 'combiner': 'AND'},
                    1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                }
            ).data()

            lisans_view = None
            arge_lisans_bolum = []
            arge_lisans_bolum_tanim = []

            try:
                lisans_view = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='view_ik_arge_lisans_bolum',
                    field='lisans_bolum_id, personel_sayisi, donem, bolum_adi ',
                    condition=
                    {
                        0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id}
                    }
                ).data()
            except Exception as exc:
                print(exc)

            if lisans_view is not None:
                for item in lisans_view:
                    arge_lisans_bolum_tanim.append(item['bolum_adi'])
                    arge_lisans_bolum.append(item)

            ik_detay['arge_lisans_bolum'] = arge_lisans_bolum
            ik_detay['arge_lisans_bolum_tanim'] = arge_lisans_bolum_tanim

            arge_uni = None
            arge_universite = []
            arge_universite_tanim = []

            try:
                arge_uni = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='view_ik_arge_universite',
                    field='universite_id, personel_sayisi, donem, universite_adi',
                    condition=
                    {0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id},
                     }
                ).data()

            except Exception as exc:
                print(exc)

            if arge_uni is not None:
                for itemi in arge_uni:
                    arge_universite_tanim.append(itemi['universite_adi'])
                    arge_universite.append(itemi)

            ik_detay['arge_universite'] = arge_universite
            ik_detay['arge_universite_tanim'] = arge_universite_tanim

            ik_detay['egitim_durumu'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='ik_egitim_durumu',
                field='egitim_durumu,personel_sayisi, donem',
                condition=
                {0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id, 'combiner': 'AND'},
                 1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                 }
            ).data()

            ik_detay['personel_dagilimi'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='ik_personel_dagilimi',
                field='dagilim,personel_sayisi, donem',
                condition=
                {0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id, 'combiner': 'AND'},
                 1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                 }
            ).data()

            ik_detay['personel_unvani'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='ik_personel_unvani',
                field='unvan,personel_sayisi, donem',
                condition=
                {0: {'col': 'paydas_id', 'operator': '=', 'value': ik_id, 'combiner': 'AND'},
                 1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                 }
            ).data()

        except Exception as exc:
            print(__file__, exc)

        return ik_detay

    def ik_insert(self, data, paydas_id, tckn, ip):

        insert_data = {
            "personel_sayisi": data['personel_sayisi'],
            "mavi_yaka_sayisi": data['mavi_yaka_sayisi'],
            "beyaz_yaka_sayisi": data['beyaz_yaka_sayisi'],
            "aktif_yukseklisans_ogrenci_orani": data['aktif_yukseklisans_ogrenci_orani'],
            "personel_calisan_devir_orani": Helpers().set_val_safe(data, 'personel_calisan_devir_orani', True),
            "aktif_doktora_ogrenci_orani": data['aktif_doktora_ogrenci_orani'],
            "deneyim_ortalamasi": data['deneyim_ortalamasi'],
            "ss_deneyim_ortalamasi": Helpers().set_val_safe(data, 'ss_deneyim_ortalamasi', True),
            "yas_ortalamasi": data['yas_ortalamasi'],
            "erkek_calisan_sayisi": data['erkek_calisan_sayisi'],
            "kadin_calisan_sayisi": data['kadin_calisan_sayisi'],
            "akademik_yayin_sayisi": data['akademik_yayin_sayisi'],
            "sayp_ogrenci_sayisi": data['sayp_ogrenci_sayisi'],
            "kadin_calisan_orani": data['kadin_calisan_orani'],
            "arge_5746_mi": data['arge_5746_mi'],
            "arge_5746_personel_sayisi": Helpers().set_val_safe(data, 'arge_5746_personel_sayisi', True),
            "arge_5746_toplam_personele_orani": Helpers().set_val_safe(data,
                                                                       'arge_5746_toplam_personele_orani',
                                                                       True),
            "arge_5746_deneyim_ortalamasi": Helpers().set_val_safe(data, 'arge_5746_deneyim_ortalamasi',
                                                                   True),
            'arge_personel_sayisi': Helpers().set_val_safe(data, 'arge_personel_sayisi', True),
            'arge_toplam_personele_orani': Helpers().set_val_safe(data, 'arge_toplam_personele_orani', True),
            'arge_deneyim_ortalamasi': Helpers().set_val_safe(data, 'arge_deneyim_ortalamasi', True),
            'sayp_protokolu_var_mi': data['sayp_protokolu_var_mi'],
            'patent_5746_faydali_model_orani': data['patent_5746_faydali_model_orani'],
            'aktif_mi': 'true',
            'paydas_id': paydas_id,
            'olusturan_id': tckn,
            'olusturan_ip': ip
        }
        query = Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_paydas', {0: insert_data})

        print("insert. ik_paydas -> ")
        print(query.error())

        if query.error() is None:

            unvanlar = Helpers().set_val_safe(data, 'unvanlar', True)
            dagilimlar = Helpers().set_val_safe(data, 'dagilimlar', True)
            aktif_doktora = Helpers().set_val_safe(data, 'aktif_doktora', True)
            aktif_yuksek_lisans = Helpers().set_val_safe(data, 'aktif_yuksek_lisans', True)
            egitim_durumlari = Helpers().set_val_safe(data, 'egitim_durumlari', True)
            ik_arge_universite = Helpers().set_val_safe(data, 'ik_arge_universite', True)
            ik_arge_lisans_bolum = Helpers().set_val_safe(data, 'ik_arge_lisans_bolum', True)

            if aktif_doktora is not None:
                self.insert_aktif_doktora(aktif_doktora, paydas_id, None, tckn, ip)

            if aktif_yuksek_lisans is not None:
                self.insert_aktif_yuksek_lisans(aktif_yuksek_lisans, paydas_id, None, tckn, ip)

            if ik_arge_lisans_bolum is not None:
                self.insert_ik_arge_lisans_bolum(ik_arge_lisans_bolum, paydas_id, None, tckn, ip)

            if ik_arge_universite is not None:
                self.insert_ik_arge_universite(ik_arge_universite, paydas_id, None, tckn, ip)

            if unvanlar is not None:
                self.insert_unvanlar(unvanlar, paydas_id, None, tckn, ip)

            if dagilimlar is not None:
                self.insert_dagilimlar(dagilimlar, paydas_id, None, tckn, ip)

            if egitim_durumlari is not None:
                self.insert_egitim_durumlari(egitim_durumlari, paydas_id, None, tckn, ip)

        return query.data()

    # noinspection PyMethodMayBeStatic
    def insert_aktif_doktora(self, aktif_doktora, paydas_id, is_before_delete=None, tckn='', ip=''):

        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_aktif_ogrenci',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id,
                                                                         'combiner': 'AND'},
                                                                     1: {'col': 'derece', 'operator': '=',
                                                                         'value': 2}
                                                                 })

        for key in aktif_doktora:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_aktif_ogrenci', {0:
                {
                    'personel_sayisi': aktif_doktora[key],
                    'derece': 2,
                    'fakulte_id': key,
                    'paydas_id': paydas_id,
                    'aktif_mi': 'true',
                    'olusturan_ip': ip,
                    'olusturan_id': tckn
                }
            })

    # noinspection PyMethodMayBeStatic
    def insert_aktif_yuksek_lisans(self, aktif_yuksek_lisans, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_aktif_ogrenci',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id, 'combiner': 'AND'},
                                                                     1: {'col': 'derece', 'operator': '=',
                                                                         'value': 1}
                                                                 })

        for key in aktif_yuksek_lisans:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_aktif_ogrenci', {0: {
                'personel_sayisi': aktif_yuksek_lisans[key],
                'derece': 1,
                'fakulte_id': key,
                'paydas_id': paydas_id,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }})

    # noinspection PyMethodMayBeStatic
    def insert_ik_arge_lisans_bolum(self, ik_arge_lisans_bolum, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_arge_lisans_bolum',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id}
                                                                 })

        for key in ik_arge_lisans_bolum:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_arge_lisans_bolum', {0: {
                'personel_sayisi': key['personel_sayisi'],
                'lisans_bolum_id': key['lisans_bolum_id'],
                'paydas_id': paydas_id,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }}).data()

    # noinspection PyMethodMayBeStatic
    def insert_ik_arge_universite(self, ik_arge_universite, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_arge_universite',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id}
                                                                 })

        for key in ik_arge_universite:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_arge_universite', {0: {
                'personel_sayisi': key['personel_sayisi'],
                'universite_id': key['universite_id'],
                'paydas_id': paydas_id,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }})

    # noinspection PyMethodMayBeStatic
    def insert_unvanlar(self, unvanlar, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_personel_unvani',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id}
                                                                 })

        for key in unvanlar:
            Model(ReaPy.get_reactor_source('yeten')).insert('ik_personel_unvani', {0: {
                'personel_sayisi': unvanlar[key],
                'paydas_id': paydas_id,
                'unvan': key,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }})

    # noinspection PyMethodMayBeStatic
    def insert_dagilimlar(self, dagilimlar, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_personel_dagilimi',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id}
                                                                 })

        for key in dagilimlar:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_personel_dagilimi', {0: {
                'personel_sayisi': dagilimlar[key],
                'paydas_id': paydas_id,
                'dagilim': key,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }})

    # noinspection PyMethodMayBeStatic
    def insert_egitim_durumlari(self, egitim_durumlari, paydas_id, is_before_delete=None, tckn='', ip=''):
        if is_before_delete is not None:
            Model(ReaPy.get_reactor_source('yeten')).delete('public.ik_egitim_durumu',
                                                                 {
                                                                     0: {'col': 'paydas_id',
                                                                         'operator': '=',
                                                                         'value': paydas_id}
                                                                 })

        for key in egitim_durumlari:
            Model(ReaPy.get_reactor_source('yeten')).insert('public.ik_egitim_durumu', {0: {
                'personel_sayisi': egitim_durumlari[key],
                'paydas_id': paydas_id,
                'egitim_durumu': key,
                'aktif_mi': 'true',
                'olusturan_ip': ip,
                'olusturan_id': tckn
            }})

    # noinspection PyMethodMayBeStatic
    def basvuru_update(self, data, basvuru_id, tckn, ip):

        update_data = {'guncelleyen': tckn, 'guncelleyen_ip': ip, 'guncelleme_tarihi': str(datetime.datetime.now())}

        if Helpers().set_val_safe(data, 'personel_sayisi') != '':
            update_data['personel_sayisi'] = data['personel_sayisi']

        if Helpers().set_val_safe(data, 'mavi_yaka_sayisi') != '':
            update_data['mavi_yaka_sayisi'] = data['mavi_yaka_sayisi']

        if Helpers().set_val_safe(data, 'beyaz_yaka_sayisi') != '':
            update_data['beyaz_yaka_sayisi'] = data['beyaz_yaka_sayisi']

        if Helpers().set_val_safe(data, 'aktif_yukseklisans_ogrenci_orani') != '':
            update_data['aktif_yukseklisans_ogrenci_orani'] = data['aktif_yukseklisans_ogrenci_orani']

        if Helpers().set_val_safe(data, 'aktif_doktora_ogrenci_orani') != '':
            update_data['aktif_doktora_ogrenci_orani'] = data['aktif_doktora_ogrenci_orani']

        if Helpers().set_val_safe(data, 'sayp_ogrenci_sayisi') != '':
            update_data['sayp_ogrenci_sayisi'] = data['sayp_ogrenci_sayisi']

        if Helpers().set_val_safe(data, 'personel_calisan_devir_orani') != '':
            update_data['personel_calisan_devir_orani'] = data['personel_calisan_devir_orani']

        if Helpers().set_val_safe(data, 'deneyim_ortalamasi') != '':
            update_data['deneyim_ortalamasi'] = data['deneyim_ortalamasi']

        if Helpers().set_val_safe(data, 'ss_deneyim_ortalamasi') != '':
            update_data['ss_deneyim_ortalamasi'] = data['ss_deneyim_ortalamasi']

        if Helpers().set_val_safe(data, 'yas_ortalamasi') != '':
            update_data['yas_ortalamasi'] = data['yas_ortalamasi']

        if Helpers().set_val_safe(data, 'kadin_calisan_sayisi') != '':
            update_data['kadin_calisan_sayisi'] = data['kadin_calisan_sayisi']

        if Helpers().set_val_safe(data, 'erkek_calisan_sayisi') != '':
            update_data['erkek_calisan_sayisi'] = data['erkek_calisan_sayisi']

        if Helpers().set_val_safe(data, 'kadin_calisan_orani') != '':
            update_data['kadin_calisan_orani'] = data['kadin_calisan_orani']

        if Helpers().set_val_safe(data, 'akademik_yayin_sayisi') != '':
            update_data['akademik_yayin_sayisi'] = data['akademik_yayin_sayisi']

        if Helpers().set_val_safe(data, 'arge_5746_personel_sayisi') != '':
            update_data['arge_5746_personel_sayisi'] = data['arge_5746_personel_sayisi']

        if Helpers().set_val_safe(data, 'arge_5746_toplam_personele_orani') != '':
            update_data['arge_5746_toplam_personele_orani'] = data['arge_5746_toplam_personele_orani']

        if Helpers().set_val_safe(data, 'arge_5746_deneyim_ortalamasi') != '':
            update_data['arge_5746_deneyim_ortalamasi'] = data['arge_5746_deneyim_ortalamasi']

        if Helpers().set_val_safe(data, 'patent_5746_faydali_model_orani') != '':
            update_data['patent_5746_faydali_model_orani'] = data['patent_5746_faydali_model_orani']

        if Helpers().set_val_safe(data, 'arge_personel_sayisi') != '':
            update_data['arge_personel_sayisi'] = data['arge_personel_sayisi']

        if Helpers().set_val_safe(data, 'arge_toplam_personele_orani') != '':
            update_data['arge_toplam_personele_orani'] = data['arge_toplam_personele_orani']

        if Helpers().set_val_safe(data, 'arge_deneyim_ortalamasi') != '':
            update_data['arge_deneyim_ortalamasi'] = data['arge_deneyim_ortalamasi']

        if Helpers().set_val_safe(data, 'olusturulma_tarihi') != '':
            update_data['olusturulma_tarihi'] = data['olusturulma_tarihi']

        if Helpers().set_val_safe(data, 'sayp_protokolu_var_mi') != '':
            update_data['sayp_protokolu_var_mi'] = data['sayp_protokolu_var_mi']

        if Helpers().set_val_safe(data, 'arge_5746_mi') != '':
            update_data['arge_5746_mi'] = data['arge_5746_mi']

        query = Model(ReaPy.get_reactor_source('yeten')).update('public.ik_paydas', {'SET': update_data,
                                                                                          'CONDITION': {
                                                                                              0: {'col': 'paydas_id',
                                                                                                  'operator': '=',
                                                                                                  'value': basvuru_id}
                                                                                          }})
        print(query.data())

        if query.error() is None:
            unvanlar = Helpers().set_val_safe(data, 'unvanlar', True)
            dagilimlar = Helpers().set_val_safe(data, 'dagilimlar', True)
            aktif_doktora = Helpers().set_val_safe(data, 'aktif_doktora', True)
            aktif_yuksek_lisans = Helpers().set_val_safe(data, 'aktif_yuksek_lisans', True)
            egitim_durumlari = Helpers().set_val_safe(data, 'egitim_durumlari', True)
            ik_arge_universite = Helpers().set_val_safe(data, 'ik_arge_universite', True)
            ik_arge_lisans_bolum = Helpers().set_val_safe(data, 'ik_arge_lisans_bolum', True)

            if aktif_doktora is not None:
                self.insert_aktif_doktora(aktif_doktora, data['paydas_id'], True, tckn, ip)

            if aktif_yuksek_lisans is not None:
                self.insert_aktif_yuksek_lisans(aktif_yuksek_lisans, data['paydas_id'], True, tckn, ip)

            if ik_arge_lisans_bolum is not None:
                self.insert_ik_arge_lisans_bolum(ik_arge_lisans_bolum, data['paydas_id'], True, tckn, ip)

            if ik_arge_universite is not None:
                self.insert_ik_arge_universite(ik_arge_universite, data['paydas_id'], True, tckn, ip)

            if unvanlar is not None:
                self.insert_unvanlar(unvanlar, data['paydas_id'], True, tckn, ip)

            if dagilimlar is not None:
                self.insert_dagilimlar(dagilimlar, data['paydas_id'], True, tckn, ip)

            if egitim_durumlari is not None:
                self.insert_egitim_durumlari(egitim_durumlari, data['paydas_id'], True, tckn, ip)

        return query.data()

    # noinspection PyMethodMayBeStatic
    def basvuru_id_kontrol(self, basvuru_id, session_user_identity_no, yetkili_tc_no):

        kontrol = Model(ReaPy.get_reactor_source('yeten')).select(
            table='view_paydas_ik',
            condition={
                0:
                    {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id, 'combiner': 'AND'},
                1:
                    {'col': 'aktif_mi', 'operator': '=', 'value': 'true', 'combiner': 'AND'},
                2: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': session_user_identity_no}
            }).data()
        if kontrol is None:
            kontrol_iki = Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_paydas_ik',
                condition={
                    0:
                        {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id, 'combiner': 'AND'},
                    1:
                        {'col': 'aktif_mi', 'operator': '=', 'value': 'true', 'combiner': 'AND'},
                    2: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': yetkili_tc_no}
                }).data()
            return kontrol_iki
        return kontrol

    def get_ik_info(self, universite_mi=False):

        sort = 'bolum_adi'
        alan = 'lisans_bolum_id,bolum_adi'
        tablo = 'sys_lisans_bolum'

        if universite_mi:
            sort = 'universite_adi'
            alan = 'universite_id,universite_adi'
            tablo = 'sys_universite'

        query = Model(ReaPy.get_reactor_source('yeten')).select(tablo, alan, sort=sort)
        print(query.error())

        return query.data()
