# -*- coding: utf-8 -*-

import base64
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Dispaydas(object):

    def __init__(self):
        # self.geodi_feed_key = 'EAAAAAE64%2FTtpKbnA4xt%2FU3tOm3hYd4BCspuwrcJLUHrpGIRyxIA%2BLXrcD4gZyouJzq1wgh5NuKTcxArTCqe%2B9Fys8nYM4APoXVXHaGz1Da%2BS5kNKXzDhsTPGDUbszjybLN0PhHV3dbK%2B43muICOJkxZQr5r5WHqyriBX1lM%2B%2FECToQIWyZNWjAldDN0yFd79dMg7H3oSMMgK5AiEIJsxXL78hecB0cuJ%2FYOBMklIfG9gEMn8nHIXBG3ae3OUVPFN3VrrbS%2BokMv40q5MerRcD0cnnv3BIU%2FacRgCfe14xKIc8JzUPlOSrpGqwAr7XM8Og5ut0IpiSJ6vQqgOBrX4qgHNgH%2BbtNqP%2BzbN733BLTAbTWv1bYEwh1RrZ3MKOzvneexQDIvXRjEF3fzYoQGNyWdux9bBGvCOdJm8NIPnqCBrx4I'
        # self.geodi_feed_key = 'EAAAAL3xZjugsk9dd9pM1UoMv8jTVjeaew5hFvKXbmeLCVODO3ZnxqCg12FvM9vi%2BGsPKKeBuIkUtR7w7F3PSs0P8I08LrspQ%2B%2F3HrqwmuF9A%2BJnQ%2BskQgcwLYdL5iaG4WacdHR39Ujt3VeTTsaCFPnVHm3IkPANv7JvwBrptvbj3zowdNXlM9pk5XId2XcIL6KZ%2BL5psYRfL3%2B4IAjTTy9L95Uc7Nu8THsr2GzLn2okJZLCMb1sshIE4p92VEvykEnVVRI6mlWy2bSthC386vEGF53L%2BGszYfbubexKLRD9wAnLg64lBpReC42uSmqvCdjk1m8yWjm2r3Z3lY1zDLlYHhgIVoJVaTUs%2BlClP3AnGjceJbSrz1FG9Q4R9iXdb%2FAdJjBnjCbO%2B2D4XrtLTGSVRnrnOiVR8tU0L%2BaItqKVwlvrdaNkl64NpbPjzr7GPBYFVA%3D%3D'
        self.geodi_feed_key = 'EAAAAJLvZqabdBNQn1h7psq2WC9IUy8DaU%2F1U3G%2BRxaSdASQ8AmW4CniVv60mOrir4zgYvsMzpbbCLgjm9yYjlahmocDrVReCepXmhkhv8XyNaTYXKNUiHNmH84dLJcY0%2BT9SaUBmxG1JBVDnt8X6dtdIASf%2B1UQZZYUk%2F5Fp3ZVxz3MyyqG%2FKl4OKvo1p4FQp9fE9ElgXj5BXma1uXWyyO3blYq1sRkQhSUHLCnFMG9sU1b6qWxb74vmhh1A8kcHuI1sVL9ggP%2FOkdoUvtN4jwI85Aj%2FT5ONhkrWRdnjspIY8IDxD8QqvHqZosPdg7z%2Fj%2Fq%2BBw25mHFR9olX1VaJLUC0j7BzH6DfJiqEp3FaqvDa32wnGldZujBOIF2gIDOfqKhAKmlGMGEPGUTUOk08qW9EmY%3D'
        self.geodi_feed_api_url = 'http://10.10.46.15:3323/'
        view_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'
        self.messages = ReaPy.presenter().get_message(view_path, 'messages')['paydas']

    def user_domain_account_is_exist(self, user_identity_no):

        try:

            condititon_data = {
                0: {'col': 'yetkili_kisi_tckn',
                    'operator': '=',
                    'value': user_identity_no}
            }

            durum = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_paydas_basvuru',
                                                                    field='basvuru_durum',
                                                                    condition=condititon_data).data()

            if durum is None:
                con_data = {
                    0: {'col': 'aktif_mi', 'operator': '=', 'value': True, 'combiner': 'AND'},
                    1: {'col': 'tc', 'operator': '=', 'value': user_identity_no}
                }
                kullanici_talep_data = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_kullanici_talep',
                                                                                       condition=con_data).data()
                if kullanici_talep_data is not None:
                    return False
                else:
                    return True
            else:
                if int(durum[0]['basvuru_durum']) == 3:
                    Model(ReaPy.get_reactor_source('yeten')).delete('dis_paydas_basvuru',
                                                                    {0: {'col': 'yetkili_kisi_tckn',
                                                                         'operator': '=',
                                                                         'value': user_identity_no}})
                    return True
                else:
                    return False
        except Exception as exc:
            print(exc)

        return False

    def file_name_save(self, file_name, dosya_tam_yolu, paydas_id=None):

        try:

            insert_data = {
                'dosya_adi': file_name,
                'aktif_mi': 'true',
                'dosya_turu': 'bilinmiyor',
                'dosya_tam_yolu': dosya_tam_yolu
            }

            if paydas_id is not None:
                insert_data['paydas_id'] = paydas_id

            insert = Model(ReaPy.get_reactor_source('yeten')).insert('public.dosya_yukleme',
                                                                     {
                                                                         0: insert_data
                                                                     }
                                                                     )

            print('insert.error()', insert.error())

            select = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dosya_yukleme',
                                                                     field='dosya_yukleme_id',
                                                                     condition={0: {'col': 'dosya_tam_yolu',
                                                                                    'operator': '=',
                                                                                    'value': dosya_tam_yolu}},
                                                                     is_first=True
                                                                     )
            result = select.data()
            return result[0]['dosya_yukleme_id']
        except Exception as exc:
            print(__file__ + ' file_name_save Exception')
            print(exc)

        return ''

    def get_paydas_tanim(self, user_identity_no):

        detay = {}
        condition_val = {
            0: {'col': 'paydas_id', 'operator': '=', 'value': user_identity_no}
        }
        print(user_identity_no)
        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                        .select(table='view_paydas',
                                                                condition=condition_val,
                                                                is_first=True,
                                                                sort='olusturma_tarihi DESC').data())

            detay = list[0]
            firma_arr_yerli = []
            firma_arr_yabanci = []
            onaylitearikci = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                                  .select(table='public.paydas_onayli_tedarikcileri',
                                                                          condition={0: {'col': 'paydas_id',
                                                                                         'operator': '=',
                                                                                         'value': user_identity_no,
                                                                                         'combiner': 'AND'},
                                                                                     1: {'col': 'aktif_mi',
                                                                                         'operator': '=',
                                                                                         'value': True}}).data())

            kalite_yonetim = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                                  .select(
                table='public.view_paydas_kaliteyonetim_belgesi',
                condition={0: {'col': 'paydas_id',
                               'operator': '=',
                               'value': user_identity_no,
                               'combiner': 'AND'},
                           1: {'col': 'aktif_mi',
                               'operator': '=',
                               'value': True}}).data())
            detay['kalite_yonetim'] = kalite_yonetim

            for item in onaylitearikci:
                if item['onayli_tedarikcisi_id'] is not None:
                    firma = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                                 .select(table='view_yerli_yabanci_tum_firmalar',
                                                                         condition={0: {'col': 'paydas_id',
                                                                                        'operator': '=',
                                                                                        'value': item[
                                                                                            'onayli_tedarikcisi_id']}}).data())
                else:
                    firma = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                                 .select(table='view_yerli_yabanci_tum_firmalar',
                                                                         condition={0: {'col': 'paydas_id',
                                                                                        'operator': '=',
                                                                                        'value': item[
                                                                                            'yabanci_firma_id']}}).data())

                if item['yabanci_firma_diger'] is not None:
                    detay['yabancidigertedarikci'] = item['yabanci_firma_diger']
                if item['onayli_tedarikci_diger'] is not None:
                    detay['yerlidigertedarikci'] = item['onayli_tedarikci_diger']

                if len(firma) > 0:
                    # print(firma[0]['yerli_yabanci'])
                    if firma[0]['yerli_yabanci'] == 'yabanci':
                        firma_arr_yabanci.append(firma[0])
                    else:
                        firma_arr_yerli.append(firma[0])

            request_basvuru_id = detay['paydas_id']
            detay['yerli_onayli_tedarikcisi_id'] = firma_arr_yerli
            detay['yabanci_onayli_tedarikcisi_id'] = firma_arr_yabanci
            detay['tesisler'] = Helpers().payload_parameters_control(
                Model(ReaPy.get_reactor_source('yeten')).select(
                    table='paydas_tesis',
                    condition=
                    {
                        0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                        1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                    }).data())

            detay['yetenek_bilgisi'] = self.get_detay_tanim_iliski(request_basvuru_id, 'view_paydas_yetenek_bilgisi',
                                                                   ['yetenek_adi'])

            detay['faaliyet_alanlari'] = self.get_detay_tanim_iliski(request_basvuru_id, 'view_paydas_faaliyet_alani',
                                                                     ['faaliyet_alani'])

            detay['nace_kodlari'] = self.get_detay_tanim_iliski(request_basvuru_id, 'view_paydas_nace',
                                                                ['nace_adi'])

            detay['sahiplik_yapisi'] = self.get_detay_tanim_iliski(request_basvuru_id, 'view_paydas_sahiplik_yapisi',
                                                                   ['sahip_adi', 'ortaklik_yuzdesi'])

            detay['istirak_orani'] = self.get_detay_tanim_iliski(request_basvuru_id,
                                                                 'view_paydas_firma_istirakler',
                                                                 ['istirak_adi', 'orani'])

            detay['savunma_faaliyet_alanlari'] = self.get_detay_tanim_iliski(
                request_basvuru_id, 'view_paydas_savunma_faaliyet_alani',
                ['savunma_faaliyet_adi'])

            detay['dosyalar'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_paydas_basvuru_dosya',
                field=' dosya_yukleme_id, dosya_adi, dosya_turu, basvuru_id ',
                condition={
                    0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                    1: {'col': 'dosya_turu', 'operator': '!=', 'value': 'null'}
                }) \
                .data()
            return detay

        except Exception as exc:
            print(__file__ + ' get_detay_basvuru1')
            print(exc)

        return None

    # noinspection PyMethodMayBeStatic
    def get_detay_basvuru_tc(self, user_identity_no):

        detay = {}
        condition_val = {
            0: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': user_identity_no}
        }

        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                        .select(table='view_paydas_basvuru',
                                                                condition=condition_val,
                                                                is_first=True,
                                                                sort='basvuru_tarihi DESC').data())
            if len(list) > 0:
                detay = list[0]

                request_basvuru_id = detay['paydas_id']
                detay['tesisler'] = Helpers().payload_parameters_control(
                    Model(ReaPy.get_reactor_source('yeten')).select(
                        table='paydas_tesis',
                        condition=
                        {
                            0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                            1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                        }).data())

                detay['yetenek_bilgisi'] = self.get_detay_iliski(request_basvuru_id,
                                                                 'view_paydas_basvuru_yetenek_bilgisi',
                                                                 ['yetenek_bilgisi_id'])

                detay['faaliyet_alanlari'] = self.get_detay_iliski(request_basvuru_id,
                                                                   'view_paydas_basvuru_faaliyet_alani',
                                                                   ['faaliyet_id'])

                detay['nace_kodlari'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_basvuru_nace',
                                                              ['nace_id'])

                detay['sahiplik_yapisi'] = self.get_detay_iliski(request_basvuru_id,
                                                                 'view_paydas_basvuru_sahiplik_yapisi',
                                                                 ['sahip_adi', 'ortaklik_yuzdesi'])

                detay['istirak_orani'] = self.get_detay_iliski(request_basvuru_id,
                                                               'view_paydas_basvuru_firma_istirakler',
                                                               ['sahip_adi', 'ortaklik_yuzdesi'])

                detay['savunma_faaliyet_alanlari'] = self.get_detay_iliski(
                    request_basvuru_id, 'view_paydas_basvuru_savunma_faaliyet_alani',
                    ['savunma_faaliyet_id'])

                detay['dosyalar'] = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='view_paydas_basvuru_dosya',
                    field=' dosya_yukleme_id, dosya_adi, dosya_turu, basvuru_id ',
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                        1: {'col': 'dosya_turu', 'operator': '!=', 'value': 'null'}
                    }) \
                    .data()

            return detay

        except Exception as exc:
            print(__file__ + ' get_detay_basvuru2')
            print(exc)

        return detay

    # noinspection PyMethodMayBeStatic
    def get_detay_basvuru(self, user_identity_no):

        detay = {}
        condition_val = {
            0: {'col': 'paydas_id', 'operator': '=', 'value': user_identity_no}
        }

        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                        .select(table='view_paydas_basvuru',
                                                                condition=condition_val,
                                                                is_first=True,
                                                                sort='basvuru_tarihi DESC').data())
            if len(list) > 0:
                detay = list[0]

                request_basvuru_id = detay['paydas_id']
                detay['tesisler'] = Helpers().payload_parameters_control(
                    Model(ReaPy.get_reactor_source('yeten')).select(
                        table='paydas_tesis',
                        condition=
                        {
                            0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                            1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                        }).data())

                detay['yetenek_bilgisi'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_basvuru_yetenek_bilgisi',
                                                                 ['yetenek_bilgisi_id'])

                detay['faaliyet_alanlari'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_basvuru_faaliyet_alani',
                                                                   ['faaliyet_id'])

                detay['nace_kodlari'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_basvuru_nace',
                                                              ['nace_id'])

                detay['sahiplik_yapisi'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_basvuru_sahiplik_yapisi',
                                                                 ['sahip_adi', 'ortaklik_yuzdesi'])

                detay['istirak_orani'] = self.get_detay_iliski(request_basvuru_id,
                                                               'view_paydas_basvuru_firma_istirakler',
                                                               ['sahip_adi', 'ortaklik_yuzdesi'])

                detay['savunma_faaliyet_alanlari'] = self.get_detay_iliski(
                    request_basvuru_id, 'view_paydas_basvuru_savunma_faaliyet_alani',
                    ['savunma_faaliyet_id'])

                detay['dosyalar'] = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='view_paydas_basvuru_dosya',
                    field=' dosya_yukleme_id, dosya_adi, dosya_turu, basvuru_id ',
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                        1: {'col': 'dosya_turu', 'operator': '!=', 'value': 'null'}
                    }) \
                    .data()

            return detay

        except Exception as exc:
            print(__file__ + ' get_detay_basvuru2')
            print(exc)

        return detay

    # noinspection PyMethodMayBeStatic
    def eski_vergi_no(self, vergi_numarasi):
        keys = ['ulke_id', 'sehir_id', 'ilce_id']
        data = Model(ReaPy.get_reactor_source('yeten')).select(table='public.view_basvuru_migration', condition={
            0: {'col': 'vergi_tc_no', 'operator': '=', 'value': str(vergi_numarasi)}
        }, is_first=True).data()
        if data is not None:
            new_data = {}
            try:
                sahiplik_yapisi = []
                for i in range(len(data[0]['sahip_adi'])):
                    sahiplik_yapisi.append(
                        {'sahip_adi': data[0]['sahip_adi'][i], 'ortaklik_yuzdesi': data[0]['ortaklik_yuzdesi'][i]})
                new_data.update({'sahiplik_yapisi': sahiplik_yapisi})
                data[0].update({'sahiplik_yapisi': sahiplik_yapisi})
                del data[0]['sahip_adi']
                del new_data['sahip_adi']
                del data[0]['ortaklik_yuzdesi']
                del new_data['ortaklik_yuzdesi']
            except Exception as e:
                print(e)
            return data

    def get_detay_tanim_iliski(self, basvuru_id, tablo, field):

        list = Model(ReaPy.get_reactor_source('yeten')).select(
            table=tablo,
            field=','.join(field),
            condition=
            {
                0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}
            }).data()

        if list is None:
            return None

        if tablo == 'view_paydas_sahiplik_yapisi' or tablo == 'view_paydas_firma_istirakler':
            return list

        return_list = []
        for key in list:
            return_list.append(key[field[0]])
            # return_list.append({field[0]: key[field[0]], field[1]: key[field[1]]})

        return return_list

    def get_detay_iliski(self, basvuru_id, tablo, field):

        list = Model(ReaPy.get_reactor_source('yeten')).select(
            table=tablo,
            field=','.join(field),
            condition=
            {
                0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}
            }).data()

        if list is None:
            return None

        if tablo == 'view_paydas_basvuru_sahiplik_yapisi' or tablo == 'view_paydas_basvuru_firma_istirakler':
            return list

        return_list = []
        for key in list:
            return_list.append(key[field[0]])
            # return_list.append({field[0]: key[field[0]], field[1]: key[field[1]]})

        return return_list

    # noinspection PyMethodMayBeStatic
    def get_all_basvuru(self, user_identity_no):

        get_list = None

        try:
            get_list = Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_paydas_basvuru',
                field=' tesis_adedi,basvuru_durum_tanim,dis_paydas_adi,basvuru_durum,basvuru_id,basvuru_no ',
                condition={0: {'col': 'yetkili_kisi_tckn', 'operator': '=', 'value': user_identity_no}},
                sort='basvuru_tarihi DESC')

            return get_list.data()
        except Exception as exc:
            # print(__file__ + ' get_all_basvuru')
            print(exc)

        if get_list is None:
            return self.messages['errors']['display_err_204']

        return get_list

    # noinspection PyMethodMayBeStatic
    def get_tanim_sehir(self):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_il').data()

    # noinspection PyMethodMayBeStatic
    def get_tanim_nace(self):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_nace').data()

    # noinspection PyMethodMayBeStatic
    def get_tanim_meslek_odasi(self):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_meslek_odasi').data()

    # noinspection PyMethodMayBeStatic
    def get_vergi_daire(self):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_vergi_dairesi').data()

    @staticmethod
    def get_basvuru_id(iam_user_domain_account_name):
        basvuru_id = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_paydas_basvuru',
                                                                     field='basvuru_id',
                                                                     condition={
                                                                         0: {'col': 'yetkili_kisi_tckn',
                                                                             'operator': '=',
                                                                             'value': iam_user_domain_account_name}
                                                                     }, sort='basvuru_tarihi desc').data()
        return basvuru_id

    # noinspection PyMethodMayBeStatic
    def get_ilce(self, il_id):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_ilce',
                                                               field='ilce_id as deger, ilce_ad as aciklama',
                                                               condition={
                                                                   0: {'col': 'il_id', 'operator': '=',
                                                                       'value': il_id}
                                                               }).data()

    # noinspection PyMethodMayBeStatic
    def basvuru_vergi_no_kontrol(self, vergi_numarasi):

        insert = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_paydas_basvuru', condition={
            0: {'col': 'vergi_tc_no', 'operator': '=', 'value': str(vergi_numarasi),
                'combiner': 'AND'},
            1: {'col': 'basvuru_durum', 'operator': '!=', 'value': '3', 'combiner': 'AND'},
            # 2: {'col': 'yetkili_kisi_tckn', 'operator': '!=', 'value': user_identity_no, 'combiner': 'AND'},
            2: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
        }).data()

        return insert

    # noinspection PyMethodMayBeStatic
    def basvuru_id_drum_kontrol(self, basvuru_id, user_identity_no):

        try:

            condititon_data = {
                0: {'col': 'basvuru_id', 'operator': '=',
                    'value': basvuru_id,
                    'combiner': 'AND'},
                1: {'col': 'basvuru_durum', 'operator': '=', 'value': '2', 'combiner': 'AND'},
                2: {'col': 'yetkili_kisi_tckn',
                    'operator': '=',
                    'value': user_identity_no}
            }

            durum = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_paydas_basvuru',
                                                                    field='basvuru_durum',
                                                                    condition=condititon_data,
                                                                    sort='basvuru_tarihi desc').data()

            if durum is None:
                condititon_data = {
                    0: {'col': 'pasif_durum', 'operator': '=', 'value': 0, 'combiner': 'AND'},
                    1: {'col': 'aktif_mi', 'operator': '=', 'value': True, 'combiner': 'AND'},
                    2: {'col': 'paydas_id', 'operator': '=',
                        'value': basvuru_id,
                        'combiner': 'AND'},
                    3: {'col': 'tc',
                        'operator': '=',
                        'value': user_identity_no}
                }
                kullanici_talep_data = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_kullanici_talep',
                                                                                       condition=condititon_data).data()
                if kullanici_talep_data is not None:
                    kullanici_talep = Helpers.payload_parameters_control(kullanici_talep_data)
                    return 2
                else:
                    return None

            # print(durum.error())
            return durum[0]['basvuru_durum']
        except Exception as exc:
            print(exc)

        return 0

    # noinspection PyMethodMayBeStatic
    def basvuru_insert(self, dpob_object, session_tckn):

        new_rand_id = self.id_generator()
        # 'kurulus_yili': dpob_object['kurulus_yili'],

        insert_data = {
            'dis_paydas_adi': dpob_object['dis_paydas_adi'],
            'basvuru_no': new_rand_id,
            'dis_paydas_tipi': dpob_object['dis_paydas_tipi'],
            'web_adres': dpob_object['web_adres'],
            'kep_adres': dpob_object['kep_adres'],
            'telefon': dpob_object['telefon'],
            'telefon2': dpob_object['telefon2'],
            'eposta_adresi': dpob_object['eposta_adresi'],
            'basvuru_durum': 0,
            'kurulus_yili': dpob_object['kurulus_yili'],
            'vergi_dairesi': dpob_object['vergi_dairesi'],
            'vergi_tc_no': dpob_object['vergi_tc_no'],
            # 'onaylayan': '',
            # 'onaylama_tarihi': '',
            'dis_paydas_turu': Helpers().set_val_safe(dpob_object, 'dis_paydas_turu', True),
            'ticari_sicil_no': dpob_object['ticari_sicil_no'],
            'ise_baslama_tarihi': dpob_object['ise_baslama_tarihi'],
            'faal_durumu': 1,  # faal olmayan başvuramaz
            # 'olusturan_id': '', # session dan gelecek
            'olusturan_ip': dpob_object['olusturan_ip'],
            'kamu_tuzel': dpob_object['kamu_tuzel'],
            'posta_kod': dpob_object['posta_kod'],
            'sahiplik_durum': dpob_object['sahiplik_durum'],
            'sicil_gazete_sayisi': dpob_object['sicil_gazete_sayisi'],
            'sicil_gazete_tarihi': dpob_object['sicil_gazete_tarihi'],
            'adres': dpob_object['adres'],
            'ulke_id': dpob_object['ulke_id'],
            'sehir_id': Helpers().set_val_safe(dpob_object, 'sehir_id', True),
            'ilce_id': Helpers().set_val_safe(dpob_object, 'ilce_id', True),
            'twitter_adres': Helpers().set_val_safe(dpob_object, 'twitter_adres', True),
            'meslek_odasi_id': dpob_object['meslek_odasi_id'],
            'tesis_toplam_alan': dpob_object['tesis_toplam_alan'],
            'kapali_alan': dpob_object['kapali_alan'],
            'diger_tesis_toplam_alan': dpob_object['diger_tesis_toplam_alan'],
            'diger_kapali_alan': dpob_object['diger_kapali_alan'],
            'faaliyet_suresi': dpob_object['faaliyet_suresi'],
            'savunma_faaliyet_suresi': Helpers().set_val_safe(dpob_object, 'savunma_faaliyet_suresi', True),
            'aktif_mi': 'true',
            'yetkili_kisi_ad_soyad': dpob_object['yetkili_kisi_ad_soyad'],
            'yetkili_kisi_tckn': session_tckn,
            'arge_merkezi_mi': dpob_object['arge_merkezi_mi'],
            'sanayi_yukumluluk_var_mi': dpob_object['sanayi_yukumluluk_var_mi'],
            'savnet': dpob_object['savnet']
        }

        query = Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru', {0: insert_data})

        print("insert.error ->  ")
        print(query.error())

        if query.error() is None:
            # TODO Ã¶nce basvuru id alınır
            query = Model(ReaPy.get_reactor_source('yeten')).select(table='dis_paydas_basvuru', field='basvuru_id',
                                                                    condition=
                                                                    {0:
                                                                         {'col': 'basvuru_no', 'operator': '=',
                                                                          'value': new_rand_id}
                                                                     }
                                                                    ).data()
            # print(query)
            basvuru_id = query[0]['basvuru_id']

            # self.__send_mail(dpob_object['eposta_adresi'], dpob_object['dis_paydas_adi'])

            # ilk faz içn istenmiyor
            # self.geodi_feed_send(dpob_object['dis_paydas_adi'], dpob_object['web_adres'], insert_data['twitter_adres'])

            self.__set_basvuru_iliski_nace_kodlari(basvuru_id,
                                                   Helpers().set_val_safe(dpob_object, 'nace_kodlari', True))
            self.__set_basvuru_iliski_faaliyet_alani(basvuru_id,
                                                     Helpers().set_val_safe(dpob_object, 'faaliyet_alanlari',
                                                                            True))
            self.__set_basvuru_iliski_yetenek_bilgisi(basvuru_id,
                                                      Helpers().set_val_safe(dpob_object, 'yetenek_bilgisi',
                                                                             True))
            self.__set_basvuru_iliski_savunma_alanlari(basvuru_id,
                                                       Helpers().set_val_safe(dpob_object,
                                                                              'savunma_faaliyet_alanlari',
                                                                              True))
            self.__set_basvuru_iliski_sahiplik_yapisi(basvuru_id,
                                                      Helpers().set_val_safe(dpob_object, 'sahiplik_yapisi',
                                                                             True))
            self.__set_basvuru_iliski_istirakleri(basvuru_id,
                                                  Helpers().set_val_safe(dpob_object, 'firma_istirakleri',
                                                                         True))

            self.__dosya_paydas_iliski(dpob_object['sahiplik_orani'], basvuru_id, 'sahiplik_orani')
            self.__dosya_paydas_iliski(dpob_object['imza_sirkuleri'], basvuru_id, 'imza_sirkuleri')
            self.__dosya_paydas_iliski(dpob_object['yetki_belgesi'], basvuru_id, 'yetki_belgesi')
            self.__dosya_paydas_iliski(dpob_object['ticaret_sicil_gazetesi'], basvuru_id, 'ticaret_sicil_gazetesi')

            return new_rand_id

        else:
            return None

    # noinspection PyMethodMayBeStatic
    def __send_mail(self, mail, firma_adi):

        try:
            mail_settings = ReaPy.configuration().get_configuration()['system']['rest_server']['mail']
            eposta_domain = mail.split('@')
            # izinli = ['prodabilisim.com', 'stm.com.tr']
            # if eposta_domain[1] in izinli:
            ReaPy.mail().send(mail_settings['host'], mail_settings['port'], mail_settings['user'],
                              mail_settings['password'], mail,
                              'YETEN Portalı | Başvurunuz alınmıştır.',
                              "Tebrikler;\n\n\"" + firma_adi + "\" isimli firmanız içn başvurunuz başarıyla alınmıştır.\n\nBaşvurunuz işleme alındıktan sonra tekrar bilgilendirme yapılacaktır.\n\nİlginiz içn teşekkürler.\n"
                              )

        except Exception as exc:
            print('E-POSTA GÃ–NDERÄ°MÄ° YAPILAMADI', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_faaliyet_alani(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_faaliyet_alani',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for faaliyet_id in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'faaliyet_id': faaliyet_id,
                'aktif_mi': 'true'
            }
        nq = Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_faaliyet_alani', insert_data)
        print(nq.error())
        print('__set_basvuru_iliski_faaliyet_alani ', nq.data())

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_yetenek_bilgisi(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_yetenek_bilgisi',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for yetenek_id in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'yetenek_bilgisi_id': yetenek_id,
                'aktif_mi': 'true',
            }
        Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_yetenek_bilgisi', insert_data)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_savunma_alanlari(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_savunma_faaliyet_alani',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for savunma_faaliyet_id in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'savunma_faaliyet_id': savunma_faaliyet_id,
                'aktif_mi': 'true',
            }
        Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_savunma_faaliyet_alani', insert_data)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_nace_kodlari(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True

        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_nace',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )

        insert_data = {}
        for nace_id in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'nace_id': nace_id,
                'aktif_mi': 'true',
            }
        Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_nace', insert_data)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_sahiplik_yapisi(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_sahiplik_yapisi',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for sahip in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'sahip_adi': sahip['sahip_adi'],
                'ortaklik_yuzdesi': sahip['ortaklik_yuzdesi'],
                'aktif_mi': 'true',
            }
        insert = Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_sahiplik_yapisi',
                                                                 insert_data).data()
        print(insert)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_istirakleri(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='dis_paydas_basvuru_firma_istirakleri',
                    condition={0: {'col': 'basvuru_id', 'operator': '=', 'value': basvuru_id}}
                )

        insert_data = {}
        for sahip in data:
            insert_data[len(insert_data)] = {
                'basvuru_id': basvuru_id,
                'istirak_adi': sahip['sahip_adi'],
                'orani': sahip['ortaklik_yuzdesi'],
                'aktif_mi': 'true'
            }
        print(insert_data)
        Model(ReaPy.get_reactor_source('yeten')).insert('dis_paydas_basvuru_firma_istirakleri', insert_data)

    # noinspection PyMethodMayBeStatic
    def basvuru_update(self, data, basvuru_id, user_identity_no):

        update_data = {}

        if Helpers().set_val_safe(data, 'dis_paydas_adi') != '':
            update_data['dis_paydas_adi'] = data['dis_paydas_adi']

        if Helpers().set_val_safe(data, 'yetkili_kisi_tckn') != '':
            update_data['yetkili_kisi_tckn'] = data['yetkili_kisi_tckn']

        if Helpers().set_val_safe(data, 'yetkili_kisi_ad_soyad') != '':
            update_data['yetkili_kisi_ad_soyad'] = data['yetkili_kisi_ad_soyad']

        if Helpers().set_val_safe(data, 'dis_paydas_turu') != '':
            update_data['dis_paydas_turu'] = data['dis_paydas_turu']

        if Helpers().set_val_safe(data, 'dis_paydas_tipi') != '':
            update_data['dis_paydas_tipi'] = data['dis_paydas_tipi']

        if Helpers().set_val_safe(data, 'kurulus_yili') != None:
            update_data['kurulus_yili'] = data['kurulus_yili']

        if Helpers().set_val_safe(data, 'dis_payas_tipi') != '':
            update_data['dis_payas_tipi'] = data['dis_payas_tipi']

        if Helpers().set_val_safe(data, 'vergi_dairesi') != '':
            update_data['vergi_dairesi'] = data['vergi_dairesi']

        if Helpers().set_val_safe(data, 'ticari_sicil_no') != '':
            update_data['ticari_sicil_no'] = data['ticari_sicil_no']

        if Helpers().set_val_safe(data, 'kep_adres') != '':
            update_data['kep_adres'] = data['kep_adres']

        if Helpers().set_val_safe(data, 'telefon') != '':
            update_data['telefon'] = data['telefon']

        if Helpers().set_val_safe(data, 'telefon2') != '':
            update_data['telefon2'] = data['telefon2']

        if Helpers().set_val_safe(data, 'eposta_adresi') != '':
            update_data['eposta_adresi'] = data['eposta_adresi']

        if Helpers().set_val_safe(data, 'sahiplik_durum') != '':
            update_data['sahiplik_durum'] = data['sahiplik_durum']

        if Helpers().set_val_safe(data, 'nace_kodu') != '':
            update_data['nace_kodu'] = data['nace_kodu']

        if Helpers().set_val_safe(data, 'ulke_id') != '':
            update_data['ulke_id'] = data['ulke_id']

        if Helpers().set_val_safe(data, 'sehir_id') != '':
            update_data['sehir_id'] = data['sehir_id']

        if Helpers().set_val_safe(data, 'tesis_toplam_alan') != '':
            update_data['tesis_toplam_alan'] = data['tesis_toplam_alan']
        if Helpers().set_val_safe(data, 'kapali_alan') != '':
            update_data['kapali_alan'] = data['kapali_alan']

        if Helpers().set_val_safe(data, 'diger_tesis_toplam_alan') != '':
            update_data['diger_tesis_toplam_alan'] = data['diger_tesis_toplam_alan']

        if Helpers().set_val_safe(data, 'diger_kapali_alan') != '':
            update_data['diger_kapali_alan'] = data['diger_kapali_alan']

        if Helpers().set_val_safe(data, 'ilce_id') != '':
            update_data['ilce_id'] = data['ilce_id']

        if Helpers().set_val_safe(data, 'faaliyet_alani') != '':
            update_data['faaliyet_alani'] = data['faaliyet_alani']

        if Helpers().set_val_safe(data, 'faaliyet_suresi') != '':
            update_data['faaliyet_suresi'] = data['faaliyet_suresi']

        if Helpers().set_val_safe(data, 'savunma_faaliyet_suresi') != '':
            update_data['savunma_faaliyet_suresi'] = data['savunma_faaliyet_suresi']

        if Helpers().set_val_safe(data, 'web_adres') != '':
            update_data['web_adres'] = data['web_adres']

        if Helpers().set_val_safe(data, 'yetkili_kisi') != '':
            update_data['yetkili_kisi'] = data['yetkili_kisi']

        if Helpers().set_val_safe(data, 'ise_baslama_tarihi') != '':
            update_data['ise_baslama_tarihi'] = data['ise_baslama_tarihi']

        if Helpers().set_val_safe(data, 'posta_kod') != '':
            update_data['posta_kod'] = data['posta_kod']

        if Helpers().set_val_safe(data, 'sicil_gazete_sayisi') != '':
            update_data['sicil_gazete_sayisi'] = data['sicil_gazete_sayisi']

        if Helpers().set_val_safe(data, 'sicil_gazete_tarihi') != '':
            update_data['sicil_gazete_tarihi'] = data['sicil_gazete_tarihi']

        if Helpers().set_val_safe(data, 'adres') != '':
            update_data['adres'] = data['adres']

        if Helpers().set_val_safe(data, 'meslek_odasi_id') != '':
            update_data['meslek_odasi_id'] = data['meslek_odasi_id']

        if Helpers().set_val_safe(data, 'twitter_adres', True) != None:
            update_data['twitter_adres'] = data['twitter_adres']

        if Helpers().set_val_safe(data, 'arge_merkezi_mi') != '':
            sdatam = 'false'
            if data['arge_merkezi_mi']:
                sdatam = 'true'
            update_data['arge_merkezi_mi'] = sdatam

        if Helpers().set_val_safe(data, 'sanayi_yukumluluk_var_mi') != '':
            update_data['sanayi_yukumluluk_var_mi'] = data['sanayi_yukumluluk_var_mi']

        if Helpers().set_val_safe(data, 'savnet') != '':
            sdata = 'false'
            if data['savnet']:
                sdata = 'true'
            update_data['savnet'] = sdata

        query = Model(ReaPy.get_reactor_source('yeten')).update('public.dis_paydas_basvuru', {'SET': update_data,
                                                                                              'CONDITION': {
                                                                                                  0: {
                                                                                                      'col': 'basvuru_id',
                                                                                                      'operator': '=',
                                                                                                      'value': basvuru_id,
                                                                                                      'combiner': 'AND'},
                                                                                                  1: {
                                                                                                      'col': 'yetkili_kisi_tckn',
                                                                                                      'operator': '=',
                                                                                                      'value': user_identity_no}
                                                                                              }
                                                                                              }).data()

        print('basvuru update', query)

        self.__set_basvuru_iliski_nace_kodlari(basvuru_id, Helpers().set_val_safe(data, 'nace_kodlari', True),
                                               True)
        self.__set_basvuru_iliski_istirakleri(basvuru_id,
                                              Helpers().set_val_safe(data, 'firma_istirakleri', True), True)
        self.__set_basvuru_iliski_yetenek_bilgisi(basvuru_id,
                                                  Helpers().set_val_safe(data, 'yetenek_bilgisi', True), True)
        self.__set_basvuru_iliski_sahiplik_yapisi(basvuru_id,
                                                  Helpers().set_val_safe(data, 'sahiplik_yapisi', True), True)
        self.__set_basvuru_iliski_faaliyet_alani(basvuru_id,
                                                 Helpers().set_val_safe(data, 'faaliyet_alanlari', True),
                                                 True)
        self.__set_basvuru_iliski_savunma_alanlari(basvuru_id,
                                                   Helpers().set_val_safe(data, 'savunma_faaliyet_alanlari',
                                                                          True), True)

        self.__dosya_paydas_iliski(data['sahiplik_orani'], basvuru_id, 'sahiplik_orani')
        self.__dosya_paydas_iliski(data['imza_sirkuleri'], basvuru_id, 'imza_sirkuleri')
        self.__dosya_paydas_iliski(data['yetki_belgesi'], basvuru_id, 'yetki_belgesi')
        self.__dosya_paydas_iliski(data['ticaret_sicil_gazetesi'], basvuru_id, 'ticaret_sicil_gazetesi')

        return query

    # noinspection PyMethodMayBeStatic
    def __dosya_paydas_iliski(self, dosyalar, dis_paydas_id, dosya_turu):

        if len(dosyalar) < 1:
            return True

        for dosya_yukleme_id in dosyalar:

            # if type(dosya_ismi) == list: dosya_ismi = dosya_ismi[0]

            query = Model(ReaPy.get_reactor_source('yeten')).update('public.dosya_yukleme', {
                'SET': {'paydas_id': dis_paydas_id, 'dosya_turu': dosya_turu, 'aktif_mi': 'true'},
                'CONDITION': {
                    0: {
                        'col': 'dosya_yukleme_id',
                        'operator': '=',
                        'value': dosya_yukleme_id
                    }
                }})
            print(query.error())

            if query.error() is None:
                self.__send_file(dosya_yukleme_id)

    def __send_file(self, dosya_yukleme_id):
        pass
        # db_file = Model(ReaPy.get_reactor_source('yeten')).select('public.dosya_yukleme',
        #                                                                field=' dosya_adi, doysa_tam_yolu ',
        #                                                                condition={
        #                                                                    0: {
        #                                                                        'col': 'dosya_yukleme_id',
        #                                                                        'operator': '=',
        #                                                                        'value': dosya_yukleme_id
        #                                                                    }
        #                                                                }).data()
        #
        # if db_file is None:
        #     return False
        #
        # file_name = db_file[0]['dosya_adi']
        # file_path = db_file[0]['doysa_tam_yolu'].replace(file_name, '')
        #
        # file_full_path_and_name = file_path + file_name
        #
        # print(file_full_path_and_name)
        #
        #
        # encoded_string = None
        # try:
        #     with open(file_full_path_and_name, "rb") as file_content:
        #         encoded_string = base64.b64encode(file_content.read())
        #
        #     if encoded_string is not None:
        #         mediation = ReaPy.configuration().get_configuration()['system']['mediation_server']
        #         url = 'http://' + mediation['host'] + ':' + str(mediation['port']) + '/source/db/file'
        #         print(url)
        #
        #         send_data = {
        #             "file_name": file_name,
        #             "file_path": file_path,
        #             "file_content": encoded_string.decode("utf-8")
        #         }
        #
        #         # request = ReaPy.requests().put(url, data=ReaPy.json().dumps(send_data))
        #         request = ReaPy.requests().put(url, json=ReaPy.json().dumps(send_data))
        #
        #         print(request)
        #
        # except Exception as exc:
        #     print('mediation __send_file hata : ', __file__)
        #     print(exc)

    # noinspection PyMethodMayBeStatic
    def id_generator(self, size=10, chars=ReaPy.string().ascii_uppercase + ReaPy.string().digits):
        return ''.join(ReaPy.random().choice(chars) for _ in range(size))

    @staticmethod
    def delete_dosya(dosya_id):
        delete_dosya = Model(ReaPy.get_reactor_source('yeten')).delete('public.dosya_yukleme', {
            0: {'col': 'dosya_yukleme_id', 'operator': '=', 'value': str(dosya_id)}}).data()
        if delete_dosya != 1:
            return False
        return True
# Dispaydas().geodi_feed_send('stm', 'http://stm.com.tr', '')
