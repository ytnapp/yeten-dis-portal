# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Kaliteyonetimsistemi(object):

    def __init__(self):
        view_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'
        self.messages = ReaPy.presenter().get_message(view_path, 'messages')['paydas']

  
    # noinspection PyMethodMayBeStatic
    def get(self):

        try:
            response = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_yerli_yabanci_tum_firmalar',
                condition={
                    0: {'col': 'tablo', 'operator': '=', 'value': 'paydas_kalite_yonetim_sistemleri'}   
                    }).data())
            return response

        except Exception as exc:
            print(__file__)
            print(exc)

        return None

  


