# -*- coding: utf-8 -*-
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class ElementValues:

    def __init__(self):
        self.error = False

    @staticmethod
    def find_technical_spec(cpc):
        unit_dict = {}
        unit_list = []
        none_unit_list = []
        data_list = []
        technical_spec = Model(ReaPy.get_reactor_source('yeten')).select('public.view_um_teknik_ozellik',
                                                                         None,
                                                                         {0: {'col': 'cpc', 'operator': '=',
                                                                              'value': cpc}},
                                                                         'teknik_ozellik_id asc', None,
                                                                         False, True).data()
        if technical_spec is not None:
            for key in technical_spec:
                if key['birim_mi'] is True and key['teknik_deger'] is None:
                    unit_list.append(key)
                    unit_dict[key['teknik_ozellik_id']] = {'teknik_ozellik_id': key['teknik_ozellik_id'],
                                                           'ozellik': key['ozellik'], 'birim_mi': key['birim_mi'],
                                                           'birim': []}
                else:
                    none_unit_list.append(key)
                    unit_dict[key['teknik_ozellik_id']] = {'teknik_ozellik_id': key['teknik_ozellik_id'],
                                                           'ozellik': key['ozellik'], 'birim_mi': key['birim_mi'],
                                                           'birim': []}
            for key in unit_list:
                unit_dict[key['teknik_ozellik_id']]['birim'].append(
                    {'key': key['birim'], 'value': key['birim_teknik_deger_id']})
            for key in none_unit_list:
                unit_dict[key['teknik_ozellik_id']]['birim'].append(
                    {'key': key['teknik_deger'], 'value': key['birim_teknik_deger_id']})

            for key, value in unit_dict.items():
                data_list.append(value)
        return data_list

    @staticmethod
    def get_sys_definition(table, column):
        sys_definition = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_tanim',
                                                                         'tanim_id as key ,aciklama as value',
                                                                         {0: {'col': 'tablo', 'operator': '=',
                                                                              'value': table,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'kolon', 'operator': '=',
                                                                              'value': column}},
                                                                         'deger asc', None,
                                                                         False, True).data()
        return sys_definition

    @staticmethod
    def get_software_standards():
        software_standards = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_yazilim_standart',
                                                                             'standart_id as key ,standart_adi as value',
                                                                             None,
                                                                             'standart_adi asc', None,
                                                                             False, True).data()
        return software_standards

    @staticmethod
    def get_duty_function():
        duty_function = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_gorev_fonksiyonu',
                                                                        'gorev_fonksiyonu_id,gorev,aciklama',
                                                                        None,
                                                                        'gorev asc', None,
                                                                        False, True).data()
        return duty_function

    @staticmethod
    def get_unit(unit_type=None):
        if unit_type is None:
            unit = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_birim',
                                                                   'birim_id,birim',
                                                                   None,
                                                                   'birim asc', None,
                                                                   False, True).data()
        else:
            unit = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_birim',
                                                                   'birim_id,birim',
                                                                   {0: {'col': 'birim_tipi', 'operator': '=',
                                                                        'value': unit_type
                                                                        }},
                                                                   'birim asc', None,
                                                                   False, True).data()

        return unit

    @staticmethod
    def get_company(iam_user_company_id):
        company = Model(ReaPy.get_reactor_source('yeten')).select('public.view_yerli_yabanci_tum_firmalar',
                                                                  'paydas_id,paydas_adi',
                                                                  {0: {'col': 'paydas_id', 'operator': '!=',
                                                                       'value': iam_user_company_id
                                                                       }},
                                                                  'paydas_adi asc', 1000,
                                                                  False).data()
        return company

    @staticmethod
    def get_standards(standard_id):
        company = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_standart',
                                                                  'standart_id,standart_adi',
                                                                  {0: {'col': 'standart_turu', 'operator': '=',
                                                                       'value': standard_id
                                                                       }},
                                                                  'standart_adi asc', None,
                                                                  False).data()
        return company

    @staticmethod
    def get_product(iam_user_company_id):
        products = Model(ReaPy.get_reactor_source('yeten')).select('public.um_kutuphane_urun',
                                                                   'urun_id,adi',
                                                                   {0: {'col': 'aktif_mi',
                                                                        'operator': '=',
                                                                        'value': True
                                                                        }
                                                                    # 1: {'col': 'paydas_id', 'operator': '=',
                                                                    #     'value': iam_user_company_id
                                                                    #     }
                                                                    },
                                                                   'adi asc', 1000,
                                                                   False).data()
        return products

    @staticmethod
    def get_countries():
        countries = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_ulke',
                                                                    'ulke_id as key ,ulke_adi as value',
                                                                    None,
                                                                    'resmi_ulke_adi asc', None,
                                                                    False, True).data()
        return countries

    @staticmethod
    def get_stakeholder(iam_user_company_id):
        stakeholders = Model(ReaPy.get_reactor_source('yeten')).select('public.paydas',
                                                                       None,
                                                                       {0: {'col': 'paydas_id', 'operator': '=',
                                                                            'value': iam_user_company_id
                                                                            }},
                                                                       None, None,
                                                                       True).data()

        return stakeholders

    @staticmethod
    def get_stakeholder_product_code(iam_user_company_id):
        product_code = Model(ReaPy.get_reactor_source('yeten')).select(
            "pg_function.public.function_paydas_urun_kodu('" + iam_user_company_id + "')",
            None,
            None,
            None, None,
            False).data()
        return product_code

    @staticmethod
    def get_prod_tr():
        prod_tr = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_prodtr',
                                                                  None,
                                                                  None,
                                                                  'adi asc', None,
                                                                  False, True).data()
        return prod_tr

    @staticmethod
    def get_cpc(parent, level, finder=None):
        cpc_list = []
        if finder is not None and len(finder) > 0:
            cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                                  'cpc_id,sembol,seviye,aciklama',
                                                                  {0: {'col': 'aciklama',
                                                                       'operator': '!=',
                                                                       'value': '',
                                                                       'combiner': 'AND'
                                                                       },
                                                                   1: {'col': 'LOWER(aciklama)',
                                                                       'operator': 'LIKE',
                                                                       'value': '%' + finder.lower() + '%',
                                                                       'combiner': 'OR'
                                                                       },
                                                                   2: {'col': 'LOWER(sembol)', 'operator': 'LIKE',
                                                                       'value': '%' + finder.lower() + '%'

                                                                       }},
                                                                  'aciklama asc', 1000,
                                                                  False, True).data()

        else:
            cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                                  'cpc_id,sembol,seviye,aciklama',
                                                                  {0: {'col': 'aciklama',
                                                                       'operator': '!=',
                                                                       'value': '',
                                                                       'combiner': 'AND'
                                                                       },
                                                                   1: {'col': 'seviye', 'operator': '=',
                                                                       'value': level, 'combiner': 'AND'
                                                                       },
                                                                   2: {'col': 'ata', 'operator': '=',
                                                                       'value': parent}},
                                                                  'sembol asc', None,
                                                                  False, True).data()

        if cpc is not None:
            for key in cpc:
                key_data = {
                    "value": key['cpc_id'],
                    "sembol": key['sembol'],
                    "seviye": key['seviye'],
                    "title": key['aciklama']

                }
                cpc_list.append(key_data)
        return cpc_list

    @staticmethod
    def find_taksonomi(finder):
        taksonomi_list = []
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    None,
                                                                    {0: {'col': 'LOWER(turkce_adi)',
                                                                         'operator': 'LIKE',
                                                                         'value': '%' + finder.lower() + '%'
                                                                         }},
                                                                    'kodu asc', None,
                                                                    False, True).data()

        if taksonomi is not None:
            for key in taksonomi:
                taksonomi_list.append(
                    {'kod': key['kodu'], 'title': key['turkce_adi'],
                     'value': key['taksonomi_id']})

        return taksonomi_list

    @staticmethod
    def get_taksonomi():
        taksonomi_list = []
        root_index = []
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    None,
                                                                    None,
                                                                    'kodu asc', None,
                                                                    False, True).data()
        if taksonomi is not None:
            for key in taksonomi:
                if key['ust_kirilim'] == key['kodu']:
                    root_index.append(key['kodu'])
                    taksonomi_list.append(
                        {'kod': key['kodu'], 'title': key['kodu'] + ' - ' + key['turkce_adi'],
                         "key": ReaPy.hash().get_uuid(),
                         'value': key['taksonomi_id'],
                         'disabled': True,
                         'children': []})
            for key in taksonomi:
                if key['ust_kirilim'] != key['kodu'] and '.' not in key['kodu']:
                    index = root_index.index(key['ust_kirilim'])
                    taksonomi_list[index]['children'].append(
                        {'kod': key['kodu'], 'title': key['kodu'] + ' - ' + key['turkce_adi'],
                         "key": ReaPy.hash().get_uuid(),
                         'value': key['taksonomi_id'],
                         'children': []})
            for key in taksonomi:
                if key['ust_kirilim'] != key['kodu'] and '.' in key['kodu']:
                    index_key = root_index.index(key['ust_kirilim'])
                    second_index = next((index for (index, d) in enumerate(taksonomi_list[index_key]['children']) if
                                         d["kod"] == key['alt_kirilim']), None)

                    taksonomi_list[index_key]['children'][second_index]['children'].append(
                        {'kod': key['kodu'], 'title': key['kodu'] + ' - ' + key['turkce_adi'],
                         "key": ReaPy.hash().get_uuid(), 'value': key['taksonomi_id']})
        return taksonomi_list
