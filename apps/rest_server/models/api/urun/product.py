# -*- coding: utf-8 -*-
from core.reapy import ReaPy
import datetime

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Product:
    um_anahtar_kelime = 'public.um_anahtar_kelime'
    anahtar_kelime_sort = 'anahtar_kelime asc'
    um_teknik_ozellik = 'public.um_teknik_ozellik'
    um_ihracat_ulke = 'public.um_ihracat_ulke'
    um_hangi_urunde_kullaniliyor = 'public.um_hangi_urunde_kullaniliyor'
    um_yazilim_dil = 'public.um_yazilim_dil'
    um_yazilim_isletim_sistemi = 'public.um_yazilim_isletim_sistemi'
    um_yazilim_standarti = 'public.um_yazilim_standarti'
    um_urun_standart = 'public.um_urun_standart'
    um_gorev_fonksiyonu = 'public.um_gorev_fonksiyonu'
    um_kutuphane_yazilim = 'public.um_kutuphane_yazilim'
    um_kutuphane_urun = 'public.um_kutuphane_urun'
    um_hangi_firmanin_alt_yuklenicisi = 'public.um_hangi_firmanin_alt_yuklenicisi'

    def __init__(self):
        self.error = False

    @staticmethod
    def get_cpc(cpc_id):
        format_cpc = None
        cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                              'sembol,aciklama',
                                                              {
                                                                  0: {'col': 'cpc_id',
                                                                      'operator': '=',
                                                                      'value': cpc_id}},
                                                              None, None,
                                                              False, True).data()

        if cpc is not None:
            format_cpc = cpc[0]['sembol'] + ', ' + cpc[0]['aciklama']
        return format_cpc

    def get_product_keywords(self, product_id):
        keys = []
        product_keywords = Model(ReaPy.get_reactor_source('yeten')).select(self.um_anahtar_kelime,
                                                                           'anahtar_kelime',
                                                                           {
                                                                               0: {'col': 'urun_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True}},
                                                                           self.anahtar_kelime_sort, None,
                                                                           False).data()
        if product_keywords is not None:
            for key in product_keywords:
                keys.append(key['anahtar_kelime'])
        return keys

    def get_product_keywords_by_lang(self, product_id, lang_id):
        product_keywords = Model(ReaPy.get_reactor_source('yeten')).select(self.um_anahtar_kelime,
                                                                           'anahtar_kelime as key,anahtar_kelime_id as value',
                                                                           {
                                                                               0: {'col': 'urun_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'dil',
                                                                                   'operator': '=',
                                                                                   'value': lang_id,
                                                                                   'combiner': 'AND'},
                                                                               2: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True}},
                                                                           self.anahtar_kelime_sort, None,
                                                                           False).data()

        return product_keywords

    def get_product_spec(self, product_id):
        product_keywords = Model(ReaPy.get_reactor_source('yeten')).select(self.um_teknik_ozellik,
                                                                           None,
                                                                           {
                                                                               0: {'col': 'urun_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True}},
                                                                           None, None,
                                                                           False).data()

        return product_keywords

    @staticmethod
    def get_normal_product_taksonomi(code):
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    'taksonomi_id as value,turkce_adi as key',
                                                                    {
                                                                        0: {'col': 'taksonomi_id',
                                                                            'operator': '=',
                                                                            'value': code,
                                                                            }
                                                                    },
                                                                    None, None,
                                                                    True, True).data()

        if taksonomi is not None:
            taksonomi_def = {'label': taksonomi[0]['key'], 'key': taksonomi[0]['value']}

            return taksonomi_def
        return {}

    @staticmethod
    def get_software_product_taksonomi(code):
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    'taksonomi_id as value,turkce_adi as key',
                                                                    {
                                                                        0: {'col': 'taksonomi_id',
                                                                            'operator': '=',
                                                                            'value': code,
                                                                            }
                                                                    },
                                                                    None, None,
                                                                    True, True).data()

        if taksonomi is not None:
            taksonomi_def = {'label': taksonomi[0]['key'], 'key': taksonomi[0]['value']}

            return taksonomi_def
        return {}

    def get_product_export_country(self, product_id):
        export_country_list = []
        export_country = Model(ReaPy.get_reactor_source('yeten')).select(self.um_ihracat_ulke,
                                                                         'ulke_id',
                                                                         {
                                                                             0: {'col': 'urun_id',
                                                                                 'operator': '=',
                                                                                 'value': product_id,
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'aktif_mi',
                                                                                 'operator': '=',
                                                                                 'value': True}},
                                                                         None, None,
                                                                         False).data()
        if export_country is not None:
            for key in export_country:
                export_country_list.append(key['ulke_id'])

        return export_country_list

    def get_product_export_country_constraints(self, product_id):
        export_country_constraints_list = {}
        export_country_constraints = Model(ReaPy.get_reactor_source('yeten')).select('public.um_ihracat_kisiti',
                                                                                     'ulke_id,ihracat_kisiti',
                                                                                     {
                                                                                         0: {'col': 'urun_id',
                                                                                             'operator': '=',
                                                                                             'value': product_id,
                                                                                             'combiner': 'AND'},
                                                                                         1: {'col': 'aktif_mi',
                                                                                             'operator': '=',
                                                                                             'value': True}},
                                                                                     None, None,
                                                                                     False).data()
        if export_country_constraints is not None:
            for key in export_country_constraints:
                export_country_constraints_list[key['ulke_id']] = []

            for key in export_country_constraints:
                export_country_constraints_list[key['ulke_id']].append(key['ihracat_kisiti'])

        return export_country_constraints_list

    def get_product_sub_product(self, product_id):
        sub_product_list = []
        sub_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_hangi_urunde_kullaniliyor,
                                                                      'kullanilan_urun_id',
                                                                      {
                                                                          0: {'col': 'urun_id',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True}},
                                                                      None, None,
                                                                      False).data()
        if sub_product is not None:
            for key in sub_product:
                sub_product_list.append(key['kullanilan_urun_id'])

        return sub_product_list

    def get_product_sub_product_other(self, product_id):
        sub_product_list = []
        sub_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_hangi_urunde_kullaniliyor,
                                                                      'diger',
                                                                      {
                                                                          0: {'col': 'urun_id',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True,
                                                                              'combiner': 'AND'},
                                                                          2: {'col': 'kullanilan_urun_id',
                                                                              'operator': '=',
                                                                              'value': '00000000-0000-0000-0000-000000000000',
                                                                              }},
                                                                      None, None,
                                                                      True).data()
        if sub_product is not None:
            for key in sub_product:
                sub_product_list.append(key['diger'])

            return sub_product_list[0]
        else:
            return None

    def get_product_keywords_by_key(self, product_id, lang_id, key):
        product_keywords = Model(ReaPy.get_reactor_source('yeten')).select(self.um_anahtar_kelime,
                                                                           'anahtar_kelime as key,anahtar_kelime_id as value',
                                                                           {
                                                                               0: {'col': 'urun_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'dil',
                                                                                   'operator': '=',
                                                                                   'value': lang_id,
                                                                                   'combiner': 'AND'},
                                                                               2: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True,
                                                                                   'combiner': 'AND'},
                                                                               3: {'col': 'anahtar_kelime',
                                                                                   'operator': '=',
                                                                                   'value': key}},
                                                                           self.anahtar_kelime_sort, None,
                                                                           False).data()

        return product_keywords

    @staticmethod
    def get_definition(definition_id):
        software_product = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_tanim',
                                                                           None,
                                                                           {0: {'col': 'tanim_id', 'operator': '=',
                                                                                'value': definition_id,
                                                                                }},
                                                                           None, None,
                                                                           False, True).data()
        return software_product

    @staticmethod
    def get_software_std_definition(definition_id):
        software_product_std = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_yazilim_standart',
                                                                               None,
                                                                               {0: {'col': 'standart_id',
                                                                                    'operator': '=',
                                                                                    'value': definition_id,
                                                                                    }},
                                                                               None, None,
                                                                               False, True).data()
        return software_product_std

    @staticmethod
    def get_country(country_id):
        country = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_ulke',
                                                                  'ulke_adi,ingilizce_ulke_adi',
                                                                  {0: {'col': 'ulke_id', 'operator': '=',
                                                                       'value': country_id,
                                                                       }},
                                                                  None, None,
                                                                  False, True).data()
        return country

    def get_software_product_pl(self, product_id):
        product_pl = []
        software_product_pl = Model(ReaPy.get_reactor_source('yeten')).select(self.um_yazilim_dil,
                                                                              'yazilim_dil_id,dil_id',
                                                                              {0: {'col': 'yazilim_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True}},
                                                                              None, None,
                                                                              False).data()
        if software_product_pl is not None:
            for key in software_product_pl:
                lang = None
                pl = Product.get_definition(key['dil_id'])
                if pl is not None:
                    lang = pl[0]['aciklama']
                product_pl.append({'key': lang, 'value': key['dil_id']})

        return product_pl

    def get_software_product_os(self, product_id):
        product_os = []
        software_product_os = Model(ReaPy.get_reactor_source('yeten')).select(self.um_yazilim_isletim_sistemi,
                                                                              'yazilim_isletim_sistemi_id,isletim_sistemi_id',
                                                                              {0: {'col': 'yazilim_id',
                                                                                   'operator': '=',
                                                                                   'value': product_id,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'aktif_mi',
                                                                                   'operator': '=',
                                                                                   'value': True}},
                                                                              None, None,
                                                                              False).data()
        if software_product_os is not None:
            for key in software_product_os:
                lang = None
                pl = Product.get_definition(key['isletim_sistemi_id'])
                if pl is not None:
                    lang = pl[0]['aciklama']
                product_os.append({'key': lang, 'value': key['isletim_sistemi_id']})

        return product_os

    def get_software_product_standards(self, product_id):
        product_std = []
        software_product_std = Model(ReaPy.get_reactor_source('yeten')).select(self.um_yazilim_standarti,
                                                                               'yazilim_standarti_id,standart_id',
                                                                               {0: {'col': 'yazilim_id',
                                                                                    'operator': '=',
                                                                                    'value': product_id,
                                                                                    'combiner': 'AND'},
                                                                                1: {'col': 'aktif_mi',
                                                                                    'operator': '=',
                                                                                    'value': True}},
                                                                               None, None,
                                                                               False).data()

        if software_product_std is not None:
            for key in software_product_std:
                std_key = None
                std = Product.get_software_std_definition(key['standart_id'])
                if std is not None:
                    std_key = std[0]['standart_adi']
                product_std.append({'key': std_key, 'value': key['standart_id']})

        return product_std

    def get_normal_product_standards(self, product_id, type):
        normal_product_list = []
        normal_product_std = Model(ReaPy.get_reactor_source('yeten')).select(self.um_urun_standart,
                                                                             'standart_id',
                                                                             {0: {'col': 'urun_id',
                                                                                  'operator': '=',
                                                                                  'value': product_id,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': True,
                                                                                  'combiner': 'AND'},
                                                                              2: {'col': 'standart_tipi',
                                                                                  'operator': '=',
                                                                                  'value': type}},
                                                                             None, None,
                                                                             False).data()
        if normal_product_std is not None:
            for key in normal_product_std:
                normal_product_list.append(key['standart_id'])

        return normal_product_list

    def get_normal_product_duty_function(self, product_id):
        duty_function_list = []
        duty_function = Model(ReaPy.get_reactor_source('yeten')).select(self.um_gorev_fonksiyonu,
                                                                        'gorev_fonksiyonu_id',
                                                                        {0: {'col': 'urun_id',
                                                                             'operator': '=',
                                                                             'value': product_id,
                                                                             'combiner': 'AND'},
                                                                         1: {'col': 'aktif_mi',
                                                                             'operator': '=',
                                                                             'value': True}
                                                                         },
                                                                        None, None,
                                                                        False).data()
        if duty_function is not None:
            for key in duty_function:
                if key['gorev_fonksiyonu_id'] not in duty_function_list:
                    duty_function_list.append(key['gorev_fonksiyonu_id'])

        return duty_function_list

    def get_normal_product_patent(self, product_id):
        patent_list = {}
        patent = Model(ReaPy.get_reactor_source('yeten')).select('public.um_urun_patent',
                                                                 'basvuru_numarasi,patent_turu',
                                                                 {0: {'col': 'urun_id',
                                                                      'operator': '=',
                                                                      'value': product_id,
                                                                      'combiner': 'AND'},
                                                                  1: {'col': 'aktif_mi',
                                                                      'operator': '=',
                                                                      'value': True}
                                                                  },
                                                                 'patent_turu asc', None,
                                                                 False).data()
        if patent is not None:
            for key in patent:
                if key['patent_turu'] not in patent_list:
                    patent_list[key['patent_turu']] = []
                patent_list[key['patent_turu']].append(key['basvuru_numarasi'])

        return patent_list

    def get_normal_product_standards_other(self, product_id, type):
        normal_product = None
        normal_product_std = Model(ReaPy.get_reactor_source('yeten')).select(self.um_urun_standart,
                                                                             'diger',
                                                                             {0: {'col': 'urun_id',
                                                                                  'operator': '=',
                                                                                  'value': product_id,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': True,
                                                                                  'combiner': 'AND'},
                                                                              2: {'col': 'standart_tipi',
                                                                                  'operator': '=',
                                                                                  'value': type,
                                                                                  'combiner': 'AND'},
                                                                              3: {'col': 'standart_id',
                                                                                  'operator': '=',
                                                                                  'value': '00000000-0000-0000-0000-000000000000'}},
                                                                             None, None,
                                                                             False).data()
        if normal_product_std is not None:
            normal_product = normal_product_std[0]['diger']

        return normal_product

    @staticmethod
    def get_technical_spec_key(spec_key):
        spec = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_teknik_ozellik',
                                                               'ozellik',
                                                               {0: {'col': 'teknik_ozellik_id', 'operator': '=',
                                                                    'value': spec_key,
                                                                    }},
                                                               None, None,
                                                               True, True).data()
        if spec is not None:
            return spec[0]['ozellik']
        else:
            return None

    @staticmethod
    def get_technical_spec_unit_key(spec_key):
        spec = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_birim',
                                                               'birim',
                                                               {0: {'col': 'birim_id', 'operator': '=',
                                                                    'value': spec_key,
                                                                    }},
                                                               None, None,
                                                               True, True).data()
        if spec is not None:
            return spec[0]['birim']
        else:
            spec2 = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_teknik_deger',
                                                                    'teknik_deger',
                                                                    {0: {'col': 'deger_id', 'operator': '=',
                                                                         'value': spec_key,
                                                                         }},
                                                                    None, None,
                                                                    True, True).data()
            if spec2 is not None:
                return spec2[0]['teknik_deger']
            else:
                return None

    def get_normal_product_technical_spec(self, product_id):
        technical_spec_list = []
        technical_spec = Model(ReaPy.get_reactor_source('yeten')).select(self.um_teknik_ozellik,
                                                                         'teknik_ozellik_id,teknik_ozellik_diger,deger_tipi,deger1,deger2,birim_teknik_deger_id,birim_teknik_deger_diger',
                                                                         {0: {'col': 'urun_id',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              },
                                                                          },
                                                                         None, None,
                                                                         False).data()

        if technical_spec is not None:
            for key in technical_spec:
                key['anahtar'] = Product.get_technical_spec_key(key['teknik_ozellik_id'])
                key['birim'] = Product.get_technical_spec_unit_key(key['birim_teknik_deger_id'])
                technical_spec_list.append(key)

        return technical_spec_list

    @staticmethod
    def get_product_files(product_id, file_type):

        file_list = Model(ReaPy.get_reactor_source('yeten')).select('public.view_urun_dosya',
                                                                    'dosya_id,dosya_adi',
                                                                    {0: {'col': 'urun_id', 'operator': '=',
                                                                         'value': product_id,
                                                                         'combiner': 'AND'},
                                                                     1: {'col': 'dosya_tipi',
                                                                         'operator': '=',
                                                                         'value': file_type}},
                                                                    None, None,
                                                                    False).data()
        return file_list

    @staticmethod
    def get_cpc_by_id(cpc_id):
        cpc_def = None
        cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                              'aciklama,sembol',
                                                              {0: {'col': 'cpc_id',
                                                                   'operator': '=',
                                                                   'value': cpc_id,
                                                                   }},
                                                              None, None,
                                                              True, True).data()
        if cpc is not None:
            cpc_def = {'label': cpc[0]['aciklama'], 'key': cpc_id, 'sembol': cpc[0]['sembol']}

        return cpc_def

    @staticmethod
    def get_prodtr_by_id(prodtr_id):
        prodtr_def = None
        prodtr = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_prodtr',
                                                                 'adi',
                                                                 {0: {'col': 'prodtr_id',
                                                                      'operator': '=',
                                                                      'value': prodtr_id,
                                                                      }},
                                                                 None, None,
                                                                 True, True).data()
        if prodtr is not None:
            prodtr_def = {'label': prodtr[0]['adi'], 'key': prodtr_id}

        return prodtr_def

    def check_software_product(self, company_id, product_name):
        software_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_yazilim,
                                                                           None,
                                                                           {0: {'col': 'adi', 'operator': '=',
                                                                                'value': product_name,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': company_id}},
                                                                           None, None,
                                                                           False).data()
        return software_product

    def check_normal_product(self, company_id, product_name):
        normal_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_urun,
                                                                         None,
                                                                         {0: {'col': 'adi', 'operator': '=',
                                                                              'value': product_name,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'paydas_id',
                                                                              'operator': '=',
                                                                              'value': company_id,
                                                                              'combiner': 'AND'},
                                                                          2: {'col': 'aktif_mi', 'operator': '=',
                                                                              'value': True}
                                                                          },
                                                                         None, None,
                                                                         False).data()
        return normal_product

    def check_software_product_by_id(self, company_id, product_id):
        software_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_yazilim,
                                                                           None,
                                                                           {0: {'col': 'urun_id', 'operator': '=',
                                                                                'value': product_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': company_id,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': True}},
                                                                           None, None,
                                                                           False).data()

        return software_product

    def check_normal_product_by_id(self, company_id, product_id):
        software_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_urun,
                                                                           None,
                                                                           {0: {'col': 'urun_id', 'operator': '=',
                                                                                'value': product_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': company_id,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': True}},
                                                                           None, None,
                                                                           False).data()

        return software_product

    def get_software_product_by_id(self, product_id):
        software_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_yazilim,
                                                                           None,
                                                                           {0: {'col': 'urun_id', 'operator': '=',
                                                                                'value': product_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': True},
                                                                            },
                                                                           None, None,
                                                                           True).data()
        if software_product is not None:
            try:
                software_product[0]['urun_sinifi_cpc'] = Product.get_cpc_by_id(software_product[0]['urun_sinifi_cpc'])
                software_product[0]['mumkun_kilan_kilit_teknoloji_taksonomi'] = Product.get_software_product_taksonomi(
                    software_product[0]['mumkun_kilan_kilit_teknoloji_taksonomi'])
                software_product[0]['urun_aciklamasi'] = software_product[0]['aciklamasi']
                software_product[0]['product_id'] = product_id
                software_product[0]['prodtr'] = Product.get_prodtr_by_id(software_product[0]['prodtr'])
                software_product[0]['anahtar_kelime'] = Product().get_product_keywords_by_lang(product_id, '1')
                software_product[0]['anahtar_kelime_ingilizce'] = Product().get_product_keywords_by_lang(product_id,
                                                                                                         '2')
                software_product[0]['isletim_sistemi_id'] = Product().get_software_product_os(product_id)
                software_product[0]['dil_id'] = Product().get_software_product_pl(product_id)
                software_product[0]['uyumlu_standartlar'] = Product().get_software_product_standards(product_id)
                software_product[0]['ihracat_ulkesi'] = Product().get_product_export_country(product_id)
                software_product[0]['acik_kaynak_kodlari'] = Product().get_software_product_open_source(product_id)
                del software_product[0]['aciklamasi']
                del software_product[0]['urun_id']
                del software_product[0]['paydas_id']
                del software_product[0]['aktif_mi']
                del software_product[0]['olusturulma_tarihi']
                del software_product[0]['olusturan']
                del software_product[0]['olusturan_ip']
                del software_product[0]['guncelleme_tarihi']
                del software_product[0]['guncelleyen']
                del software_product[0]['guncelleyen_ip']
            except Exception as e:
                print(e)

        return software_product

    def get_products(self, company_id):
        software_product_list = []
        normal_product_list = []
        software_products = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_yazilim,
                                                                            'urun_id as product_id,adi,urun_sinifi_cpc',
                                                                            {
                                                                                0: {'col': 'paydas_id',
                                                                                    'operator': '=',
                                                                                    'value': company_id,
                                                                                    'combiner': 'AND'},
                                                                                1: {'col': 'aktif_mi',
                                                                                    'operator': '=',
                                                                                    'value': True}},
                                                                            'olusturulma_tarihi desc', None,
                                                                            False).data()
        if software_products is not None:
            for key in software_products:
                keywords = Product().get_product_keywords(key['product_id'])
                key['anahtar_kelime'] = keywords
                key['urun_sinifi_cpc'] = Product.get_cpc(key['urun_sinifi_cpc'])
                software_product_list.append(key)
        normal_products = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_urun,
                                                                          'urun_id as product_id,adi,urun_sinifi_cpc,katolog_dosya_yukleme_id,fotograf_dosya_yukleme_id',
                                                                          {
                                                                              0: {'col': 'paydas_id',
                                                                                  'operator': '=',
                                                                                  'value': company_id,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': True}},
                                                                          'olusturulma_tarihi desc', None,
                                                                          False).data()
        if normal_products is not None:
            for key in normal_products:
                key['fotograf_dosya'] = Product().get_product_files(key['product_id'], 2)
                key['katolog_dosya'] = Product().get_product_files(key['product_id'], 1)
                keywords = Product().get_product_keywords(key['product_id'])
                key['anahtar_kelime'] = keywords
                key['urun_sinifi_cpc'] = Product.get_cpc(key['urun_sinifi_cpc'])

                normal_product_list.append(key)

        product_list = {
            'software_products': software_product_list,
            'normal_products': normal_product_list
        }
        return product_list

    def software_product_rollback(self, product_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kutuphane_yazilim, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_anahtar_kelime, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_yazilim_isletim_sistemi, {
            0: {'col': 'yazilim_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_yazilim_dil, {
            0: {'col': 'yazilim_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_yazilim_standarti, {
            0: {'col': 'yazilim_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_ihracat_ulke, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_yazilim_acik_kaynak', {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        return True

    def normal_product_rollback(self, product_id):
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_dosya', {
            0: {'col': 'urun_id', 'operator': '=', 'value': product_id}
        })
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kutuphane_urun, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_anahtar_kelime, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_urun_standart, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_hangi_urunde_kullaniliyor, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_hangi_firmanin_alt_yuklenicisi, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_teknik_ozellik, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_gorev_fonksiyonu, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_ihracat_kisiti', {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_patent', {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        return True

    def software_product_delete(self, product_id, user, user_ip):
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_kutuphane_yazilim,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})

        Model(ReaPy.get_reactor_source('yeten')).update(self.um_anahtar_kelime,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_ihracat_ulke,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_isletim_sistemi,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'yazilim_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_dil,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'yazilim_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_standarti,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'yazilim_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        return True

    def add_software_product(self, product_code, product_id, company_id, user, user_ip, product_data):

        try:
            product_data['yabancidan_tedarik_ediliyorsa_nedeni']
        except KeyError:
            product_data['yabancidan_tedarik_ediliyorsa_nedeni'] = ""
        try:
            product_data['ihracat_ulkesi']
        except KeyError:
            product_data['ihracat_ulkesi'] = ""

        try:
            product_data['yol_haritasi']
        except KeyError:
            product_data['yol_haritasi'] = ""

        if 'lisans_ucreti' not in product_data:
            product_data['lisans_ucreti'] = None
        if 'lisans_turu' not in product_data:
            product_data['lisans_turu'] = None
        if 'lisans_suresi' not in product_data:
            product_data['lisans_suresi'] = None
        if 'lisans_ulkesi' not in product_data:
            product_data['lisans_ulkesi'] = None
        else:
            product_data['lisans_ulkesi'] = str(product_data['lisans_ulkesi'])
        add_product = Model(ReaPy.get_reactor_source('yeten')). \
            insert(self.um_kutuphane_yazilim,
                   {
                       0: {
                           'urun_id': str(product_id),
                           'paydas_id': str(company_id),
                           'adi': product_data['adi'],
                           'aciklamasi': product_data['urun_aciklamasi'],
                           'aktif_kullanici_sayisi': product_data['aktif_kullanici_sayisi'],
                           'gomulu_mu': product_data['gomulu_mu'],
                           'acik_kaynakli_urun_kullaniliyor_mu': product_data['acik_kaynakli_urun_kullaniliyor_mu'],
                           'gelistirme_suresi': product_data['gelistirme_suresi'],
                           'lisans_altinda_mi_satiliyor': product_data['lisans_altinda_mi_satiliyor'],
                           'lisans_altinda_mi_kullaniliyor': product_data['lisans_altinda_mi_kullaniliyor'],
                           'lisans_ucreti': product_data['lisans_ucreti'],
                           'lisans_turu': product_data['lisans_turu'],
                           'lisans_suresi': product_data['lisans_suresi'],
                           'lisans_ulkesi': product_data['lisans_ulkesi'],
                           'ihracat_yapiliyor_mu': product_data['ihracat_yapiliyor_mu'],
                           'api_destegi_var_mi': product_data['api_destegi_var_mi'],
                           'yol_haritasi': product_data['yol_haritasi'],
                           'mobil_uyumlu': product_data['mobil_uyumlu'],
                           'dual_use': product_data['dual_use'],
                           'ssb_urunlerinde_kullaniliyor_mu': product_data['ssb_urunlerinde_kullaniliyor_mu'],
                           'fiziksel_bir_donanim_gerekiyor_mu': product_data['fiziksel_bir_donanim_gerekiyor_mu'],
                           'muadili_yerli_yabanci_urunler': product_data['muadili_yerli_yabanci_urunler'],
                           'yabancidan_tedarik_ediliyorsa_nedeni': product_data['yabancidan_tedarik_ediliyorsa_nedeni'],
                           'urun_sinifi_cpc': str(product_data['urun_sinifi_cpc']),
                           'mumkun_kilan_kilit_teknoloji_taksonomi': product_data[
                               'mumkun_kilan_kilit_teknoloji_taksonomi'],
                           'prodtr': str(product_data['prodtr']),
                           'ssb_katologunda_gorunsun_mu': product_data['ssb_katologunda_gorunsun_mu'],
                           'urun_numarasi': product_code,
                           'olusturan': user,
                           'olusturan_ip': user_ip,
                           'aktif_mi': True

                       }
                   }).data()
        if add_product != 1:
            Product().software_product_rollback(str(product_id))
            return False

        for key in product_data['ihracat_ulkesi']:
            add_country = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_ihracat_ulke,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'ulke_id': key,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_country != 1:
                Product().software_product_rollback(str(product_id))
                return False

        for key in product_data['anahtar_kelime']:
            add_keyword_tr = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_anahtar_kelime,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'anahtar_kelime': key,
                               'dil': 1,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_keyword_tr != 1:
                Product().software_product_rollback(str(product_id))
                return False

        for key in product_data['anahtar_kelime_ingilizce']:
            add_keyword_tr = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_anahtar_kelime,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'anahtar_kelime': key,
                               'dil': 2,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_keyword_tr != 1:
                Product().software_product_rollback(str(product_id))
                return False

        for key in product_data['isletim_sistemi_id']:
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_isletim_sistemi,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'isletim_sistemi_id': str(key),
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().software_product_rollback(str(product_id))
                return False

        for key in product_data['dil_id']:
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_dil,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'dil_id': str(key),
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().software_product_rollback(str(product_id))
                return False

        for key in product_data['uyumlu_standartlar']:
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_standarti,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'standart_id': str(key),
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().software_product_rollback(str(product_id))
                return False

        if 'acik_kaynak_kodlari' in product_data:
            for key in product_data['acik_kaynak_kodlari']:
                add_os = Model(ReaPy.get_reactor_source('yeten')). \
                    insert('public.um_yazilim_acik_kaynak',
                           {
                               0: {
                                   'urun_id': str(product_id),
                                   'acik_kaynak_adi': key,
                                   'olusturan': user,
                                   'olusturan_ip': user_ip
                               }
                           }).data()
                if add_os != 1:
                    Product().software_product_rollback(str(product_id))
                    return False

        return True

    def update_software_product(self, product_id, company_id, user, user_ip, product_data):
        if 'lisans_ucreti' not in product_data:
            product_data['lisans_ucreti'] = None
        if 'lisans_turu' not in product_data:
            product_data['lisans_turu'] = None
        if 'lisans_suresi' not in product_data:
            product_data['lisans_suresi'] = None
        if 'lisans_ulkesi' not in product_data:
            product_data['lisans_ulkesi'] = None
        else:
            product_data['lisans_ulkesi'] = str(product_data['lisans_ulkesi'])
        add_product = Model(ReaPy.get_reactor_source('yeten')). \
            update(self.um_kutuphane_yazilim,
                   {

                       'SET': {
                           'adi': product_data['adi'],
                           'aciklamasi': product_data['urun_aciklamasi'],
                           'aktif_kullanici_sayisi': product_data['aktif_kullanici_sayisi'],
                           'gomulu_mu': product_data['gomulu_mu'],
                           'acik_kaynakli_urun_kullaniliyor_mu': product_data['acik_kaynakli_urun_kullaniliyor_mu'],
                           'gelistirme_suresi': product_data['gelistirme_suresi'],
                           'lisans_altinda_mi_satiliyor': product_data['lisans_altinda_mi_satiliyor'],
                           'lisans_altinda_mi_kullaniliyor': product_data['lisans_altinda_mi_kullaniliyor'],
                           'lisans_ucreti': product_data['lisans_ucreti'],
                           'lisans_turu': product_data['lisans_turu'],
                           'lisans_suresi': product_data['lisans_suresi'],
                           'lisans_ulkesi': product_data['lisans_ulkesi'],
                           'ihracat_yapiliyor_mu': product_data['ihracat_yapiliyor_mu'],
                           'api_destegi_var_mi': product_data['api_destegi_var_mi'],
                           'yol_haritasi': product_data['yol_haritasi'],
                           'mobil_uyumlu': product_data['mobil_uyumlu'],
                           'dual_use': product_data['dual_use'],
                           'ssb_urunlerinde_kullaniliyor_mu': product_data['ssb_urunlerinde_kullaniliyor_mu'],
                           'fiziksel_bir_donanim_gerekiyor_mu': product_data['fiziksel_bir_donanim_gerekiyor_mu'],
                           'muadili_yerli_yabanci_urunler': product_data['muadili_yerli_yabanci_urunler'],
                           'yabancidan_tedarik_ediliyorsa_nedeni': product_data['yabancidan_tedarik_ediliyorsa_nedeni'],
                           'urun_sinifi_cpc': str(product_data['urun_sinifi_cpc']),
                           'mumkun_kilan_kilit_teknoloji_taksonomi': product_data[
                               'mumkun_kilan_kilit_teknoloji_taksonomi'],
                           'prodtr': str(product_data['prodtr']),
                           'ssb_katologunda_gorunsun_mu': product_data['ssb_katologunda_gorunsun_mu'],
                           'guncelleme_tarihi': str(datetime.datetime.now()),
                           'guncelleyen': user,
                           'guncelleyen_ip': user_ip
                       },
                       'CONDITION': {0: {'col': 'urun_id', 'operator': '=',
                                         'value': product_id,
                                         'combiner': 'AND'},
                                     1: {'col': 'paydas_id',
                                         'operator': '=',
                                         'value': company_id}}
                   }).data()
        if add_product != 1:
            return False
        if product_data['acik_kaynakli_urun_kullaniliyor_mu']:
            Product().update_software_product_open_source(product_data['acik_kaynak_kodlari'], product_id, user,
                                                          user_ip)
        else:
            Product().delete_software_product_open_source(product_data['acik_kaynakli_urun_kullaniliyor_mu'],
                                                          product_id)

        Product().update_product_keyword(product_data['anahtar_kelime'], product_id, 1, user, user_ip)
        Product().update_product_keyword(product_data['anahtar_kelime_ingilizce'], product_id, 2, user, user_ip)
        Product().update_product_os(product_data['isletim_sistemi_id'], product_id, user, user_ip)
        Product().update_product_pl(product_data['dil_id'], product_id, user, user_ip)
        Product().update_product_standard(product_data['uyumlu_standartlar'], product_id, user, user_ip)
        Product().update_product_standard(product_data['uyumlu_standartlar'], product_id, user, user_ip)

        if product_data['ihracat_yapiliyor_mu']:
            if 'ihracat_ulkesi' in product_data:
                Product().update_product_export_country(product_data['ihracat_ulkesi'], product_id, user, user_ip)
        else:
            Product().delete_product_export_country(product_id)

        return True

    def update_product_keyword(self, keywords_data, product_id, lang, user, user_ip):
        keywords_inserts = []
        keywords_exists = []
        keywords_deletes = []
        keywords = Product().get_product_keywords_by_lang(str(product_id), lang)
        try:
            for key in keywords:
                if key['key'] not in keywords_data:
                    keywords_deletes.append(key['key'])
                else:
                    keywords_exists.append(key['key'])
        except Exception as e:
            print(e)

        for key in keywords_data:
            if key not in keywords_exists:
                keywords_inserts.append(key)

        for key in keywords_inserts:
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_anahtar_kelime,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'anahtar_kelime': key,
                               'dil': lang,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

        for key in keywords_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_anahtar_kelime,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'urun_id', 'operator': '=',
                                                                        'value': str(product_id),
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'anahtar_kelime',
                                                                        'operator': '=',
                                                                        'value': key}}})

    def update_product_technical_spec(self, spec_data, product_id, user, user_ip):
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_teknik_ozellik,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        for key in spec_data:
            if 'teknik_ozellik_diger' not in key:
                key['teknik_ozellik_diger'] = None
            if 'birim_teknik_deger_diger' not in key:
                key['birim_teknik_deger_diger'] = None
            if 'deger1' not in key:
                key['deger1'] = None
            if 'deger2' not in key:
                key['deger2'] = None
            if 'deger_tipi' not in key:
                key['deger_tipi'] = None
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_teknik_ozellik,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'teknik_ozellik_id': key['teknik_ozellik_id'],
                               'teknik_ozellik_diger': key['teknik_ozellik_diger'],
                               'deger_tipi': key['deger_tipi'],
                               'deger1': key['deger1'],
                               'deger2': key['deger2'],
                               'birim_teknik_deger_id': key['birim_teknik_deger_id'],
                               'birim_teknik_deger_diger': key['birim_teknik_deger_diger'],
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

    def update_product_os(self, os_data, product_id, user, user_ip):
        os_inserts = []
        os_exists = []
        os_deletes = []
        oss = Product().get_software_product_os(str(product_id))
        for key in oss:
            if key['value'] not in os_data:
                os_deletes.append(key['value'])
            else:
                os_exists.append(key['value'])

        for key in os_data:
            if key not in os_exists:
                os_inserts.append(key)

        for key in os_inserts:
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_isletim_sistemi,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'isletim_sistemi_id': key,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

        for key in os_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_isletim_sistemi,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'yazilim_id', 'operator': '=',
                                                                        'value': str(product_id),
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'isletim_sistemi_id',
                                                                        'operator': '=',
                                                                        'value': key}}})

    def update_product_pl(self, pl_data, product_id, user, user_ip):
        pl_inserts = []
        pl_exists = []
        pl_deletes = []
        pls = Product().get_software_product_pl(str(product_id))
        for key in pls:
            if key['value'] not in pl_data:
                pl_deletes.append(key['value'])
            else:
                pl_exists.append(key['value'])

        for key in pl_data:
            if key not in pl_exists:
                pl_inserts.append(key)

        for key in pl_inserts:
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_dil,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'dil_id': key,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

        for key in pl_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_dil,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'yazilim_id', 'operator': '=',
                                                                        'value': str(product_id),
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'dil_id',
                                                                        'operator': '=',
                                                                        'value': key}}})

    def update_product_standard(self, standard_data, product_id, user, user_ip):
        standard_inserts = []
        standard_exists = []
        standard_deletes = []
        standards = Product().get_software_product_standards(str(product_id))
        for key in standards:
            if key['value'] not in standard_data:
                standard_deletes.append(key['value'])
            else:
                standard_exists.append(key['value'])

        for key in standard_data:
            if key not in standard_exists:
                standard_inserts.append(key)

        for key in standard_inserts:
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_yazilim_standarti,
                       {
                           0: {
                               'yazilim_id': str(product_id),
                               'standart_id': key,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

        for key in standard_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_yazilim_standarti,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'yazilim_id', 'operator': '=',
                                                                        'value': str(product_id),
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'standart_id',
                                                                        'operator': '=',
                                                                        'value': key}}})

    def update_product_export_country_constraints(self, constraints, product_id, user, user_ip):
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_ihracat_kisiti', {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})

        for key in constraints:
            for c_key in constraints[key]:
                Model(ReaPy.get_reactor_source('yeten')).insert('public.um_ihracat_kisiti',
                                                                {
                                                                    0: {
                                                                        'urun_id': str(product_id),
                                                                        'ulke_id': key,
                                                                        'ihracat_kisiti': c_key,
                                                                        'olusturan': user,
                                                                        'olusturan_ip': user_ip
                                                                    }
                                                                }).data()

    def update_normal_product_standard(self, standard_data, product_id, type, user, user_ip):
        standard_inserts = []
        standard_exists = []
        standard_deletes = []
        standards = Product().get_normal_product_standards(str(product_id), type)
        for key in standards:
            if key not in standard_data[0]:
                standard_deletes.append(key)
            else:
                standard_exists.append(key)

        for key in standard_data[0]:
            if key not in standard_exists:
                standard_inserts.append(key)

        for key in standard_inserts:
            sartlar_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                sartlar_diger = standard_data[1]
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_urun_standart,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'standart_id': str(key),
                               'diger': sartlar_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip,
                               'standart_tipi': type
                           }
                       }).data()
        for key in standard_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_urun_standart,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'urun_id', 'operator': '=',
                                                                        'value': product_id,
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'standart_id',
                                                                        'operator': '=',
                                                                        'value': key, 'combiner': 'AND'},
                                                                    2: {'col': 'standart_tipi',
                                                                        'operator': '=',
                                                                        'value': type}}})
        for key in standard_exists:
            if key == '00000000-0000-0000-0000-000000000000':
                sartlar_diger = standard_data[1]
                Model(ReaPy.get_reactor_source('yeten')).update(self.um_urun_standart,
                                                                {'SET': {
                                                                    'diger': sartlar_diger,
                                                                    'guncelleme_tarihi': str(
                                                                        datetime.datetime.now()),
                                                                    "guncelleyen": user,
                                                                    "guncelleyen_ip": user_ip
                                                                },
                                                                    'CONDITION': {
                                                                        0: {'col': 'urun_id', 'operator': '=',
                                                                            'value': product_id,
                                                                            'combiner': 'AND'},
                                                                        1: {'col': 'standart_id',
                                                                            'operator': '=',
                                                                            'value': key, 'combiner': 'AND'},
                                                                        2: {'col': 'standart_tipi',
                                                                            'operator': '=',
                                                                            'value': type, 'combiner': 'AND'},
                                                                        3: {'col': 'diger',
                                                                            'operator': '!=',
                                                                            'value': sartlar_diger}}})

    def update_product_export_country(self, country_data, product_id, user, user_ip):
        country_inserts = []
        country_exists = []
        country_deletes = []
        countries = Product().get_product_export_country(str(product_id))
        for key in countries:
            if key not in country_data:
                country_deletes.append(key)
            else:
                country_exists.append(key)

        for key in country_data:
            if key not in country_exists:
                country_inserts.append(key)

        for key in country_inserts:
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_ihracat_ulke,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'ulke_id': key,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

        for key in country_deletes:
            Model(ReaPy.get_reactor_source('yeten')).update(self.um_ihracat_ulke,
                                                            {'SET': {
                                                                'aktif_mi': False,
                                                                'guncelleme_tarihi': str(datetime.datetime.now()),
                                                                "guncelleyen": user,
                                                                "guncelleyen_ip": user_ip
                                                            },
                                                                'CONDITION': {
                                                                    0: {'col': 'urun_id', 'operator': '=',
                                                                        'value': str(product_id),
                                                                        'combiner': 'AND'},
                                                                    1: {'col': 'ulke_id',
                                                                        'operator': '=',
                                                                        'value': key}}})

    def delete_product_export_country(self, product_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_ihracat_ulke, {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})

    def add_normal_product(self, product_code, product_id, company_id, user, user_ip, product_data):
        if 'katolog_dosya_yukleme_id' not in product_data:
            product_data['katolog_dosya_yukleme_id'] = None
        if 'fotograf_dosya_yukleme_id' not in product_data:
            product_data['fotograf_dosya_yukleme_id'] = None
        if 'ssb_proje_adi' not in product_data:
            product_data['ssb_proje_adi'] = None
        if 'gtip_no' not in product_data:
            product_data['gtip_no'] = None
        if 'nato_stok_numarasi' not in product_data:
            product_data['nato_stok_numarasi'] = None
        if 'ihracat_kisiti_ulke' not in product_data:
            product_data['ihracat_kisiti_ulke'] = None
        if 'hangi_firmanin_alt_yuklenicisisiniz_diger' not in product_data:
            product_data['hangi_firmanin_alt_yuklenicisisiniz_diger'] = None
        if 'ihracat_yapilan_ulke' not in product_data:
            product_data['ihracat_yapilan_ulke'] = []
        add_product = Model(ReaPy.get_reactor_source('yeten')). \
            insert(self.um_kutuphane_urun,
                   {
                       0: {
                           'urun_id': str(product_id),
                           'paydas_id': str(company_id),
                           'adi': product_data['adi'],
                           'urun_numarasi': product_code,
                           'aciklamasi': product_data['urun_aciklamasi'],
                           'urun_sinifi_cpc': str(product_data['urun_sinifi_cpc']),
                           'mumkun_kilan_kilit_teknoloji_taksonomi': product_data[
                               'mumkun_kilan_kilit_teknoloji_taksonomi'],
                           'yillik_uretim_kapasitesi': product_data['yillik_uretim_kapasitesi'],
                           'tasarim': product_data['tasarim'],
                           'uretim_yeri_id': product_data['uretim_yeri_id'],
                           'lisans_altinda_uretim_mi': product_data['lisans_altinda_uretim_mi'],
                           'uretim_suresi': product_data['uretim_suresi'],
                           'yillik_uretim_kapasitesi_birim': product_data['yillik_uretim_kapasitesi_birim'],
                           'teknoloji_hazirlik_seviyesi': product_data['teknoloji_hazirlik_seviyesi'],
                           'ihracat_kisiti_var_mi': product_data['ihracat_kisiti_var_mi'],
                           'askeri_alanda_kullanim_dual_use': product_data['askeri_alanda_kullanim_dual_use'],
                           'ssb_projesi_mi': product_data['ssb_projesi_mi'],
                           'ssb_proje_adi': product_data['ssb_proje_adi'],
                           'marka': product_data['marka'],
                           'model': product_data['model'],
                           'gtip_no': product_data['gtip_no'],
                           'prodtr': product_data['prodtr'],
                           'alt_yuklenici_varmi': product_data['alt_yuklenici_varmi'],
                           'nato_stok_numarasi': product_data['nato_stok_numarasi'],
                           'ssb_katologunda_gorunsun_mu': product_data['ssb_katologunda_gorunsun_mu'],
                           'yurtdisi_ihracat_yapiyor_mu': product_data['yurtdisi_ihracat_yapiyor_mu'],
                           'ihracat_kisiti_ulke_id': product_data['ihracat_kisiti_ulke'],
                           'olusturan': user,
                           'olusturan_ip': user_ip
                       }
                   }).data()
        if add_product != 1:
            Product().normal_product_rollback(str(product_id))
            return False

        if product_data['katolog_dosya'] is not None and isinstance(product_data['katolog_dosya'], list):
            for key in product_data['katolog_dosya']:
                if isinstance(key, str):
                    Model(ReaPy.get_reactor_source('yeten')).insert('public.um_urun_dosya',
                                                                    {
                                                                        0: {
                                                                            'urun_id': str(product_id),
                                                                            'dosya_id': key,
                                                                            'dosya_tipi': 1,
                                                                            'olusturan': user,
                                                                            'olusturan_ip': user_ip
                                                                        }
                                                                    }).data()

        if product_data['fotograf_dosya'] is not None and isinstance(product_data['katolog_dosya'], list):
            for key in product_data['fotograf_dosya']:
                if isinstance(key, str):
                    Model(ReaPy.get_reactor_source('yeten')).insert('public.um_urun_dosya',
                                                                    {
                                                                        0: {
                                                                            'urun_id': str(product_id),
                                                                            'dosya_id': key,
                                                                            'dosya_tipi': 2,
                                                                            'olusturan': user,
                                                                            'olusturan_ip': user_ip
                                                                        }
                                                                    }).data()

        if 'patent_bilgisi' in product_data:
            for key in product_data['patent_bilgisi']:
                for value in product_data['patent_bilgisi'][key]:
                    add_os = Model(ReaPy.get_reactor_source('yeten')). \
                        insert('public.um_urun_patent',
                               {
                                   0: {
                                       'urun_id': str(product_id),
                                       'patent_turu': key,
                                       'basvuru_numarasi': value,
                                       'olusturan': user,
                                       'olusturan_ip': user_ip
                                   }
                               }).data()
                    if add_os != 1:
                        Product().normal_product_rollback(str(product_id))
                        return False

        if 'ihracat_kisiti_ulkeler' in product_data:
            Product().update_product_export_country_constraints(
                product_data['ihracat_kisiti_ulkeler'],
                product_id, user, user_ip)
        for key in product_data['anahtar_kelime']:
            add_keyword_tr = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_anahtar_kelime,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'anahtar_kelime': key,
                               'dil': 1,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_keyword_tr != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['anahtar_kelime_ingilizce']:
            add_keyword_tr = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_anahtar_kelime,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'anahtar_kelime': key,
                               'dil': 2,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_keyword_tr != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['cevre_sartlari']:
            cevre_sartlari_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                cevre_sartlari_diger = product_data['cevre_sartlari_diger']
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_urun_standart,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'standart_id': str(key),
                               'diger': cevre_sartlari_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip,
                               'standart_tipi': 1
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['uretim_proses']:
            uretim_proses_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                uretim_proses_diger = product_data['uretim_proses_diger']
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_urun_standart,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'standart_id': str(key),
                               'diger': uretim_proses_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip,
                               'standart_tipi': 3
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['elektro_uyum']:
            elektro_uyum_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                elektro_uyum_diger = product_data['elektro_uyum_diger']
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_urun_standart,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'standart_id': str(key),
                               'diger': elektro_uyum_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip,
                               'standart_tipi': 2
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['hangi_urun']:
            hangi_urun_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                hangi_urun_diger = product_data['hangi_urun_diger']
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_hangi_urunde_kullaniliyor,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'kullanilan_urun_id': str(key),
                               'diger': hangi_urun_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['gorev_fonksiyonu']:
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_gorev_fonksiyonu,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'gorev_fonksiyonu_id': str(key),
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['hangi_firmanin_alt_yuklenicisisiniz']:
            hangi_firmanin_alt_yuklenicisisiniz_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                hangi_firmanin_alt_yuklenicisisiniz_diger = product_data['hangi_firmanin_alt_yuklenicisisiniz_diger']
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_hangi_firmanin_alt_yuklenicisi,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'paydas_id': str(key),
                               'diger': hangi_firmanin_alt_yuklenicisisiniz_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['ihracat_yapilan_ulke']:
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_ihracat_ulke,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'ulke_id': str(key),
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        for key in product_data['teknik_ozellik']:
            if 'teknik_ozellik_diger' not in key:
                key['teknik_ozellik_diger'] = None
            if 'birim_teknik_deger_diger' not in key:
                key['birim_teknik_deger_diger'] = None
            if 'deger1' not in key:
                key['deger1'] = None
            if 'deger2' not in key:
                key['deger2'] = None
            if 'deger_tipi' not in key:
                key['deger_tipi'] = None
            add_os = Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_teknik_ozellik,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'teknik_ozellik_id': key['teknik_ozellik_id'],
                               'teknik_ozellik_diger': key['teknik_ozellik_diger'],
                               'deger_tipi': key['deger_tipi'],
                               'deger1': key['deger1'],
                               'deger2': key['deger2'],
                               'birim_teknik_deger_id': key['birim_teknik_deger_id'],
                               'birim_teknik_deger_diger': key['birim_teknik_deger_diger'],
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()
            if add_os != 1:
                Product().normal_product_rollback(str(product_id))
                return False

        return True

    def normal_product_delete(self, product_id, user, user_ip):
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_kutuphane_urun,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})

        Model(ReaPy.get_reactor_source('yeten')).update(self.um_anahtar_kelime,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_ihracat_ulke,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_urun_standart,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_teknik_ozellik,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_hangi_urunde_kullaniliyor,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_hangi_firmanin_alt_yuklenicisi,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})

        Model(ReaPy.get_reactor_source('yeten')).update(self.um_gorev_fonksiyonu,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        return True

    def get_normal_product_by_id(self, product_id):
        normal_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kutuphane_urun,
                                                                         None,
                                                                         {0: {'col': 'urun_id', 'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True},
                                                                          },
                                                                         None, None,
                                                                         True).data()
        if normal_product is not None:
            try:

                normal_product[0]['urun_sinifi_cpc'] = Product.get_cpc_by_id(normal_product[0]['urun_sinifi_cpc'])
                normal_product[0]['urun_aciklamasi'] = normal_product[0]['aciklamasi']
                normal_product[0]['product_id'] = product_id
                normal_product[0]['fotograf_dosya'] = Product().get_product_files(product_id, 2)
                normal_product[0]['katolog_dosya'] = Product().get_product_files(product_id, 1)
                normal_product[0]['prodtr_id'] = normal_product[0]['prodtr']
                normal_product[0]['prodtr'] = Product.get_prodtr_by_id(normal_product[0]['prodtr'])
                normal_product[0]['anahtar_kelime'] = Product().get_product_keywords_by_lang(product_id, '1')
                normal_product[0]['anahtar_kelime_ingilizce'] = Product().get_product_keywords_by_lang(product_id, '2')
                normal_product[0]['cevre_sartlari'] = Product().get_normal_product_standards(product_id, 1)
                normal_product[0]['elektro_uyum'] = Product().get_normal_product_standards(product_id, 2)
                normal_product[0]['uretim_proses'] = Product().get_normal_product_standards(product_id, 3)
                normal_product[0]['mumkun_kilan_kilit_teknoloji_taksonomi'] = Product.get_normal_product_taksonomi(
                    normal_product[0]['mumkun_kilan_kilit_teknoloji_taksonomi'])

                normal_product[0]['cevre_sartlari_diger'] = Product().get_normal_product_standards_other(product_id, 1)
                normal_product[0]['elektro_uyum_diger'] = Product().get_normal_product_standards_other(product_id, 2)
                normal_product[0]['uretim_proses_diger'] = Product().get_normal_product_standards_other(product_id, 3)
                normal_product[0]['teknik_ozellik'] = Product().get_normal_product_technical_spec(product_id)
                normal_product[0]['gorev_fonksiyonu'] = Product().get_normal_product_duty_function(product_id)
                normal_product[0]['patent_bilgisi'] = Product().get_normal_product_patent(product_id)

                normal_product[0]['hangi_urun'] = Product().get_product_sub_product(product_id)
                normal_product[0]['hangi_urun_diger'] = Product().get_product_sub_product_other(product_id)
                normal_product[0]['hangi_firmanin_alt_yuklenicisisiniz'] = Product().get_product_sub_contractor(
                    product_id)
                normal_product[0][
                    'hangi_firmanin_alt_yuklenicisisiniz_diger'] = Product().get_product_sub_contractor_other(
                    product_id)
                normal_product[0]['ihracat_ulkesi'] = Product().get_product_export_country(product_id)
                normal_product[0]['ihracat_kisiti_ulkeler'] = Product().get_product_export_country_constraints(
                    product_id)
                del normal_product[0]['aciklamasi']
                del normal_product[0]['urun_id']
                del normal_product[0]['paydas_id']
                del normal_product[0]['aktif_mi']
                del normal_product[0]['olusturulma_tarihi']
                del normal_product[0]['olusturan']
                del normal_product[0]['olusturan_ip']
                del normal_product[0]['guncelleme_tarihi']
                del normal_product[0]['guncelleyen']
                del normal_product[0]['guncelleyen_ip']
            except Exception as e:
                print(e)

        return normal_product

    def get_product_sub_contractor(self, product_id):
        sub_contractor_list = []
        sub_contractor = Model(ReaPy.get_reactor_source('yeten')).select(
            self.um_hangi_firmanin_alt_yuklenicisi,
            'paydas_id',
            {
                0: {'col': 'urun_id',
                    'operator': '=',
                    'value': product_id,
                    'combiner': 'AND'},
                2: {'col': 'aktif_mi',
                    'operator': '=',
                    'value': True}},
            None, None,
            False).data()
        if sub_contractor is not None:
            for key in sub_contractor:
                sub_contractor_list.append(key['paydas_id'])

        return sub_contractor_list

    def get_product_sub_contractor_other(self, product_id):
        sub_contractor_list = []
        sub_contractor = Model(ReaPy.get_reactor_source('yeten')).select(
            self.um_hangi_firmanin_alt_yuklenicisi,
            'diger',
            {
                0: {'col': 'urun_id',
                    'operator': '=',
                    'value': product_id,
                    'combiner': 'AND'},
                2: {'col': 'aktif_mi',
                    'operator': '=',
                    'value': True,
                    'combiner': 'AND'},
                3: {'col': 'paydas_id',
                    'operator': '=',
                    'value': '00000000-0000-0000-0000-000000000000',
                    }},
            None, None,
            True).data()
        if sub_contractor is not None:
            for key in sub_contractor:
                sub_contractor_list.append(key['diger'])

            return sub_contractor_list[0]
        else:
            return None

    def upload_product_files(self, files, file_type, product_id, user, user_ip, is_updated=False):
        f_type = 'urun_fotograf'
        if file_type == 1:
            f_type = 'urun_katalog'
        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_dosya', {
            0: {'col': 'urun_id', 'operator': '=', 'value': product_id, 'combiner': 'AND'},
            1: {'col': 'dosya_tipi', 'operator': '=', 'value': file_type}
        })
        condition = {0: {
            'urun_id': product_id,
            'dosya_tipi': file_type
        }}
        if is_updated:
            condition[0].update(
                {'guncelleyen': user, 'guncelleyen_ip': user_ip, 'guncelleme_tarihi': str(ReaPy.rea_now())})
        else:
            condition[0].update({'olusturan': user, 'olusturan_ip': user_ip})

        for item in files:
            if isinstance(item, dict):
                condition[0].update({'dosya_id': item['dosya_id']})
                upload_katalog = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_urun_dosya',
                                                                                 condition).data()
                upload_type = Model(ReaPy.get_reactor_source('yeten')).update('public.dosya_yukleme',
                                                                              {'SET': {'dosya_turu': f_type},
                                                                               'CONDITION': {
                                                                                   0: {'col': 'dosya_yukleme_id',
                                                                                       'operator': '=',
                                                                                       'value': item['dosya_id'],
                                                                                       }}}).data()
                if upload_katalog != 1 and upload_type != 1:
                    Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_dosya', {
                        0: {'col': 'urun_id', 'operator': '=', 'value': product_id, 'combiner': 'AND'},
                        1: {'col': 'dosya_tipi', 'operator': '=', 'value': file_type}
                    })
                    return False

    def update_normal_product(self, product_id, company_id, user, user_ip, product_data):
        if 'katolog_dosya' not in product_data:
            product_data['katolog_dosya'] = None
        if 'fotograf_dosya' not in product_data:
            product_data['fotograf_dosya'] = None
        if 'ssb_proje_adi' not in product_data:
            product_data['ssb_proje_adi'] = None
        if 'gtip_no' not in product_data:
            product_data['gtip_no'] = None
        if 'nato_stok_numarasi' not in product_data:
            product_data['nato_stok_numarasi'] = None
        if 'nato_stok_numarasi' not in product_data:
            product_data['nato_stok_numarasi'] = None
        if 'ihracat_kisiti_ulke' not in product_data:
            product_data['ihracat_kisiti_ulke'] = None
        if 'patent_bilgisi' not in product_data:
            product_data['patent_bilgisi'] = None

        add_product = Model(ReaPy.get_reactor_source('yeten')). \
            update(self.um_kutuphane_urun,
                   {
                       'SET': {
                           'urun_id': str(product_id),
                           'paydas_id': str(company_id),
                           'adi': product_data['adi'],
                           'aciklamasi': product_data['urun_aciklamasi'],
                           'urun_sinifi_cpc': str(product_data['urun_sinifi_cpc']),
                           'mumkun_kilan_kilit_teknoloji_taksonomi': product_data[
                               'mumkun_kilan_kilit_teknoloji_taksonomi'],
                           'yillik_uretim_kapasitesi': product_data['yillik_uretim_kapasitesi'],
                           'tasarim': product_data['tasarim'],
                           'uretim_yeri_id': product_data['uretim_yeri_id'],
                           'lisans_altinda_uretim_mi': product_data['lisans_altinda_uretim_mi'],
                           'uretim_suresi': product_data['uretim_suresi'],
                           'yillik_uretim_kapasitesi_birim': product_data['yillik_uretim_kapasitesi_birim'],
                           'teknoloji_hazirlik_seviyesi': product_data['teknoloji_hazirlik_seviyesi'],
                           'ihracat_kisiti_var_mi': product_data['ihracat_kisiti_var_mi'],
                           'askeri_alanda_kullanim_dual_use': product_data['askeri_alanda_kullanim_dual_use'],
                           'ssb_projesi_mi': product_data['ssb_projesi_mi'],
                           'ssb_proje_adi': product_data['ssb_proje_adi'],
                           'marka': product_data['marka'],
                           'model': product_data['model'],
                           'gtip_no': product_data['gtip_no'],
                           'prodtr': product_data['prodtr_id'],
                           'alt_yuklenici_varmi': product_data['alt_yuklenici_varmi'],
                           'nato_stok_numarasi': product_data['nato_stok_numarasi'],
                           'ssb_katologunda_gorunsun_mu': product_data['ssb_katologunda_gorunsun_mu'],
                           'yurtdisi_ihracat_yapiyor_mu': product_data['yurtdisi_ihracat_yapiyor_mu'],
                           'ihracat_kisiti_ulke_id': product_data['ihracat_kisiti_ulke'],
                           'guncelleme_tarihi': str(datetime.datetime.now()),
                           'guncelleyen': user,
                           'guncelleyen_ip': user_ip
                       },
                       'CONDITION': {0: {'col': 'urun_id', 'operator': '=',
                                         'value': product_id,
                                         'combiner': 'AND'},
                                     1: {'col': 'paydas_id',
                                         'operator': '=',
                                         'value': company_id}}
                   }).data()
        try:
            Model(ReaPy.get_reactor_source('yeten')).delete(self.um_gorev_fonksiyonu, {
                0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
            for key in product_data['gorev_fonksiyonu']:
                add_os = Model(ReaPy.get_reactor_source('yeten')). \
                    insert(self.um_gorev_fonksiyonu,
                           {
                               0: {
                                   'urun_id': str(product_id),
                                   'gorev_fonksiyonu_id': str(key),
                                   'olusturan': user,
                                   'olusturan_ip': user_ip
                               }
                           }).data()
                if add_os != 1:
                    Product().normal_product_rollback(str(product_id))
                    return False
        except Exception as e:
            print(e)

        if add_product != 1:
            return False

        Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_dosya', {
            0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}
        })

        if product_data['katolog_dosya'] is not None and isinstance(product_data['katolog_dosya'], list):
            for key in product_data['katolog_dosya']:
                if isinstance(key, str):
                    Model(ReaPy.get_reactor_source('yeten')).insert('public.um_urun_dosya',
                                                                    {
                                                                        0: {
                                                                            'urun_id': str(product_id),
                                                                            'dosya_id': key,
                                                                            'dosya_tipi': 1,
                                                                            'olusturan': user,
                                                                            'olusturan_ip': user_ip
                                                                        }
                                                                    }).data()

        if product_data['fotograf_dosya'] is not None and isinstance(product_data['katolog_dosya'], list):
            for key in product_data['fotograf_dosya']:
                if isinstance(key, str):
                    Model(ReaPy.get_reactor_source('yeten')).insert('public.um_urun_dosya',
                                                                    {
                                                                        0: {
                                                                            'urun_id': str(product_id),
                                                                            'dosya_id': key,
                                                                            'dosya_tipi': 2,
                                                                            'olusturan': user,
                                                                            'olusturan_ip': user_ip
                                                                        }
                                                                    }).data()

        Product().update_normal_product_pattern(product_data['patent_bilgisi'], product_id, user, user_ip)
        Product().update_product_keyword(product_data['anahtar_kelime'], product_id, 1, user, user_ip)
        Product().update_product_keyword(product_data['anahtar_kelime_ingilizce'], product_id, 2, user, user_ip)
        if product_data['ihracat_kisiti_var_mi']:
            if 'ihracat_kisiti_ulkeler' in product_data:
                Product().update_product_export_country_constraints(
                    product_data['ihracat_kisiti_ulkeler'],
                    product_id, user, user_ip)
        else:
            Product().update_product_export_country_constraints(
                [], product_id, user, user_ip)
        if 'cevre_sartlari' in product_data:
            if 'cevre_sartlari_diger' not in product_data:
                product_data['cevre_sartlari_diger'] = None
            Product().update_normal_product_standard(
                [product_data['cevre_sartlari'], product_data['cevre_sartlari_diger']],
                product_id, 1, user, user_ip)
        if 'elektro_uyum' in product_data:
            if 'elektro_uyum_diger' not in product_data:
                product_data['elektro_uyum_diger'] = None
            Product().update_normal_product_standard([product_data['elektro_uyum'], product_data['elektro_uyum_diger']],
                                                     product_id, 2, user, user_ip)
        if 'uretim_proses' in product_data:
            if 'uretim_proses_diger' not in product_data:
                product_data['uretim_proses_diger'] = None
            Product().update_normal_product_standard(
                [product_data['uretim_proses'], product_data['uretim_proses_diger']],
                product_id, 3, user, user_ip)

        Product().update_product_technical_spec(product_data['teknik_ozellik'], product_id, user, user_ip)

        if 'hangi_urun' in product_data:
            if 'hangi_urun_diger' not in product_data:
                product_data['hangi_urun_diger'] = None
            Product().update_normal_product_sub_product([product_data['hangi_urun'], product_data['hangi_urun_diger']],
                                                        product_id, user, user_ip)

        if 'hangi_firmanin_alt_yuklenicisisiniz' in product_data:
            if 'hangi_firmanin_alt_yuklenicisisiniz_diger' not in product_data:
                product_data['hangi_firmanin_alt_yuklenicisisiniz_diger'] = None
            Product().update_normal_product_sub_contractor([product_data['hangi_firmanin_alt_yuklenicisisiniz'],
                                                            product_data['hangi_firmanin_alt_yuklenicisisiniz_diger']],
                                                           product_id, user, user_ip, product_data['hangi_urun'])

        if 'ihracat_yapilan_ulke' in product_data:
            Product().update_product_export_country(product_data['ihracat_yapilan_ulke'], product_id, user, user_ip)
        else:
            Product().update_product_export_country([], product_id, user, user_ip)

        return True

    def update_normal_product_pattern(self, data, product_id, user, user_ip):
        try:
            if data is not None:
                Model(ReaPy.get_reactor_source('yeten')).delete('public.um_urun_patent', {
                    0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
                for key in data:
                    for value in data[key]:
                        add_os = Model(ReaPy.get_reactor_source('yeten')). \
                            insert('public.um_urun_patent',
                                   {
                                       0: {
                                           'urun_id': str(product_id),
                                           'patent_turu': key,
                                           'basvuru_numarasi': value,
                                           'olusturan': user,
                                           'olusturan_ip': user_ip
                                       }
                                   }).data()
        except Exception as e:
            print(e)

    def update_software_product_open_source(self, data, product_id, user, user_ip):
        try:
            if data is not None:
                Model(ReaPy.get_reactor_source('yeten')).delete('public.um_yazilim_acik_kaynak', {
                    0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
                for key in data:
                    add_os = Model(ReaPy.get_reactor_source('yeten')). \
                        insert('public.um_yazilim_acik_kaynak',
                               {
                                   0: {
                                       'urun_id': str(product_id),
                                       'acik_kaynak_adi': key,
                                       'olusturan': user,
                                       'olusturan_ip': user_ip
                                   }
                               }).data()
        except Exception as e:
            print(e)

    def delete_software_product_open_source(self, data, product_id):
        try:
            if not data:
                Model(ReaPy.get_reactor_source('yeten')).delete('public.um_yazilim_acik_kaynak', {
                    0: {'col': 'urun_id', 'operator': '=', 'value': str(product_id)}})
        except Exception as e:
            print(e)

    def get_software_product_open_source(self, product_id):
        try:
            open_source_list = []
            get_open_source = Model(ReaPy.get_reactor_source('yeten')). \
                select('public.um_yazilim_acik_kaynak', 'acik_kaynak_adi',
                       {
                           0: {'col': 'urun_id', 'operator': '=',
                               'value': str(product_id)}}
                       ).data()
            if get_open_source is not None:
                for key in get_open_source:
                    open_source_list.append(key['acik_kaynak_adi'])
            return open_source_list
        except Exception as e:
            print(e)

    def update_normal_product_sub_product(self, product_data, product_id, user, user_ip):
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_hangi_urunde_kullaniliyor,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        for key in product_data[0]:
            hangi_urun_diger = None
            if key == '00000000-0000-0000-0000-000000000000':
                hangi_urun_diger = product_data[1]
            Model(ReaPy.get_reactor_source('yeten')). \
                insert(self.um_hangi_urunde_kullaniliyor,
                       {
                           0: {
                               'urun_id': str(product_id),
                               'kullanilan_urun_id': str(key),
                               'diger': hangi_urun_diger,
                               'olusturan': user,
                               'olusturan_ip': user_ip
                           }
                       }).data()

    def update_normal_product_sub_contractor(self, product_data, product_id, user, user_ip, hangi_urun):
        Model(ReaPy.get_reactor_source('yeten')).update(self.um_hangi_firmanin_alt_yuklenicisi,
                                                        {'SET': {
                                                            'aktif_mi': False,
                                                            'guncelleme_tarihi': str(datetime.datetime.now()),
                                                            "guncelleyen": user,
                                                            "guncelleyen_ip": user_ip
                                                        },
                                                            'CONDITION': {
                                                                0: {'col': 'urun_id', 'operator': '=',
                                                                    'value': str(product_id)}}})
        if hangi_urun[0] != '99999999-9999-9999-9999-999999999999':
            for key in product_data[0]:
                hangi_urun_diger = None
                if key == '00000000-0000-0000-0000-000000000000':
                    hangi_urun_diger = product_data[1]
                Model(ReaPy.get_reactor_source('yeten')). \
                    insert(self.um_hangi_firmanin_alt_yuklenicisi,
                           {
                               0: {
                                   'urun_id': str(product_id),
                                   'paydas_id': str(key),
                                   'diger': hangi_urun_diger,
                                   'olusturan': user,
                                   'olusturan_ip': user_ip
                               }
                           }).data()
