# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Kullanici(object):

    def get_tckn(self, dis_kullanici_id):
        tc = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_kullanici_talep',
                                                             field='tc',
                                                             condition={
                                                                 0: {'col': 'dis_kullanici_talep_id',
                                                                     'operator': '=',
                                                                     'value': dis_kullanici_id,
                                                                     'combiner': 'AND'},
                                                                 1: {'col': 'aktif_mi',
                                                                     'operator': '=',
                                                                     'value': True}
                                                             }, is_first=True).data()
        return tc[0]['tc']

    def get_detay(self, paydas_id, dis_kullanici_talep_id=None):

        conditionTaleb = {
            0: {'col': 'paydas_id',
                'operator': '=',
                'value': paydas_id,
                'combiner': 'AND'}
        }

        if dis_kullanici_talep_id is not None:
            conditionTaleb[1] = {'col': 'dis_kullanici_talep_id',
                                 'operator': '=',
                                 'value': dis_kullanici_talep_id,
                                 'combiner': 'AND'}

        conditionTaleb[len(conditionTaleb)] = {'col': 'aktif_mi', 'operator': '=', 'value': True}

        data_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_kullanici_talep',
                                                                     sort=' olusturulma_tarihi desc',
                                                                     condition=conditionTaleb).data()

        if data_query is not None:
            return Helpers().payload_parameters_control(data_query)
        return []

    def get_detay_from_tckn(self, dis_kullanici_tckn):

        data_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_kullanici_talep',
                                                                     sort=' dis_kullanici_talep_id desc',
                                                                     condition={
                                                                         0: {'col': 'tc',
                                                                             'operator': '=',
                                                                             'value': dis_kullanici_tckn,
                                                                             'combiner': 'AND'
                                                                             },
                                                                         1: {'col': 'talep_durumu',
                                                                             'operator': '=',
                                                                             'value': '2',
                                                                             'combiner': 'AND'
                                                                             },
                                                                         2: {'col': 'pasif_durum',
                                                                             'operator': '=',
                                                                             'value': '0',
                                                                             'combiner': 'AND'
                                                                             },
                                                                         3: {'col': 'aktif_mi', 'operator': '=',
                                                                             'value': 'true'}
                                                                     }
                                                                     ).data()

        return data_query[0]

    def user_is_exist(self, tc):
        data_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_kullanici_talep',
                                                                     condition={
                                                                         0: {'col': 'tc',
                                                                             'operator': '=',
                                                                             'value': tc
                                                                             }
                                                                     }
                                                                     ).data()
        if data_query is None:
            data_query1 = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_paydas_basvuru',
                                                                          condition={
                                                                              0: {'col': 'yetkili_kisi_tckn',
                                                                                  'operator': '=',
                                                                                  'value': tc,
                                                                                  'combiner': 'AND'
                                                                                  },
                                                                              1: {
                                                                                  'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': True,
                                                                                  'combiner': 'AND'
                                                                              },
                                                                              2: {
                                                                                  'col': 'basvuru_durum',
                                                                                  'operator': '=',
                                                                                  'value': 2
                                                                              }
                                                                          }
                                                                          ).data()
            if data_query1 is None:
                return True
            return False
        else:
            return False

    def insert(self, data, paydas_id, tckn, ip):

        data_insert = data
        data_insert['aktif_mi'] = 'true'
        data_insert['pasif_durum'] = '1'
        data_insert['paydas_id'] = paydas_id
        data_insert['talep_durumu'] = 1
        data_insert['olusturan_ip'] = ip
        data_insert['olusturan_id'] = tckn

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.dis_kullanici_talep',
                                                                     {0: data_insert}).data()

        return data_query

    def update(self, data, paydas_id, tckn, ip):
        if 'pasif_durum' not in data:
            pasif_durum = Model(ReaPy.get_reactor_source('yeten')).select('public.dis_kullanici_talep',
                                                                          'pasif_durum',
                                                                          {0: {
                                                                              'col': 'dis_kullanici_talep_id',
                                                                              'operator': '=',
                                                                              'value': data[
                                                                                  'dis_kullanici_talep_id']}}).data()
            data['pasif_durum'] = pasif_durum[0]['pasif_durum']
        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now()),
            'pasif_durum': data['pasif_durum']
        }

        for key, value in data.items():
            if Helpers().set_val_safe(data, key) != '':
                data_update[key] = data[key]

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.dis_kullanici_talep',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'dis_kullanici_talep_id',
                                                                                 'operator': '=',
                                                                                 'value': data[
                                                                                     'dis_kullanici_talep_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        ).data()
        except Exception as exc:
            print(__file__, exc)

        return set_durum

    def delete(self, dis_kullanici_talep_id):

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).delete('public.dis_kullanici_talep',
                                                                        {0: {'col': 'dis_kullanici_talep_id',
                                                                             'operator': '=',
                                                                             'value': dis_kullanici_talep_id
                                                                             }
                                                                         }
                                                                        ).data()
        except Exception as exc:
            print(__file__)
            print(exc)

        return set_durum

    def get_tanimlar(self):
        return Model(ReaPy.get_reactor_source('yeten')) \
            .select(table='sys_tanim',
                    field='deger, aciklama',
                    condition={
                        0: {'col': 'kolon', 'operator': '=', 'value': 'talep_durumu', 'combiner': 'AND'},
                        1: {'col': 'tablo', 'operator': '=', 'value': 'dis_kullanici_talep'},
                    },
                    sort='deger').data()

    def get_destek_verenler(self):
        return Model(ReaPy.get_reactor_source('yeten')).select('view_destekler').data()

    def get_detay_yetki(self, paydas_id):

        condition = {
            0: {'col': 'paydas_id', 'operator': '=', 'value': paydas_id, 'combiner': 'AND'},
            1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true', 'combiner': 'AND'},
            2: {'col': 'talep_durumu', 'operator': '=', 'value': '2', 'combiner': 'AND'},
            3: {'col': 'pasif_durum', 'operator': '=', 'value': '0'}
        }

        data_query = Model(ReaPy.get_reactor_source('yeten')).select(table='public.dis_kullanici_talep',
                                                                     field='tc',
                                                                     sort=' olusturulma_tarihi desc',
                                                                     condition=condition).data()

        if data_query is not None:
            return Helpers().payload_parameters_control(data_query)
        return []
