# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Savunma_havacilik_cirosu(object):

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_savunma_havacilik_cirosu',
                                                                 condition={0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': paydas_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true'}
                                                                            }
                                                                 ).data())

        return data_query

    def update(self, data, tckn, paydas_id, ip):

        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'sav_savunma_cirosu') != '':
            data_update['sav_savunma_cirosu'] = data['sav_savunma_cirosu']

        if Helpers().set_val_safe(data, 'sav_dolaylı_savunma_cirosu') != '':
            data_update['sav_dolaylı_savunma_cirosu'] = data['sav_dolaylı_savunma_cirosu']

        if Helpers().set_val_safe(data, 'sav_dolaysiz_savunma_cirosu') != '':
            data_update['sav_dolaysiz_savunma_cirosu'] = data['sav_dolaysiz_savunma_cirosu']

        if Helpers().set_val_safe(data, 'sav_savunma_disi_havacilik_cirosu') != '':
            data_update['sav_savunma_disi_havacilik_cirosu'] = data['sav_savunma_disi_havacilik_cirosu']

        if Helpers().set_val_safe(data, 'sav_toplam_savunma_havacilik_cirosu') != '':
            data_update['sav_toplam_savunma_havacilik_cirosu'] = data['sav_toplam_savunma_havacilik_cirosu']

        if Helpers().set_val_safe(data, 'sav_diger_ciro') != '':
            data_update['sav_diger_ciro'] = data['sav_diger_ciro']

        if Helpers().set_val_safe(data, 'sav_toplam_ciro') != '':
            data_update['sav_toplam_ciro'] = data['sav_toplam_ciro']

        if Helpers().set_val_safe(data, 'sav_hava_araclari_cirosu') != '':
            data_update['sav_hava_araclari_cirosu'] = data['sav_hava_araclari_cirosu']

        if Helpers().set_val_safe(data, 'sav_uzay_cirosu') != '':
            data_update['sav_uzay_cirosu'] = data['sav_uzay_cirosu']

        if Helpers().set_val_safe(data, 'sav_deniz_araclari_cirosu') != '':
            data_update['sav_deniz_araclari_cirosu'] = data['sav_deniz_araclari_cirosu']

        if Helpers().set_val_safe(data, 'sav_kara_araclari_cirosu') != '':
            data_update['sav_kara_araclari_cirosu'] = data['sav_kara_araclari_cirosu']

        if Helpers().set_val_safe(data, 'sav_silah_sistemleri_cirosu') != '':
            data_update['sav_silah_sistemleri_cirosu'] = data['sav_silah_sistemleri_cirosu']

        if Helpers().set_val_safe(data, 'sav_toplam_ciro2') != '':
            data_update['sav_toplam_ciro2'] = data['sav_toplam_ciro2']

        if Helpers().set_val_safe(data, 'loj_savunma_bakim_onarim_satislari') != '':
            data_update['loj_savunma_bakim_onarim_satislari'] = data['loj_savunma_bakim_onarim_satislari']

        if Helpers().set_val_safe(data, 'loj_diger_bakim_onarim_satislari') != '':
            data_update['loj_diger_bakim_onarim_satislari'] = data['loj_diger_bakim_onarim_satislari']

        if Helpers().set_val_safe(data, 'loj_toplam_bakim_onarim_satislari') != '':
            data_update['loj_toplam_bakim_onarim_satislari'] = data['loj_toplam_bakim_onarim_satislari']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_savunma_havacilik_cirosu',
                                                                             {'SET':
                                                                                  data_update,
                                                                              'CONDITION': {
                                                                                  0: {
                                                                                      'col': 'savunma_havacilik_cirosu_id',
                                                                                      'operator': '=',
                                                                                      'value': data[
                                                                                          'savunma_havacilik_cirosu_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def delete(self, data, tckn, paydas_id, ip):

        delete_data = {
            'aktif_mi': 'false',
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_savunma_havacilik_cirosu',
                                                                             {'SET':
                                                                                  delete_data,
                                                                              'CONDITION': {
                                                                                  0: {
                                                                                      'col': 'savunma_havacilik_cirosu_id',
                                                                                      'operator': '=',
                                                                                      'value': data[
                                                                                          'savunma_havacilik_cirosu_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def insert(self, data, tckn, paydas_id, ip):

        data_insert = data
        data_insert['aktif_mi'] = 'true'
        data_insert['paydas_id'] = paydas_id
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = ip
        data_insert['donem'] = 2019

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_savunma_havacilik_cirosu',
                                                                          {0: data_insert}).data()

        return data_query
