# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
import datetime
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class UretimdenSatisAvans:

    def insert(self, data, tckn, user_ip, paydas_id):

        data_insert = data
        data_insert['paydas_id'] = paydas_id
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = user_ip
        data_insert['donem'] = 2019

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_uretimden_satis_avans',
                                                                     {0: data_insert}).data()

        return data_query

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_uretimden_satis_avans',
                                                            condition={0: {'col': 'paydas_id',
                                                                           'operator': '=',
                                                                           'value': paydas_id,
                                                                           'combiner': 'AND'
                                                                           },
                                                                       1: {'col': 'aktif_mi',
                                                                           'operator': '=',
                                                                           'value': 'true'}
                                                                       }
                                                            ).data())
        if data_query is not None and len(data_query) > 0:
            data_query = data_query[0]

        return data_query

    def update(self, data_update, tckn, user_ip, paydas_id):

        # data_update = {}
        data_update['paydas_id'] = paydas_id
        data_update['guncelleyen'] = tckn
        data_update['guncelleme_tarihi'] = str(datetime.datetime.now())
        data_update['guncelleyen_ip'] = user_ip

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_uretimden_satis_avans',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'uretimden_satis_avans_id',
                                                                                 'operator': '=',
                                                                                 'value': data_update[
                                                                                     'uretimden_satis_avans_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum
