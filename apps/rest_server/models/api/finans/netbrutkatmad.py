# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
import datetime
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Netbrutkatmad:

    def insert(self, data, tckn, user_ip, paydas_id):

        data_insert = data
        data_insert['paydas_id'] = paydas_id
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = user_ip
        data_insert['donem'] = 2019
        data_insert['aktif_mi'] = 'true'

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_net_brut_katmadeger',
                                                                     {0: data_insert})

        print(data_query.data())
        print(data_query.error())

        return data_query.data()

    def update(self, data, tckn, user_ip, paydas_id):

        data_update = {'guncelleyen': tckn, 'guncelleme_tarihi': str(datetime.datetime.now()),
                       'guncelleyen_ip': user_ip}

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'kapasite_kullanim_orani') != '':
            data_update['kapasite_kullanim_orani'] = data['kapasite_kullanim_orani']

        if Helpers().set_val_safe(data, 'brut_katma_deger') != '':
            data_update['brut_katma_deger'] = data['brut_katma_deger']

        if Helpers().set_val_safe(data, 'toplam_net_katma_deger') != '':
            data_update['toplam_net_katma_deger'] = data['toplam_net_katma_deger']

        if Helpers().set_val_safe(data, 'milli_gelir_anlaminda_kar') != '':
            data_update['milli_gelir_anlaminda_kar'] = data['milli_gelir_anlaminda_kar']

        if Helpers().set_val_safe(data, 'odenen_faizler') != '':
            data_update['odenen_faizler'] = data['odenen_faizler']

        if Helpers().set_val_safe(data, 'odenen_kiralar') != '':
            data_update['odenen_kiralar'] = data['odenen_kiralar']

        if Helpers().set_val_safe(data, 'odenen_maas_ucretler') != '':
            data_update['odenen_maas_ucretler'] = data['odenen_maas_ucretler']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_net_brut_katmadeger',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'net_brut_katmadeger_id',
                                                                                 'operator': '=',
                                                                                 'value': data[
                                                                                     'net_brut_katmadeger_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_net_brut_katmadeger',
                                                            condition={0: {'col': 'paydas_id',
                                                                           'operator': '=',
                                                                           'value': paydas_id,
                                                                           'combiner': 'AND'},
                                                                       1: {'col': 'aktif_mi',
                                                                           'operator': '=',
                                                                           'value': 'true'}
                                                                       }
                                                            ).data())
        if data_query is not None and len(data_query) > 0:
            data_query = data_query[0]

        return data_query
