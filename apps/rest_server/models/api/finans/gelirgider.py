# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
import datetime
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Gelirgider:

    def insert(self, data, tckn, user_ip, paydas_id):

        data_insert = data
        data_insert['paydas_id'] = paydas_id
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = user_ip
        data_insert['donem'] = 2019

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_gelir_gider',
                                                                     {0: data_insert})
        print(data_query.error())
        return data_query.data()

    def update(self, data, tckn, user_ip, paydas_id):

        data_update = {'guncelleyen': tckn, 'guncelleme_tarihi': str(datetime.datetime.now()),
                       'guncelleyen_ip': user_ip}

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'yurt_ici_brut_satis') != '':
            data_update['yurt_ici_brut_satis'] = data['yurt_ici_brut_satis']

        if Helpers().set_val_safe(data, 'yurt_disi_brut_satis') != '':
            data_update['yurt_disi_brut_satis'] = data['yurt_disi_brut_satis']

        if Helpers().set_val_safe(data, 'diger_brut_satis') != '':
            data_update['diger_brut_satis'] = data['diger_brut_satis']

        if Helpers().set_val_safe(data, 'brut_satis') != '':
            data_update['brut_satis'] = data['brut_satis']

        if Helpers().set_val_safe(data, 'satis_indirimleri') != '':
            data_update['satis_indirimleri'] = data['satis_indirimleri']

        if Helpers().set_val_safe(data, 'satislar_net') != '':
            data_update['satislar_net'] = data['satislar_net']

        if Helpers().set_val_safe(data, 'satislarin_maliyeti') != '':
            data_update['satislarin_maliyeti'] = data['satislarin_maliyeti']

        if Helpers().set_val_safe(data, 'brut_satis_kari') != '':
            data_update['brut_satis_kari'] = data['brut_satis_kari']

        if Helpers().set_val_safe(data, 'faaliyet_giderleri') != '':
            data_update['faaliyet_giderleri'] = data['faaliyet_giderleri']

        if Helpers().set_val_safe(data, 'faaliyet_kari_zarari') != '':
            data_update['faaliyet_kari_zarari'] = data['faaliyet_kari_zarari']

        if Helpers().set_val_safe(data, 'diger_faaliyetlerden_gelirler_karlar') != '':
            data_update['diger_faaliyetlerden_gelirler_karlar'] = data['diger_faaliyetlerden_gelirler_karlar']

        if Helpers().set_val_safe(data, 'diger_faaliyetlerden_giderler_zararlar') != '':
            data_update['diger_faaliyetlerden_giderler_zararlar'] = data['diger_faaliyetlerden_giderler_zararlar']

        if Helpers().set_val_safe(data, 'finansman_gideri') != '':
            data_update['finansman_gideri'] = data['finansman_gideri']

        if Helpers().set_val_safe(data, 'olagan_kar_zarar') != '':
            data_update['olagan_kar_zarar'] = data['olagan_kar_zarar']

        if Helpers().set_val_safe(data, 'olagan_disi_gelir_kar') != '':
            data_update['olagan_disi_gelir_kar'] = data['olagan_disi_gelir_kar']

        if Helpers().set_val_safe(data, 'olagan_disi_gider_zarar') != '':
            data_update['olagan_disi_gider_zarar'] = data['olagan_disi_gider_zarar']

        if Helpers().set_val_safe(data, 'donem_kari_zarari') != '':
            data_update['donem_kari_zarari'] = data['donem_kari_zarari']

        if Helpers().set_val_safe(data, 'vergiler_diger_kanuni_yukumlulukler') != '':
            data_update['vergiler_diger_kanuni_yukumlulukler'] = data['vergiler_diger_kanuni_yukumlulukler']

        if Helpers().set_val_safe(data, 'donem_net_kari_zarari') != '':
            data_update['donem_net_kari_zarari'] = data['donem_net_kari_zarari']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_gelir_gider',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'gelir_gider_id',
                                                                                 'operator': '=',
                                                                                 'value': data['gelir_gider_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_gelir_gider',
                                                            condition={0: {'col': 'paydas_id',
                                                                           'operator': '=',
                                                                           'value': paydas_id,
                                                                           'combiner': 'AND'},
                                                                       1: {'col': 'aktif_mi',
                                                                           'operator': '=',
                                                                           'value': 'true'}
                                                                       }
                                                            ).data())
        if data_query is not None and len(data_query) > 0:
            data_query = data_query[0]

        return data_query
