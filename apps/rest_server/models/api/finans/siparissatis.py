# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
import datetime
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Siparissatis:

    def insert(self, data, tckn, user_ip, paydas_id):

        data_insert = data
        data_insert['paydas_id'] = paydas_id
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = user_ip
        data_insert['donem'] = 2019
        data_insert['aktif_mi'] = 'true'

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_siparis_satis_ongoru',
                                                                     {0: data_insert})

        print(data_query.data())
        print(data_query.error())

        return data_query.data()

    def update(self, data, tckn, user_ip, paydas_id):

        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': user_ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'siparis_savunma_odeme_alinmamis') != '':
            data_update['siparis_savunma_odeme_alinmamis'] = data['siparis_savunma_odeme_alinmamis']

        if Helpers().set_val_safe(data, 'siparis_havacilik_odeme_alinmamis') != '':
            data_update['siparis_havacilik_odeme_alinmamis'] = data['siparis_havacilik_odeme_alinmamis']

        if Helpers().set_val_safe(data, 'siparis_savunma_havacilik_toplami') != '':
            data_update['siparis_savunma_havacilik_toplami'] = data['siparis_savunma_havacilik_toplami']

        if Helpers().set_val_safe(data, 'siparis_diger_odeme_alinmamis') != '':
            data_update['siparis_diger_odeme_alinmamis'] = data['siparis_diger_odeme_alinmamis']

        if Helpers().set_val_safe(data, 'siparis_toplam') != '':
            data_update['siparis_toplam'] = data['siparis_toplam']

        if Helpers().set_val_safe(data, 'ihracat_savunma') != '':
            data_update['ihracat_savunma'] = data['ihracat_savunma']

        if Helpers().set_val_safe(data, 'ihracat_havacilik') != '':
            data_update['ihracat_havacilik'] = data['ihracat_havacilik']

        if Helpers().set_val_safe(data, 'ihracat_savunma_havacilik_toplam') != '':
            data_update['ihracat_savunma_havacilik_toplam'] = data['ihracat_savunma_havacilik_toplam']

        if Helpers().set_val_safe(data, 'ihracat_diger') != '':
            data_update['ihracat_diger'] = data['ihracat_diger']

        if Helpers().set_val_safe(data, 'ihracat_toplam') != '':
            data_update['ihracat_toplam'] = data['ihracat_toplam']

        if Helpers().set_val_safe(data, 'satis_savunma') != '':
            data_update['satis_savunma'] = data['satis_savunma']

        if Helpers().set_val_safe(data, 'satis_havacilik') != '':
            data_update['satis_havacilik'] = data['satis_havacilik']

        if Helpers().set_val_safe(data, 'satis_savunma_havacilik_toplam') != '':
            data_update['satis_savunma_havacilik_toplam'] = data['satis_savunma_havacilik_toplam']

        if Helpers().set_val_safe(data, 'satis_savunma_havacilik_toplam') != '':
            data_update['satis_savunma_havacilik_toplam'] = data['satis_savunma_havacilik_toplam']

        if Helpers().set_val_safe(data, 'satis_diger') != '':
            data_update['satis_diger'] = data['satis_diger']

        if Helpers().set_val_safe(data, 'satis_toplam') != '':
            data_update['satis_toplam'] = data['satis_toplam']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_siparis_satis_ongoru',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'siparis_satis_ongoru_id',
                                                                                 'operator': '=',
                                                                                 'value': data[
                                                                                     'siparis_satis_ongoru_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_siparis_satis_ongoru',
                                                            condition={0: {'col': 'paydas_id',
                                                                           'operator': '=',
                                                                           'value': paydas_id,
                                                                           'combiner': 'AND'},
                                                                       1: {'col': 'aktif_mi',
                                                                           'operator': '=',
                                                                           'value': 'true'}
                                                                       }
                                                            ).data())
        if data_query is not None and len(data_query) > 0:
            data_query = data_query[0]

        return data_query
