# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class ArgeYanSanayi(object):

    def get_detay(self, paydas_id):
        arge_detay = []
        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.fb_arge',
                condition=
                {
                    0: {'col': 'paydas_id', 'operator': '=', 'value': paydas_id, 'combiner': 'AND'},
                    1: {'col': 'donem', 'operator': '=', 'value': '2019', 'combiner': 'AND'},
                    2: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'},
                    # TODO Dönem gelecek 1: {'col': 'donem', 'operator': '=', 'value': 'true'},
                },
                sort='olusturulma_tarihi desc'
            ).data())
            if list is not None and len(list) > 0:
                arge_detay = list[0]

                arge_detay['devlet_destekli_harcama'] = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='public.view_paydas_finans_devlet_destekli_harcama',
                    condition=
                    {
                        0: {'col': 'arge_id', 'operator': '=', 'value': arge_detay['arge_id']}
                    }
                ).data()

                mm_data = Model(ReaPy.get_reactor_source('yeten')).select(
                    field='mukemmeliyet_merkezi, mm_tanim',
                    table='public.view_paydas_finans_mukemmeliyet_merkezi',
                    condition=
                    {
                        0: {'col': 'arge_id', 'operator': '=', 'value': arge_detay['arge_id']}
                    }
                ).data()

                mmd = []
                mmd_tanim = []
                for mms in mm_data:
                    mmd.append(mms['mukemmeliyet_merkezi'])
                    mmd_tanim.append(mms['mm_tanim'])

                arge_detay['mukemmeliyet_merkezi'] = mmd
                arge_detay['mukemmeliyet_merkezi_tanim'] = mmd_tanim

                universite = Model(ReaPy.get_reactor_source('yeten')).select(
                    table='public.view_paydas_finans_arge_universite',
                    field='universite_id, yerli_mi, universite_adi',
                    condition=
                    {
                        0: {'col': 'arge_id', 'operator': '=', 'value': arge_detay['arge_id'], 'combiner': 'AND'},
                        1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'},
                    }
                ).data()

                yerli_universite = []
                yerli_universite_tanim = []
                yabanci_universite = []
                yabanci_universite_tanim = []
                for uni in universite:
                    print('uni[yerli_mi] ', uni['yerli_mi'])
                    if uni['yerli_mi'] is False:
                        yabanci_universite.append(uni['universite_id'])
                        yabanci_universite_tanim.append(uni['universite_adi'])
                    else:
                        yerli_universite.append(uni['universite_id'])
                        yerli_universite_tanim.append(uni['universite_adi'])

                arge_detay['yurt_ici_universite_id'] = yerli_universite
                arge_detay['yurt_ici_universite_tanim'] = yerli_universite_tanim
                arge_detay['yurt_disi_universite_id'] = yabanci_universite
                arge_detay['yurt_disi_universite_tanim'] = yabanci_universite_tanim

        except Exception as exc:
            print(__file__, exc)

        return arge_detay

    def insert(self, data, tckn, paydas_id, ip):
        insert_data = {
            "ozkaynakla_gelistirilen_harcama": data['ozkaynakla_gelistirilen_harcama'],
            "dis_finansmanla_gerceklestirilen_harcama": data['dis_finansmanla_gerceklestirilen_harcama'],
            "yeni_urunlerden_ciro": data['yeni_urunlerden_ciro'],
            "toplam_arge_harcama": data['toplam_arge_harcama'],
            "ispayi_tutari": data['ispayi_tutari'],
            "yan_sanayi_miktari": data['yan_sanayi_miktari'],
            "devlet_destekli_arge_harcama_toplami": data['devlet_destekli_arge_harcama_toplami'],
            "devlet_destekli_proje_sayisi": data['devlet_destekli_proje_sayisi'],
            "paydas_bunyesinde_toplami": data['paydas_bunyesinde_toplami'],
            "paydas_bunyesinde_proje_sayisi": int(data['paydas_bunyesinde_proje_sayisi']),
            "universite_isbirligi_var_mi": data['universite_isbirligi_var_mi'],
            'yurt_ici_universite_proje_sayisi': Helpers().set_val_safe(data,
                                                                       'yurt_ici_universite_proje_sayisi',
                                                                       True),
            'yurt_disi_universite_proje_sayisi': Helpers().set_val_safe(data,
                                                                        'yurt_disi_universite_proje_sayisi',
                                                                        True),
            'donem': 2019,
            'aktif_mi': 'true',
            'paydas_id': paydas_id,
            'olusturan_id': tckn,
            'olusturan_ip': ip
        }

        query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_arge', {0: insert_data})

        if query.error() is None:
            degistir = Model(ReaPy.get_reactor_source('yeten')).select('public.fb_arge',
                                                                       'arge_id',
                                                                       condition=
                                                                       {
                                                                           0: {'col': 'paydas_id',
                                                                               'operator': '=',
                                                                               'value': paydas_id}
                                                                       }, sort='olusturulma_tarihi desc').data()

            ta_detay_test_alt_yapi_id = degistir[0]

            arge_id = ta_detay_test_alt_yapi_id['arge_id']

            mmd = Helpers().set_val_safe(data, 'mukemmeliyet_merkezi', True)
            ddh = Helpers().set_val_safe(data, 'devlet_destekli_harcama', True)

            yiu = Helpers().set_val_safe(data, 'yurt_ici_universite_id', True)
            ydu = Helpers().set_val_safe(data, 'yurt_disi_universite_id', True)

            self.__universite_iliski(yiu, arge_id, 'true', data['universite_isbirligi_var_mi'])
            self.__universite_iliski(ydu, arge_id, 'false', data['universite_isbirligi_var_mi'])
            self.__mukemmeliyet_merkezi_iliski(mmd, arge_id)
            self.__devlet_destekli_harcama_iliski(ddh, arge_id)

            return query.data()
        else:
            print("insert. mevcut insert ->  ")
            print(query.error())

        return None

    def __universite_iliski(self, universiteler, arge_id, yerli_mi, is_delete_first):

        tabd = Model(ReaPy.get_reactor_source('yeten')).delete('public.fb_arge_universite', {
            0: {'col': 'arge_id',
                'operator': '=',
                'value': arge_id, 'combiner': 'AND'
                },
            1: {'col': 'yerli_mi',
                'operator': '=',
                'value': yerli_mi

                }
        })
        if is_delete_first:
            if universiteler is not None:
                tab_data = {}
                for data in universiteler:
                    tab_data[len(tab_data)] = {
                        'aktif_mi': 'true',
                        'arge_id': arge_id,
                        'yerli_mi': yerli_mi,
                        'universite_id': data
                    }
                tab = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_arge_universite', tab_data)

    def __mukemmeliyet_merkezi_iliski(self, data_mmi, arge_id, is_delete_first=None):

        if is_delete_first is not None:
            tabd = Model(ReaPy.get_reactor_source('yeten')).delete('public.fb_arge_mukemmeliyet', {
                0: {'col': 'arge_id',
                    'operator': '=',
                    'value': arge_id,
                    }
            })
            print(tabd.error())
            print(tabd.data())

        if data_mmi is not None:
            tab_data = {}
            for data in data_mmi:
                tab_data[len(tab_data)] = {
                    'aktif_mi': 'true',
                    'arge_id': arge_id,
                    'mukemmeliyet_merkezi': data
                }

            tab = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_arge_mukemmeliyet', tab_data)
            print(tab.error())
            print(tab.data())

    def __devlet_destekli_harcama_iliski(self, data_ddh, arge_id, is_delete_first=None):

        if is_delete_first is not None:
            tabd = Model(ReaPy.get_reactor_source('yeten')).delete('public.fb_arge_devlet_destekli_harcama', {
                0: {'col': 'arge_id',
                    'operator': '=',
                    'value': arge_id,
                    }
            })
            print(tabd.error())
            print(tabd.data())
        if data_ddh is not None:
            tab_data = {}
            for data in data_ddh:
                tab_data[len(tab_data)] = {
                    'aktif_mi': 'true',
                    'arge_id': arge_id,
                    'tesvik_veren_kurum_id': data['tesvik_veren_kurum_id'],
                    'tesvik_turu': data['tesvik_turu'],
                    'tutar': data['tutar']
                }
            if len(tab_data) > 0:
                tab = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_arge_devlet_destekli_harcama',
                                                                      tab_data)

    def update(self, data, user_identity_no, paydas_id, ip):

        update_data = {
            'guncelleyen_ip': ip,
            'guncelleyen': user_identity_no,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        if Helpers().set_val_safe(data, 'ozkaynakla_gelistirilen_harcama') != '':
            update_data['ozkaynakla_gelistirilen_harcama'] = data['ozkaynakla_gelistirilen_harcama']

        if Helpers().set_val_safe(data, 'dis_finansmanla_gerceklestirilen_harcama') != '':
            update_data['dis_finansmanla_gerceklestirilen_harcama'] = data['dis_finansmanla_gerceklestirilen_harcama']

        if Helpers().set_val_safe(data, 'yeni_urunlerden_ciro') != '':
            update_data['yeni_urunlerden_ciro'] = data['yeni_urunlerden_ciro']

        if Helpers().set_val_safe(data, 'toplam_arge_harcama') != '':
            update_data['toplam_arge_harcama'] = data['toplam_arge_harcama']

        if Helpers().set_val_safe(data, 'ispayi_tutari') != '':
            update_data['ispayi_tutari'] = data['ispayi_tutari']

        if Helpers().set_val_safe(data, 'yan_sanayi_miktari') != '':
            update_data['yan_sanayi_miktari'] = data['yan_sanayi_miktari']

        if Helpers().set_val_safe(data, 'devlet_destekli_arge_harcama_toplami') != '':
            update_data['devlet_destekli_arge_harcama_toplami'] = data['devlet_destekli_arge_harcama_toplami']

        if Helpers().set_val_safe(data, 'devlet_destekli_proje_sayisi') != '':
            update_data['devlet_destekli_proje_sayisi'] = data['devlet_destekli_proje_sayisi']

        if Helpers().set_val_safe(data, 'paydas_bunyesinde_toplami') != '':
            update_data['paydas_bunyesinde_toplami'] = data['paydas_bunyesinde_toplami']

        if Helpers().set_val_safe(data, 'paydas_bunyesinde_proje_sayisi') != '':
            update_data['paydas_bunyesinde_proje_sayisi'] = data['paydas_bunyesinde_proje_sayisi']

        if Helpers().set_val_safe(data, 'universite_isbirligi_var_mi') != '':
            update_data['universite_isbirligi_var_mi'] = data['universite_isbirligi_var_mi']

        if Helpers().set_val_safe(data, 'yurt_ici_universite_proje_sayisi', True) != '':
            update_data['yurt_ici_universite_proje_sayisi'] = data['yurt_ici_universite_proje_sayisi']

        if Helpers().set_val_safe(data, 'yurt_disi_universite_proje_sayisi', True) != '':
            update_data['yurt_disi_universite_proje_sayisi'] = data['yurt_disi_universite_proje_sayisi']

        query = None
        if not data['universite_isbirligi_var_mi']:
            update_data['yurt_disi_universite_proje_sayisi'] = 0
            update_data['yurt_ici_universite_proje_sayisi'] = 0
        try:
            query = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_arge',
                                                                    {'SET': update_data,
                                                                     'CONDITION': {
                                                                         0: {
                                                                             'col': 'arge_id',
                                                                             'operator': '=',
                                                                             'value':
                                                                                 data['arge_id'],
                                                                             'combiner': 'AND'
                                                                         },
                                                                         1: {
                                                                             'col': 'paydas_id',
                                                                             'operator': '=',
                                                                             'value': paydas_id
                                                                         }
                                                                     }})
        except Exception as exc:
            print(exc)

        if query is not None:
            mmd = Helpers().set_val_safe(data, 'mukemmeliyet_merkezi', True)
            ddh = Helpers().set_val_safe(data, 'devlet_destekli_harcama', True)
            yiu = Helpers().set_val_safe(data, 'yurt_ici_universite_id', True)
            ydu = Helpers().set_val_safe(data, 'yurt_disi_universite_id', True)

            self.__universite_iliski(yiu, data['arge_id'], 'true', data['universite_isbirligi_var_mi'])
            self.__universite_iliski(ydu, data['arge_id'], 'false', data['universite_isbirligi_var_mi'])
            self.__mukemmeliyet_merkezi_iliski(mmd, data['arge_id'], True)
            self.__devlet_destekli_harcama_iliski(ddh, data['arge_id'], True)

            return query.data()

        return None
