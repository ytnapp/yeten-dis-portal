# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Bilanco(object):

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_bilanco',
                                                                 condition={0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': paydas_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true'}
                                                                            }
                                                                 ).data())

        return data_query

    def insert(self, data, paydas_id, tckn, ip):

        data_insert = data
        data_insert['aktif_mi'] = 'true'
        data_insert['paydas_id'] = paydas_id
        data_insert['donem'] = 2019
        data_insert['olusturan_ip'] = ip
        data_insert['olusturan_id'] = tckn

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_bilanco', {0: data_insert}).data()

        return data_query

    def update(self, data, paydas_id, tckn, ip):

        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now()),
        }

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'donen_varliklar') != '':
            data_update['donen_varliklar'] = data['donen_varliklar']

        if Helpers().set_val_safe(data, 'dv_hazir_degerler') != '':
            data_update['dv_hazir_degerler'] = data['dv_hazir_degerler']

        if Helpers().set_val_safe(data, 'dv_hd_kasa') != '':
            data_update['dv_hd_kasa'] = data['dv_hd_kasa']

        if Helpers().set_val_safe(data, 'dv_hd_banka') != '':
            data_update['dv_hd_banka'] = data['dv_hd_banka']

        if Helpers().set_val_safe(data, 'dv_hd_alinan_cekler') != '':
            data_update['dv_hd_alinan_cekler'] = data['dv_hd_alinan_cekler']

        if Helpers().set_val_safe(data, 'dv_hd_verilen_cekler_odeme_emirleri') != '':
            data_update['dv_hd_verilen_cekler_odeme_emirleri'] = data['dv_hd_verilen_cekler_odeme_emirleri']

        if Helpers().set_val_safe(data, 'dv_hz_diger_hazir_degerler') != '':
            data_update['dv_hz_diger_hazir_degerler'] = data['dv_hz_diger_hazir_degerler']

        if Helpers().set_val_safe(data, 'dv_menkul_kiymetler') != '':
            data_update['dv_menkul_kiymetler'] = data['dv_menkul_kiymetler']

        if Helpers().set_val_safe(data, 'dv_ticari_alacaklar') != '':
            data_update['dv_ticari_alacaklar'] = data['dv_ticari_alacaklar']

        if Helpers().set_val_safe(data, 'dv_diger_alacaklar') != '':
            data_update['dv_diger_alacaklar'] = data['dv_diger_alacaklar']

        if Helpers().set_val_safe(data, 'dv_stoklar') != '':
            data_update['dv_stoklar'] = data['dv_stoklar']

        if Helpers().set_val_safe(data, 'dv_yillara_yaygin_bakim_onarim_maliyeti') != '':
            data_update['dv_yillara_yaygin_bakim_onarim_maliyeti'] = data['dv_yillara_yaygin_bakim_onarim_maliyeti']

        if Helpers().set_val_safe(data, 'dv_gelecek_aylara_ait_gelir_gider_tahakkuklari') != '':
            data_update['dv_gelecek_aylara_ait_gelir_gider_tahakkuklari'] = data[
                'dv_gelecek_aylara_ait_gelir_gider_tahakkuklari']

        if Helpers().set_val_safe(data, 'dv_diger_donen_varliklar') != '':
            data_update['dv_diger_donen_varliklar'] = data['dv_diger_donen_varliklar']

        if Helpers().set_val_safe(data, 'duran_varliklar') != '':
            data_update['duran_varliklar'] = data['duran_varliklar']

        if Helpers().set_val_safe(data, 'du_ticari_alacaklar') != '':
            data_update['du_ticari_alacaklar'] = data['du_ticari_alacaklar']

        if Helpers().set_val_safe(data, 'du_diger_alacaklar') != '':
            data_update['du_diger_alacaklar'] = data['du_diger_alacaklar']

        if Helpers().set_val_safe(data, 'du_maddi_duran_varliklar') != '':
            data_update['du_maddi_duran_varliklar'] = data['du_maddi_duran_varliklar']

        if Helpers().set_val_safe(data, 'du_mali_duran_varliklar') != '':
            data_update['du_mali_duran_varliklar'] = data['du_mali_duran_varliklar']

        if Helpers().set_val_safe(data, 'du_maddi_olmayan') != '':
            data_update['du_maddi_olmayan'] = data['du_maddi_olmayan']

        if Helpers().set_val_safe(data, 'du_gelecek_ay_gelir_gider_tahakkuk') != '':
            data_update['du_gelecek_ay_gelir_gider_tahakkuk'] = data['du_gelecek_ay_gelir_gider_tahakkuk']

        if Helpers().set_val_safe(data, 'du_diger_duran_varliklar') != '':
            data_update['du_diger_duran_varliklar'] = data['du_diger_duran_varliklar']

        if Helpers().set_val_safe(data, 'toplam_aktifler') != '':
            data_update['toplam_aktifler'] = data['toplam_aktifler']

        if Helpers().set_val_safe(data, 'kisa_vadeli_borclar') != '':
            data_update['kisa_vadeli_borclar'] = data['kisa_vadeli_borclar']

        if Helpers().set_val_safe(data, 'kv_mali_borclar') != '':
            data_update['kv_mali_borclar'] = data['kv_mali_borclar']

        if Helpers().set_val_safe(data, 'kv_ticari_borclar') != '':
            data_update['kv_ticari_borclar'] = data['kv_ticari_borclar']

        if Helpers().set_val_safe(data, 'kv_diger_borclar') != '':
            data_update['kv_diger_borclar'] = data['kv_diger_borclar']

        if Helpers().set_val_safe(data, 'kv_alinan_avanslar') != '':
            data_update['kv_alinan_avanslar'] = data['kv_alinan_avanslar']

        if Helpers().set_val_safe(data, 'kv_yillara_yaygin_ins_onarim_hakedisleri') != '':
            data_update['kv_yillara_yaygin_ins_onarim_hakedisleri'] = data['kv_yillara_yaygin_ins_onarim_hakedisleri']

        if Helpers().set_val_safe(data, 'kv_odenecek_vergi_diger_yukumluluk') != '':
            data_update['kv_odenecek_vergi_diger_yukumluluk'] = data['kv_odenecek_vergi_diger_yukumluluk']

        if Helpers().set_val_safe(data, 'kv_borc_gider_karsitlari') != '':
            data_update['kv_borc_gider_karsitlari'] = data['kv_borc_gider_karsitlari']

        if Helpers().set_val_safe(data, 'kv_gelecek_aylara_ait_gider_tahakkuk') != '':
            data_update['kv_gelecek_aylara_ait_gider_tahakkuk'] = data['kv_gelecek_aylara_ait_gider_tahakkuk']

        if Helpers().set_val_safe(data, 'kv_diger_yabanci_kaynaklar') != '':
            data_update['kv_diger_yabanci_kaynaklar'] = data['kv_diger_yabanci_kaynaklar']

        if Helpers().set_val_safe(data, 'uzun_vadeli_borclar') != '':
            data_update['uzun_vadeli_borclar'] = data['uzun_vadeli_borclar']

        if Helpers().set_val_safe(data, 'uv_mali_borclar') != '':
            data_update['uv_mali_borclar'] = data['uv_mali_borclar']

        if Helpers().set_val_safe(data, 'uv_ticari_borclar') != '':
            data_update['uv_ticari_borclar'] = data['uv_ticari_borclar']

        if Helpers().set_val_safe(data, 'uv_diger_borclar') != '':
            data_update['uv_diger_borclar'] = data['uv_diger_borclar']

        if Helpers().set_val_safe(data, 'uv_alinan_avanslar') != '':
            data_update['uv_alinan_avanslar'] = data['uv_alinan_avanslar']

        if Helpers().set_val_safe(data, 'uv_borc_gider_karsitlari') != '':
            data_update['uv_borc_gider_karsitlari'] = data['uv_borc_gider_karsitlari']

        if Helpers().set_val_safe(data, 'uv_gelecek_aylara_ait_gider_tahakkuk') != '':
            data_update['uv_gelecek_aylara_ait_gider_tahakkuk'] = data['uv_gelecek_aylara_ait_gider_tahakkuk']

        if Helpers().set_val_safe(data, 'uv_diger_yabanci_kaynaklar') != '':
            data_update['uv_diger_yabanci_kaynaklar'] = data['uv_diger_yabanci_kaynaklar']

        if Helpers().set_val_safe(data, 'ozkaynaklar') != '':
            data_update['ozkaynaklar'] = data['ozkaynaklar']

        if Helpers().set_val_safe(data, 'oz_sermaye') != '':
            data_update['oz_sermaye'] = data['oz_sermaye']

        if Helpers().set_val_safe(data, 'oz_sermaye_yedekleri') != '':
            data_update['oz_sermaye_yedekleri'] = data['oz_sermaye_yedekleri']

        if Helpers().set_val_safe(data, 'oz_kar_yedekleri') != '':
            data_update['oz_kar_yedekleri'] = data['oz_kar_yedekleri']

        if Helpers().set_val_safe(data, 'oz_gecmis_yil_karlari') != '':
            data_update['oz_gecmis_yil_karlari'] = data['oz_gecmis_yil_karlari']

        if Helpers().set_val_safe(data, 'oz_gecmis_yil_zararlari') != '':
            data_update['oz_gecmis_yil_zararlari'] = data['oz_gecmis_yil_zararlari']

        if Helpers().set_val_safe(data, 'oz_donem_kari') != '':
            data_update['oz_donem_kari'] = data['oz_donem_kari']

        if Helpers().set_val_safe(data, 'oz_donem_zarari') != '':
            data_update['oz_donem_zarari'] = data['oz_donem_zarari']

        if Helpers().set_val_safe(data, 'toplam_pasifler') != '':
            data_update['toplam_pasifler'] = data['toplam_pasifler']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_bilanco',
                                                                             {'SET':
                                                                                  data_update,
                                                                              'CONDITION': {
                                                                                  0: {'col': 'fb_bilanco_id',
                                                                                      'operator': '=',
                                                                                      'value': data['fb_bilanco_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def delete(self, data, paydas_id, tckn, ip):

        delete_data = {'aktif_mi': 'false', 'guncelleyen': tckn, 'guncelleyen_ip': ip,
                       'guncelleme_tarihi': str(datetime.datetime.now())}

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_bilanco',
                                                                             {'SET':
                                                                                  delete_data,
                                                                              'CONDITION': {
                                                                                  0: {'col': 'fb_bilanco_id',
                                                                                      'operator': '=',
                                                                                      'value': data['fb_bilanco_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)

        return set_durum
