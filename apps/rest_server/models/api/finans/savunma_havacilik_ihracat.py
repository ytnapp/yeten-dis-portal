# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Savunma_havacilik_ihracat(object):

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_savunma_havacilik_ihracat',
                                                                 condition={0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': paydas_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true'}
                                                                            }
                                                                 ).data())

        return data_query

    def update(self, data, paydas_id, tckn, ip):

        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'ih_savunma_urunleri_ihracatı') != '':
            data_update['ih_savunma_urunleri_ihracatı'] = data['ih_savunma_urunleri_ihracatı']

        if Helpers().set_val_safe(data, 'ih_offset_disi_savunma_ihracati') != '':
            data_update['ih_offset_disi_savunma_ihracati'] = data['ih_offset_disi_savunma_ihracati']

        if Helpers().set_val_safe(data, 'ih_offset_kapsaminda_savunma_ihracati') != '':
            data_update['ih_offset_kapsaminda_savunma_ihracati'] = data['ih_offset_kapsaminda_savunma_ihracati']

        if Helpers().set_val_safe(data, 'ih_sivil_havacilik_urunleri_ihracati') != '':
            data_update['ih_sivil_havacilik_urunleri_ihracati'] = data['ih_sivil_havacilik_urunleri_ihracati']

        if Helpers().set_val_safe(data, 'ih_offset_disi_sivil_havacilik_ihracati') != '':
            data_update['ih_offset_disi_sivil_havacilik_ihracati'] = data['ih_offset_disi_sivil_havacilik_ihracati']

        if Helpers().set_val_safe(data, 'ih_offser_kapsaminda_sivil_havacilik_ihracati') != '':
            data_update['ih_offser_kapsaminda_sivil_havacilik_ihracati'] = data[
                'ih_offser_kapsaminda_sivil_havacilik_ihracati']

        if Helpers().set_val_safe(data, 'ih_savunma_sivil_havacilik_urunleri_ihracati') != '':
            data_update['ih_savunma_sivil_havacilik_urunleri_ihracati'] = data[
                'ih_savunma_sivil_havacilik_urunleri_ihracati']

        if Helpers().set_val_safe(data, 'ih_diger_ihracat') != '':
            data_update['ih_diger_ihracat'] = data['ih_diger_ihracat']

        if Helpers().set_val_safe(data, 'ih_toplam_ihracat') != '':
            data_update['ih_toplam_ihracat'] = data['ih_toplam_ihracat']

        if Helpers().set_val_safe(data, 'it_savunma_urunleri_ithalati') != '':
            data_update['it_savunma_urunleri_ithalati'] = data['it_savunma_urunleri_ithalati']

        if Helpers().set_val_safe(data, 'it_savunma_disi_havacilik_ithalati') != '':
            data_update['it_savunma_disi_havacilik_ithalati'] = data['it_savunma_disi_havacilik_ithalati']

        if Helpers().set_val_safe(data, 'it_savunma_sivil_havacilik_urunleri_ithalati') != '':
            data_update['it_savunma_sivil_havacilik_urunleri_ithalati'] = data[
                'it_savunma_sivil_havacilik_urunleri_ithalati']

        if Helpers().set_val_safe(data, 'it_diger_ithalat') != '':
            data_update['it_diger_ithalat'] = data['it_diger_ithalat']

        if Helpers().set_val_safe(data, 'it_toplam_ithalat') != '':
            data_update['it_toplam_ithalat'] = data['it_toplam_ithalat']

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_savunma_havacilik_ihracat',
                                                                             {'SET':
                                                                                  data_update,
                                                                              'CONDITION': {
                                                                                  0: {
                                                                                      'col': 'savunma_havacilik_ihracat_id',
                                                                                      'operator': '=',
                                                                                      'value': data[
                                                                                          'savunma_havacilik_ihracat_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def delete(self, data, paydas_id, tckn, ip):
        set_durum = None

        delete_data = {
            'aktif_mi': 'false',
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now())
        }

        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_savunma_havacilik_ihracat',
                                                                             {'SET': delete_data,
                                                                              'CONDITION': {
                                                                                  0: {
                                                                                      'col': 'savunma_havacilik_ihracat_id',
                                                                                      'operator': '=',
                                                                                      'value': data[
                                                                                          'savunma_havacilik_ihracat_id'],
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
            set_durum = False

        return set_durum

    def insert(self, data, paydas_id, tckn, ip):
        data_insert = data
        data_insert['aktif_mi'] = 'true'
        data_insert['paydas_id'] = paydas_id
        data_insert['donem'] = 2019

        data_insert['olusturan_ip'] = ip
        data_insert['olusturan_id'] = tckn

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_savunma_havacilik_ihracat',
                                                                          {0: data_insert}).data()

        return data_query
