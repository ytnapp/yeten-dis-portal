# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class FinasGenel(object):

    def get_liste(self):

        all_data = {
            'tesvik_turu': None,
            'yerli_universiteler': None,
            'yabanci_universiteler': None,
            'mukemmeliyet_merkezi': None,
            'tesvik_veren_kurumlar': None
        }

        try:

            tesvik_veren_kurumlar = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_destek_veren_kurumlar',
                condition={
                    0:
                        {'col': 'aktif_mi',
                         'operator': '=',
                         'value': 'true'}
                }
            ).data()

            tesvik_turu = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_tanim',
                condition={0:
                               {'col': 'kolon',
                                'operator': '=',
                                'value': 'tesvik_turu',
                                'combiner': 'AND'},
                           1:
                               {'col': 'tablo',
                                'operator': '=',
                                'value': 'fb_arge_devlet_destekli_harcama'}
                           }
            ).data()

            mukemmeliyet_merkezi = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_tanim',
                condition={0:
                               {'col': 'kolon',
                                'operator': '=',
                                'value': 'mukemmeliyet_merkezi',
                                'combiner': 'AND'},
                           1:
                               {'col': 'tablo',
                                'operator': '=',
                                'value': 'fb_arge_mukemmeliyet'}
                           }
            ).data()

            yerli_universiteler = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_universite',
                condition={0:
                               {'col': 'universite_tipi',
                                'operator': '=',
                                'value': '1',
                                'combiner': 'OR'},
                           1:
                               {'col': 'universite_tipi',
                                'operator': '=',
                                'value': '2'}
                           },
                sort='universite_adi'
            ).data()

            yabanci_universiteler = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_universite',
                condition={0:
                               {'col': 'universite_tipi',
                                'operator': '=',
                                'value': '3',
                                'combiner': 'OR'},
                           1:
                               {'col': 'universite_tipi',
                                'operator': '=',
                                'value': '4'}
                           },
                sort='universite_adi'
            ).data()

            all_data = {
                'tesvik_turu': Helpers().payload_parameters_control(tesvik_turu),
                'yerli_universiteler': Helpers().payload_parameters_control(yerli_universiteler),
                'yabanci_universiteler': Helpers.payload_parameters_control(yabanci_universiteler),
                'mukemmeliyet_merkezi': Helpers().payload_parameters_control(mukemmeliyet_merkezi),
                'tesvik_veren_kurumlar': Helpers().payload_parameters_control(tesvik_veren_kurumlar)
            }
        except Exception as exc:
            print(exc)

        return all_data

    def get_ozet_view(self, paydas_id):

        all_data = None

        try:

            all_data = Helpers.payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.view_fb_finans_display',
                condition={
                    0:
                        {'col': 'paydas_id',
                         'operator': '=',
                         'value': paydas_id}
                }
            ).data())

            all_data = all_data[0]

        except Exception as exc:
            print(exc)

        return all_data

    def insert(self, data, tckn, user_ip, paydas_id):

        data_insert = data
        data_insert['paydas_id'] = paydas_id
        data_insert['aktif_mi'] = 'true'
        data_insert['olusturan_id'] = tckn
        data_insert['olusturan_ip'] = user_ip
        data_insert['donem'] = 2019

        if Helpers().set_val_safe(data, 'risk_raporu_dosya_id', True) is not None:
            data_insert['risk_raporu_dosya_id'] = data['risk_raporu_dosya_id']
        else:
            try:
                del data_insert['risk_raporu_dosya_id']
            except Exception as exc:
                print(exc)

        data_query = Model(ReaPy.get_reactor_source('yeten')).insert('public.fb_paydas',
                                                                     {0: data_insert})
        print(data_query.error())
        print(data_query.data())

        return data_query.data()

    def update(self, data, tckn, user_ip, paydas_id):

        data_update = {'guncelleyen': tckn, 'guncelleme_tarihi': str(datetime.datetime.now()),
                       'guncelleyen_ip': user_ip}

        if Helpers().set_val_safe(data, 'donem') != '':
            data_update['donem'] = data['donem']

        if Helpers().set_val_safe(data, 'stok_devir_hizi') != '':
            data_update['stok_devir_hizi'] = data['stok_devir_hizi']

        if Helpers().set_val_safe(data, 'firma_sermaye_bedeli') != '':
            data_update['firma_sermaye_bedeli'] = data['firma_sermaye_bedeli']

        if Helpers().set_val_safe(data, 'firma_sermaye_bedeli_1') != '':
            data_update['firma_sermaye_bedeli_1'] = data['firma_sermaye_bedeli_1']

        if Helpers().set_val_safe(data, 'firma_sermaye_bedeli_2') != '':
            data_update['firma_sermaye_bedeli_2'] = data['firma_sermaye_bedeli_2']

        if Helpers().set_val_safe(data, 'odenmemis_sermaye_bedeli') != '':
            data_update['odenmemis_sermaye_bedeli'] = data['odenmemis_sermaye_bedeli']

        if Helpers().set_val_safe(data, 'odenmemis_sermaye_bedeli_1') != '':
            data_update['odenmemis_sermaye_bedeli_1'] = data['odenmemis_sermaye_bedeli_1']

        if Helpers().set_val_safe(data, 'odenmemis_sermaye_bedeli_2') != '':
            data_update['odenmemis_sermaye_bedeli_2'] = data['odenmemis_sermaye_bedeli_2']

        if Helpers().set_val_safe(data, 'toplam_ihracat_miktari') != '':
            data_update['toplam_ihracat_miktari'] = data['toplam_ihracat_miktari']

        if Helpers().set_val_safe(data, 'toplam_ihracat_miktari_1') != '':
            data_update['toplam_ihracat_miktari_1'] = data['toplam_ihracat_miktari_1']

        if Helpers().set_val_safe(data, 'toplam_ihracat_miktari_2') != '':
            data_update['toplam_ihracat_miktari_2'] = data['toplam_ihracat_miktari_2']

        if Helpers().set_val_safe(data, 'savunma_havacilik_net_ihracat') != '':
            data_update['savunma_havacilik_net_ihracat'] = data['savunma_havacilik_net_ihracat']

        if Helpers().set_val_safe(data, 'savunma_havacilik_net_ihracat_1') != '':
            data_update['savunma_havacilik_net_ihracat_1'] = data['savunma_havacilik_net_ihracat_1']

        if Helpers().set_val_safe(data, 'savunma_havacilik_net_ihracat_2') != '':
            data_update['savunma_havacilik_net_ihracat_2'] = data['savunma_havacilik_net_ihracat_2']

        if Helpers().set_val_safe(data, 'ssb_kredi_beklentisi_var_mi') != '':
            data_update['ssb_kredi_beklentisi_var_mi'] = data['ssb_kredi_beklentisi_var_mi']

        if Helpers().set_val_safe(data, 'vergi_levhasi_dosya_id') != '':
            data_update['vergi_levhasi_dosya_id'] = data['vergi_levhasi_dosya_id']

        if Helpers().set_val_safe(data, 'kurumlar_beyannamesi_dosya_id') != '':
            data_update['kurumlar_beyannamesi_dosya_id'] = data['kurumlar_beyannamesi_dosya_id']

        if Helpers().set_val_safe(data, 'sgk_hizmet_dokumu_dosya_id') != '':
            data_update['sgk_hizmet_dokumu_dosya_id'] = data['sgk_hizmet_dokumu_dosya_id']

        if Helpers().set_val_safe(data, 'vergi_borcu_yoktur_dosya_id') != '':
            data_update['vergi_borcu_yoktur_dosya_id'] = data['vergi_borcu_yoktur_dosya_id']

        if Helpers().set_val_safe(data, 'risk_raporu_dosya_id', True) is not None:
            data_update['risk_raporu_dosya_id'] = data['risk_raporu_dosya_id']
        else:
            try:
                del data_update['risk_raporu_dosya_id']
            except Exception as exc:
                print(exc)

        print(data_update)

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.fb_paydas',
                                                                        {'SET':
                                                                             data_update,
                                                                         'CONDITION': {
                                                                             0: {'col': 'fb_id',
                                                                                 'operator': '=',
                                                                                 'value': data['fb_id'],
                                                                                 'combiner': 'AND'},
                                                                             1: {'col': 'paydas_id',
                                                                                 'operator': '=',
                                                                                 'value': paydas_id}
                                                                         }}
                                                                        )

        except Exception as exc:
            print(exc)

        if set_durum is not None:
            set_durum = set_durum.data()

        return set_durum

    def get_detay(self, paydas_id):

        data_query = Helpers().payload_parameters_control(
            Model(ReaPy.get_reactor_source('yeten')).select(table='public.fb_paydas',
                                                            condition={0: {'col': 'paydas_id',
                                                                           'operator': '=',
                                                                           'value': paydas_id,
                                                                           'combiner': 'AND'},
                                                                       1: {'col': 'aktif_mi',
                                                                           'operator': '=',
                                                                           'value': 'true'}
                                                                       }
                                                            ).data())
        if data_query is not None and len(data_query) > 0:
            data_query = data_query[0]

            data_query['dosyalar'] = self.__get_dosyalar(data_query)
            del data_query['risk_raporu_dosya_id']
            del data_query['vergi_levhasi_dosya_id']
            del data_query['kurumlar_beyannamesi_dosya_id']
            del data_query['sgk_hizmet_dokumu_dosya_id']
            del data_query['vergi_borcu_yoktur_dosya_id']

        return data_query

    def __get_dosyalar(self, data_query):

        arr_genel = []

        try:

            risk_dosya = Helpers.payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='dosya_yukleme',
                field=" dosya_yukleme_id, dosya_adi, 'risk_raporu_dosya_id' as dosya_turu, paydas_id",
                condition={
                    0: {'col': 'paydas_id', 'operator': '=', 'value': data_query['paydas_id'],
                        'combiner': 'AND'},
                    1: {'col': 'dosya_yukleme_id', 'operator': '=', 'value': data_query['risk_raporu_dosya_id'],
                        'combiner': 'AND'},
                    2: {'col': 'aktif_mi',
                        'operator': '=',
                        'value': 'true'}
                }
            ).data())
            if risk_dosya is not None and len(risk_dosya) > 0:
                arr_genel.append(risk_dosya[0])
        except Exception as exc:
            print(exc)
        try:
            vergi_levha = Helpers.payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='dosya_yukleme',
                field=" dosya_yukleme_id, dosya_adi, 'vergi_levhasi_dosya_id' as dosya_turu, paydas_id",
                condition={
                    0: {'col': 'paydas_id', 'operator': '=', 'value': data_query['paydas_id'],
                        'combiner': 'AND'},
                    1: {'col': 'dosya_yukleme_id', 'operator': '=', 'value': data_query['vergi_levhasi_dosya_id'],
                        'combiner': 'AND'},
                    2: {'col': 'aktif_mi',
                        'operator': '=',
                        'value': 'true'}
                }
            ).data())
            if vergi_levha is not None:
                arr_genel.append(vergi_levha[0])
        except Exception as exc:
            print(exc)
        try:
            kurumlar_beyannamesi = Helpers.payload_parameters_control(
                Model(ReaPy.get_reactor_source('yeten')).select(
                    table='dosya_yukleme',
                    field=" dosya_yukleme_id, dosya_adi, 'kurumlar_beyannamesi_dosya_id' as dosya_turu, paydas_id",
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': data_query['paydas_id'],
                            'combiner': 'AND'},
                        1: {'col': 'dosya_yukleme_id', 'operator': '=',
                            'value': data_query['kurumlar_beyannamesi_dosya_id'],
                            'combiner': 'AND'},
                        2: {'col': 'aktif_mi',
                            'operator': '=',
                            'value': 'true'}
                    }
                ).data())
            if kurumlar_beyannamesi is not None:
                arr_genel.append(kurumlar_beyannamesi[0])
        except Exception as exc:
            print(exc)
        try:
            sgk_hizmet_dokumu = Helpers.payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='dosya_yukleme',
                field=" dosya_yukleme_id, dosya_adi, 'sgk_hizmet_dokumu_dosya_id' as dosya_turu, paydas_id",
                condition={
                    0: {'col': 'paydas_id', 'operator': '=', 'value': data_query['paydas_id'],
                        'combiner': 'AND'},
                    1: {'col': 'dosya_yukleme_id', 'operator': '=', 'value': data_query['sgk_hizmet_dokumu_dosya_id'],
                        'combiner': 'AND'},
                    2: {'col': 'aktif_mi',
                        'operator': '=',
                        'value': 'true'}
                }
            ).data())
            if sgk_hizmet_dokumu is not None:
                arr_genel.append(sgk_hizmet_dokumu[0])
        except Exception as exc:
            print(exc)

        try:
            vergi_borcu_yoktur = Helpers.payload_parameters_control(
                Model(ReaPy.get_reactor_source('yeten')).select(
                    table='dosya_yukleme',
                    field=" dosya_yukleme_id, dosya_adi, 'vergi_borcu_yoktur_dosya_id' as dosya_turu",
                    condition={
                        0: {'col': 'paydas_id', 'operator': '=', 'value': data_query['paydas_id'],
                            'combiner': 'AND'},
                        1: {'col': 'dosya_yukleme_id', 'operator': '=',
                            'value': data_query['vergi_borcu_yoktur_dosya_id'],
                            'combiner': 'AND'},
                        2: {'col': 'aktif_mi',
                            'operator': '=',
                            'value': 'true'}
                    }
                ).data())
            if vergi_borcu_yoktur is not None:
                arr_genel.append(vergi_borcu_yoktur[0])

        except Exception as exc:
            print(exc)

        return arr_genel

    def get_aktif_donem(self):

        donem = None

        try:

            dquery = Helpers.payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.sys_tanim',
                field='deger as aktif_donem',
                condition={0:
                               {'col': 'kolon',
                                'operator': '=',
                                'value': 'aktif_donem',
                                'combiner': 'AND'},
                           1:
                               {'col': 'tablo',
                                'operator': '=',
                                'value': 'fb_paydas'}
                           }
            ).data())

            donem = dquery[0]['aktif_donem']

        except Exception as exc:
            print(exc)

        return donem
