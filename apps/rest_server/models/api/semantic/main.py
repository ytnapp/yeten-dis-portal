# -*- coding: utf-8 -*-

from core.reapy import ReaPy


class Main(ReaPy):
    caller = None

    def __init__(self, caller):
        super(Main).__init__()
        self.caller = caller

    def json_validate(self, schema):
        validate = self.validator().json_validate(self.caller.master.schema[schema], self.caller.request_data)
        if validate['success'] is False:
            message = self.caller.master.messages['errors']['errors_temp']
            message['description'] = validate['message']
            return self.rest_response(message, 400)

    def check_token(self):
        try:
            bearer_token = self.caller.request.environ['HTTP_AUTHORIZATION'].split(' ')[1]
        except Exception as e:
            print(e)
            bearer_token = None
        token_control = ReaPy.hash().check_jwt(bearer_token)
        if token_control is None:
            message = self.caller.master.messages['errors']['resource_sf_err_100']
            return ReaPy.rest_response(message['data'], 401)
