# -*- coding: utf-8 -*-

from core.model import Model
# from core.utilities import epo_ops
# from core.utilities.epo_ops import models as model
import xml.etree.ElementTree as et


class Cpc(object):

    def __init__(self, key, secret):
        self.key = key
        self.secret = secret
        self.root = 'root'
        self.level = 2
        self.ref = ''
        self.u = ''
        self.text = ''
        self.search_key = None
        self.listed = ['text', 'title-part', 'class-title', 'classification-statistics', 'u', 'class-ref',
                       'explanation', 'comment']

    def reset(self):
        self.ref = ''
        self.u = ''
        self.text = ''

    def set_client(self):
        client = epo_ops.Client(key=self.key, secret=self.secret)
        return client

    def search(self, search_key):
        listt = ['ops:', 'cpc:']
        self.search_key = search_key
        research = self.set_client().classification_cpc_search(self.search_key)
        text = str(research.text)
        for it in listt:
            text = text.replace(it, '')
        # with open('/home/buyukveri/Documents/cpc_search/cpc-search.xml', 'w') as file:
        #     file.write(text)
        return text

    def published_data_model(self):
        response = self.set_client().published_data(reference_type=self.set_client().publication,
                                                    input=model.Docdb('1000000', 'EP', 'A1'),
                                                    endpoint='biblio',
                                                    constituents=[])
        print(response.text)

    def rec_get(self, tree):
        try:
            for item in tree:
                self.rec_get(item)
                if tree.tag == 'explanation':
                    self.text += '(' + item.text + self.u + ' ' + self.ref + ')'
                elif tree.tag == 'comment':
                    self.text += '{ ' + item.text + self.u + self.ref + ' }'
                elif tree.tag == 'title-part':
                    if item.tag == 'text':
                        if self.text == '':
                            self.text += item.text
                        else:
                            self.text += '; ' + item.text
            if tree.tag == 'u':
                self.u += tree.text + ' ' + tree.tail.strip()
            elif tree.tag == 'class-ref':
                if self.ref == '':
                    self.ref += tree.text + tree.tail.strip()
                self.ref += ' ' + tree.text + tree.tail.strip()
        except Exception as e:
            pass
        finally:
            return self.text

    def get_cpc_search(self, search_key):
        r_dict = []
        search_result = self.search(search_key)
        # search_tree = et.parse(search_result)
        search_tree = et.fromstring(search_result)
        cls = search_tree.findall('.//classification-statistics')
        for item in cls:
            self.reset()
            # level = Model(ReaPy.get_reactor_source('yeten')) \
            #     .select(table='sys_cpc', field='seviye',
            #             condition={
            #                 0: {'col': 'sembol', 'operator': '=',
            #                     'value': item.attrib.get('classification-symbol', None)}
            #             }, sort='sembol', is_first=True).data()
            # r_dict[item.attrib.get('classification-symbol', None)] = {}
            # r_dict[item.attrib.get('classification-symbol', None)].update(
            r_dict.append({'key': item.attrib.get('classification-symbol', None), 'title': self.rec_get(item)})
            # r_dict[item.attrib.get('classification-symbol', None)].update(level[0])
        return r_dict

    def get_cpc_data(self, root=None, level=None, send_data=None):
        try:
            if send_data is None and root is not None:
                send_data = {}
            if '/' in root and level == 0:
                root = '%' + root.split('/')[0] + '%'
                record = Model(ReaPy.get_reactor_source('yeten')) \
                    .select(table='sys_cpc', field='sembol, seviye, aciklama',
                            condition={
                                0: {'col': 'ata', 'operator': 'LIKE', 'value': root}
                            }, sort='sembol').data()
                root = root[1:-1]
                send_data[root] = {}
                send_data[root]['children'] = []
                for item in record:
                    send_data[root]['children'].append({'key': item['sembol'], 'title': item['aciklama'], 'seviye': item['seviye']})
                return send_data
            else:
                record = Model(ReaPy.get_reactor_source('yeten')) \
                    .select(table='sys_cpc', field='ata, sembol, seviye, aciklama',
                            condition={
                                0: {'col': 'ata', 'operator': '=', 'value': root,
                                    'combiner': 'AND'},
                                1: {'col': 'seviye', 'operator': '=', 'value': level}
                            }, sort='sembol').data()
            if record is not None and record != []:
                send_data[root] = {}
                send_data[root]['children'] = []
                for item in record:
                    if item['ata'] == root:
                        send_data[root]['children'].append(
                            {'key': item['sembol'], 'title': item['aciklama'], 'seviye': item['seviye']})
            return send_data
        except Exception as e:
            print(e)

