# -*- coding: utf-8 -*-
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class ElementValues:

    def __init__(self):
        self.error = False

    @staticmethod
    def get_stakeholder_product(iam_user_company_id, keyword, referenced_product=2):
        products = Model(ReaPy.get_reactor_source('yeten')).select('public.view_urun_listesi',
                                                                   'urun_id,urun_adi,urun_turu,referansli_urun',
                                                                   {
                                                                       0: {'col': 'urun_sahibi', 'operator': '=',
                                                                           'value': iam_user_company_id,
                                                                           'combiner': 'AND'
                                                                           },
                                                                       1: {'col': 'referansli_urun',
                                                                           'operator': '!=',
                                                                           'value': referenced_product,
                                                                           'combiner': 'AND'
                                                                           },
                                                                       2: {'col': 'LOWER(urun_adi)',
                                                                           'operator': 'LIKE',
                                                                           'value': '%' + keyword.lower() + '%'
                                                                           }
                                                                   },
                                                                   'urun_adi asc', None,
                                                                   False).data()

        return products

    @staticmethod
    def get_stakeholder_list(iam_user_company_id, keyword, origin):
        stakeholder_list = Model(ReaPy.get_reactor_source('yeten')).select(
            'public.view_yerli_yabanci_tum_firmalar', 'paydas_id,paydas_adi,yerli_yabanci',
            {
                0: {'col': 'paydas_id', 'operator': '!=',
                    'value': iam_user_company_id,
                    'combiner': 'AND'
                    , 'scope': 0
                    },
                1: {'col': 'yerli_yabanci', 'operator': '!=',
                    'value': origin,
                    'combiner': 'AND'
                    , 'scope': 1
                    },
                2: {'col': 'LOWER(paydas_adi)', 'operator': 'LIKE',
                    'value': '%' + keyword.lower() + '%',
                    'combiner': 'OR'
                    , 'scope': 0
                    },
                3: {'col': 'vergi_tc_no', 'operator': 'LIKE',
                    'value': '%' + keyword + '%', 'scope': 1
                    }
            },
            'paydas_adi asc', None,
            False).data()

        return stakeholder_list

    @staticmethod
    def get_countries():
        countries = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_ulke',
                                                                    'ulke_id as key ,ulke_adi as value',
                                                                    None,
                                                                    'resmi_ulke_adi asc', None,
                                                                    False, False).data()
        return countries

    @staticmethod
    def get_units(unit_type=None):
        if unit_type is None:
            units = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_birim',
                                                                   'birim_id,birim',
                                                                   None,
                                                                   'birim asc', None,
                                                                   False, True).data()
        else:
            units = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_birim',
                                                                   'birim_id,birim',
                                                                   {0: {'col': 'birim_tipi', 'operator': '=',
                                                                        'value': unit_type
                                                                        }},
                                                                   'birim asc', None,
                                                                   False, True).data()

        return units

    @staticmethod
    def get_cpc(parent, level, finder=None):
        cpc_list = []
        if finder is not None and len(finder) > 0:
            cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                                  'cpc_id,sembol,seviye,aciklama',
                                                                  {0: {'col': 'LOWER(aciklama)',
                                                                       'operator': 'LIKE',
                                                                       'value': '%' + finder.lower() + '%',
                                                                       'combiner': 'OR'
                                                                       },
                                                                   1: {'col': 'LOWER(sembol)', 'operator': 'LIKE',
                                                                       'value': '%' + finder.lower() + '%'
                                                                       }},
                                                                  'aciklama asc', None,
                                                                  False, False).data()

        else:
            cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                                  'cpc_id,sembol,seviye,aciklama',
                                                                  {0: {'col': 'seviye', 'operator': '=',
                                                                       'value': level, 'combiner': 'AND'
                                                                       },
                                                                   1: {'col': 'ata', 'operator': '=',
                                                                       'value': parent}},
                                                                  'sembol asc', None,
                                                                  False, False).data()

        if cpc is not None and cpc is not False:
            for key in cpc:
                key_data = {
                    "value": key['cpc_id'],
                    "sembol": key['sembol'],
                    "seviye": key['seviye'],
                    "title": key['aciklama']

                }
                cpc_list.append(key_data)
        return cpc_list

    @staticmethod
    def find_taksonomi(finder):
        taksonomi_list = []
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    None,
                                                                    {0: {'col': 'LOWER(turkce_adi)',
                                                                         'operator': 'LIKE',
                                                                         'value': '%' + finder.lower() + '%'
                                                                         }},
                                                                    'kodu asc', None,
                                                                    False, False).data()

        if taksonomi is not None:
            for key in taksonomi:
                taksonomi_list.append(
                    {'kod': key['kodu'], 'title': key['turkce_adi'],
                     'key': key['taksonomi_id']})

        return taksonomi_list

    @staticmethod
    def get_taksonomi():
        taksonomi_list = []
        root_index = []
        taksonomi = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_taksonomi',
                                                                    None,
                                                                    None,
                                                                    'kodu asc', None,
                                                                    False, False).data()
        if taksonomi is not None:
            for key in taksonomi:
                if key['ust_kirilim'] == key['kodu']:
                    root_index.append(key['kodu'])
                    taksonomi_list.append(
                        {'kod': key['kodu'], 'title': key['turkce_adi'], "key": ReaPy.hash().get_uuid(),
                         'value': key['taksonomi_id'], 'disabled': True,
                         'children': []})
            for key in taksonomi:
                if key['ust_kirilim'] != key['kodu'] and '.' not in key['kodu']:
                    index = root_index.index(key['ust_kirilim'])
                    taksonomi_list[index]['children'].append({'kod': key['kodu'], 'title': key['turkce_adi'],
                                                              "key": ReaPy.hash().get_uuid(),
                                                              'value': key['taksonomi_id'],
                                                              'children': []})
            for key in taksonomi:
                if key['ust_kirilim'] != key['kodu'] and '.' in key['kodu']:
                    index_key = root_index.index(key['ust_kirilim'])
                    second_index = next((index for (index, d) in enumerate(taksonomi_list[index_key]['children']) if
                                         d["kod"] == key['alt_kirilim']), None)

                    taksonomi_list[index_key]['children'][second_index]['children'].append(
                        {'kod': key['kodu'], 'title': key['turkce_adi'],
                         "key": ReaPy.hash().get_uuid(), 'value': key['taksonomi_id']})
        return taksonomi_list

    @staticmethod
    def get_sys_definition(table, column):
        sys_definition = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_tanim',
                                                                         'tanim_id as key ,aciklama as value,deger',
                                                                         {0: {'col': 'tablo', 'operator': '=',
                                                                              'value': table,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'kolon', 'operator': '=',
                                                                              'value': column}},
                                                                         'deger asc', None,
                                                                         False, False).data()
        return sys_definition

    @staticmethod
    def get_pending_product(iam_user_company_id, keyword):
        pending_product = Model(ReaPy.get_reactor_source('yeten')).select('public.view_bekleyen_urun_listesi',
                                                                          'referansli_urun_id,referans_veren,urun_adi,marka,model,urun_turu,referans_durumu,eslestirilen_urun,reddedilme_aciklamasi',
                                                                          {
                                                                              0: {'col': 'urun_sahibi_paydas_id',
                                                                                  'operator': '=',
                                                                                  'value': iam_user_company_id,
                                                                                  'scope': 0,
                                                                                  'combiner': 'AND'},
                                                                              1: {'col': 'referans_durumu',
                                                                                  'operator': '!=',
                                                                                  'value': 4,
                                                                                  'scope': 1,
                                                                                  'combiner': 'AND'},
                                                                              2: {'col': 'LOWER(urun_adi)',
                                                                                  'operator': 'LIKE',
                                                                                  'value': '%' + keyword.lower() + '%',
                                                                                  'scope': 0,
                                                                                  'combiner': 'OR'},
                                                                              3: {'col': 'LOWER(marka)',
                                                                                  'operator': 'LIKE',
                                                                                  'value': '%' + keyword.lower() + '%',
                                                                                  'combiner': 'OR'},
                                                                              4: {'col': 'LOWER(model)',
                                                                                  'operator': 'LIKE',
                                                                                  'value': '%' + keyword.lower() + '%',
                                                                                  'scope': 1,
                                                                                  }
                                                                          },
                                                                          'urun_adi asc', None,
                                                                          False).data()
        return pending_product

    @staticmethod
    def get_assigned_product(iam_user_company_id, keyword):
        assigned_product = Model(ReaPy.get_reactor_source('yeten')).select('public.view_atanan_urun_listesi',
                                                                           'referansli_urun_id,urun_sahibi,urun_adi,marka,model,urun_turu,referans_durumu,atanan_urun,reddedilme_aciklamasi',
                                                                           {
                                                                               0: {
                                                                                   'col': 'referans_veren_paydas_id',
                                                                                   'operator': '=',
                                                                                   'value': iam_user_company_id,
                                                                                   'scope': 0,
                                                                                   'combiner': 'AND'},
                                                                               1: {'col': 'referans_durumu',
                                                                                   'operator': '!=',
                                                                                   'value': 4,
                                                                                   'scope': 1,
                                                                                   'combiner': 'AND'},
                                                                               2: {'col': 'LOWER(urun_adi)',
                                                                                   'operator': 'LIKE',
                                                                                   'value': '%' + keyword.lower() + '%',
                                                                                   'scope': 0,
                                                                                   'combiner': 'OR'},
                                                                               3: {'col': 'LOWER(marka)',
                                                                                   'operator': 'LIKE',
                                                                                   'value': '%' + keyword.lower() + '%',
                                                                                   'combiner': 'OR'},
                                                                               4: {'col': 'LOWER(model)',
                                                                                   'operator': 'LIKE',
                                                                                   'value': '%' + keyword.lower() + '%',
                                                                                   'scope': 1,
                                                                                   }
                                                                           },
                                                                           None, None,
                                                                           False).data()
        return assigned_product

    @staticmethod
    def get_prod_tr():
        prod_tr = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_prodtr',
                                                                  'prodtr_id,kodu,adi,birim',
                                                                  None,
                                                                  'adi asc', None,
                                                                  False).data()
        return prod_tr

    @staticmethod
    def get_software_standards():
        software_standards = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_yazilim_standart',
                                                                             'standart_id as key ,standart_adi as value',
                                                                             None,
                                                                             'standart_adi asc', None,
                                                                             False, False).data()
        return software_standards
