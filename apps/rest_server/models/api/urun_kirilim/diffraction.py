# -*- coding: utf-8 -*-
from core.reapy import ReaPy
import datetime

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Diffraction:
    um_kirilim = 'public.um_kirilim'
    um_kirilim_kendi = 'public.um_kirilim_kendi'
    um_kirilim_yerli = 'public.um_kirilim_yerli'
    um_referansli_urun = 'public.um_referansli_urun'
    um_kirilim_alternatif_uretici = 'public.um_kirilim_alternatif_uretici'
    um_kirilim_yabanci = 'public.um_kirilim_yabanci'
    um_kirilim_diger_yazilim = 'public.um_kirilim_diger_yazilim'
    um_kirilim_diger_urun = 'public.um_kirilim_diger_urun'
    diffraction_level = 0

    @staticmethod
    def check_product(iam_user_company_id, product_id):
        products = Model(ReaPy.get_reactor_source('yeten')).select('public.view_urun_listesi',
                                                                   None,
                                                                   {
                                                                       0: {'col': 'urun_sahibi', 'operator': '=',
                                                                           'value': iam_user_company_id,
                                                                           'combiner': 'AND'
                                                                           },
                                                                       1: {'col': 'urun_id',
                                                                           'operator': '=',
                                                                           'value': product_id
                                                                           }
                                                                   },
                                                                   'urun_adi asc', None,
                                                                   False).data()

        if products is None:
            products = Model(ReaPy.get_reactor_source('yeten')).select('public.view_alt_kirilim_listesi',
                                                                       None,
                                                                       {
                                                                           0: {'col': 'paydas_id', 'operator': '=',
                                                                               'value': iam_user_company_id,
                                                                               'combiner': 'AND'
                                                                               },
                                                                           1: {'col': 'kirilim_id',
                                                                               'operator': '=',
                                                                               'value': product_id,
                                                                               'combiner': 'AND'
                                                                               },
                                                                           2: {'col': 'kirilim_tipi',
                                                                               'operator': '>',
                                                                               'value': 3
                                                                               }
                                                                       },
                                                                       'node_urun_adi asc', None,
                                                                       False).data()
        return products

    @staticmethod
    def check_product_by_id(product_id):
        products = Model(ReaPy.get_reactor_source('yeten')).select('public.view_urun_listesi',
                                                                   None,
                                                                   {
                                                                       0: {'col': 'urun_id',
                                                                           'operator': '=',
                                                                           'value': product_id
                                                                           }
                                                                   },
                                                                   'urun_adi asc', None,
                                                                   False).data()

        return products

    @staticmethod
    def check_product_company(product_company_id):
        products = Model(ReaPy.get_reactor_source('yeten')).select('public.view_yerli_yabanci_tum_firmalar',
                                                                   None,
                                                                   {
                                                                       0: {'col': 'paydas_id', 'operator': '=',
                                                                           'value': product_company_id
                                                                           }
                                                                   },
                                                                   'paydas_id asc', None,
                                                                   True).data()

        return products

    def get_diffraction_by_id(self, iam_user_company_id, product_id):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'paydas_id', 'operator': '=',
                                                                              'value': iam_user_company_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'torun',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          2: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'torun asc', None,
                                                                      False).data()

        return diffraction

    def get_diffraction_by_child(self, iam_user_company_id, diffraction_product_id, product_id, loc):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select('um_kirilim_' + loc,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'paydas_id', 'operator': '=',
                                                                              'value': iam_user_company_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'urun_id',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          2: {'col': 'ata_urun_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          3: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'urun_id asc', None,
                                                                      False).data()

        return diffraction

    def insert_diffraction(self, diffraction_id, diffraction_type, parent, child, iam_user_company_id, depth, user,
                           user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim,
                                                                          {
                                                                              0: {
                                                                                  'kirilim_id':
                                                                                      diffraction_id,
                                                                                  'kirilim_tipi': diffraction_type,
                                                                                  'ata': parent,
                                                                                  'torun': child,
                                                                                  'paydas_id':
                                                                                      iam_user_company_id,
                                                                                  'derinlik': depth,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_own_diffraction(self, own_diffraction_id, diffraction_id, iam_user_company_id, product_other, brand,
                               model, product_amount,
                               amount_unit_id, amount_unit_other, product_id, product_type, user,
                               user_ip, diffraction_product_id):
        add_own_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim_kendi,
                                                                              {
                                                                                  0: {
                                                                                      'kirilim_kendi_id': own_diffraction_id,
                                                                                      'kirilim_id': diffraction_id,
                                                                                      'paydas_id': iam_user_company_id,
                                                                                      'urun_diger': product_other,
                                                                                      'marka': brand,
                                                                                      'model': model,
                                                                                      'talep_miktari': product_amount,
                                                                                      'talep_miktari_birim': amount_unit_id,
                                                                                      'talep_miktari_birim_diger': amount_unit_other,
                                                                                      'urun_id': product_id,
                                                                                      'olusturan': user,
                                                                                      'olusturan_ip': user_ip,
                                                                                      'aktif_mi': True,
                                                                                      'urun_tipi': product_type,
                                                                                      'ata_urun_id': diffraction_product_id
                                                                                  }
                                                                              }).data()

        return add_own_diffraction

    @staticmethod
    def get_sys_definition(definition_id):
        sys_definition = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_tanim',
                                                                         'deger',
                                                                         {
                                                                             0: {'col': 'tabim_id', 'operator': '=',
                                                                                 'value': definition_id,
                                                                                 }
                                                                         },
                                                                         'deger asc', None,
                                                                         False, False).data()
        return sys_definition

    def insert_own_diffraction_rollback(self, first_diffraction_id, diffraction_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(first_diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim_kendi, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})

    def insert_referenced_product(self, referencing_id, product_owner_id,product_owner_other,product_owner_other_tax_number, product_name, brand, model, diffraction_id,
                                  parent_product_id, reference_status, product_type, user,
                                  user_ip):
        add_referenced_product = Model(ReaPy.get_reactor_source('yeten')).insert(self.um_referansli_urun,
                                                                                 {
                                                                                     0: {
                                                                                         'referans_veren_paydas_id': referencing_id,
                                                                                         'urun_sahibi_paydas_id': product_owner_id,
                                                                                         'urun_sahibi_paydas_diger': product_owner_other,
                                                                                         'urun_sahibi_paydas_diger_vergi_no': product_owner_other_tax_number,
                                                                                         'urun_adi': product_name,
                                                                                         'marka': brand,
                                                                                         'model': model,
                                                                                         'kirilim_id': diffraction_id,
                                                                                         'parent_urun_id': parent_product_id,
                                                                                         'referans_durumu': reference_status,
                                                                                         'referans_urun_tipi': product_type,
                                                                                         'olusturan': user,
                                                                                         'olusturan_ip': user_ip,
                                                                                         'aktif_mi': True
                                                                                     }
                                                                                 }).data()

        return add_referenced_product

    def get_diffraction_by_referenced(self, referencing_id, product_owner_id, product_name, brand, model,
                                      parent_product_id):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_referansli_urun,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'referans_veren_paydas_id',
                                                                              'operator': '=',
                                                                              'value': referencing_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'urun_sahibi_paydas_id',
                                                                              'operator': '=',
                                                                              'value': product_owner_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          2: {'col': 'LOWER(urun_adi)',
                                                                              'operator': '=',
                                                                              'value': product_name.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          3: {'col': 'LOWER(marka)',
                                                                              'operator': '=',
                                                                              'value': brand.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          4: {'col': 'LOWER(model)',
                                                                              'operator': '=',
                                                                              'value': model.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          5: {'col': 'parent_urun_id',
                                                                              'operator': '=',
                                                                              'value': parent_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          6: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'parent_urun_id asc', None,
                                                                      False).data()

        return diffraction

    def get_diffraction_pending_product(self, iam_user_company_id, pending_product_id, pending_status):
        pending_product = Model(ReaPy.get_reactor_source('yeten')).select(self.um_referansli_urun,
                                                                          None,
                                                                          {
                                                                              0: {'col': 'urun_sahibi_paydas_id',
                                                                                  'operator': '=',
                                                                                  'value': iam_user_company_id,
                                                                                  'combiner': 'AND'
                                                                                  },
                                                                              1: {'col': 'referansli_urun_id',
                                                                                  'operator': '=',
                                                                                  'value': pending_product_id,
                                                                                  'combiner': 'AND'
                                                                                  },
                                                                              2: {'col': 'referans_durumu',
                                                                                  'operator': '=',
                                                                                  'value': pending_status,
                                                                                  'combiner': 'AND'
                                                                                  },
                                                                              3: {'col': 'aktif_mi',
                                                                                  'operator': '=',
                                                                                  'value': True
                                                                                  }
                                                                          },
                                                                          'referansli_urun_id asc', None,
                                                                          False).data()

        return pending_product

    def update_pending_product(self, pending_product_id, assigned_product_id, pending_status, refusal_description, user,
                               user_ip, ):
        pending_product = Model(ReaPy.get_reactor_source('yeten')).update(self.um_referansli_urun, {
            'SET': {'referans_durumu': pending_status,
                    'guncelleme_tarihi': str(datetime.datetime.now()),
                    'guncelleyen': user,
                    'guncelleyen_ip': user_ip,
                    'reddedilme_aciklamasi': refusal_description,
                    'eslestirilen_urun_id': assigned_product_id
                    },
            'CONDITION': {
                0: {
                    'col': 'referansli_urun_id',
                    'operator': '=',
                    'value': pending_product_id
                }
            }}).data()

        return pending_product

    @staticmethod
    def update_pending_diffraction_product(diffraction_id, product_id, product_type, brand, model, loc, user,
                                           user_ip):
        diffraction_product = Model(ReaPy.get_reactor_source('yeten')).update('um_kirilim_' + loc, {
            'SET': {
                'urun_id': product_id,
                'urun_diger': None,
                'urun_tipi': product_type,
                'marka': brand,
                'model': model,
                'guncelleme_tarihi': str(datetime.datetime.now()),
                'guncelleyen': user,
                'guncelleyen_ip': user_ip
            },
            'CONDITION': {
                0: {
                    'col': 'kirilim_id',
                    'operator': '=',
                    'value': diffraction_id
                }
            }}).data()

        return diffraction_product

    def get_product_diffraction_by_id(self, iam_user_company_id, product_id):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select('public.view_alt_kirilim_listesi',
                                                                      'kirilim_id,node_urun_adi,node_turu,derinlik,kirilim_tipi,urun_id,cpc,urun_sahibi',
                                                                      {
                                                                          0: {'col': 'paydas_id', 'operator': '=',
                                                                              'value': iam_user_company_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'ata',
                                                                              'operator': '=',
                                                                              'value': product_id}
                                                                      },
                                                                      'olusturulma_tarihi asc', None,
                                                                      False).data()

        return diffraction

    def check_product_diffraction_by_id(self, iam_user_company_id, diffraction_id):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select('public.view_alt_kirilim_listesi',
                                                                      'kirilim_id,node_urun_adi,alt_kirilim_sayisi,toplam_kirilim_sayisi,derinlik',
                                                                      {
                                                                          0: {'col': 'paydas_id', 'operator': '=',
                                                                              'value': iam_user_company_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'kirilim_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_id}
                                                                      },
                                                                      None, None,
                                                                      True).data()

        return diffraction

    def delete_product_diffraction_by_id(self, iam_user_company_id, diffraction_id, user, user_ip):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim', {
            'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                    'guncelleme_tarihi': str(datetime.datetime.now())},
            'CONDITION': {0: {'col': 'paydas_id', 'operator': '=', 'value': iam_user_company_id,
                              'combiner': 'AND'},
                          1: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                              }}}).data()
        if diffraction > 0:
            Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim_yerli', {
                'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                        'guncelleme_tarihi': str(datetime.datetime.now())},
                'CONDITION': {0: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                                  }}}).data()
            Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim_kendi', {
                'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                        'guncelleme_tarihi': str(datetime.datetime.now())},
                'CONDITION': {0: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                                  }}}).data()
            Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim_yabanci', {
                'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                        'guncelleme_tarihi': str(datetime.datetime.now())},
                'CONDITION': {0: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                                  }}}).data()
            Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim_diger_urun', {
                'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                        'guncelleme_tarihi': str(datetime.datetime.now())},
                'CONDITION': {0: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                                  }}}).data()
            Model(ReaPy.get_reactor_source('yeten')).update('public.um_kirilim_diger_yazilim', {
                'SET': {'aktif_mi': False, 'guncelleyen': user, 'guncelleyen_ip': user_ip,
                        'guncelleme_tarihi': str(datetime.datetime.now())},
                'CONDITION': {0: {'col': 'kirilim_id', 'operator': '=', 'value': diffraction_id,
                                  }}}).data()
        return diffraction

    def insert_domestic_supply_diffraction_rollback(self, first_diffraction_id, diffraction_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(first_diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim_yerli, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})

    def insert_foreign_supply_diffraction_rollback(self, first_diffraction_id, diffraction_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(first_diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim_yabanci, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})

    def insert_other_software_diffraction_rollback(self, first_diffraction_id, diffraction_id):
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(first_diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})
        Model(ReaPy.get_reactor_source('yeten')).delete(self.um_kirilim_diger_yazilim, {
            0: {'col': 'kirilim_id', 'operator': '=', 'value': str(diffraction_id)}})

    def insert_domestic_supply_diffraction(self, domestic_diffraction_id, diffraction_id, product_company_id,
                                           product_company_other, product_id, product_other, brand,
                                           model, product_amount,
                                           amount_unit_id, amount_unit_other, diffraction_product_id,
                                           alternative_manufacturer, product_type, user,
                                           user_ip, product_company_tax_number):
        add_domestic_supply_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim_yerli,
                                                                                          {
                                                                                              0: {
                                                                                                  'kirilim_yerli_id': domestic_diffraction_id,
                                                                                                  'kirilim_id': diffraction_id,
                                                                                                  'paydas_id': product_company_id,
                                                                                                  'paydas_diger': product_company_other,
                                                                                                  'urun_id': product_id,
                                                                                                  'urun_diger': product_other,
                                                                                                  'marka': brand,
                                                                                                  'model': model,
                                                                                                  'talep_miktari': product_amount,
                                                                                                  'talep_miktari_birim': amount_unit_id,
                                                                                                  'talep_miktari_birim_diger': amount_unit_other,
                                                                                                  'alternatif_uretici_var_mi': alternative_manufacturer,
                                                                                                  'olusturan': user,
                                                                                                  'olusturan_ip': user_ip,
                                                                                                  'aktif_mi': True,
                                                                                                  'ata_urun_id': diffraction_product_id,
                                                                                                  'urun_tipi': product_type,
                                                                                                  'paydas_diger_vergi_no': product_company_tax_number

                                                                                              }
                                                                                          }).data()

        return add_domestic_supply_diffraction

    def get_diffraction_by_domestic(self, diffraction_product_id, product_company_name, product_name, product_brand,
                                    product_model):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim_yerli,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'ata_urun_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'paydas_diger',
                                                                              'operator': '=',
                                                                              'value': product_company_name,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          2: {'col': 'LOWER(urun_diger)',
                                                                              'operator': '=',
                                                                              'value': product_name.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          3: {'col': 'LOWER(marka)',
                                                                              'operator': '=',
                                                                              'value': product_brand.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          4: {'col': 'LOWER(model)',
                                                                              'operator': '=',
                                                                              'value': product_model.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          5: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'kirilim_yerli_id asc', None,
                                                                      False).data()

        return diffraction

    def insert_alternative_manufacturer_id(self, alternative_manufacturer_id, company_type, company_id, company_name,
                                           tax_number,
                                           company_country, user, user_ip):

        if company_id == '00000000-0000-0000-0000-000000000000':
            company_id = None

        if company_type == 1:
            paydas_id = company_id
            paydas_diger = company_name
            yabanci_id = None
            yabanci_diger = None
        else:
            paydas_id = None
            paydas_diger = None
            yabanci_id = company_id
            yabanci_diger = company_name

        add_alternative_manufacturer = Model(ReaPy.get_reactor_source('yeten')).insert(
            self.um_kirilim_alternatif_uretici,
            {
                0: {
                    'kirilim_alternatif_uretici_id': alternative_manufacturer_id,
                    'uretici_turu': company_type,
                    'paydas_id': paydas_id,
                    'paydas_diger': paydas_diger,
                    'yabanci_id': yabanci_id,
                    'yabanci_diger': yabanci_diger,
                    'paydas_vergi_no': tax_number,
                    'yabanci_ulke': company_country,
                    'olusturan': user,
                    'olusturan_ip': user_ip,
                    'aktif_mi': True,

                }
            }).data()

        return add_alternative_manufacturer

    def insert_alternative_domestic_manufacturer_id(self, alternative_manufacturer_id, diffraction_id,
                                                    domestic_diffraction_id, user, user_ip):

        add_alternative_manufacturer = Model(ReaPy.get_reactor_source('yeten')).insert(
            'public.um_kirilim_alternatif_uretici_yerli',
            {
                0: {
                    'kirilim_alternatif_uretici_id': alternative_manufacturer_id,
                    'kirilim_id': diffraction_id,
                    'kirilim_yerli_id': domestic_diffraction_id,
                    'olusturan': user,
                    'olusturan_ip': user_ip,
                    'aktif_mi': True,

                }
            }).data()

        return add_alternative_manufacturer

    def insert_alternative_foreign_manufacturer_id(self, alternative_manufacturer_id, diffraction_id,
                                                   foreign_diffraction_id, user, user_ip):

        add_alternative_manufacturer = Model(ReaPy.get_reactor_source('yeten')).insert(
            'public.um_kirilim_alternatif_uretici_yabanci',
            {
                0: {
                    'kirilim_alternatif_uretici_id': alternative_manufacturer_id,
                    'kirilim_id': diffraction_id,
                    'kirilim_yabanci_id': foreign_diffraction_id,
                    'olusturan': user,
                    'olusturan_ip': user_ip,
                    'aktif_mi': True,

                }
            }).data()

        return add_alternative_manufacturer

    def get_diffraction_by_foreign(self, diffraction_product_id, product_company_id, product_company_name, product_name,
                                   product_brand,
                                   product_model):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim_yabanci,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'ata_urun_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'yabanci_id',
                                                                              'operator': '=',
                                                                              'value': product_company_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          2: {'col': 'yabanci_diger',
                                                                              'operator': '=',
                                                                              'value': product_company_name,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          3: {'col': 'LOWER(urun_ismi)',
                                                                              'operator': '=',
                                                                              'value': product_name.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          4: {'col': 'LOWER(marka)',
                                                                              'operator': '=',
                                                                              'value': product_brand.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          5: {'col': 'LOWER(model)',
                                                                              'operator': '=',
                                                                              'value': product_model.lower(),
                                                                              'combiner': 'AND'
                                                                              },
                                                                          6: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'kirilim_yabanci_id asc', None,
                                                                      False).data()

        return diffraction

    def insert_foreign_supply_diffraction(self, foreign_diffraction_id, diffraction_id, product_company_id,
                                          product_company_name, product_name, product_brand, product_model,
                                          production_country, supplier_company_id, supplier_company_name,
                                          supplier_origin_country, supply_time, buying_unit_price,
                                          buying_unit_price_unit, cpc_id, supply_life, dual_use, end_user_document,
                                          taksonomi, product_amount, amount_unit_id, amount_unit_other,
                                          alternative_manufacturer, diffraction_product_id, product_type, user,
                                          user_ip, reason_for_supply, product_origin_country, product_country_origin):
        if end_user_document == 1:
            end_user_document = True
        else:
            end_user_document = False
        add_foreign_supply_diffraction = \
            Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim_yabanci,
                                                            {
                                                                0: {
                                                                    'kirilim_yabanci_id': foreign_diffraction_id,
                                                                    'kirilim_id': diffraction_id,
                                                                    'yabanci_id': product_company_id,
                                                                    'yabanci_diger': product_company_name,
                                                                    'urun_ismi': product_name,
                                                                    'marka': product_brand,
                                                                    'model': product_model,
                                                                    'uretim_yeri': production_country,
                                                                    'tedarikci_firma_id': supplier_company_id,
                                                                    'tedarikci_firma_diger': supplier_company_name,
                                                                    'tedarikci_mensei': supplier_origin_country,
                                                                    'tedarik_temin_suresi': supply_time,
                                                                    'alis_birim_fiyati': buying_unit_price,
                                                                    'alis_birim_fiyat_cinsi': buying_unit_price_unit,
                                                                    'urun_sinifi_cpc': cpc_id,
                                                                    'teknoloji_omur_devri_safhasi': supply_life,
                                                                    'askeri_alanda_kullanim_dual_use': dual_use,
                                                                    'son_kullanici_belgesi_var_mi': end_user_document,
                                                                    'mumkun_kilan_kilit_teknoloji_taksonomi': taksonomi,
                                                                    'talep_miktari': product_amount,
                                                                    'talep_miktari_birim': amount_unit_id,
                                                                    'talep_miktari_birim_diger': amount_unit_other,
                                                                    'alternatif_uretici_var_mi': alternative_manufacturer,
                                                                    'ata_urun_id': diffraction_product_id,
                                                                    'urun_tipi': product_type,
                                                                    'olusturan': user,
                                                                    'olusturan_ip': user_ip,
                                                                    'aktif_mi': True,
                                                                    'neden_yabancidan_tedarik_aciklamasi': reason_for_supply,
                                                                    'urun_mensei': product_origin_country,
                                                                    'yabanci_diger_ulke': product_country_origin

                                                                }
                                                            }).data()

        return add_foreign_supply_diffraction

    def insert_diffraction_foreign_infrastructure(self, foreign_diffraction_id, infrastructure, user,
                                                  user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_kirilim_alt_yapi_yabanci',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_yabanci_id':
                                                                                      foreign_diffraction_id,
                                                                                  'alt_yapi': infrastructure,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_foreign_keyword(self, foreign_diffraction_id, key, user,
                                           user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_anahtar_kelime_yabanci',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_yabanci_id':
                                                                                      foreign_diffraction_id,
                                                                                  'anahtar_kelime': key,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def get_diffraction_by_other_software(self, diffraction_product_id, product_name):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim_diger_yazilim,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'ata_urun_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'kirilim_adi',
                                                                              'operator': '=',
                                                                              'value': product_name,
                                                                              'combiner': 'AND'
                                                                              },

                                                                          2: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'kirilim_adi asc', None,
                                                                      False).data()

        return diffraction

    def insert_other_software_diffraction(self, other_software_diffraction_id, diffraction_id, diffraction_product_id,
                                          product_name, product_description,
                                          active_users,
                                          development_period,
                                          sold_under_license, used_under_license, license_price, license_type,
                                          license_period, license_country,
                                          is_it_exported, api_support, is_it_embedded, open_source, road_map,
                                          mobile_compatible, dual_use,
                                          use_ssb_products,
                                          physical_hardware, equivalent_domestic_foreign,
                                          reason_for_foreign_supply, cpc_id,
                                          prodtr, user,
                                          user_ip):

        add_other_software_diffraction = \
            Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim_diger_yazilim,
                                                            {
                                                                0: {
                                                                    'kirilim_diger_yazilim_id': other_software_diffraction_id,
                                                                    'kirilim_id': diffraction_id,
                                                                    'kirilim_adi': product_name,
                                                                    'aktif_kullanici_sayisi': active_users,
                                                                    'gomulu_mu': is_it_embedded,
                                                                    'acik_kaynakli_urun_kullaniliyor_mu': open_source,
                                                                    'gelistirme_suresi': development_period,
                                                                    'lisans_altinda_mi_satiliyor': sold_under_license,
                                                                    'lisans_altinda_mi_kullaniliyor': used_under_license,
                                                                    'lisans_ucreti': license_price,
                                                                    'lisans_turu': license_type,
                                                                    'lisans_suresi': license_period,
                                                                    'lisans_ulkesi': license_country,
                                                                    'ihracat_yapiliyor_mu': is_it_exported,
                                                                    'api_destegi_var_mi': api_support,
                                                                    'yol_haritasi': road_map,
                                                                    'mobil_uyumlu': mobile_compatible,
                                                                    'askeri_alanda_kullanim_dual_use': dual_use,
                                                                    'ssb_urunlerinde_kullaniliyor_mu': use_ssb_products,
                                                                    'fiziksel_bir_donanim_gerekiyor_mu': physical_hardware,
                                                                    'muadili_yerli_yabanci_urunler': equivalent_domestic_foreign,
                                                                    'yabancidan_tedarik_ediliyorsa_nedeni': reason_for_foreign_supply,
                                                                    'urun_sinifi_cpc': cpc_id,
                                                                    'prodtr': prodtr,
                                                                    'aciklamasi': product_description,
                                                                    'ata_urun_id': diffraction_product_id,
                                                                    'olusturan': user,
                                                                    'olusturan_ip': user_ip,
                                                                    'aktif_mi': True,

                                                                }
                                                            }).data()

        return add_other_software_diffraction

    def insert_diffraction_other_software_keyword(self, other_software_diffraction_id, key, lang, user,
                                                  user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_anahtar_kelime_diger_yazilim',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_yazilim_id':
                                                                                      other_software_diffraction_id,
                                                                                  'anahtar_kelime': key,
                                                                                  'dil': lang,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_software_lang(self, other_software_diffraction_id, key, user,
                                               user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_yazilim_dil_diger_yazilim',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_yazilim_id':
                                                                                      other_software_diffraction_id,
                                                                                  'dil_id': key,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_software_os(self, other_software_diffraction_id, key, user,
                                             user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert(
            'public.um_yazilim_isletim_sistemi_diger_yazilim',
            {
                0: {
                    'kirilim_diger_yazilim_id':
                        other_software_diffraction_id,
                    'isletim_sistemi_id': key,
                    'olusturan': user,
                    'olusturan_ip': user_ip,
                    'aktif_mi': True

                }
            }).error()

        return add_diffraction

    def insert_diffraction_other_software_standarts(self, other_software_diffraction_id, key, user,
                                                    user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_yazilim_standarti_diger_yazilim',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_yazilim_id':
                                                                                      other_software_diffraction_id,
                                                                                  'standart_id': key,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_software_export_country(self, other_software_diffraction_id, key, user,
                                                         user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_diger_yazilim_ihracat_ulke',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_yazilim_id':
                                                                                      other_software_diffraction_id,
                                                                                  'ulke_id': key,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def get_diffraction_by_other_product(self, diffraction_product_id, product_name):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim_diger_urun,
                                                                      None,
                                                                      {
                                                                          0: {'col': 'ata_urun_id',
                                                                              'operator': '=',
                                                                              'value': diffraction_product_id,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'kirilim_adi',
                                                                              'operator': '=',
                                                                              'value': product_name,
                                                                              'combiner': 'AND'
                                                                              },

                                                                          2: {'col': 'aktif_mi',
                                                                              'operator': '=',
                                                                              'value': True
                                                                              }
                                                                      },
                                                                      'kirilim_adi asc', None,
                                                                      False).data()

        return diffraction

    def insert_other_product_diffraction(self, other_product_diffraction_id, diffraction_id, diffraction_product_id,
                                         product_name, design,
                                         product_origin_country, supplier_company_id, supplier_company_name,
                                         supplier_origin_country,
                                         product_company_id, product_company_name, product_company_country,
                                         production_country,
                                         alternative_manufacturer, production_time, supply_time, product_capacity,
                                         product_capacity_unit_id,
                                         product_capacity_unit_other, buying_unit_price, buying_unit_price_unit,
                                         request_amount,
                                         request_amount_unit_id, request_amount_unit_other, cpc_id, ths, tlc, dual_use,
                                         end_user_document, taksonomi,
                                         reason_for_foreign_supply, user, user_ip, produce_at_license):

        if end_user_document == 1:
            end_user_document = True
        else:
            end_user_document = False

        add_other_product_diffraction = \
            Model(ReaPy.get_reactor_source('yeten')).insert(self.um_kirilim_diger_urun,
                                                            {
                                                                0: {
                                                                    'kirilim_diger_urun_id': other_product_diffraction_id,
                                                                    'kirilim_id': diffraction_id,
                                                                    'kirilim_adi': product_name,
                                                                    'tasarim': design,
                                                                    'uretim_yeri': product_origin_country,
                                                                    'lisans_altinda_uretim_mi': produce_at_license,
                                                                    'tedarikci_firma_id': supplier_company_id,
                                                                    'tedarikci_firma_diger': supplier_company_name,
                                                                    'tedarikci_mensei': supplier_origin_country,
                                                                    'uretici_firma_id': product_company_id,
                                                                    'uretici_firma_diger': product_company_name,
                                                                    'uretici_firma_mensei': product_company_country,
                                                                    'urun_mensei': production_country,
                                                                    'alternatif_uretici_var_mi': alternative_manufacturer,
                                                                    'uretim_suresi': production_time,
                                                                    'tedarik_temin_suresi': supply_time,
                                                                    'talep_miktari': request_amount,
                                                                    'talep_miktari_birim': request_amount_unit_id,
                                                                    'talep_miktari_birim_diger': request_amount_unit_other,
                                                                    'yillik_uretim_kapasitesi': product_capacity,
                                                                    'yillik_uretim_kapasitesi_birim': product_capacity_unit_id,
                                                                    'yillik_uretim_kapasitesi_birim_diger': product_capacity_unit_other,
                                                                    'alis_birim_fiyati': buying_unit_price,
                                                                    'alis_birim_fiyat_cinsi': buying_unit_price_unit,
                                                                    'mumkun_kilan_kilit_teknoloji_taksonomi': taksonomi,
                                                                    'teknoloji_hazirlik_seviyesi': ths,
                                                                    'teknoloji_omur_devri_safhasi': tlc,
                                                                    'askeri_alanda_kullanim_dual_use': dual_use,
                                                                    'son_kullanici_belgesi_var_mi': end_user_document,
                                                                    'neden_yabancidan_tedarik_aciklamasi': reason_for_foreign_supply,
                                                                    'urun_sinifi_cpc': cpc_id,
                                                                    'ata_urun_id': diffraction_product_id,
                                                                    'olusturan': user,
                                                                    'olusturan_ip': user_ip,
                                                                    'aktif_mi': True,
                                                                }
                                                            }).data()

        return add_other_product_diffraction

    def insert_diffraction_other_product_keyword(self, other_product_diffraction_id, key, lang, user,
                                                 user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_anahtar_kelime_diger_urun',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_urun_id':
                                                                                      other_product_diffraction_id,
                                                                                  'anahtar_kelime': key,
                                                                                  'dil': lang,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_infrastructure(self, other_diffraction_id, infrastructure, user,
                                                user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_kirilim_alt_yapi_diger_urun',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_alt_yapi_id':
                                                                                      other_diffraction_id,
                                                                                  'alt_yapi': infrastructure,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_test_country(self, other_diffraction_id, country, user,
                                              user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_kirilim_test_altyapisi_ulkeleri',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_urun_id':
                                                                                      other_diffraction_id,
                                                                                  'ulke_id': country,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_import_blocker(self, other_diffraction_id, blocker, user,
                                                user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_kirilim_ithalat_engel',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_urun_id':
                                                                                      other_diffraction_id,
                                                                                  'ithalat_engeli': blocker,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def insert_diffraction_other_export_blocker(self, other_diffraction_id, blocker, user,
                                                user_ip):
        add_diffraction = Model(ReaPy.get_reactor_source('yeten')).insert('public.um_kirilim_ihracat_engel',
                                                                          {
                                                                              0: {
                                                                                  'kirilim_diger_urun_id':
                                                                                      other_diffraction_id,
                                                                                  'ihracat_engeli': blocker,
                                                                                  'olusturan': user,
                                                                                  'olusturan_ip': user_ip,
                                                                                  'aktif_mi': True

                                                                              }
                                                                          }).data()

        return add_diffraction

    def get_product_sub_diffraction_by_id(self, iam_user_company_id, product_id, caller):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select('public.view_alt_kirilim_listesi',
                                                                      None,
                                                                      {
                                                                          0: {'col': 'ata',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              'combiner': 'AND'},
                                                                          1: {'col': 'paydas_id',
                                                                              'operator': '=',
                                                                              'value': iam_user_company_id,
                                                                              'combiner': 'AND'},
                                                                          2: {'col': 'torun',
                                                                              'operator': 'IS NOT',
                                                                              'value': None}
                                                                      },
                                                                      None, None,
                                                                      False).data()
        if diffraction is None:
            return 0
        else:

            for key in diffraction:
                if key['kirilim_tipi'] > 3:
                    key['urun_id'] = key['kirilim_id']
                    if key['urun_id'] not in caller.sub_diffraction:
                        self.get_product_sub_diffraction_by_id(iam_user_company_id, key['urun_id'], caller)
                        caller.sub_diffraction.append(key['urun_id'])
            caller.sub_dif_level += 1
            caller.sub_dif_level_diffraction += len(diffraction)
        return 1

    def get_product_sub_diffraction_level(self, iam_user_company_id, product_id):
        pass

    def get_product_total_diffraction_by_id(self, iam_user_company_id, product_id):
        diffraction = Model(ReaPy.get_reactor_source('yeten')).select(self.um_kirilim,
                                                                      'COUNT(*)',
                                                                      {
                                                                          # 0: {'col': 'paydas_id', 'operator': '=',
                                                                          #     'value': iam_user_company_id,
                                                                          #     'combiner': 'AND'
                                                                          #     },
                                                                          0: {'col': 'aktif_mi', 'operator': '=',
                                                                              'value': True,
                                                                              'combiner': 'AND'
                                                                              },
                                                                          1: {'col': 'torun',
                                                                              'operator': '=',
                                                                              'value': product_id,
                                                                              }
                                                                      },
                                                                      None, None,
                                                                      False).data()

        return diffraction

    def get_product_keywords_diffraction_by_id(self, product_id):
        keywords = Model(ReaPy.get_reactor_source('yeten')).select('public.view_anahtar_kelimeler',
                                                                   'anahtar_kelime',
                                                                   {
                                                                       0: {'col': 'urun_id',
                                                                           'operator': '=',
                                                                           'value': product_id,
                                                                           }
                                                                   },
                                                                   None, None,
                                                                   False).data()
        if keywords is not None:
            new_keywords = []
            for key in keywords:
                new_keywords.append(key['anahtar_kelime'])

            return new_keywords
        return []

    def get_product_cpc_diffraction_by_id(self, cpc_id):
        cpc = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_cpc',
                                                              'aciklama,sembol',
                                                              {
                                                                  0: {'col': 'cpc_id',
                                                                      'operator': '=',
                                                                      'value': cpc_id,
                                                                      }
                                                              },
                                                              None, None,
                                                              False, True).data()

        if cpc is not None:
            cpc_data = {
                0: cpc[0]['aciklama'],
                1: cpc[0]['sembol']
            }
            return cpc_data
        return {
            0: '',
            1: ''
        }
