# -*- coding: utf-8 -*-
import datetime

from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Tesis(object):

    def get_check_for_ana_tesis(self, basvuru_id):
        check = 0
        try:
            data = Model(ReaPy.get_reactor_source('yeten')).select(table='public.paydas_tesis',
                                                                        condition=
                                                                        {
                                                                            0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': basvuru_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'ana_tesis',
                                                                                'operator': '=',
                                                                                'value': 0,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true'},
                                                                        }).data()
            if data is not None:
                check += 1
        except Exception as exc:
            print('Tesis get_check_for_ana_tesis : ', exc)

        return check

    def get_check_for_ana_tesis_update(self, basvuru_id, tesis_id):
        check = 0
        try:
            data = Model(ReaPy.get_reactor_source('yeten')).select(table='public.paydas_tesis',
                                                                        condition=
                                                                        {
                                                                            0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': basvuru_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'ana_tesis',
                                                                                'operator': '=',
                                                                                'value': 0,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true',
                                                                                'combiner': 'AND'},
                                                                            3: {'col': 'tesis_id',
                                                                                'operator': '=',
                                                                                'value': tesis_id}
                                                                        }).data()
            if data is None:
                check += 1
        except Exception as exc:
            print('Tesis get_check_for_ana_tesis : ', exc)

        return check

    # noinspection PyMethodMayBeStatic
    def get_check_tesis_for_delete(self, basvuru_id, tesis_id):

        check = 0
        try:
            data = Model(ReaPy.get_reactor_source('yeten')).select(table='public.ua_uretim_altyapi',
                                                                        condition=
                                                                        {
                                                                            0: {'col': 'paydas_id',
                                                                                'operator': '=',
                                                                                'value': basvuru_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'tesis_id',
                                                                                'operator': '=',
                                                                                'value': tesis_id,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi',
                                                                                'operator': '=',
                                                                                'value': 'true'},
                                                                        }).data()
            if data is not None:
                check += 1
        except Exception as exc:
            print('Tesis get_check_tesis_for_delete ua_uretim_altyapi : ', exc)

        try:
            data = Model(ReaPy.get_reactor_source('yeten')).select(table='public.ta_paydas_test_altyapi',
                                                                        condition=
                                                                        {
                                                                            0: {'col': 'paydas_id', 'operator': '=',
                                                                                'value': basvuru_id,
                                                                                'combiner': 'AND'},
                                                                            1: {'col': 'paydas_tesis_id',
                                                                                'operator': '=',
                                                                                'value': tesis_id,
                                                                                'combiner': 'AND'},
                                                                            2: {'col': 'aktif_mi', 'operator': '=',
                                                                                'value': 'true'},
                                                                        }).data()
            if data is not None:
                check += 1
        except Exception as exc:
            print('Tesis get_check_tesis_for_delete ta_paydas_test_altyapi : ', exc)

        return check

    # noinspection PyMethodMayBeStatic
    def tesis_update(self, data, paydas_id, tesis_id, tckn, ip):

        data_update = {
            'guncelleyen': tckn,
            'guncelleyen_ip': ip,
            'guncelleme_tarihi': str(datetime.datetime.now()),
            # 'aktif_mi': True
        }

        for key, value in data.items():
            if Helpers().set_val_safe(data, key) != '':
                data_update[key] = data[key]

        set_durum = None
        try:
            set_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_tesis',
                                                                             {'SET':
                                                                                  data_update,
                                                                              'CONDITION': {
                                                                                  0: {
                                                                                      'col': 'tesis_id',
                                                                                      'operator': '=',
                                                                                      'value': tesis_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': paydas_id}
                                                                              }}
                                                                             ).data()
        except Exception as exc:
            print(__file__)
            print(exc)
        return set_durum

    # noinspection PyMethodMayBeStatic
    def ekle_tesis_bilgileri(self, tesis_bilgileri, basvuru_id, tckn, olusturan_ip):

        status = None

        if tesis_bilgileri == []:
            return status

        try:
            insert_data = {}
            for tesis in tesis_bilgileri:
                insert_data[len(insert_data)] = {
                    'paydas_id': basvuru_id,
                    'olusturan_id': tckn,
                    'olusturan_ip': olusturan_ip,
                    'aktif_mi': 'true',
                    'ad': tesis['ad'],
                    'adres': tesis['adres'],
                    'ulke_id': tesis['ulke_id'],
                    'il_id': Helpers().set_val_safe(tesis, 'il_id', True),
                    'ilce_id': Helpers().set_val_safe(tesis, 'ilce_id', True),
                    'posta_kodu': tesis['posta_kodu'],
                    'ana_tesis': tesis['ana_tesis'],
                    'sahiplik_durumu': tesis['sahiplik_durumu']
                }

            print(insert_data)
            islem = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_tesis', insert_data)
            print(islem.data())
            print(islem.error())
            return islem

        except Exception as exc:
            print(' ekle_tesis_bilgileri exc')
            print(exc)

        return status

    # noinspection PyMethodMayBeStatic
    def get_tesis(self, request_basvuru_id, user_identity_no):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='view_paydas_tesis',
                                                                    condition=
                                                                    {0: {'col': 'paydas_id', 'operator': '=',
                                                                         'value': request_basvuru_id,
                                                                         'combiner': 'AND'},
                                                                     1: {'col': 'yetkili_kisi_tckn', 'operator': '=',
                                                                         'value': user_identity_no}
                                                                     }).data()

    # noinspection PyMethodMayBeStatic
    def delete_tesis(self, basvuru_id, tesis_id):
        try:
            set_basvuru_durum = Model(ReaPy.get_reactor_source('yeten')).update('paydas_tesis',
                                                                                     {'SET':
                                                                                          {'aktif_mi': 'false',
                                                                                           # 'guncelleme_tarihi': str(datetime.now())
                                                                                           },
                                                                                      'CONDITION': {
                                                                                          0: {'col': 'tesis_id',
                                                                                              'operator': '=',
                                                                                              'value': tesis_id,
                                                                                              'combiner': 'AND'},
                                                                                          1: {'col': 'paydas_id',
                                                                                              'operator': '=',
                                                                                              'value': basvuru_id}
                                                                                      }}
                                                                                     ).data()
        except Exception as exc:
            print(exc)
            set_basvuru_durum = False

        return set_basvuru_durum
