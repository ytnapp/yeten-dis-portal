# -*- coding: utf-8 -*-

# from core.model import Model
from core.reapy import ReaPy
from apps.rest_server.models.helpers import Helpers

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Frontend(object):

    # noinspection PyMethodMayBeStatic
    def get_tanim_nace(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_nace').data()

    def get_file_from_id(self, dosya_numara):
        query = Model(ReaPy.get_reactor_source('yeten')).select(table='dosya_yukleme',
                                                                     field='dosya_tam_yolu, dosya_adi',
                                                                     condition={
                                                                         0: {'col': 'dosya_yukleme_id', 'operator': '=',
                                                                             'value': dosya_numara}
                                                                     })

        print(query.error())

        return query.data()

    # noinspection PyMethodMayBeStatic
    def get_like_nace(self, uc_karakter):
        query = Model(ReaPy.get_reactor_source('yeten')).select(table='sys_nace',
                                                                     condition={
                                                                         0: {'col': 'nace_kodu', 'operator': 'LIKE',
                                                                             'value': '%' + uc_karakter + '%',
                                                                             'combiner': 'OR'},
                                                                         1: {'col': 'nace_adi', 'operator': 'LIKE',
                                                                             'value': '%' + uc_karakter + '%',
                                                                             'combiner': 'OR'},
                                                                         2: {'col': 'nace_adi', 'operator': '=',
                                                                             'value': 'true'}
                                                                     },top="50").data()

        if query is not None:
            #print(query.error())
            return query
        return []

    # noinspection PyMethodMayBeStatic
    def get_nace_uuid(self, uuid_array):
        condition_nace = {}

        print(uuid_array)
        for iduu in uuid_array:

            nace_bi = ''
            if type(iduu) is dict:
                nace_bi = iduu['nace_id']
            else:
                nace_bi = iduu

            condition_nace[len(condition_nace)] = {
                'col': 'nace_id',
                'operator': '=',
                'value': nace_bi,
                'combiner': 'OR'
            }

        condition_nace[len(condition_nace)] = {
            'col': 'aktif_mi',
            'operator': '=',
            'value': 'true'
        }

        query = Model(ReaPy.get_reactor_source('yeten')).select(table='sys_nace',
                                                                     field='nace_id, nace_kodu,nace_adi',
                                                                     condition=condition_nace,
                                                                     sort='nace_adi asc')

        print(query.error())

        return query.data()

    # noinspection PyMethodMayBeStatic
    def get_tanim_meslek_odasi(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_meslek_odasi').data()

    # noinspection PyMethodMayBeStatic
    def get_vergi_daire(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_vergi_dairesi').data()

    # noinspection PyMethodMayBeStatic
    def get_ilce(self, il_id):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_ilce',
                                                                    field='ilce_id as deger, ilce_ad as aciklama',
                                                                    condition={
                                                                        0: {'col': 'il_id', 'operator': '=',
                                                                            'value': il_id}
                                                                    },
                                                                    sort='ilce_ad asc').data()

    # noinspection PyMethodMayBeStatic
    def get_tanim_sehir(self):
        return Model(ReaPy.get_reactor_source('yeten')).select(table='sys_il', sort='il_adi').data()

    def static_update(self, kolon):
        try:
            set_basvuru_durum = Model(ReaPy.get_reactor_source('yeten')).select(table='public.sys_tanim',
                                                                                     field='aciklama',
                                                                                     condition={
                                                                                         0: {'col': 'kolon',
                                                                                             'operator': '=',
                                                                                             'value': kolon}
                                                                                     }
                                                                                     ).data()
            return set_basvuru_durum

        except Exception as exc:
            print(exc)
            return False

    def static_get(self, kolon):
        try:
            get_static = Model(ReaPy.get_reactor_source('yeten')).select('public.sys_tanim',
                                                                              field='aciklama',
                                                                                     condition= {
                                                                                          0: {'col': 'kolon',
                                                                                              'operator': '=',
                                                                                              'value': kolon
                                                                                              }
                                                                                     }).data()

            if get_static is not None:
                return get_static[0]['aciklama']

        except Exception as exc:
            print(exc)

        return None


    def get_kalite_yonetim_sistemi(self):
        return  Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='sys_tanim',
                condition={
                    0: {'col': 'tablo', 'operator': '=', 'value': 'paydas_kalite_yonetim_sistemleri'}   
                    }).data())