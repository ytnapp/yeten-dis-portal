# -*- coding: utf-8 -*-
from apps.rest_server.models.helpers import Helpers
from core.reapy import ReaPy

try:
    if ReaPy.configuration().get_configuration()['system']['use_mediation'] is True:
        from core.mediation_model import MediationModel as Model
    else:
        from core.model import Model
except ImportError:
    from core.model import Model


class Paydas(object):

    def __init__(self):
        view_path = str(ReaPy.presenter().get_project_root()) + '/apps/rest_server/views/api/eng.json'
        self.messages = ReaPy.presenter().get_message(view_path, 'messages')['paydas']

    # noinspection PyMethodMayBeStatic
    def update_sozlesme(self, data, paydas_id):

        kolon = 'paydas_sozlesme_okudu_mu'
        try:
            kolon = data['paydas_urun_sozlesme_okudu_mu']
            kolon = 'paydas_urun_sozlesme_okudu_mu'
        except Exception as exc:
            print(exc)

        try:
            set_basvuru_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas',
                                                                                {'SET':
                                                                                     {kolon: 'true'},
                                                                                 'CONDITION': {
                                                                                     0: {'col': 'paydas_id',
                                                                                         'operator': '=',
                                                                                         'value': paydas_id}
                                                                                    }
                                                                                 }
                                                                                ).data()
        except Exception as exc:
            print(exc)
            set_basvuru_durum = None

        return set_basvuru_durum

    # noinspection PyMethodMayBeStatic
    def get_detay(self, user_identity_no):

        detay = {}
        condition_val = {
            0: {'col': 'paydas_id', 'operator': '=', 'value': user_identity_no}
        }

        try:
            list = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')) \
                                                        .select(table='view_paydas',
                                                                condition=condition_val,
                                                                is_first=True).data())

            detay = list[0]
            detay['dis_paydas_adi'] = detay['paydas_adi']
            detay['dis_paydas_tipi'] = detay['paydas_tipi']
            request_basvuru_id = detay['paydas_id']
            detay['tesisler'] = Helpers().payload_parameters_control(Model(ReaPy.get_reactor_source('yeten')).select(
                table='paydas_tesis',
                condition=
                {
                    0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                    1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
                }).data())

            detay['yetenek_bilgisi'] = self.get_detay_iliski(request_basvuru_id,
                                                             'view_paydas_yetenek_bilgisi',
                                                             ['yetenek_bilgisi_id'])

            detay['faaliyet_alanlari'] = self.get_detay_iliski(request_basvuru_id,
                                                               'view_paydas_faaliyet_alani',
                                                               ['faaliyet_id'])

            detay['nace_kodlari'] = self.get_detay_iliski(request_basvuru_id, 'view_paydas_nace',
                                                          ['nace_id'])

            detay['sahiplik_yapisi'] = self.get_detay_iliski(request_basvuru_id,
                                                             'view_paydas_sahiplik_yapisi',
                                                             ['sahip_adi', 'ortaklik_yuzdesi'])

            detay['istirak_orani'] = self.get_detay_iliski(request_basvuru_id,
                                                           'view_paydas_firma_istirakler',
                                                           ['istirak_adi', 'orani'])

            detay['savunma_faaliyet_alanlari'] = self.get_detay_iliski(
                request_basvuru_id, 'view_paydas_savunma_faaliyet_alani',
                ['savunma_faaliyet_id'])

            detay['dosyalar'] = Model(ReaPy.get_reactor_source('yeten')).select(
                table='view_paydas_basvuru_dosya',
                field=' dosya_yukleme_id, dosya_adi, dosya_turu, basvuru_id as paydas_id, basvuru_id ',
                condition={
                    0: {'col': 'paydas_id', 'operator': '=', 'value': request_basvuru_id, 'combiner': 'AND'},
                    1: {'col': 'dosya_turu', 'operator': '!=', 'value': 'null'}
                }) \
                .data()

            return detay

        except Exception as exc:
            print(__file__ + ' get_detay_basvuru')
            print(exc)

        return None

    def get_detay_iliski(self, basvuru_id, tablo, field):

        list = Model(ReaPy.get_reactor_source('yeten')).select(
            table=tablo,
            field=','.join(field),
            condition=
            {
                0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}
            }).data()

        if list is None:
            return None

        if tablo == 'view_paydas_sahiplik_yapisi' or tablo == 'view_paydas_firma_istirakler':
            return list

        return_list = []
        for key in list:
            return_list.append(key[field[0]])
            # return_list.append({field[0]: key[field[0]], field[1]: key[field[1]]})

        return return_list

    # noinspection PyMethodMayBeStatic
    def get_tesis(self, paydas_id):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='public.paydas_tesis',
                                                               condition=
                                                               {0: {'col': 'paydas_id', 'operator': '=',
                                                                    'value': paydas_id}})

    # noinspection PyMethodMayBeStatic
    def delete_tesis(self, basvuru_id, tesis_id):
        try:
            set_basvuru_durum = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_tesis',
                                                                                {'SET':
                                                                                     {'aktif_mi': 'false'
                                                                                      # 'guncelleme_tarihi': datetime.now()
                                                                                      },
                                                                                 'CONDITION': {
                                                                                     0: {'col': 'tesis_id',
                                                                                         'operator': '=',
                                                                                         'value': tesis_id,
                                                                                         'combiner': 'AND'},
                                                                                     1: {'col': 'paydas_id',
                                                                                         'operator': '=',
                                                                                         'value': basvuru_id}
                                                                                 }
                                                                                 }
                                                                                ).data()
        except Exception as exc:
            print(exc)
            set_basvuru_durum = False

        return set_basvuru_durum

    # noinspection PyMethodMayBeStatic
    def get_all_record(self):

        get_list = None

        try:
            get_list = Model(ReaPy.get_reactor_source('yeten')).select(
                table='public.view_paydas',
                field=' tesis_adedi, paydas_adi,web_adres,paydas_id,olusturma_tarihi,adres ',
                sort='olusturma_tarihi desc')

            return get_list.data()
        except Exception as exc:
            print(__file__ + ' get_all_record')
            print(exc)

        if get_list is None:
            return self.messages['errors']['display_err_204']

        return get_list

    # noinspection PyMethodMayBeStatic
    def basvuru_id_drum_kontrol(self, basvuru_id):

        return Model(ReaPy.get_reactor_source('yeten')).select(table='public.paydas', condition={
            0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id,
                'combiner': 'AND'},
            1: {'col': 'aktif_mi', 'operator': '=', 'value': 'true'}
        }).data()

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_faaliyet_alani(self, basvuru_id, data, is_before_delete=False):
        if data is None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='public.paydas_faaliyet_alani',
                    condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for faaliyet_id in data:
            insert_data[len(insert_data)] = {
                'paydas_id': basvuru_id,
                'faaliyet_id': faaliyet_id,
                'aktif_mi': 'true',
            }
        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_faaliyet_alani', insert_data)
        except Exception as exc:
            print('faaliyet_alani ', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_yetenek_bilgisi(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='public.paydas_yetenek_bilgisi',
                    condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
                )
        insert_data = {}
        for yetenek_id in data:
            insert_data[len(insert_data)] = {
                'paydas_id': basvuru_id,
                'yetenek_bilgisi_id': yetenek_id,
                'aktif_mi': 'true',
            }
        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_yetenek_bilgisi', insert_data)
        except Exception as exc:
            print('yetenek_bilgisi ', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_savunma_alanlari(self, basvuru_id, data, is_before_delete=False):
        Model(ReaPy.get_reactor_source('yeten')).delete(
            table='public.paydas_savunma_faaliyet_alani',
            condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
        )
        insert_data = {}
        if data is not None:
            for savunma_faaliyet_id in data:
                insert_data[len(insert_data)] = {
                    'paydas_id': basvuru_id,
                    'savunma_faaliyet_id': savunma_faaliyet_id,
                    'aktif_mi': 'true',
                }

        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_savunma_faaliyet_alani', insert_data)
        except Exception as exc:
            print('savunma_alanlari ', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_nace_kodlari(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True

        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='public.paydas_nace',
                    condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
                )

        insert_data = {}
        for nace_id in data:
            insert_data[len(insert_data)] = {
                'paydas_id': basvuru_id,
                'nace_id': nace_id,
                'aktif_mi': 'true',  # olusturan_id ve ip gelecek
            }
        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_nace', insert_data)
        except Exception as exc:
            print('nace ', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_sahiplik_yapisi(self, basvuru_id, data, is_before_delete=False):
        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='public.paydas_sahiplik_yapisi',
                    condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
                )

        insert_data = {}
        for sahip in data:
            insert_data[len(insert_data)] = {
                'paydas_id': basvuru_id,
                'sahip_adi': sahip['sahip_adi'],
                'ortaklik_yuzdesi': sahip['ortaklik_yuzdesi'],
                'aktif_mi': 'true',
            }
        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_sahiplik_yapisi', insert_data)
        except Exception as exc:
            print('sahiplik_yapisi ', exc)

    # noinspection PyMethodMayBeStatic
    def __set_basvuru_iliski_istirakleri(self, basvuru_id, data, is_before_delete=False):

        if data == None:
            return True
        if len(data) < 1:
            return True
        else:
            if is_before_delete is True:
                Model(ReaPy.get_reactor_source('yeten')).delete(
                    table='public.paydas_firma_istirakleri',
                    condition={0: {'col': 'paydas_id', 'operator': '=', 'value': basvuru_id}}
                )

        insert_data = {}
        try:
            for sahip in data:
                insert_data[len(insert_data)] = {
                    'paydas_id': basvuru_id,
                    'istirak_adi': sahip['istirak_adi'],
                    'orani': sahip['orani'],
                    'aktif_mi': 'true'
                }
        except Exception as exc:
            print('istirakleri ', exc)
        try:
            nq = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_firma_istirakleri', insert_data)
        except Exception as exc:
            print('istirakleri ', exc)

    # noinspection PyMethodMayBeStatic
    def ekle_tesis_bilgileri(self, tesis_bilgileri, basvuru_id):

        status = None

        if tesis_bilgileri == []:
            return status

        try:
            insert_data = {}
            for tesis in tesis_bilgileri:
                insert_data[len(insert_data)] = {
                    'paydas_id': basvuru_id,
                    'aktif_mi': True,
                    'ad': tesis['ad'],
                    'adres': tesis['adres'],
                    'ulke_id': tesis['ulke_id'],
                    'il_id': tesis['il_id'],
                    'ilce_id': tesis['ilce_id'],
                    'posta_kodu': tesis['posta_kodu'],
                    'ana_tesis': tesis['ana_tesis'],
                    'sahiplik_durumu': tesis['sahiplik_durumu']
                }

            return Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_tesis', insert_data)

        except Exception as exc:
            print(' ekle_tesis_bilgileri exc')
            print(exc)

        return status

    # noinspection PyMethodMayBeStatic
    def update_paydas(self, data, basvuru_id, user_identity_no):

        update_data = {}

        # kontroller sonrası açılanlar

        if Helpers().set_val_safe(data, 'dis_paydas_tipi') != '':
            update_data['paydas_tipi'] = data['dis_paydas_tipi']

        if Helpers().set_val_safe(data, 'kurulus_yili') != '':
            update_data['kurulus_yili'] = data['kurulus_yili']

        # kontroller sonrası açılanlar

        if Helpers().set_val_safe(data, 'yetkili_kisi_ad_soyad') != '':
            update_data['yetkili_kisi_ad_soyad'] = data['yetkili_kisi_ad_soyad']

        if Helpers().set_val_safe(data, 'paydas_turu') != '':
            update_data['paydas_turu'] = data['paydas_turu']

        if Helpers().set_val_safe(data, 'vergi_dairesi') != '':
            update_data['vergi_dairesi'] = data['vergi_dairesi']

        if Helpers().set_val_safe(data, 'ticari_sicil_no') != '':
            update_data['ticari_sicil_no'] = data['ticari_sicil_no']

        if Helpers().set_val_safe(data, 'kep_adres') != '':
            update_data['kep_adres'] = data['kep_adres']

        if Helpers().set_val_safe(data, 'telefon') != '':
            update_data['telefon'] = data['telefon']

        if Helpers().set_val_safe(data, 'telefon2') != '':
            update_data['telefon2'] = data['telefon2']

        if Helpers().set_val_safe(data, 'eposta_adresi') != '':
            update_data['eposta_adresi'] = data['eposta_adresi']

        if Helpers().set_val_safe(data, 'sahiplik_durum') != '':
            update_data['sahiplik_durum'] = data['sahiplik_durum']

        if Helpers().set_val_safe(data, 'ulke_id') != '':
            update_data['ulke_id'] = data['ulke_id']

        if Helpers().set_val_safe(data, 'sehir_id') != '':
            update_data['sehir_id'] = data['sehir_id']

        if Helpers().set_val_safe(data, 'tesis_toplam_alan') != '':
            update_data['tesis_toplam_alan'] = data['tesis_toplam_alan']

        if Helpers().set_val_safe(data, 'kapali_alan') != '':
            update_data['kapali_alan'] = data['kapali_alan']

        if Helpers().set_val_safe(data, 'diger_tesis_toplam_alan') != '':
            update_data['diger_tesis_toplam_alan'] = data['diger_tesis_toplam_alan']

        if Helpers().set_val_safe(data, 'diger_kapali_alan') != '':
            update_data['diger_kapali_alan'] = data['diger_kapali_alan']

        if Helpers().set_val_safe(data, 'ilce_id') != '':
            update_data['ilce_id'] = data['ilce_id']

        if Helpers().set_val_safe(data, 'faaliyet_alani') != '':
            update_data['faaliyet_alani'] = data['faaliyet_alani']

        if Helpers().set_val_safe(data, 'faaliyet_suresi') != '':
            update_data['faaliyet_suresi'] = data['faaliyet_suresi']

        if Helpers().set_val_safe(data, 'savunma_faaliyet_suresi') != '':
            update_data['savunma_faaliyet_suresi'] = data['savunma_faaliyet_suresi']

        if Helpers().set_val_safe(data, 'web_adres') != '':
            update_data['web_adres'] = data['web_adres']

        if Helpers().set_val_safe(data, 'yetkili_kisi') != '':
            update_data['yetkili_kisi'] = data['yetkili_kisi']

        if Helpers().set_val_safe(data, 'ise_baslama_tarihi') != '':
            update_data['ise_baslama_tarihi'] = data['ise_baslama_tarihi']

        if Helpers().set_val_safe(data, 'posta_kod') != '':
            update_data['posta_kod'] = data['posta_kod']

        if Helpers().set_val_safe(data, 'adres') != '':
            update_data['adres'] = data['adres']

        if Helpers().set_val_safe(data, 'meslek_odasi_id') != '':
            update_data['meslek_odasi_id'] = data['meslek_odasi_id']

        if Helpers().set_val_safe(data, 'twitter_adres') != '':
            update_data['twitter_adres'] = data['twitter_adres']

        if Helpers().set_val_safe(data, 'arge_merkezi_mi') != '':
            update_data['arge_merkezi_mi'] = data['arge_merkezi_mi']

        if Helpers().set_val_safe(data, 'sanayi_yukumluluk_var_mi') != '':
            update_data['sanayi_yukumluluk_var_mi'] = data['sanayi_yukumluluk_var_mi']

        if Helpers().set_val_safe(data, 'savnet') != '':
            update_data['savnet'] = data['savnet']

        if Helpers().set_val_safe(data, 'kaliteyonetimsistemi') != '':
            update_data['kalite_yonetim_sistemi_mevcut_mu'] = data['kaliteyonetimsistemi']




        query = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas', {'SET': update_data,
                                                                                  'CONDITION': {
                                                                                      0: {
                                                                                          'col': 'paydas_id',
                                                                                          'operator': '=',
                                                                                          'value': basvuru_id,
                                                                                          'combiner': 'AND'},
                                                                                      1: {
                                                                                          'col': 'yetkili_kisi_tckn',
                                                                                          'operator': '=',
                                                                                          'value': user_identity_no}
                                                                                  }})


        insert_data = {}
        data_insert = {}
        if Helpers().set_val_safe(data, 'yerli_tedarikci') != '':
            data_insert['yerli_tedarikci'] = data['yerli_tedarikci']
        print(data_insert['yerli_tedarikci'])

        print("/****")
  
        query_insert = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_onayli_tedarikcileri',
                                                                            {'SET': {'aktif_mi':False},
                                                                                'CONDITION': 
                                                                                {
                                                                                  0: {
                                                                                      'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': basvuru_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {
                                                                                      'col': 'aktif_mi',
                                                                                      'operator': '=',
                                                                                      'value': True}
                                                                                }
                                                                            })
        
        
        #yabancidigertedarikci ekle

    
        try:
            if data['yerlidigertedarikci']!='':
                insert_data['paydas_id'] = basvuru_id
                insert_data['onayli_tedarikci_diger'] = data['yerlidigertedarikci']
                insert_data['aktif_mi'] = True
                print("/****4")
                print(insert_data)
                query_insert = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_onayli_tedarikcileri',{0:insert_data})
                print(query_insert.error())
                insert_data = {}
                print("yerli ekledi")
                print(data['yabancidigertedarikci'])
            if data['yabancidigertedarikci']!='':
                insert_data['paydas_id'] = basvuru_id
                insert_data['yabanci_firma_diger'] = data['yabancidigertedarikci']
                insert_data['aktif_mi'] = True
                print(insert_data)
                query_insert = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_onayli_tedarikcileri',{0:insert_data})
                print(query_insert.error())
                insert_data = {}
                print("yabanci ekledi")
        except:
            print("yerliyabancitedarikci")

        
        
        insert_data['paydas_id'] = basvuru_id
        # #tedarikci ekleme
        for item in data_insert['yerli_tedarikci']:
            #insert_data['onayli_tedarikci_turu'] = data['yerli_yabanci']
           
            #selectbox da Diğer seçmiş ise textinputundaki değeri kaydet
            if item=='':
                insert_data['onayli_tedarikci_diger'] = data['yerlidigertedarikci']
               
            else:
                insert_data['onayli_tedarikcisi_id'] = item
            diffraction_id = str(ReaPy.hash().get_uuid())
            insert_data['onayli_tedarikci_id'] = diffraction_id #uuid olustur
            insert_data['aktif_mi'] = True
            
            query_insert = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_onayli_tedarikcileri',{0:insert_data})

        insert_data = {}
        data_insert = {}
        if Helpers().set_val_safe(data, 'yabanci_tedarikci') != '':
            data_insert['yabanci_tedarikci'] = data['yabanci_tedarikci']
        print(data_insert['yabanci_tedarikci'])
        # #tedarikci ekleme
        for item in data_insert['yabanci_tedarikci']:
            insert_data['paydas_id'] = basvuru_id
            #insert_data['onayli_tedarikci_turu'] = data['yerli_yabanci']
           
            #selectbox da Diğer seçmiş ise textinputundaki değeri kaydet
            if item=='':
                    insert_data['yabanci_firma_diger'] = data['yabancidigertedarikci']
            else:
                  insert_data['yabanci_firma_id'] = item
            diffraction_id = str(ReaPy.hash().get_uuid())
            insert_data['onayli_tedarikci_id'] = diffraction_id #uuid olustur
            insert_data['aktif_mi'] = True
            
            query_insert = Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_onayli_tedarikcileri',{0:insert_data})



        #aktif siparis kurumlarını kaydet
        if data['savunmahavacilikaktifsiparis']==True:
            query_insert = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_aktif_siparis_kurumlari',
                                                                            {'SET': {'aktif_mi':False},
                                                                                'CONDITION': 
                                                                                {
                                                                                  0: {
                                                                                      'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': basvuru_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {
                                                                                      'col': 'aktif_mi',
                                                                                      'operator': '=',
                                                                                      'value': True}
                                                                                }
                                                                            })
            for item in data['aktifsipariskurumlar']:
                insert_data = {}
                diffraction_id = str(ReaPy.hash().get_uuid())
                insert_data['aktif_siparis_kurumlari_id'] = diffraction_id
                insert_data['paydas_id'] = basvuru_id
                insert_data['siparisi_veren'] = item
                insert_data['aktif_mi'] = True
                Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_aktif_siparis_kurumlari',{0:insert_data})
        else:
            query_insert = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_aktif_siparis_kurumlari',
                                                                            {'SET': {'aktif_mi':False},
                                                                                'CONDITION': 
                                                                                {
                                                                                  0: {
                                                                                      'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': basvuru_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {
                                                                                      'col': 'aktif_mi',
                                                                                      'operator': '=',
                                                                                      'value': True}
                                                                                }
                                                                            })

        #kalite yonetim sistemini kaydet
        if data['kaliteyonetimsistemi']==True:
            query_insert = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_kalite_yonetim_sistemleri',
                                                                            {'SET': {'aktif_mi':False},
                                                                                'CONDITION': 
                                                                                {
                                                                                  0: {
                                                                                      'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': basvuru_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {
                                                                                      'col': 'aktif_mi',
                                                                                      'operator': '=',
                                                                                      'value': True}
                                                                                }
                                                                            })
            for item in data['yonetimsistemleri']:
                
                insert_data = {}
                diffraction_id = str(ReaPy.hash().get_uuid())
                insert_data['paydas_kalite_yonetim_sistemi_id'] = diffraction_id
                insert_data['paydas_id'] = basvuru_id
                insert_data['kalite_yonetim_sistemi'] = item
                insert_data['aktif_mi'] = True
                Model(ReaPy.get_reactor_source('yeten')).insert('public.paydas_kalite_yonetim_sistemleri',{0:insert_data})
        else:
            query_insert = Model(ReaPy.get_reactor_source('yeten')).update('public.paydas_kalite_yonetim_sistemleri',
                                                                            {'SET': {'aktif_mi':False},
                                                                                'CONDITION': 
                                                                                {
                                                                                  0: {
                                                                                      'col': 'paydas_id',
                                                                                      'operator': '=',
                                                                                      'value': basvuru_id,
                                                                                      'combiner': 'AND'},
                                                                                  1: {
                                                                                      'col': 'aktif_mi',
                                                                                      'operator': '=',
                                                                                      'value': True}
                                                                                }
                                                                            })

        if query.error() is None:
            # query_geodi = Model(ReaPy.get_reactor_source('yeten')).update('geodi_feed', {'SET': {'durum': False},
            #                                                                              'CONDITION': {
            #                                                                                  0: {
            #                                                                                      'col': 'paydas_id',
            #                                                                                      'operator': '=',
            #                                                                                      'value': basvuru_id}
            #                                                                              }})
            self.__set_basvuru_iliski_nace_kodlari(basvuru_id, Helpers().set_val_safe(data, 'nace_kodlari', True), True)
            self.__set_basvuru_iliski_istirakleri(basvuru_id, Helpers().set_val_safe(data, 'firma_istirakleri', True),
                                                  True)
            self.__set_basvuru_iliski_yetenek_bilgisi(basvuru_id, Helpers().set_val_safe(data, 'yetenek_bilgisi', True),
                                                      True)
            self.__set_basvuru_iliski_sahiplik_yapisi(basvuru_id, Helpers().set_val_safe(data, 'sahiplik_yapisi', True),
                                                      True)
            self.__set_basvuru_iliski_faaliyet_alani(basvuru_id,
                                                     Helpers().set_val_safe(data, 'faaliyet_alanlari', True),
                                                     True)
            self.__set_basvuru_iliski_savunma_alanlari(basvuru_id,
                                                       Helpers().set_val_safe(data, 'savunma_faaliyet_alanlari', True),
                                                       True)

            self.__dosya_paydas_iliski(data['sahiplik_orani'], basvuru_id, 'sahiplik_orani')
            self.__dosya_paydas_iliski(data['imza_sirkuleri'], basvuru_id, 'imza_sirkuleri')
            self.__dosya_paydas_iliski(data['yetki_belgesi'], basvuru_id, 'yetki_belgesi')
            self.__dosya_paydas_iliski(data['ticaret_sicil_gazetesi'], basvuru_id, 'ticaret_sicil_gazetesi')

        return query.data()

        # noinspection PyMethodMayBeStatic

    def __dosya_paydas_iliski(self, dosyalar, dis_paydas_id, dosya_turu):

        if len(dosyalar) < 1:
            return True


        for dosya_ismi in dosyalar:
            query = Model(ReaPy.get_reactor_source('yeten')).update('public.dosya_yukleme', {
                'SET': {'paydas_id': dis_paydas_id, 'dosya_turu': dosya_turu},
                'CONDITION': {
                    0: {
                        'col': 'dosya_yukleme_id',
                        'operator': '=',
                        'value': dosya_ismi
                    }
                }})
