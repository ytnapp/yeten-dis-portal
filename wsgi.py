# -*- coding: utf-8 -*-
from apps.rest_server.controllers.api import app

if __name__ == "__main__":
    app.run()
